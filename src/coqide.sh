#~/.opam/system/bin/coqide SimplProp.v SimplBool.v BasicBool.v SimplSimpl.v SimplEq.v \
# SomeWhere :
#	- BasicBool.v

#coqide=~/.opam/4.08.1/bin/coqide
coqide=coqide
$coqide \
	FiniteCurryfy.v FiniteBooleanFunction.v LddO_Primitives.v \
	MaxLddGen.v \
	PrimOGen.v LddO_UCX_Extract.v \
	LddO_OPrim.v PrimUGen.v LddU_Primitives.v LddU_UCX_Extract.v \
	LddL_NatPrimitives.v LddL_VecPrimitives.v LddL_CountPrimitives.v LddL_Primitives.v LddL_UCX_Extract.v
