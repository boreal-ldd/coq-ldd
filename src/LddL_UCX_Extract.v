Require Import Arith .
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.

Require Import Ground.SimplProp.
Require Import Ground.SimplBool.
Require Import Ground.SimplOption.
Require Import Ground.SimplEq.
Require Import Ground.SimplFun.
Require Import Ground.SimplBoolFun.
Require Import Ground.SimplCmp.
Require Import Ground.SimplSimpl.

Require Import Ground.SimplNat.
Require Import Ground.SimplAB.

Require Import Ground.SimplList_Chap0_Standard.
Require Import Ground.SimplList_Chap1_Construct.
Require Import Ground.SimplList_Chap2_Predicate.
Require Import Ground.SimplList_Chap3_PredicateOnConstruct.
Require Import Ground.SimplList.
Require Import Ground.SimplABList.
Require Import Ground.SimplMore.
Require Import Ground.SimplDTT.
Require Import Ground.SimplLtN_Chap0_Construct.
Require Import Ground.SimplLtN_Chap1_Array_as_Function.
Require Import Ground.SimplLtN_Chap1_Sec1_Boolean_Array.
Require Import Ground.SimplLtN_Chap2_Cartesian_Operators.
Require Import Ground.SimplLtNIndexedArray.

Require Import Ground.SimplDTT.
Require Import Ground.SimplLtN.
Require Import Ground.SimplEnum.
Require Import Ground.SimplEnumCmp.
Require Import Ground.SimplEnumCount.
Require Import Ground.SimplEnumArray.

Require Import Ground.MiniBool_VAX.
Require Import Ground.MiniBool_Logic.

Require Import FiniteCurryfy.
Require Import FiniteBooleanFunction.

Require Import LddO_Primitives.
Require Import LddO_OPrim.
Require Import LddU_Primitives.

Require Import LddL_NatPrimitives.
Require Import LddL_VecPrimitives.
Require Import LddL_CountPrimitives.
Require Import LddL_Primitives.

Require Import Ground.SimplSizedList.

Require Import PrimUGen.
Require Import MaxLddGen.

(* Section 1. Definition if Terminal and Primitives *)
(* SubSection 1. Defining Terminals *)
Definition term (n:nat) : Type := bool * slist bool n.

Definition sem_term {n} (t:term n) : bf n := dot_bf (fst t) (array_of_slist (snd t)).

Definition extract_term_bool {n} (b:bool) (f:bf n) : option(array bool n) :=
  minsat_bf (fun a => beq_bf (dot_bf b a) f).

Definition extract_term {n} (f:bf n) : option(term n) :=
  let f0 := f(fun _ => false) in
  match extract_term_bool f0 f with
  | Some a => Some (f0, (slist_of_array a))
  | None   => None
  end.

Lemma extract_term_bool_eq_Some {n} (f:bf n) (a:array bool n) b :
  extract_term_bool b f = Some a <-> f = dot_bf b a.
Proof with curry1.
unfold extract_term_bool.
rewrite (minsat_alternative_minsat_bf _ _)...
unfold is_min_positive...
rewrite (beq_bf_iff_true _ _)...
rewrite (eq_sym_iff  _ f)...
Qed.
Hint Rewrite @extract_term_bool_eq_Some : curry1_rewrite.

Lemma extract_term_bool_dot_bf b1 b2 {n} (a2:array bool n) :
  extract_term_bool b1 (dot_bf b2 a2) = if eqb b1 b2 then Some a2 else None.
Proof with curry1.
dest_match_step...
unfold extract_term_bool.
rewrite (minsat_alternative_minsat_bf _ _)...
apply functional_extensionality...
Qed.
Hint Rewrite @extract_term_bool_dot_bf : curry1_rewrite.

Definition extract_term_smart {n} (f:bf n) : option(term n) :=
  let b := f(fun _ => false) in
  let s := slist_of_array (fun k => xorb b (f(array_cdirac true k))) in
  if beq_bf f (dot_bf b (array_of_slist s))
  then Some(b, s)
  else None.

Lemma extract_term_dot_bf {n} b a : extract_term (@dot_bf n b a) = Some(b, slist_of_array a).
Proof. unfold extract_term; curry1. Qed.
Hint Rewrite @extract_term_dot_bf : curry1_rewrite.

Lemma extract_term_eq_Some {n} (f:bf n) t : extract_term f = Some t <-> f = sem_term t.
Proof with curry1.
unfold extract_term, sem_term...
unfold opmap; dest_match_step...
rewrite D...
destruct t...
Qed.
Hint Rewrite @extract_term_eq_Some : curry1_rewrite.

Lemma extract_term_smart_spec {n} : @extract_term_smart n = extract_term.
Proof with curry1.
apply functional_extensionality...
unfold extract_term_smart...
dest_match_step...
- rewrite (beq_bf_iff_true _ _) in D...
  rewrite D...
  apply functional_extensionality...
- destruct(extract_term x)eqn:E0...
  assert(H:beq_bf (sem_term t)
      (dot_bf (sem_term t (fun _ : ltN n => false))
         (fun k : ltN n => xorb (sem_term t (fun _ : ltN n => false)) (sem_term t (array_cdirac true k)))) = true);
  [clear D | ]...
  rewrite (beq_bf_iff_true _ _)...
  unfold sem_term in *...
  destruct t...
  rewrite <- array_of_slist_shift.
  apply functional_extensionality...
Qed.

(* [NOTE] we enforce constantes to be part of terminals *)
Definition term_of_bool (f:bf 0) : term 0 := ((f(fun _ => false)), slist_of_array (fun _ => false)).

Lemma extract_term_cst_bf n b : extract_term (cst_bf n  b) = Some(b, slist_of_array (fun _ => false)).
Proof with curry1.
rewrite extract_term_eq_Some...
unfold sem_term...
Qed.
Hint Rewrite extract_term_cst_bf : curry1_rewrite.

Lemma term_of_bool_cst_bf b : term_of_bool (cst_bf 0 b) = (b, slist_of_array (fun _ => false)).
Proof. reflexivity. Qed.
Hint Rewrite term_of_bool_cst_bf : curry1_rewrite.

Lemma extract_term_0 (f:bf 0) : extract_term f = Some(term_of_bool f).
Proof. rewrite (bf0 f); curry1. Qed.

Definition is_term {n} (f:bf n) : bool := @PrimUGen.is_term term (@extract_term) n f.

(* SubSection 2. Defining Primitives *)

Definition svec (n:nat) : Type := { v : slist bool n | non_false_array(array_of_slist v) = true }.

Definition cmp_svec {n} (v1 v2:svec n) : ord := cmp_sig cmp_slist_bool _ v1 v2.

Lemma cmp_P_cmp_svec n : cmp_P (@cmp_svec n).
Proof. apply cmp_P_cmp_sig, cmp_P_cmp_slist_bool. Qed.

Definition prim (n:nat) : Type := oprim * svec n.

Definition vec_of_svec {n} (v:svec n) : vec n :=
  exist _ (array_of_slist(proj1_sig v)) (proj2_sig v).

Lemma svec_of_vec_lemma1 {n} (v:vec n) : non_false_array (array_of_slist (slist_of_array (proj1_sig v))) = true.
Proof. destruct v; curry1. Qed.

Definition svec_of_vec {n} (v:vec n) : svec n :=
  exist _ (slist_of_array(proj1_sig v)) (svec_of_vec_lemma1 v).

Definition sem_svec (op:oprim) {n} (s:svec n) : pvec n :=
  match op with
  | OC _ _ => pvec_of_vec_max (vec_of_svec s)
  | _      => pvec_of_vec_min (vec_of_svec s)
  end.

Definition oPrim_of_oprim (p:oprim) : oPrim :=
  match p with OU => oU | OX => oX | OC if0 th0 => oC if0 th0 end.

Definition sem_prim {n} (p:prim(S n)) (f:bf n) : bf (S n) := intro_lP_bf (fst p) (sem_svec (fst p) (snd p)) f.
Definition detect_prim {n} (p:prim(S n)) (f:bf(S n)) : bool := is_lP_bf (fst p) (sem_svec (fst p) (snd p)) f.
Definition elim_prim {n} (p:prim(S n)) (f:bf(S n)) : bf n := elim_lP_bf (fst p) (sem_svec (fst p) (snd p)) f.

Definition extract_prim {n} p (f:bf(S n)) : option(bf n) :=
  if detect_prim p f then Some(elim_prim p f) else None.

Definition is_prim {n} (p:prim _) (f:bf(S n)) :=
  @PrimUGen.is_prim prim (@extract_prim) n p f.

Definition is_prim' {n} (p:prim _) (f:bf n) :=
  @PrimUGen.is_prim' prim (@extract_prim) n p f.

Lemma sem_prim_as_detect_prim {n} p f g :
  @sem_prim n p f = g <-> detect_prim p g = true /\ f = elim_prim p g.
Proof with curry1.
unfold detect_prim, elim_prim, sem_prim.
apply (intro_lP_bf_as_is_lP_bf _ _ _ _).
Qed.

Lemma extract_prim_iff_sem_prim {n} p f xf :
  @extract_prim n p f = Some xf <-> sem_prim p xf = f.
Proof with curry1.
unfold extract_prim...
rewrite sem_prim_as_detect_prim...
Qed.
Hint Rewrite @extract_prim_iff_sem_prim.

Lemma is_term_cst_bf n c : is_term (cst_bf n c) = true.
Proof. unfold is_term, PrimUGen.is_term; curry1. Qed.
Hint Rewrite is_term_cst_bf : curry1_rewrite.

Lemma is_term_dot_bf n b a : is_term (@dot_bf n b a) = true.
Proof. unfold is_term, PrimUGen.is_term; curry1. Qed.
Hint Rewrite is_term_cst_bf : curry1_rewrite.

Lemma array_of_slist_injective {A n} (s1 s2:slist A n) : array_of_slist s1 = array_of_slist s2 <-> s1 = s2.
Proof. rewrite array_of_slist_shift; curry1. Qed.

Lemma is_term_intro_uP_bf_false_using_is_term_false {n} (f:bf n) :
  is_term f = false -> forall o pv, is_term (intro_lP_bf o pv f) = false.
Proof with curry1.
intros; apply reverse_bool_eq...
unfold sem_prim, is_term, PrimUGen.is_term in *...
unfold isSome, isNone in H0...
dest_match_step...
rewrite intro_lP_bf_as_is_lP_bf in D...
destruct pv as [[a0 k0] Hpv0], t as [b1 a1]...
unfold sem_svec, sem_term in *...
destruct o...
Qed.

Lemma is_term_sem_prim {n} (f:bf n): is_term f = false -> forall p, is_term (sem_prim p f) = false.
Proof with curry1.
intros H [o s].
apply(is_term_intro_uP_bf_false_using_is_term_false f H o (sem_svec o s)).
Qed.

Lemma elim_prim_sem_prim {n} p (f:bf n) : elim_prim p (sem_prim p f) = f.
Proof. unfold elim_prim, sem_prim; curry1. Qed.
Hint Rewrite @elim_prim_sem_prim : curry1_rewrite.

Definition cmp_prim {n} : prim n -> prim n -> ord := cmp_prod cmp_oprim (@cmp_svec n).

Lemma cmp_P_cmp_prim n : cmp_P (@cmp_prim n).
Proof. apply (cmp_P_cmp_prod _ _ cmp_P_cmp_oprim (cmp_P_cmp_svec _)). Qed.

Definition pcmp_of_cmp {A} (cA:cmp A) (x y:A) : option ord := Some(cA x y).

Lemma pcmp_P_pcmp_of_cmp {A} (cA:cmp A) (PA:cmp_P cA) : pcmp_P (pcmp_of_cmp cA).
Proof with curry1.
destruct PA as [RP TP AP].
unfold pcmp_of_cmp.
constructor...
- unfold pcmp_reflexive...
- unfold pcmp_transitive...
  specialize(TP x y z)...
- unfold pcmp_asymmetric...
Qed.

Definition pcmp_prim {n} (x y:prim n) : option ord := pcmp_of_cmp (@cmp_prim n) x y.

Lemma pcmp_P_pcmp_prim n : pcmp_P (@pcmp_prim n).
Proof. apply pcmp_P_pcmp_of_cmp, cmp_P_cmp_prim. Qed.

Definition minsat_bf_vec {n} : minsat_t (vec n) :=
  minsat_sig non_false_array cmp_array_bool minsat_bf minsat_positive_minsat_bf.

Definition minsat_bf_svec {n} : minsat_t (svec n) :=
  minsat_map svec_of_vec minsat_bf_vec.

Definition minsat_prim {n} (p:prim n -> bool) : option(prim n) :=
  minsat_prod minsat_oprim minsat_bf_svec p.

Lemma vec_of_svec_of_vec {n} (v:vec n) : vec_of_svec (svec_of_vec v) = v.
Proof with curry1.
unfold vec_of_svec, svec_of_vec...
setoid_rewrite exist_eq_l...
Qed.
Hint Rewrite @vec_of_svec_of_vec : curry1_rewrite.

Lemma svec_of_vec_of_svec {n} (v:svec n) : svec_of_vec(vec_of_svec v) = v.
Proof with curry1.
unfold vec_of_svec, svec_of_vec...
setoid_rewrite exist_eq_l...
Qed.
Hint Rewrite @svec_of_vec_of_svec : curry1_rewrite.

Lemma explicit_bijection_constr_svec_vec n : explicit_bijection_constr (@svec_of_vec n) vec_of_svec.
Proof. constructor; apply functional_extensionality; curry1. Qed.

Definition cmp_vec {n} := cmp_sig (@cmp_array_bool n) (fun a => non_false_array a = true) .

Lemma cmp_P_cmp_vec n : cmp_P (@cmp_vec n).
Proof. apply cmp_P_cmp_sig, cmp_P_cmp_array_bool. Qed.

Definition slist_nil {A} : slist A 0 := exist _ nil eq_refl.
Definition slist_cons {A n} (a0:A) (a:slist A n) : slist A (S n) :=
  exist _ (cons a0(proj1_sig a)) (proj2_sig a).

Lemma slist_of_array_scons {A n} a0 (a:array A n) :
  slist_of_array (scons a0 a) = slist_cons a0 (slist_of_array a).
Proof. unfold slist_cons, slist_of_array; curry1. Qed.
Hint Rewrite @slist_of_array_scons : curry1_rewrite.

Lemma cmp_slist_slist_cons_both {A n} cA a0 b0 (a b:slist A n) :
  cmp_slist cA (slist_cons a0 a) (slist_cons b0 b) = cmp_comp (cA a0 b0) (cmp_slist cA a b).
Proof. unfold cmp_slist, cmp_sig; curry1. Qed.
Hint Rewrite @cmp_slist_slist_cons_both : curry1_rewrite.

Lemma is_cmp_compatible_array_slist {A} (cA:cmp A) (HCA:cmp_P cA) {n} :
  is_cmp_compatible (@slist_of_array A n) (cmp_array cA) (cmp_slist cA).
Proof with curry1.
intros a1 a2.
induction n...
rewrite <- (scons_shead_stail a1).
rewrite <- (scons_shead_stail a2).
rewrite cmp_array_scons.
repeat rewrite slist_of_array_scons.
rewrite cmp_slist_slist_cons_both.
rewrite IHn...
Qed.

Lemma is_cmp_compatible_svec_vec {n} : is_cmp_compatible (@svec_of_vec n) cmp_vec cmp_svec.
Proof with curry1.
unfold is_cmp_compatible...
unfold cmp_vec, cmp_svec...
destruct a1 as [v1 H1], a2 as [v2 H2]...
unfold cmp_sig...
apply is_cmp_compatible_array_slist.
apply cmp_P_bool.
Qed.

(* Definition cmp_svec {n} := cmp_sig (fun v => non_false_array (array_of_slist v) = true) (@cmp_slist_bool n). *)

Lemma minsat_positive_minsat_bf_vec {n} : minsat_positive (@cmp_vec n) minsat_bf_vec.
Proof with curry1.
apply minsat_positive_minsat_sig.
apply cmp_P_cmp_array_bool.
Qed.

Lemma minsat_positive_minsat_bf_svec {n} : minsat_positive (@cmp_svec n) minsat_bf_svec.
Proof with curry1.
apply (minsat_positive_minsat_map_using_is_cmp_compatible_and_bijection svec_of_vec vec_of_svec
(explicit_bijection_constr_svec_vec n) cmp_vec cmp_svec).
- apply is_cmp_compatible_svec_vec.
- apply minsat_positive_minsat_bf_vec.
Qed.

Lemma minsat_positive_minsat_prim {n} : minsat_positive (@cmp_prim n) (@minsat_prim n).
Proof with curry1.
apply minsat_positive_minsat_prod.
- apply cmp_P_cmp_oprim.
- apply cmp_P_cmp_svec.
- apply minsat_positive_minsat_oprim.
- apply minsat_positive_minsat_bf_svec.
Qed.

Definition minsat_prim_uP_bf {n} (f:bf(S n)) : option(prim(S n)) :=
  minsat_prim (fun p => detect_prim p f).

Definition is_prim_free{n} (f:bf n) : bool :=
  match n as n0 return bf n0 -> bool with
  | 0 => (fun _ => true)
  | S n' => (fun f => isNone(minsat_prim_uP_bf f))
  end f.

Lemma minsat_prim_eq_None_iff_forall_is_uP_bf_free n f :
  @minsat_prim_uP_bf n f = None <-> (forall p, detect_prim p f = false).
Proof with curry1.
rewrite <- functional_extensionality_iff...
unfold minsat_prim_uP_bf...
specialize(@minsat_positive_minsat_prim (S n)) as HH.
rewrite minsat_positive_iff_minsat_alternative in HH.
rewrite (HH _ _)...
Qed.

Lemma minsat_prim_eq_None_iff_is_prim_free n f :
  @minsat_prim_uP_bf n f = None <-> is_prim_free f = true.
Proof. unfold is_prim_free; curry1. Qed.

Lemma is_prim_free_correct {n} (f:bf n) :
  is_prim_free f = true <-> forall p, is_prim' p f = false.
Proof with curry1.
destruct n...
unfold PrimUGen.is_prim...
rewrite minsat_prim_eq_None_iff_forall_is_uP_bf_free...
unfold extract_prim...
split; intros H x; specialize(H x)...
Qed.

Definition xprim {F} : nat -> Type :=
  @PrimUGen.xprim term prim F.

Definition extract_xprim {n} (f:bf n) : @xprim bf n :=
match n as n0 return bf n0 -> @xprim bf n0 with
| O => (fun f => XTerm _ _ (term_of_bool f))
| S n' => (fun f =>
  match extract_term f with
  | Some t => XTerm _ _ t
  | None =>
  match minsat_prim_uP_bf f with
  | Some p => XPrim _ _ p (elim_prim p f)
  | None => (XSha _ _ (fnary_evalo1 f false) (fnary_evalo1 f true))
  end
  end)
end f.

Definition sem_xprim {n} (xp:@xprim bf n) : bf n :=
  @PrimUGen.sem_xprim term (@sem_term) prim (@sem_prim) n xp.

Lemma isSome_extract_prim {n} (f:bf(S n)) p : isSome (extract_prim p f) = detect_prim p f.
Proof. unfold extract_prim; dest_match_step; curry1. Qed.
Hint Rewrite @isSome_extract_prim : curry1_rewrite.

Lemma extract_prim_eq_None {n} p f : @extract_prim n p f = None <-> detect_prim p f = false.
Proof. unfold extract_prim; curry1. Qed.

Lemma minsat_prim_uP_bf_eq_Some {n} f p:
  @minsat_prim_uP_bf n f = Some p <-> is_min_positive cmp_prim (fun p => is_prim' p f) p.
Proof with curry1.
unfold minsat_prim_uP_bf.
specialize(@minsat_positive_minsat_prim(S n)) as H.
rewrite minsat_positive_iff_minsat_alternative in H.
rewrite (H _)...
unfold PrimUGen.is_prim...
specialize(isSome_extract_prim f) as HH.
rewrite <- functional_extensionality_iff in HH.
rewrite HH...
Qed.

Definition is_lowest_prim' {n} (p:prim n) f := (is_lowest_prim' prim (@extract_prim) (@pcmp_prim)) p f.

Lemma is_lP_bf_intro_lP_bf_using_oprim_compatible_eq_false {n} o1 pv1 o2 pv2 (f:bf n) :
  oprim_compatible o1 o2 = false ->
  is_term (intro_lP_bf o2 pv2 f) = false -> is_lP_bf o1 pv1 (intro_lP_bf o2 pv2 f) = false.
Proof with curry1.
intros; apply reverse_bool_eq...
destruct o1, o2...
- rewrite is_term_dot_bf in H0...
- rewrite <- intro_lP_bf_elim_lP_bf_same_eq_same in H1.
  symmetry in H1.
  rewrite intro_lP_bf_as_is_lP_bf in H1...
  rewrite H2 in H1...
  rewrite is_term_dot_bf in H0...
- rewrite is_lC_bf_intro_lC_bf_using_th0_neq_th1 in H1...
  rewrite is_term_dot_bf in H0...
Qed.

Lemma detect_prim_sem_prim_same {n} p (f:bf n) : detect_prim p (sem_prim p f) = true.
Proof. unfold detect_prim, sem_prim; curry1. Qed.
Hint Rewrite @detect_prim_sem_prim_same : curry1_rewrite.

Lemma sem_prim_elim_prim_same {n} p (f:bf(S n)) :
  sem_prim p (elim_prim p f) = f <-> detect_prim p f = true.
Proof. unfold sem_prim, elim_prim, detect_prim; curry1. Qed.
Hint Rewrite @sem_prim_elim_prim_same : curry1_rewrite.

Lemma extract_xprim_minimal : forall {n} (f:bf n),
    match extract_xprim f with
    | XTerm _ _ _ => True
    | XPrim _ _  p fp => is_term f = false /\ is_lowest_prim' p (sem_prim p fp)
    | XSha _ _ _ _ => is_term f = false /\ is_prim_free f = true
    end.
Proof with curry1.
unfold extract_xprim...
dest_match_step...
dest_match_step...
assert(is_term f = false).
{ unfold is_term, PrimUGen.is_term... }
dest_match...
rewrite minsat_prim_uP_bf_eq_Some in D0...
unfold is_min_positive, PrimUGen.is_prim in *...
unfold is_lowest_prim', PrimUGen.is_lowest_prim'...
unfold PrimUGen.is_prim...
specialize(H1 p')...
unfold pcmp_prim...
rewrite <- sem_prim_elim_prim_same in H0.
rewrite H0 in H3...
unfold pcmp_of_cmp...
Qed.

Lemma sem_xprim_extract_xprim {n} (f:bf n) : sem_xprim (extract_xprim f) = f.
Proof with curry1.
destruct n...
- rewrite(bf0 f)...
  unfold sem_term...
- dest_match_step...
  dest_match_step...
  rewrite minsat_prim_uP_bf_eq_Some in D0...
  unfold is_min_positive in D0...
  unfold PrimUGen.is_prim in H...
Qed.

Definition same_prim {n} (f1 f2:bf n) : Prop :=
  @PrimUGen.same_prim prim (@extract_prim) n f1 f2.

Lemma extract_term_intro_lP_bf_cst_bf {n} (o:oprim) (pv:pvec _) b :
  extract_term (intro_lP_bf o pv (cst_bf n b)) =
    let t := intro_lP_pvec_cst_as_dot_pvec o pv b in
    Some(fst t, slist_of_array(snd t)).
Proof. rewrite intro_lP_bf_cst_bf; curry1; unfold sem_term; curry1. Qed.

Lemma is_term_sem_prim_cst_bf n p b : is_term (sem_prim p (cst_bf n b)) = true.
Proof with curry1.
unfold is_term, PrimUGen.is_term, sem_prim...
rewrite extract_term_intro_lP_bf_cst_bf...
Qed.
Hint Rewrite @is_term_sem_prim_cst_bf : curry1_rewrite.

(* [MOVEME] *)
Lemma exists_P_using_P {P:Prop} (p:P) (Q:P -> Prop) : (exists p, Q p) <-> Q p.
Proof with curry1.
split...
- destruct H...
- exists p...
Qed.

Ltac exists_P_using_P :=
  match goal with
  | H : ?P |- _ =>
    match type of P with
    | Prop =>
      match goal with
      | |- context[exists p, @?Q p] =>
        rewrite (exists_P_using_P H Q)
      | H' : context[exists p, @?Q p] |- _ =>
        rewrite (exists_P_using_P H Q) in H'
      end
    end
  end.

Lemma asum_pv_eq_false_implies_non_false_array_spop_k_v {n} (pv:pvec(S n)) (H:asum (fst (proj1_sig pv)) = false) :
  non_false_array (spop (snd (proj1_sig pv)) (fst (proj1_sig pv))) = true.
Proof with curry1.
apply reverse_bool_eq...
unfold non_false_array in *...
destruct pv as [[a k] Hpv]...
Qed.

(* [MOVEME] *)
Lemma factorize_iff (F:Prop) {P Q:Prop} : (P -> F) -> (Q -> F) -> (P <-> Q) <-> (F -> (P <-> Q)).
Proof. firstorder. Qed.

Lemma is_lU_bf_intro_intro_lC_bf_same_using_is_term_false if0 th0 {n} (pv:pvec(S(S n))) (f:bf(S n)) :
  is_term(intro_lP_bf (OC if0 th0) pv f) = false ->
    is_lP_bf OU pv (intro_lP_bf (OC if0 th0) pv f) = true <->
      asum (fst (proj1_sig pv)) = false /\
      exists (H:asum (fst (proj1_sig pv)) = false),
      let HH := asum_pv_eq_false_implies_non_false_array_spop_k_v _ H in
      let v' := exist _ (spop (snd (proj1_sig pv)) (fst (proj1_sig pv))) HH : vec (S n) in
      let pv' := pvec_of_vec_min v' : pvec (S n) in
      is_lP_bf OU pv' f = true.
Proof with curry1.
intros H1...
rewrite is_lU_bf_intro_lC_bf_same...
rewrite (factorize_iff (asum (fst (proj1_sig pv)) = false))...
- exists_P_using_P...
  rewrite rewrite_is_lU_bf...
- apply reverse_bool_eq...
Qed.

Lemma is_term_intro_lX_bf {n} pv (f:bf n) : is_term (intro_lP_bf OX pv f) = is_term f.
Proof with curry1.
rewrite eq_iff_eq_true...
unfold is_term, PrimUGen.is_term in *...
unfold isSome...
dest_match_step...
- destruct t as [b0 a0]...
  unfold sem_term in D...
  rewrite intro_lP_bf_as_is_lP_bf in D...
- dest_match_step...
  unfold sem_term in D...
Qed.
Hint Rewrite @is_term_intro_lX_bf : curry1_rewrite.

Lemma is_lP_bf_intro_intro_lP_bf_same_using_is_term_false {n} p1 p2 pv (f:bf n) :
   weirdly_lin_compatible p1 p2 = false ->
  is_term(intro_lP_bf p2 pv f) = false -> is_lP_bf p1 pv (intro_lP_bf p2 pv f) = true <-> p1 = p2.
Proof with curry1.
intro H1.
split; intro H2...
destruct p1 eqn:E1;
  destruct p2 eqn:E2...
- rewrite rewrite_is_lU_bf in H2...
- rewrite rewrite_is_lX_bf in H2...
- rewrite is_term_dot_bf in H...
- rewrite <- intro_lP_bf_elim_lP_bf_same_eq_same in H2.
  symmetry in H2.
  rewrite intro_lP_bf_as_is_lP_bf in H2...
  rewrite H2 in H1...
  rewrite is_term_dot_bf in H...
- dest_match_step...
  dest_match_step...
  rewrite is_term_dot_bf in H...
Qed.

Lemma is_lC_bf_intro_lC_bf_comm_same_th0_is_term_false ifC ifc th0 {n} (pvC pvc:pvec _) (f:bf(S n)) :
  is_term(intro_lP_bf (OC ifc th0) pvc f) = false ->
  is_lP_bf (OC ifC th0) pvC (intro_lP_bf (OC ifc th0) pvc f) =
    match is_lC_pvec_intro_lC_pvec_comm_same_th0 ifC ifc pvC pvc with
    | AA b => b
    | BB b'_pvC' => is_lP_bf (OC (fst b'_pvC') th0) (snd b'_pvC') f
    end.
Proof with curry1.
rewrite is_lC_bf_intro_lC_bf_comm_same_th0...
dest_match_step...
dest_match_step...
- dest_match_step...
  rewrite is_term_dot_bf in H...
- rewrite VAX.andb_eq_l...
  rewrite is_term_dot_bf in H...
Qed.

Lemma pvec_of_array_min_lemma0 {n} (a:array bool n) k :
  array_minsat a = Some k -> pvec_p (a, k) = true.
Proof with curry1.
rewrite (minsat_alternative_array_minsat _ _).
unfold is_min_positive...
Qed.

Definition pvec_of_array_min {n} (a:array bool n) : option(pvec n) :=
match array_minsat a as r return array_minsat a = r -> option(pvec n) with
| Some k => fun E => @Some (pvec n) (exist _ (a, k) (pvec_of_array_min_lemma0 a k E))
| None   => fun _ => None
end eq_refl.

Definition is_lU_pvec_intro_lC_pvec_comm
  {n} (pvC pvc:pvec(S(S n))) : @AB bool (pvec(S n)) :=
  let aC := fst(proj1_sig pvC) in
  let ac := fst(proj1_sig pvc) in
  let kc := snd(proj1_sig pvc) in
  if adot ac aC
  then @AA bool (pvec(S n)) false
  else
    match pvec_of_array_min (spop kc aC) with
    | Some pvC' => @BB bool (pvec(S n)) pvC'
    | None      => @AA bool (pvec(S n)) true
    end.

Lemma pvec_of_array_min_alternative n (a:array bool n) o :
  pvec_of_array_min a = o <->
  match o with
  | None => a = fun _ => false
  | Some pv => (
    a = fst(proj1_sig pv) /\
    let k := snd(proj1_sig pv) in
    is_min_positive cmp_ltN a k
  )
  end.
Proof with curry1.
unfold pvec_of_array_min.
unfold pvec.
pattern(match array_minsat a as r return (array_minsat a = r -> option {ak : array bool n * ltN n | pvec_p ak = true}) with
| Some k =>
    fun E : array_minsat a = Some k =>
    Some (exist (fun ak : array bool n * ltN n => pvec_p ak = true) (a, k) (pvec_of_array_min_lemma0 a k E))
| None => fun _ : array_minsat a = None => None
end eq_refl).
rewrite match_option_dtt...
split...
- split...
  dest_match_step...
- assert(H:=p).
  rewrite (minsat_alternative_array_minsat _ _) in H...
  split...
  dest_match_step...
  destruct p0 as [[a' k'] H']...
  specialize(is_min_positive_unique _ (cmp_P_cmp_ltN _) _ _ _ H H1)...
Qed.

Lemma pvec_of_array_min_false n : pvec_of_array_min (fun _ : ltN n => false) = None.
Proof. rewrite pvec_of_array_min_alternative; curry1. Qed.
Hint Rewrite @pvec_of_array_min_false : curry1_rewrite.

Lemma pvec_of_array_min_eq_None {n} (a:array bool n) : pvec_of_array_min a = None <-> a = fun _ => false.
Proof. rewrite pvec_of_array_min_alternative; curry1. Qed.

Lemma pvec_of_array_min_eq_Some {n} (a:array bool n) (pv:pvec n) :
  pvec_of_array_min a = Some pv <->
    (a = fst(proj1_sig pv) /\
      let k := snd(proj1_sig pv) in is_min_positive cmp_ltN a k).
Proof. rewrite pvec_of_array_min_alternative; curry1. Qed.

Lemma is_lU_pvec_intro_lC_pvec_comm_eq_AA {n} pvC pvc b :
  @is_lU_pvec_intro_lC_pvec_comm n pvC pvc = AA b <->
    let aC := fst(proj1_sig pvC) in
    let ac := fst(proj1_sig pvc) in
    let kc := snd(proj1_sig pvc) in
    b = negb(adot ac aC) /\ (b = true -> aC = array_cdirac (aC kc) kc).
Proof with curry1.
unfold is_lU_pvec_intro_lC_pvec_comm.
dest_match_step...
- split...
- dest_match_step...
  + replace (spop (snd (proj1_sig pvc)) (fst (proj1_sig pvC))) with (fun _ : ltN (S n) => false) in D0...
    symmetry; setoid_rewrite spop_eq_fun_cst...
  + rewrite pvec_of_array_min_eq_None in D0...
Qed.

Lemma is_lU_pvec_intro_lC_pvec_comm_eq_BB {n} pvC pvc pv :
  @is_lU_pvec_intro_lC_pvec_comm n pvC pvc = BB pv <->
    let aC := fst(proj1_sig pvC) in
    let ac := fst(proj1_sig pvc) in
    let kc := snd(proj1_sig pvc) in
    let a := fst(proj1_sig pv) in
    let k := snd(proj1_sig pv) in
    adot ac aC = false /\
    a = spop kc aC /\ is_min_positive cmp_ltN a k.
Proof with curry1.
unfold is_lU_pvec_intro_lC_pvec_comm.
destruct pv as [[a k] Hpv]...
destruct pvC as [[aC kC] HC], pvc as [[ac kc] Hc]...
rewrite (factorize_iff (adot ac aC = false))...
- dest_match_step...
  + rewrite pvec_of_array_min_eq_Some in D...
    destruct p as [[a' k'] Hpv']...
    rewrite eq_sym_iff...
    split...
    specialize(is_min_positive_unique _ (cmp_P_cmp_ltN _) _ _ _ H1 H0)...
  + rewrite pvec_of_array_min_eq_None in D...
    rewrite D in *...
- apply reverse_bool_eq...
Qed.

Lemma is_lU_bf_intro_intro_lC_bf ifc thc {n} pvC pvc (f:bf(S n)) :
    is_lP_bf OU pvC (intro_lP_bf (OC ifc thc) pvc f) =
      match is_lU_pvec_intro_lC_pvec_comm pvC pvc with
      | AA b => b || (match extract_cst f with Some f0 => eqb f0 thc | None => false end)
      | BB pvC' =>  is_lP_bf OU pvC' f
      end.
Proof with curry1.
rewrite eq_iff_eq_true.
rewrite rewrite_is_lU_bf.
rewrite axor_input_intro_lC_bf...
dest_match_step...
- rewrite is_lU_pvec_intro_lC_pvec_comm_eq_AA in D...
  rewrite adot_comm.
  destruct(adot (fst (proj1_sig pvc)) (fst (proj1_sig pvC)))eqn:E0...
  + dest_match_step...
  + rewrite H0 in *...
- rewrite is_lU_pvec_intro_lC_pvec_comm_eq_BB in D...
  rewrite <- H0.
  rewrite adot_comm, H...
  rewrite <- rewrite_is_lU_bf...
Qed.

Lemma is_lU_bf_intro_intro_lC_bf_using_is_term_false ifc thc {n} pvC pvc (f:bf(S n)) :
    is_term(intro_lP_bf (OC ifc thc) pvc f) = false ->
    is_lP_bf OU pvC (intro_lP_bf (OC ifc thc) pvc f) =
      match is_lU_pvec_intro_lC_pvec_comm pvC pvc with
      | AA b => b
      | BB pvC' =>  is_lP_bf OU pvC' f
      end.
Proof with curry1.
rewrite is_lU_bf_intro_intro_lC_bf.
dest_match_step...
dest_match_step...
apply reverse_bool_eq...
destruct b...
Qed.

Lemma spush_spop_same {A n} k v (a:array A (S n)) : spush k v (spop k a) = array_set k v a.
Proof. rewrite (array_expansion_u k); curry1. Qed.
Hint Rewrite @spush_spop_same : curry1_rewrite.

Lemma array_set_spush_same {A n} k v1 v2 (a:array A n) : array_set k v1 (spush k v2 a) = spush k v1 a.
Proof with curry1.
rewrite <- spush_spop_same.
rewrite spop_spush_same.
reflexivity.
Qed.
Hint Rewrite @array_set_spush_same : curry1_rewrite.

Lemma is_lC_bf_intro_intro_lU_bf_lemma0 ifC thC {n} pvC pvc (f:bf(S n)) :
  adot (fst (proj1_sig pvc)) (fst (proj1_sig pvC)) = true ->
  extract_cst f = None ->
  is_lP_bf (OC ifC thC) pvC (intro_lP_bf OU pvc f) = true -> False.
Proof with curry1.
intros H1 H2 H3.
rewrite <- intro_lP_bf_elim_lP_bf_same_eq_same in H3.
symmetry in H3.
rewrite intro_lP_bf_as_is_lP_bf in H3...
rewrite is_lU_bf_intro_intro_lC_bf in H...
dest_match_step...
- rewrite is_lU_pvec_intro_lC_pvec_comm_eq_AA in D...
  rewrite (adot_comm _ (fst (proj1_sig pvC)) _) in H, H4.
  rewrite H1 in H, H4...
  dest_match_step...
  rewrite D in H0...
- rewrite is_lU_pvec_intro_lC_pvec_comm_eq_BB in D...
  rewrite adot_comm, H1 in H3...
Qed.

Lemma fst_proj1_sig_pvec_eq_fun_false {n} (pv:pvec n) : fst(proj1_sig pv) = (fun _ => false) <-> False.
Proof. destruct pv as [[a k] H]; curry1. Qed.
Hint Rewrite @fst_proj1_sig_pvec_eq_fun_false : curry1_rewrite.

Lemma intro_lU_bf_eval_spush_same_l {n} (pv:pvec(S n)) (f:bf n) xk (x:array bool n) :
  intro_lP_bf OU pv f (spush (snd (proj1_sig pv)) xk x) =
    f (axor x (ascal xk (spop (snd(proj1_sig pv)) (fst(proj1_sig pv))))).
Proof with curry1.
unfold intro_lP_bf...
unfold lUX_twist_bf, lUX_twist_array...
unfold intro_uP_bf...
Qed.

(* very similar to is_lU_pvec_intro_lC_pvec_comm *)
Definition is_lC_pvec_intro_lU_pvec_comm
  {n} (pvC pvc:pvec(S(S n))) : @AB bool (pvec(S n)) :=
  let aC := fst(proj1_sig pvC) in
  let ac := fst(proj1_sig pvc) in
  let kc := snd(proj1_sig pvc) in
  if adot ac aC
  then @AA bool (pvec(S n)) false
  else
    match pvec_of_array_max (spop kc aC) with
    | Some pvC' => @BB bool (pvec(S n)) pvC'
    | None      => @AA bool (pvec(S n)) true
    end.

Lemma pvec_of_array_max_fun_false n : @pvec_of_array_max n (fun _ => false) = None.
Proof. rewrite pvec_of_array_max_eq_None; reflexivity. Qed.
Hint Rewrite pvec_of_array_max_fun_false : curry1_rewrite.

Lemma is_lC_pvec_intro_lU_pvec_comm_eq_AA {n} pvC pvc b :
  @is_lC_pvec_intro_lU_pvec_comm n pvC pvc = AA b <->
    let aC := fst(proj1_sig pvC) in
    let ac := fst(proj1_sig pvc) in
    let kc := snd(proj1_sig pvc) in
    b = negb(adot ac aC) /\ (b = true -> aC = array_cdirac (aC kc) kc).
Proof with curry1.
unfold is_lC_pvec_intro_lU_pvec_comm.
dest_match_step...
- split...
- dest_match_step...
  + replace (spop (snd (proj1_sig pvc)) (fst (proj1_sig pvC))) with (fun _ : ltN (S n) => false) in D0...
    symmetry; setoid_rewrite spop_eq_fun_cst...
  + rewrite pvec_of_array_max_eq_None in D0...
Qed.

Lemma is_lC_pvec_intro_lU_pvec_comm_eq_BB {n} pvC pvc pv :
  @is_lC_pvec_intro_lU_pvec_comm n pvC pvc = BB pv <->
    let aC := fst(proj1_sig pvC) in
    let ac := fst(proj1_sig pvc) in
    let kc := snd(proj1_sig pvc) in
    let a := fst(proj1_sig pv) in
    let k := snd(proj1_sig pv) in
    adot ac aC = false /\
    a = spop kc aC /\ is_max_positive cmp_ltN a k.
Proof with curry1.
unfold is_lC_pvec_intro_lU_pvec_comm.
destruct pv as [[a k] Hpv]...
destruct pvC as [[aC kC] HC], pvc as [[ac kc] Hc]...
rewrite (factorize_iff (adot ac aC = false))...
- dest_match_step...
  + rewrite pvec_of_array_max_eq_Some in D...
    destruct p as [[a' k'] Hpv']...
    rewrite eq_sym_iff...
    split...
    specialize(is_max_positive_unique _ (cmp_P_cmp_ltN _) _ _ _ H1 H0)...
  + rewrite pvec_of_array_max_eq_None in D...
    rewrite D in *...
- apply reverse_bool_eq...
Qed.

Lemma is_lC_bf_intro_intro_lU_bf ifC thC {n} pvC pvc (f:bf(S n)) :
    is_lP_bf (OC ifC thC) pvC (intro_lP_bf OU pvc f) =
      match is_lC_pvec_intro_lU_pvec_comm pvC pvc with
      | AA b => b || (match extract_cst f with Some f0 => eqb f0 thC | None => false end)
      | BB pvC' =>  is_lP_bf (OC ifC thC) pvC' f
      end.
Proof with curry1.
rewrite eq_iff_eq_true.
dest_match_step...
- rewrite is_lC_pvec_intro_lU_pvec_comm_eq_AA in D...
  destruct(adot (fst (proj1_sig pvc)) (fst (proj1_sig pvC)))eqn:E0...
  + dest_match_step...
    * rewrite intro_lP_bf_cst_bf...
    * eauto using is_lC_bf_intro_intro_lU_bf_lemma0.
  + rewrite H0 in *...
- rewrite is_lC_pvec_intro_lU_pvec_comm_eq_BB in D...
  repeat rewrite rewrite_is_lC_bf...
  split...
  + specialize(H2(spush (snd(proj1_sig pvc)) false x))...
    destruct p as [[ap kp] Hp]...
    rewrite intro_lU_bf_eval_spush_same_l...
  + rewrite <- (SimplLtN_Chap1_Array_as_Function.spush_spop_same (snd(proj1_sig pvc)) x).
    rewrite intro_lU_bf_eval_spush_same_l.
    destruct p as [[ap kp] Hp]...
    apply H2...
    rewrite adot_axor_r.
    rewrite adot_ascal_r.
    repeat rewrite adot_spop_both.
    rewrite adot_comm in H...
    BPS.MicroBPSolver...
Qed.

Lemma is_lC_bf_intro_intro_lU_bf_using_is_term_false ifC thC {n} pvC pvc (f:bf(S n)) :
    is_term(intro_lP_bf OU pvc f) = false ->
    is_lP_bf (OC ifC thC) pvC (intro_lP_bf OU pvc f) =
      match is_lC_pvec_intro_lU_pvec_comm pvC pvc with
      | AA b => b
      | BB pvC' =>  is_lP_bf (OC ifC thC) pvC' f
      end.
Proof with curry1.
rewrite is_lC_bf_intro_intro_lU_bf.
dest_match_step...
dest_match_step...
rewrite intro_lP_bf_cst_bf in H...
Qed.

Definition is_lP_pvec_intro_lP_pvec_comm (pC pc:oprim) {n} (pvC pvc:pvec(S(S n))) : @AB bool (oprim * pvec(S n)) :=
  match UX_C_of_oprim pC, UX_C_of_oprim pc with
  | AA bC, AA bc => (* case UX vs UX *)
    match is_lUX_pvec_intro_lUX_pvec_comm bC bc pvC pvc with
    | AA b => AA b
    | BB (bC', pvC') => BB(UXb bC', pvC')
    end
  | BB(ifC, thC), BB(ifc, thc) => (* case C vs C *)
    if eqb thC thc
    then
      match is_lC_pvec_intro_lC_pvec_comm_same_th0 ifC ifc pvC pvc with
      | AA b => AA b
      | BB (ifC', pvC') => BB(OC ifC' thC, pvC')
      end
    else AA false
  | AA false, BB(ifc, thc) => (* case U vs C *)
    match is_lU_pvec_intro_lC_pvec_comm pvC pvc with
    | AA b => AA b
    | BB pvC' => BB(OU, pvC')
    end
  | BB(ifC, thC), AA false => (* case C vs U *)
    match is_lC_pvec_intro_lU_pvec_comm pvC pvc with
    | AA b => AA b
    | BB pvC' => BB(OC ifC thC, pvC')
    end
  | _, _ => AA false
  end.

Lemma array_set_with_eval_same {A n} k (a:array A n) : array_set k (a k) a = a.
Proof with curry1.
destruct n...
rewrite (array_expansion_u k)...
Qed.
Hint Rewrite @array_set_with_eval_same : curry1_rewrite.

Lemma is_lC_bf_intro_lX_bf_using_is_term_false (if0 th0 : bool) {n : nat} (pv1 pv2 : pvec (S n)) (f : bf n) :
  is_term(intro_lP_bf OX pv2 f) = false ->
  is_lP_bf (OC if0 th0) pv1 (intro_lP_bf OX pv2 f) = false.
Proof with curry1.
intros.
apply reverse_bool_eq...
rewrite <- intro_lP_bf_elim_lP_bf_same_eq_same in H0.
symmetry in H0.
rewrite intro_lP_bf_as_is_lP_bf in H0.
rewrite is_lX_bf_intro_lC_bf in H0...
rewrite H2 in H1...
rewrite is_term_dot_bf in H...
Qed.

Lemma is_lP_bf_intro_lP_bf_comm_using_is_term_false (pC pc:oprim) {n} (pvC pvc:pvec(S(S n))) (f:bf(S n)) :
  is_term(intro_lP_bf pc pvc f) = false ->
  is_lP_bf pC pvC (intro_lP_bf pc pvc f) =
    match is_lP_pvec_intro_lP_pvec_comm pC pc pvC pvc with
    | AA b => b
    | BB (pC', pvC') => is_lP_bf pC' pvC' f
    end.
Proof with curry1.
unfold is_lP_pvec_intro_lP_pvec_comm.
destruct(UX_C_of_oprim pC)eqn:EC;
  rewrite UX_C_iff_oprim in EC;
  destruct(UX_C_of_oprim pc)eqn:Ec;
    rewrite UX_C_iff_oprim in Ec...
- rewrite (is_lUX_bf_intro_lUX_bf_comm b b0)...
  dest_match_step...
- destruct b...
  + apply reverse_bool_eq...
    rewrite is_term_dot_bf in H...
  + rewrite is_lU_bf_intro_intro_lC_bf_using_is_term_false...
    dest_match_step...
- destruct b...
  + apply is_lC_bf_intro_lX_bf_using_is_term_false...
  + rewrite is_lC_bf_intro_intro_lU_bf_using_is_term_false...
    dest_match_step...
- destruct(eqb b0 b2)eqn:E0...
  + rewrite is_lC_bf_intro_lC_bf_comm_same_th0_is_term_false...
    dest_match_step...
  + apply reverse_bool_eq...
    rewrite is_lC_bf_intro_lC_bf_using_th0_neq_th1 in H0...
    rewrite is_term_dot_bf in H...
Qed.

Lemma is_lP_bf_sem_svec_svec_of_vec_vec_of_pvec o {n} (pv:pvec(S n)) f :
  is_lP_bf o (sem_svec o (svec_of_vec (vec_of_pvec pv))) f = is_lP_bf o pv f.
Proof with curry1.
apply same_vec_implies_same_is_lP_bf...
destruct o...
Qed.

Lemma rewrite_same_prim {n} (f1 f2:bf(S n)) : same_prim f1 f2 <-> (forall o pv, is_lP_bf o pv f1 = is_lP_bf o pv f2).
Proof with curry1.
unfold same_prim, PrimUGen.same_prim...
unfold PrimUGen.is_prim, extract_prim...
unfold detect_prim...
split...
- specialize(H(o, (svec_of_vec (vec_of_pvec pv))))...
  repeat rewrite is_lP_bf_sem_svec_svec_of_vec_vec_of_pvec in H.
  dest_match_step...
  dest_match_step...
- rewrite H...
  dest_match_step...
Qed.

(* [same_prim_preserved] $2021-07-03 *)
Lemma same_prim_preserved {n} (f1 f2:bf n) :
  same_prim f1 f2 -> forall p,
    let pf1 := sem_prim p f1 in
    let pf2 := sem_prim p f2 in
    is_term pf1 = false -> is_term pf2 = false -> same_prim pf1 pf2.
Proof with curry1.
destruct n...
- rewrite (bf0 f1) in *...
- rewrite rewrite_same_prim in H.
  rewrite rewrite_same_prim.
  curry1.
  unfold sem_prim...
  repeat rewrite is_lP_bf_intro_lP_bf_comm_using_is_term_false...
  dest_match_step...
Qed.

Lemma array_set_eq_array_cdirac v0 v2 {n} k a :
  @array_set _ n k v0 a = array_cdirac v2 k <-> v0 = v2 /\ a = array_cdirac (a k) k.
Proof with curry1.
destruct n...
repeat rewrite (array_expansion_u k).
rewrite spop_array_set_same.
repeat rewrite spop_array_cdirac_same.
curry1.
Qed.

Lemma is_lUX_pvec_intro_lUX_pvec_comm_eq_AA bC bc {n} (pvC pvc:pvec(S(S n))) b :
  is_lUX_pvec_intro_lUX_pvec_comm bC bc pvC pvc = AA b <->
    b = eqb bC bc /\ fst(proj1_sig pvC) = fst(proj1_sig pvc).
Proof with curry1.
unfold is_lUX_pvec_intro_lUX_pvec_comm.
dest_match_step...
- rewrite vec_of_array_bool_eq_Some in D...
  rewrite H0 in D.
  rewrite lUX_twist_array_same in D...
  destruct v...
- rewrite eq_sym_iff...
  unfold lUX_twist_array in D...
  destruct pvC as [[aC kC] HC], pvc as [[ac kc] Hc]...
  rewrite (array_expansion_u kc)...
  destruct(aC kc)eqn:E0...
  + rewrite <- spop_axor in D...
    unfold array_cdirac in D...
    rewrite (array_expansion_u kc) in D...
    rewrite axor_shift in H0...
  + specialize(f_equal(fun f => f kC)D)...
    unfold array_set in H...
Qed.

Lemma is_lUX_pvec_intro_lUX_pvec_comm_eq_BB bC bc {n} (pvC pvc:pvec(S(S n))) bC' pvC' :
  is_lUX_pvec_intro_lUX_pvec_comm bC bc pvC pvc = BB (bC', pvC') <->
    bC' = xorb bC (fst (proj1_sig pvC) (snd (proj1_sig pvc)) && bc) /\
    fst(proj1_sig pvC') = spop (snd (proj1_sig pvc)) (lUX_twist_array pvc (fst (proj1_sig pvC))) /\
    is_min_positive cmp_ltN (fst(proj1_sig pvC')) (snd(proj1_sig pvC')).
Proof with curry1.
unfold is_lUX_pvec_intro_lUX_pvec_comm.
dest_match_step...
- rewrite vec_of_array_bool_eq_Some in D...
  symmetry in D...
  split...
  + remember(array_minsat_true (proj1_sig v) (pvec_of_vec_min_lemma1 v)) as k eqn:E.
    symmetry in E.
    rewrite array_minsat_iff_is_min_positive in E...
  + destruct pvC' as [[aC' kC'] HC']...
    unfold pvec_of_vec_min...
    rewrite array_minsat_iff_is_min_positive...
    rewrite D...
- unfold lUX_twist_array in *...
  rewrite ascal_spop_l in D.
  rewrite <- spop_axor in D.
  curry1.
  rewrite array_set_eq_array_cdirac in D...
  rewrite axor_shift in H2...
  rewrite ascal_spop_l in H.
  rewrite H2 in H...
Qed.

Lemma OU_eq_OU : OU = OU <-> True. Proof. simplprop. Qed.
Lemma OU_eq_OX : OU = OX <-> False. Proof. solve_by_invert. Qed.
Lemma OU_eq_OC x y : OU = OC x y <-> False. Proof. solve_by_invert. Qed.
Lemma OX_eq_OU : OX = OU <-> False. Proof. solve_by_invert. Qed.
Lemma OX_eq_OX : OX = OX <-> True. Proof. simplprop. Qed.
Lemma OX_eq_OC x y : OX = OC x y <-> False. Proof. solve_by_invert. Qed.
Lemma OC_eq_OU x y : OC x y = OU <-> False. Proof. solve_by_invert. Qed.
Lemma OC_eq_OX x y : OC x y = OX <-> False. Proof. solve_by_invert. Qed.
Lemma OC_eq_OC if0 th0 if1 th1 : OC if0 th0 = OC if1 th1 <-> if0 = if1 /\ th0 = th1. Proof. split; solve_by_invert. Qed.

Hint Rewrite OU_eq_OU : curry1_rewrite.
Hint Rewrite OU_eq_OX : curry1_rewrite.
Hint Rewrite OU_eq_OC : curry1_rewrite.
Hint Rewrite OX_eq_OU : curry1_rewrite.
Hint Rewrite OX_eq_OX : curry1_rewrite.
Hint Rewrite OX_eq_OC : curry1_rewrite.
Hint Rewrite OC_eq_OU : curry1_rewrite.
Hint Rewrite OC_eq_OX : curry1_rewrite.
Hint Rewrite OC_eq_OC : curry1_rewrite.

Lemma is_lP_pvec_intro_lP_pvec_comm_eq_AA_true (pC pc:oprim) {n} (pvC pvc:pvec(S(S n))) :
  is_lP_pvec_intro_lP_pvec_comm pC pc pvC pvc = AA true <-> pC = pc /\ fst(proj1_sig pvC) = fst(proj1_sig pvc).
Proof with curry1.
unfold is_lP_pvec_intro_lP_pvec_comm.
destruct(UX_C_of_oprim pC)eqn:EC;
  rewrite UX_C_iff_oprim in EC;
  destruct(UX_C_of_oprim pc)eqn:Ec;
    rewrite UX_C_iff_oprim in Ec...
- dest_match_step...
  + rewrite is_lUX_pvec_intro_lUX_pvec_comm_eq_AA in D...
  + rewrite is_lUX_pvec_intro_lUX_pvec_comm_eq_BB in D...
    rewrite H0 in H.
    rewrite lUX_twist_array_same in H...
- destruct b...
  dest_match_step...
  rewrite is_lU_pvec_intro_lC_pvec_comm_eq_AA in D...
  rewrite H0 in H...
- dest_match_step...
  dest_match_step...
  rewrite is_lC_pvec_intro_lU_pvec_comm_eq_AA in D...
  rewrite H0 in H...
- dest_match_step...
  dest_match_step...
  + rewrite is_lC_pvec_intro_lC_pvec_comm_same_th0_eq_AA in D...
  + rewrite is_lC_pvec_intro_lC_pvec_comm_same_th0_eq_BB in D...
    rewrite H0 in H...
Qed.

Lemma minsat_prim_uP_bf_eq_None_iff_forall_is_lP_bf_false {n} (f:bf(S n)) :
  minsat_prim_uP_bf f = None <-> (forall p (pv:pvec(S n)), is_lP_bf p pv f = false).
Proof with curry1.
rewrite minsat_prim_eq_None_iff_forall_is_uP_bf_free.
unfold detect_prim...
split...
specialize(H(p, svec_of_vec (vec_of_pvec pv)))...
rewrite is_lP_bf_sem_svec_svec_of_vec_vec_of_pvec in H...
Qed.

Lemma is_prim_sem_prim_with_is_prim_free {n} f :
    is_prim_free f = true -> is_term f = false -> forall p p', is_prim p' (@sem_prim n p f) = true -> p = p'.
Proof with curry1.
destruct n...
- rewrite (bf0 f) in *...
- unfold is_prim, sem_prim in H1...
  unfold PrimUGen.is_prim in H1...
  unfold detect_prim in H1...
  rewrite is_lP_bf_intro_lP_bf_comm_using_is_term_false in H1...
  + dest_match_step...
    * rewrite is_lP_pvec_intro_lP_pvec_comm_eq_AA_true in D...
      destruct p as [o p], p' as [o' p']...
      destruct p as [s k], p' as [s' k']...
      destruct o...
    * rewrite minsat_prim_uP_bf_eq_None_iff_forall_is_lP_bf_false in H...
      rewrite H in H1...
  + apply is_term_intro_uP_bf_false_using_is_term_false...
Qed.

(* describe transformations (that is non-normalized trans) *)
Definition trans : option nat -> nat -> Type :=
  PrimUGen.trans term prim.

Definition LDD : nat -> Type :=
 (PrimUGen.LDD
    term (@sem_term) (@extract_term) (@extract_term_eq_Some)
  term_of_bool extract_term_0 prim (@sem_prim) (@extract_prim)
  (@extract_prim_iff_sem_prim)
  (@pcmp_prim) (@pcmp_P_pcmp_prim)
  (@is_prim_free)
  (@is_prim_free_correct)
  (@extract_xprim)
  (@sem_xprim_extract_xprim)
  (@extract_xprim_minimal)
  (@is_term_sem_prim)
  (@same_prim_preserved)).

Definition extract : forall {n}, bf n -> LDD n.
Proof. apply @PrimUGen.extract. Defined.

Definition sem : forall {n}, LDD n -> bf n.
Proof. eapply @PrimUGen.sem. Defined.

Theorem sem_extract {n} (f:bf n) : sem(extract f) = f.
Proof. apply PrimUGen.sem_extract. Qed.

Theorem extract_sem {n} (l:LDD n) : extract(sem l) = l.
Proof. apply PrimUGen.extract_sem. Qed.

Theorem LDD_uniqueness {n} (l1 l2:LDD n) : sem l1 = sem l2 <-> l1 = l2.
Proof. apply PrimUGen.LDD_uniqueness. Qed.

Theorem LDD_existence {n} (f:bf n) : exists (l:LDD n), sem l = f.
Proof. apply PrimUGen.LDD_existence. Qed.

(* TESTS *)

Definition f1 : bf 4 := (fun (x:ba 4) =>
  let x0 := x(ltN_of_nat 0 4 (eq_refl _)) in
  let x1 := x(ltN_of_nat 1 4 (eq_refl _)) in
  let x2 := x(ltN_of_nat 2 4 (eq_refl _)) in
  let x3 := x(ltN_of_nat 3 4 (eq_refl _)) in
  xorb x0 (xorb x1 (xorb x2 x3))).

Definition ldd1 : LDD 4 := extract f1.

Definition f2 : bf 10 := (fun (x:ba 10) =>
  let x0 := x(ltN_of_nat 0 10 (eq_refl _)) in
  let x1 := x(ltN_of_nat 1 10 (eq_refl _)) in
  let x2 := x(ltN_of_nat 2 10 (eq_refl _)) in
  let x3 := x(ltN_of_nat 3 10 (eq_refl _)) in
  let x4 := x(ltN_of_nat 4 10 (eq_refl _)) in
  let x5 := x(ltN_of_nat 5 10 (eq_refl _)) in
  let x6 := x(ltN_of_nat 6 10 (eq_refl _)) in
  let x7 := x(ltN_of_nat 7 10 (eq_refl _)) in
  let x8 := x(ltN_of_nat 8 10 (eq_refl _)) in
  let x9 := x(ltN_of_nat 9 10 (eq_refl _)) in
  xorb x0 (xorb x1 (xorb x2 (xorb x3 (xorb x4
 (xorb x5 (xorb x6 (xorb x7 (xorb x8 x9))))))))).

Definition ldd2 : LDD 10 := extract f2.

Definition f3 : bf 10 := (fun (x:ba 10) =>
  let x1 := x(ltN_of_nat 1 10 (eq_refl _)) in
  let x2 := x(ltN_of_nat 2 10 (eq_refl _)) in
  let x3 := x(ltN_of_nat 3 10 (eq_refl _)) in
  let x4 := x(ltN_of_nat 4 10 (eq_refl _)) in
  (negb x1) && (xorb x3 (x2 && x4))).

Definition ldd3 : LDD 10 := extract f3.

Definition f4 : bf 10 := (fun (x:ba 10) =>
  let x0 := x(ltN_of_nat 0 10 (eq_refl _)) in
  let x1 := x(ltN_of_nat 1 10 (eq_refl _)) in
  let x2 := x(ltN_of_nat 2 10 (eq_refl _)) in
  let x3 := x(ltN_of_nat 3 10 (eq_refl _)) in
  let x4 := x(ltN_of_nat 4 10 (eq_refl _)) in
  let x5 := x(ltN_of_nat 5 10 (eq_refl _)) in
  let x6 := x(ltN_of_nat 6 10 (eq_refl _)) in
  let x7 := x(ltN_of_nat 7 10 (eq_refl _)) in
  let x8 := x(ltN_of_nat 8 10 (eq_refl _)) in
  let x9 := x(ltN_of_nat 9 10 (eq_refl _)) in
  (xorb x0 (xorb x3 x6)) && (xorb x1 (xorb x4 x7)) &&
  (xorb x2 (xorb x5 x8)) && (negb x9)).

Definition ldd4 : LDD 10 := extract f4.

Extraction "ldd_l_ucx_tests" extract ldd1 ldd2 ldd3 ldd4.

(* When Evaluated in OCaml using OCaml's toplevel.
val ldd1 : ((bool, bool list) prod, (oprim, bool list) prod) trans ldd =
  Leaf (S (S (S (S O))),
   ETerm (S (S (S (S O))),
    Pair (False, Cons (True, Cons (True, Cons (True, Cons (True, Nil)))))))

val ldd2 : ((bool, bool list) prod, (oprim, bool list) prod) trans ldd =
  Leaf (S (S (S (S (S (S (S (S (S (S O))))))))),
   ETerm (S (S (S (S (S (S (S (S (S (S O))))))))),
    Pair (False,
     Cons (True,
      Cons (True,
       Cons (True,
        Cons (True,
         Cons (True,
          Cons (True,
           Cons (True, Cons (True, Cons (True, Cons (True, Nil)))))))))))))
val f3 : (nat -> bool) -> bool = <fun>
val ldd3 : ((bool, bool list) prod, (oprim, bool list) prod) trans ldd =
  Leaf (S (S (S (S (S (S (S (S (S (S O))))))))),
   EPrim (None, S (S (S (S (S (S (S (S (S O)))))))),
    Pair (OU,
     Cons (False,
      Cons (False,
       Cons (False,
        Cons (False,
         Cons (False,
          Cons (False,
           Cons (False, Cons (False, Cons (False, Cons (True, Nil))))))))))),
    EPrim (None, S (S (S (S (S (S (S (S O))))))),
     Pair (OU,
      Cons (False,
       Cons (False,
        Cons (False,
         Cons (False,
          Cons (False,
           Cons (False, Cons (False, Cons (False, Cons (True, Nil)))))))))),
     EPrim (None, S (S (S (S (S (S (S O)))))),
      Pair (OU,
       Cons (False,
        Cons (False,
         Cons (False,
          Cons (False,
           Cons (False, Cons (False, Cons (False, Cons (True, Nil))))))))),
      EPrim (None, S (S (S (S (S (S O))))),
       Pair (OU,
        Cons (False,
         Cons (False,
          Cons (False,
           Cons (False, Cons (False, Cons (False, Cons (True, Nil)))))))),
       EPrim (None, S (S (S (S (S O)))),
        Pair (OU,
         Cons (False,
          Cons (False,
           Cons (False, Cons (False, Cons (False, Cons (True, Nil))))))),
        EPrim (None, S (S (S (S O))),
         Pair (OU,
          Cons (True,
           Cons (False, Cons (False, Cons (False, Cons (False, Nil)))))),
         EPrim (None, S (S (S O)),
          Pair (OC (True, False),
           Cons (True, Cons (False, Cons (False, Cons (False, Nil))))),
          EPrim (None, S (S O),
           Pair (OX, Cons (False, Cons (True, Cons (False, Nil)))),
           EPrim (None, S O,
            Pair (OC (False, False), Cons (False, Cons (True, Nil))),
            ETerm (S O, Pair (False, Cons (True, Nil)))))))))))))

 *)
