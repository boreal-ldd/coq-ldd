#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

FileRName = sys.argv[1]

FileR = open(FileRName)
TXT = FileR.read().split('\n')
FileR.close()

DIC = dict()

def begin_with(txt, seq) :
	return txt[:len(seq)] == seq

def deal_with_cmd(txt) :
        txt = txt.replace('\xa7', '')
	L = [v for v in txt.split(' ') if v != '']
	if L[0] == "SET" :
		DIC[L[1]] = L[2]

for line in TXT :
	if begin_with(line, "§§ ") :
		deal_with_cmd(line[3:])
	elif begin_with(line, "real ") :
		DIC['time-real'] = float(line.split(' ')[1])
	elif begin_with(line, "user ") :
		DIC['time-user'] = float(line.split(' ')[1])
	elif begin_with(line, "sys ") :
		DIC['time-sys'] = float(line.split(' ')[1])

print(DIC)
