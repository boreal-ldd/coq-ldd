# SimplStar

The SimplStar library is mostly designed as a stack of trivialities,
extended with appropriate reduction rules using both "Hint Rewrite" and
Ltac facilities.
The core concept is to keep trivial proof trivial, however it happens that
Coq does not know of what trivial means to us, thus this library.
Any addition to the set of reduction rule should be done while keeping in mind
that no transformation that could deteriorate the proof state should be introduced.
Optionnaly additionnal tactics can be designed to allow the user to make use of
such particular case.
An example of such additional tactic is the `for_lia1` strategy introduced in `SimplNat`,
which allows to systematically rewrite terms into `lia` parsable logic (Presburger Arithmetic),
in particular `max` and `min` operators.
Each file tends to deal with one particular aspect of Coq basic library.

## Section 1. SimplSimpl

### SimplProp
### SimplBool : SimplProp
### BasicBool : SimplBool
### SimplEq : SimplBool
### SimplCmp : SimplBool
### SimplSimpl : SimplEq SimplCmp

## Section 2. SimplMore

### SimplAB : SimplSimpl
### SimplNat : SimplSimpl
### SimplList : SimplSimpl SimplNat
### SimplUCList : SimplList
	This one is current work in progress [WIP] and is not
	recquired to compile by any subsequent files

### SimplABList : SimplAB SimplList
	Details methods on [list (@AB A B)] especially used for
	composing positional vectors.

### SimplMore : SimplABList SimplUCList SimplNat

## Section 3. MiniBool / MXCore

### MiniBool : SimplMore
	Introduces a simple yet effective way of normalizing Boolean
	expression using Shannon expansion.
	Includes a proof of canonicity of the proposed normalized
	form.

### MXCore : BasicBool SimplSimpl
	Introduces a binary-decision-based and type-dependent-free
	formalization of Boolean Functions.
	[TODO] add conversion facilities between MXCore and MiniBool

## Section 4. LDD-Ordered

## Section 5. LDD-Uniform 

### MX_U1_Sec1_detect : MiniBool MXCore
### MX_U1_Sec2_intro [PF] (previous file)
### MX_U1_Sec3_detect_intro [PF]
### MX_U1_Sec4_extract_Sub0_defs [PF]
### MX_U1_Sec4_extract_Sub0_UCX [PF]
### MX_U1_Sec4_extract_Sub1_hCX [PF]
### MX_U1_Sec4_extract_Sub2_UhCX [PF]
### MX_U1_Sec5_canonicity [PF]

## Section 6. LDD-Linear


## Section 7. Disjoint Support Decomposition

DsdSU

ABlist
AlgSUA
AlgSU
AlgSUX
BasicMaths
Booleq
BoolFunc
BoolTransPrim
BTree
MiniBool
ModNCore
ModNOps
ModNUACore
ModNUCore
ModNU
ModN
MX_A
MXCore_U1
MXCore
MX_C
MX_Ext_L1
MXlCore
MXNCore
reifyEx1
reifyEx2
SizedList
SOlist
TRG
