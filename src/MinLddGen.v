Require Import Arith .
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.

Require Import Ground.SimplProp.
Require Import Ground.SimplBool.
Require Import Ground.SimplEq.
Require Import Ground.SimplCmp.
Require Import Ground.SimplSimpl.

Require Import Ground.SimplNat.
Require Import Ground.SimplAB.
Require Import Ground.SimplList.
Require Import Ground.SimplABList.
Require Import Ground.SimplMore.

Require Import MiniBool_Logic.

Require Import Ground.SimplDTT.
Require Import Ground.SimplLtN.
Require Import Ground.SimplArray.

Require Import FiniteCurryfy.
Require Import FiniteBooleanFunction.

Require Import LddO_Primitives.

Require Import FunInd.
Require Import Recdef.

Section MinLdd1.

  (* Section. Defining a Syntax for Lambda Decision Diagram (LDD) *)

  Variable edge : nat -> nat -> Type.
  Variable leaf : nat -> Type.

  Variable edge_le : forall {n m}, edge n m -> n <= m.

  Inductive ldd : nat -> Type :=
  | Leaf {n} (lf:leaf n) : ldd n
  | Edge {n} (nx:ldd n) {m} (ee:edge n m) : ldd m
  | LSha {n} (f0 f1:ldd n) : ldd (S n).

  (* Section. Extracting an LDD from a Boolean Function *)

  (* SubSection. Dealing with Leaves *)
  Variable extract_leaf : forall {n}, bf n -> option(leaf n).
  (* [NOTE] to simplify the proof of termination of the extraction method,
            we enforce that all functions of arity 0 can be mapped to leaves
   *)
  Variable leaf_of_bool : bf 0 -> leaf 0.
  (* [NOTE] furthermore, we ensure that this new function is consistent with
            the first one *)
  Variable extract_leaf_bool :
    forall (f:bf 0), extract_leaf f = Some(leaf_of_bool f).

  (* SubSection. Extracting Edges *)
  (* [NOTE] ideally we would have wanted to write something like
            extract_edge {m} (f:bf m) : { (n, ee, ff) : nat * edge n m * bf n } .
            However, as far as we know it cannot be written in Coq.
            Instead we define a single-constructor type termed extraction-edges,
            [xedge n], which does the trick.
   *)
  Inductive xedge (m:nat) : Type :=
    XEdge (n:nat) (ee:edge n m) (ff:bf n).

  Variable extract_edge : forall {n}, bf n -> xedge n.

  (* [NOTE] to simplify the manipulation of [xedge] we add projection operators *)
  Definition xedge_projN {m} (xe:xedge m) : nat :=
    match xe with
    | XEdge _ n _ _ => n
    end.

  Definition xedge_projE {m} (xe:xedge m) : edge (xedge_projN xe) m :=
    match xe with
    | XEdge _ _ ee _ => ee
    end.

  Definition xedge_projF {m} (xe:xedge m) : bf (xedge_projN xe) :=
    match xe with
    | XEdge _ _ _ ff => ff
    end.

  (* SubSection. Recursive Extraction using Well-Founded Induction *)

(* [INITIAL DEFINITION]
  Definition extract_ldd : forall {n}, bf n -> ldd n.
  Proof.
  apply(Fix lt_wf (fun n => bf n -> ldd n)).
  intros n extract f.
  destruct(extract_edge f) as [m ee xf].
  destruct m.
  - apply (Edge (Leaf (leaf_of_bool xf)) ee).
  - destruct(extract_leaf xf) as [lf|].
    + apply (Edge (Leaf lf) ee).
    + pose (f0 := fnary_evalo1 xf false).
      pose (f1 := fnary_evalo1 xf true ).
      refine(Edge (LSha (extract _ _ f0) (extract _ _ f1)) ee).
      * apply (edge_le ee).
      * apply (edge_le ee).
  Defined.
 *)

  Definition extract_ldd : forall {n}, bf n -> ldd n :=
    Fix lt_wf (fun n : nat => bf n -> ldd n)
    (fun (n : nat) (extract : forall y : nat, y < n -> bf y -> ldd y) (f : bf n) =>
     match extract_edge f with
     | XEdge _ m ee xf =>
       match m as n0 return (edge n0 n -> bf n0 -> ldd n) with
       | 0 => fun (ee0 : edge 0 n) (xf0 : bf 0) =>
         Edge (Leaf (leaf_of_bool xf0)) ee0
       | S m0 => fun (ee0 : edge (S m0) n) (xf0 : bf (S m0)) =>
         match extract_leaf xf0 with
         | Some lf => Edge (Leaf lf) ee0
         | None =>
             let f0 := fnary_evalo1 xf0 false in
             let f1 := fnary_evalo1 xf0 true in
             let f01 := LSha (extract m0 (edge_le ee0) f0)
                             (extract m0 (edge_le ee0) f1) in
             Edge f01 ee0
         end
       end ee xf
     end).

  Lemma extract_ldd_eq n (f:bf n) :
    extract_ldd f =
      match extract_edge f with
      | XEdge _ m ee xf =>
        match m as n0 return (edge n0 n -> bf n0 -> ldd n) with
        | 0 =>
          fun (ee0 : edge 0 n) (xf0 : bf 0) =>
            Edge (Leaf (leaf_of_bool xf0)) ee0
        | S m0 =>
          fun (ee0 : edge (S m0) n) (xf0 : bf (S m0)) =>
          match extract_leaf xf0 with
          | Some lf => Edge (Leaf lf) ee0
          | None =>
            let f0 := fnary_evalo1 xf0 false in
            let f1 := fnary_evalo1 xf0 true  in
            let f01 := LSha (extract_ldd f0) (extract_ldd f1) in
            Edge f01 ee0
          end
        end ee xf
     end.
  Proof with simpldtt.
  unfold extract_ldd.
  rewrite Fix_eq; fold (@extract_ldd)...
  clear f. clear n.
  apply functional_extensionality...
  destruct(extract_edge x0)eqn:E...
  dest_match_step...
  dest_match_step...
  rewrite H...
  Qed.

  Lemma extract_ldd_ind (P:forall n, bf n -> ldd n -> Prop)
    (P0:forall n (f:bf n),
        forall ee xf, extract_edge f = XEdge _ 0 ee xf ->
        let res := Edge (Leaf (leaf_of_bool xf)) ee in
        P n f res)
    (PSSome :
        forall n (f:bf n),
        forall m ee xf, extract_edge f = XEdge _ (S m) ee xf ->
        forall lf, extract_leaf xf = Some lf ->
        let res := Edge (Leaf lf) ee in
        P n f res)
    (PSNone :
        forall n (f:bf n),
        forall m ee xf, extract_edge f = XEdge _ (S m) ee xf ->
        extract_leaf xf = None ->
        let f0 := fnary_evalo1 xf false in
        let f1 := fnary_evalo1 xf true  in
        let res := Edge (LSha (extract_ldd f0) (extract_ldd f1)) ee in
        P _ f0 (extract_ldd f0) ->
        P _ f1 (extract_ldd f1) ->
        P n f res) : forall n f, P n f (extract_ldd f).
  Proof with simpldtt.
  intro n.
  induction (lt_wf n)...
  specialize(P0 x f).
  specialize(PSSome x f).
  specialize(PSNone x f).
  rewrite extract_ldd_eq...
  dest_match_step...
  dest_match_step...
  dest_match_step...
  clear P0. clear PSNone.
  specialize(PSSome _ _ _ (eq_refl _))...
  Qed.

  Fixpoint minleaf_ldd {n} (f:ldd n) (edge:bool) {struct f} : bool :=
  match edge with
  | true  =>
    match f with
    | Edge nx _ => minleaf_ldd nx false
    | _ => false
    end
  | false =>
    match f with
    | Leaf _ => true
    | LSha f0 f1 => minleaf_ldd f0 true && minleaf_ldd f1 true
    | Edge _ _ => false
    end
  end.

  Lemma minleaf_extract_ldd n (f:bf n) :
    minleaf_ldd (extract_ldd f) true = true.
  Proof with simpldtt.
  apply (extract_ldd_ind (fun n f l => minleaf_ldd l true = true))...
  Qed.

  (* Section. Semantic *)

  Variable sem_leaf : forall {n}, leaf n -> bf n.

  Variable sem_extract_leaf : forall {n} {f:bf n} {lf:leaf n},
    extract_leaf f = Some lf -> sem_leaf lf = f.

  Variable sem_edge : forall {n m}, edge n m -> bf n -> bf m.

  Definition sem_xedge {n} (xe:xedge n) : bf n :=
    sem_edge (xedge_projE xe) (xedge_projF xe).

  Variable sem_extract_edge : forall {n} (f:bf n),
    sem_xedge(extract_edge f) = f.

  Function sem_ldd {n} (l:ldd n) : bf n :=
  match l with
  | Leaf lf => sem_leaf lf
  | Edge nx ee => sem_edge ee (sem_ldd nx)
  | LSha f0 f1 => sha_bf(sem_ldd f0)(sem_ldd f1)
  end.

  Lemma sem_leaf_leaf_of_bool (f:bf 0) : sem_leaf (leaf_of_bool f) = f.
  Proof with curry1.
  destruct(extract_leaf f)eqn:E...
  Qed.

  Theorem sem_ldd_extract_ldd {n} (f:bf n) :
    sem_ldd(extract_ldd f) = f.
  Proof with curry1.
  apply (extract_ldd_ind (fun n f l => sem_ldd l = f));
    intros n0 f0;
    specialize(sem_extract_edge f0) as SE; curry1;
    rewrite H in SE...
  - rewrite sem_leaf_leaf_of_bool...
  - rewrite (sem_extract_leaf H0)...
  - rewrite H1, H2...
  Qed.
  Hint Rewrite @sem_ldd_extract_ldd : curry1_rewrite.

  (* [warning] leaves must be reduced (cf. reduced_bf_sem_leaf) *)
  Definition reduced_bf {n} (f:bf n) : bool :=
    n =? xedge_projN(extract_edge f).

  Definition reduced_xedge {n} (xe:xedge n) : bool :=
    reduced_bf(xedge_projF xe).

  Variable id_edge : forall n, edge n n.

  Variable sem_edge_id_edge : forall n, sem_edge(id_edge n) = id.

  Variable extract_edge_single_fixpoint : forall {n} (f:bf n) ee xf,
    extract_edge f = XEdge n n ee xf <-> xf = f /\ ee = id_edge _ .

  Variable reduced_extract_edge : forall {n} (f:bf n),
    reduced_xedge (extract_edge f) = true.

  Variable extract_edge_sem_edge_with_reduced_xedge :
    forall {n m} (ee:edge n m) (ff:bf n),
    reduced_bf ff = true -> extract_edge(sem_edge ee ff) = XEdge _ _ ee ff.

  Variable reduced_bf_sem_leaf : forall {n} (lf:leaf n),
    reduced_bf(sem_leaf lf) = true.

  Variable extract_leaf_sem_leaf : forall {n} (lf:leaf n),
    extract_leaf(sem_leaf lf) = Some lf.

  Lemma leaf_canonical {n} (lf1 lf2:leaf n) :
    sem_leaf lf1 = sem_leaf lf2 <-> lf1 = lf2.
  Proof with curry1.
  split...
  specialize(apply_f extract_leaf H)...
  repeat rewrite extract_leaf_sem_leaf in H0...
  Qed.

  Lemma extract_edge_sem_leaf {n} (lf:leaf n) :
    extract_edge (sem_leaf lf) = XEdge n n (id_edge n) (sem_leaf lf).
  Proof with curry1.
  specialize(reduced_bf_sem_leaf lf) as H.
  rewrite extract_edge_single_fixpoint...
  Qed.
  Hint Rewrite @extract_edge_sem_leaf : curry1_rewrite.

  Definition is_leaf_ldd {n} (l:ldd n) : bool :=
    match l with
    | Leaf _ => true
    | _ => false
    end.

  Definition is_leaf_bf {n} (f:bf n) : bool :=
    isSome(extract_leaf f).

  Function reduced_ldd {n} (l:ldd n) : bool :=
    match l with
    | Leaf _ => true
    | Edge ff ee =>
      reduced_bf(sem_ldd ff) &&
      reduced_ldd ff
    | LSha f0 f1 =>
      negb(is_leaf_bf(sem_ldd (LSha f0 f1))) &&
      reduced_ldd f0 &&
      reduced_ldd f1
    end.

  Theorem reduced_ldd_extract_ldd {n} (f:bf n) :
    reduced_ldd (extract_ldd f) = true.
  Proof with curry1.
  apply (extract_ldd_ind (fun n f l => reduced_ldd l = true))...
  specialize(reduced_extract_edge f0) as HH...
  unfold reduced_xedge in HH...
  rewrite H in HH...
  unfold is_leaf_bf...
  Qed.

  Lemma leaf_of_bool_sem_leaf lf : leaf_of_bool (sem_leaf lf) = lf.
  Proof with curry1.
  specialize(extract_leaf_bool(sem_leaf lf)) as HH.
  rewrite extract_leaf_sem_leaf in HH...
  Qed.
  Hint Rewrite leaf_of_bool_sem_leaf : curry1_rewrite.

  Definition is_edge {n} (f:ldd n) : bool :=
  match f with
  | Edge _ _ => true
  | _ => false
  end.

  Lemma extract_ldd_sem_ldd_eq_same_ind {n} (l:ldd n) :
    reduced_ldd l = true ->
    minleaf_ldd l (is_edge l) = true ->
    match l with
    | Leaf lf => extract_leaf(sem_leaf lf) = Some lf
    | LSha f0 f1 =>
      extract_ldd(sem_ldd f0) = f0 /\
      extract_ldd(sem_ldd f1) = f1
    | Edge ff ee =>
      extract_ldd(sem_edge ee (sem_ldd ff)) = Edge ff ee
    end.
  Proof with curry1.
  induction l...
  - rewrite extract_ldd_eq.
    rewrite extract_edge_sem_edge_with_reduced_xedge...
    destruct l...
    + destruct n...
    + unfold is_leaf_bf in H1...
      rewrite_subst...
  - destruct l1...
    destruct l2...
  Qed.

  Theorem extract_ldd_sem_ldd_eq_same {n} (l:ldd n) :
    minleaf_ldd l true = true ->
    reduced_ldd l = true ->
    extract_ldd(sem_ldd l) = l.
  Proof with curry1.
  specialize(extract_ldd_sem_ldd_eq_same_ind l)...
  destruct l...
  Qed.

  Theorem ldd_canonical {n} (l1 l2:ldd n) :
    minleaf_ldd l1 true = true ->
    reduced_ldd l1 = true ->
    minleaf_ldd l2 true = true ->
    reduced_ldd l2 = true ->
    sem_ldd l1 = sem_ldd l2 <-> l1 = l2.
  Proof with curry1.
  split...
  specialize(apply_f extract_ldd H3) as HH.
  rewrite extract_ldd_sem_ldd_eq_same in HH...
  rewrite extract_ldd_sem_ldd_eq_same...
  Qed.

  (* Section. Packaging Using SubType Notation *)

  Definition norm {n} (l:ldd n) :=
    reduced_ldd l && minleaf_ldd l true.

  Definition LDD (n:nat) : Type :=
    { l : ldd n | norm l = true }.

  Lemma norm_extract_ldd {n} (f:bf n) : norm(extract_ldd f) = true.
  Proof with curry1.
  unfold norm...
  rewrite reduced_ldd_extract_ldd...
  rewrite minleaf_extract_ldd...
  Qed.

  Definition extract {n} (f:bf n) : LDD n :=
    (exist _ (extract_ldd f) (norm_extract_ldd f)).

  Definition sem {n} (l:LDD n) : bf n :=
    sem_ldd(proj1_sig l).

  Definition LDD_PI {n} (l1 l2:LDD n) : l1 = l2 <-> proj1_sig l1 = proj1_sig l2.
  Proof with curry1.
  split...
  destruct l1, l2...
  Qed.

  Theorem sem_extract {n} (f:bf n) :sem(extract f) = f.
  Proof. unfold sem, extract; curry1. Qed.
  Hint Rewrite @sem_extract : curry1_rewrite.

  Theorem extract_sem {n} (l:LDD n) : extract(sem l) = l.
  Proof with curry1.
  rewrite LDD_PI...
  destruct l as [l p].
  unfold sem...
  unfold norm in p...
  rewrite extract_ldd_sem_ldd_eq_same...
  Qed.
  Hint Rewrite @extract_sem : curry1_rewrite.

  Theorem LDD_uniqueness {n} (l1 l2:LDD n) : sem l1 = sem l2 <-> l1 = l2.
  Proof with curry1.
  split...
  specialize(apply_f extract H)...
  Qed.

  Theorem LDD_existence {n} (f:bf n) : exists (l:LDD n), sem l = f.
  Proof. exists (extract f); curry1. Qed.

End MinLdd1.

  Extraction extract_ldd.

(*
val extract_ldd : nat -> bf -> ldd

let rec extract_ldd x f =
  let XEdge (m, ee, xf) = extract_edge x f in
  match m with
   | O -> Edge (O, (Leaf (O, (leaf_of_bool xf))), x, ee)
   | S m0 ->
     match extract_leaf (S m0) xf with
     | Some lf -> Edge (S m0, Leaf (S m0, lf), x, ee)
     | None ->
       let f0 = fnary_evalo1 m0 xf False in
       let f1 = fnary_evalo1 m0 xf True  in
       let f01 = LSha (m0, extract_ldd m0 f0, extract_ldd m0 f1) in
       Edge (S m0, f01, x, ee)
 *)
