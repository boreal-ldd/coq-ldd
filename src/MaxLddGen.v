Require Import Arith .
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.

Require Import Ground.SimplProp.
Require Import Ground.SimplBool.
Require Import Ground.SimplOption.
Require Import Ground.SimplEq.
Require Import Ground.SimplFun.
Require Import Ground.SimplBoolFun.
Require Import Ground.SimplCmp.
Require Import Ground.SimplSimpl.

Require Import Ground.SimplNat.
Require Import Ground.SimplAB.

Require Import Ground.SimplList_Chap0_Standard.
Require Import Ground.SimplList_Chap1_Construct.
Require Import Ground.SimplList_Chap2_Predicate.
Require Import Ground.SimplList_Chap3_PredicateOnConstruct.
Require Import Ground.SimplList.
Require Import Ground.SimplABList.
Require Import Ground.SimplMore.
Require Import Ground.SimplDTT.
Require Import Ground.SimplLtN_Chap0_Construct.
Require Import Ground.SimplLtN_Chap1_Array_as_Function.
Require Import Ground.SimplLtN_Chap1_Sec1_Boolean_Array.
Require Import Ground.SimplLtNIndexedArray.

Require Import Ground.SimplDTT.
Require Import Ground.SimplLtN.
Require Import Ground.SimplEnum.

Require Import Ground.MiniBool_VAX.
Require Import Ground.MiniBool_Logic.

Require Import FiniteCurryfy.
Require Import FiniteBooleanFunction.

Section MaxLdd1.

  (* Section. Defining a Syntax for Lambda Decision Diagram (LDD) *)

  (* we unify edges and leaves in a single type *)
  Variable edge : option nat -> nat -> Type.

  Variable edge_le : forall {n m}, edge (Some n) m -> n <= m.

(* [ALT?]
  Inductive ldd : nat -> Type :=
  | Edge {opn m} (ee:edge opn m) (nd:node opn) : ldd m
  with      node : option nat -> Type :=
  | Term : node None
  | Next {n} (f0 f1:ldd n) : node (Some(S n)).
 *)
(* [ALT=False]
  Inductive TFalse : Type := .

  Inductive ldd : nat -> Type :=
  | Edge {opn m} (ee:edge opn m) (nd:match opn with None => (unit:Type) | Some 0 => (TFalse:Type) | Some(S n) => (((ldd n)*(ldd n)):Type) end) : ldd m.
 *)

  Inductive ldd : nat -> Type :=
  | Leaf {n  } (lf:edge None n) : ldd n
  | Edge {n m} (ee:edge (Some(S n)) m) (f0 f1:ldd n) : ldd m.

  Lemma Leaf_eq_Leaf n lf1 lf2 : @Leaf n lf1 = @Leaf n lf2 <-> lf1 = lf2.
  Proof. split; solve_by_invert. Qed.
  Hint Rewrite Leaf_eq_Leaf : curry1_rewrite.

  Lemma Edge_eq_Edge m n0 ee0 f0 g0 n1 ee1 f1 g1 :
    @Edge n0 m ee0 f0 g0 = @Edge n1 m ee1 f1 g1 <->
      n0 = n1 /\ ee0 =J ee1 /\ f0 =J f1 /\ g0 =J g1.
  Proof. split; solve_by_invert. Qed.
  Hint Rewrite Edge_eq_Edge : curry1_rewrite.

  Lemma Leaf_eq_Edge opn m lf ee f0 g0 : @Leaf m lf = @Edge opn m ee f0 g0 <-> False.
  Proof. solve_by_invert. Qed.
  Hint Rewrite Leaf_eq_Edge : curry1_rewrite.

  Lemma Edge_eq_Leaf opn m lf ee f0 g0 : @Edge opn m ee f0 g0 = @Leaf m lf <-> False.
  Proof. solve_by_invert. Qed.
  Hint Rewrite Edge_eq_Leaf : curry1_rewrite.

  Inductive node {F:nat -> Type} : nat -> Type :=
  | Node {n} (f0 f1:F n) : node (S n).

  Inductive next {F:nat -> Type} : (option nat) -> Type :=
  | Term : next None
  | Next {n:nat} (f:F n) : next (Some n).

  Definition ldd_projN {n} (l:ldd n) : option nat :=
  match l with
  | Leaf _ => None
  | @Edge n _ _ _ _ => Some(S n)
  end.

  Definition ldd_projE {n} (l:ldd n) : edge (ldd_projN l) n :=
  match l in ldd n0 return edge (ldd_projN l) n0 with
  | Leaf lf => lf
  | Edge ee _ _ => ee
  end.

  Definition ldd_projF {n} (l:ldd n) : @next node (ldd_projN l) :=
  match l in ldd n0 return @next node (ldd_projN l) with
  | Leaf _ => Term
  | Edge _ f0 f1 => Next(Node f0 f1)
  end.

  (* Section. Extracting an LDD from a Boolean Function *)

  (* SubSection. Extracting Edges *)
  (* [NOTE] ideally we would have wanted to write something like
            extract_edge {m} (f:bf m) : { (n, ee, ff) : nat * edge n m * bf n } .
            However, as far as we know it cannot be written in Coq.
            Instead we define a single-constructor type termed extraction-edges,
            [xedge n], which does the trick.
   *)
  (* [NOTE] furthermore, to enforce that constants of arity 0 are necessarily
            leaves, we enforce that the returned result is node of BF, this way
            [extract_edge] cannot a function of arity 0 as by-product.
   *)
  (* [NOTE] contrary to MinLddGen, we do not need several extraction procedures,
            howevert at the risk of diving deeper in DTT entanglement *)
  Inductive xedge {F:nat -> Type} : nat -> Type :=
  | XEdge {opn:option nat} {m:nat} (ee:edge opn m) (nx:@next (@node F) opn) : xedge m.

  Lemma XEdge_eq_XEdge F n opm0 ee0 nx0 opm1 ee1 nx1 :
    @XEdge F opm0 n ee0 nx0 = @XEdge F opm1 n ee1 nx1 <->
      opm0 = opm1 /\ ee0 =J ee1 /\ nx0 =J nx1.
  Proof with curry1.
  split...
  inversion H...
  Qed.
  Hint Rewrite XEdge_eq_XEdge : curry1_rewrite.

  Variable extract_edge : forall {n}, bf n -> @xedge bf n.

  Definition xedge_projN {F n} (xe:@xedge F n) : option nat :=
  match xe with
  | @XEdge _ opn _ _ _ => opn
  end.

  Definition xedge_projE {F n} (xe:@xedge F n) : edge (xedge_projN xe) n :=
  match xe with
  | @XEdge _ _ _ ee _ => ee
  end.

  Definition xedge_projF {F n} (xe:@xedge F n) : next (xedge_projN xe) :=
  match xe with
  | @XEdge _ _ _ _ nx => nx
  end.

  (* SubSection. Recursive Extraction using Well-Founded Induction *)

  Definition extract_ldd {n} (f:bf n) : ldd n :=
  Fix lt_wf (fun n => bf n -> ldd n)
  (fun (n : nat) (extract : forall y, y < n -> bf y -> ldd y) (f : bf n) =>
   match extract_edge f in (xedge m)
     return ((forall y, y < m -> bf y -> ldd y) -> ldd m)
   with
   | @XEdge _ opn m0 ee nx => (
       fun (extract0 : forall y : nat, y < m0 -> bf y -> ldd y) =>
       match nx in (next o) return (edge o m0 -> ldd m0) with
       | Term => fun xf0 : edge None m0 => Leaf xf0
       | @Next _ n' f' =>
           fun ee0 : edge (Some n') m0 =>
           match f' in (node n1) return (edge (Some n1) m0 -> ldd m0) with
           | @Node _ n'' f'0 f'1 =>
               fun ee1 : edge (Some (S n'')) m0 =>
               let ldd0 := extract0 n'' (edge_le ee1) f'0 in
               let ldd1 := extract0 n'' (edge_le ee1) f'1 in
               Edge ee1 ldd0 ldd1
           end ee0
       end ee
    )
   end extract) n f .

  Lemma extract_ldd_eq n (f:bf n) :
    extract_ldd f =
      match extract_edge f in (xedge n0) return (ldd n0) with
      | @XEdge _ opn m0 ee nx =>
        match nx in (next opn) return (edge opn m0 -> ldd m0) with
        | Term => fun ee : edge None m0 => Leaf ee
        | @Next _ n0 f1 =>
          fun ee : edge (Some n0) m0 =>
          match f1 in (node n1) return (edge (Some n1) m0 -> ldd m0) with
          | @Node _ n1 f2 f3 =>
            fun xf1 : edge (Some (S n1)) m0 =>
            Edge xf1 (extract_ldd f2) (extract_ldd f3)
          end ee
        end ee
      end.
  Proof with simpldtt.
  unfold extract_ldd.
  rewrite Fix_eq; fold (@extract_ldd)...
  - dest_match_step...
  - apply functional_extensionality...
    dest_match_step...
    dest_match_step...
    dest_match_step...
    rewrite H...
  Qed.

  Lemma extract_ldd_ind (P:forall n, bf n -> ldd n -> Prop)
    (PTerm:forall m (f:bf m),
           forall ee (pX:extract_edge f = XEdge ee Term),
           P m f (Leaf ee))
    (PNext:forall m (f:bf m),
           forall n ee f0 f1 (pX:extract_edge  f = XEdge ee (Next(Node f0 f1))),
           let l0 := extract_ldd f0 in
           let l1 := extract_ldd f1 in
           forall (IH0:P n f0 l0) (IH1:P n f1 l1),
           P m f (Edge ee l0 l1)) : forall n f, P n f (extract_ldd f).
  Proof with simpldtt.
  intro n.
  induction (lt_wf n)...
  rewrite extract_ldd_eq...
  dest_match_step...
  dest_match_step...
  dest_match_step...
  Qed.

  (* Section. Semantic o Extract *)

  Variable sem_edge : forall {opn m}, edge opn m -> @next bf opn -> bf m.

  Definition sem_term {m} (ee:edge None m) : bf m :=
    sem_edge ee Term.

  Definition sem_next {n m} (ee:edge(Some n) m) (f:bf n) : bf m :=
    sem_edge ee (Next f).

  Definition sem_next_node {opn} (nx:@next (@node bf) opn) : @next bf opn :=
  match nx in (next o) return (next o) with
  | Term => Term
  | @Next _ n f =>
    (fun f0 : node n =>
      match f0 in (node n0) return next (Some n0) with
      | @Node _ n0 f1 f2 => Next (sha_bf f1 f2)
      end) f
  end.

  Definition sem_xedge {m} (xe:@xedge bf m) : bf m :=
    sem_edge (xedge_projE xe) (sem_next_node (xedge_projF xe)).

  Variable sem_xedge_extract_edge : forall {m} (f:bf m), sem_xedge(extract_edge f) = f.

  Lemma sem_term_extract_edge : forall {m} (f:bf m) ee,
    extract_edge f = XEdge ee Term -> sem_term ee = f.
  Proof with curry1.
  intros...
  specialize(sem_xedge_extract_edge f) as HH...
  rewrite_subst...
  Qed.

  Lemma sem_next_extract_edge : forall {m} (f:bf m) n ee (f0 f1:bf n),
    extract_edge f = XEdge ee (Next(Node f0 f1)) ->
      sem_next ee (sha_bf f0 f1) = f.
  Proof with curry1.
  intros...
  specialize(sem_xedge_extract_edge f) as HH...
  rewrite_subst...
  Qed.

  Function sem_ldd {n} (l:ldd n) : bf n :=
  match l with
  | Leaf lf => sem_term lf
  | Edge ee f0 f1 => sem_next ee (sha_bf(sem_ldd f0)(sem_ldd f1))
  end.

  Theorem sem_ldd_extract_ldd {n} (f:bf n) :
    sem_ldd(extract_ldd f) = f.
  Proof with curry1.
  apply (extract_ldd_ind (fun n f l => sem_ldd l = f))...
  - apply sem_term_extract_edge...
  - rewrite IH0, IH1...
    apply sem_next_extract_edge...
  Qed.
  Hint Rewrite @sem_ldd_extract_ldd : curry1_rewrite.

  (* Section. Extract o Semantic *)

  Definition reduced_bf {n} (f:bf n) : bool :=
    match extract_edge f with
    | XEdge _ nx =>
      match nx with
      | @Next _ n' _ => n =? n'
      | _ => false
      end
    end.
(* [NO USE]
  Variable id_edge : forall n, edge (Some(S n)) (S n).
  Variable sem_id_edge : forall n, sem_next (id_edge n) = id.
 *)

  Definition reduced_next {opn} (nx:@next bf opn) : bool :=
    match nx with
    | Term => true
    | Next f => reduced_bf f
    end.

  Variable reduced_next_projF_extract_edge : forall {n} f,
    reduced_next(sem_next_node(xedge_projF(@extract_edge n f))) = true.

  Lemma reduced_bf_extract_edge {n f m ee f0 f1} :
    extract_edge f = @XEdge _ (Some(S m)) n ee (Next(Node f0 f1)) ->
      reduced_bf(sha_bf f0 f1) = true.
  Proof with curry1.
  specialize(reduced_next_projF_extract_edge f) as HH...
  rewrite_subst...
  Qed.

  Variable extract_edge_with_reduced_next : forall {opn m} (ee:edge opn m) (nx:@next (@node bf) opn),
    reduced_next(sem_next_node nx) = true -> extract_edge(sem_edge ee (sem_next_node nx)) = XEdge ee nx.

  Lemma extract_edge_sem_term {n} (lf:edge None n) :
    extract_edge(sem_term lf) = @XEdge _ None n lf Term.
  Proof with curry1.
  specialize(extract_edge_with_reduced_next lf Term) as HH...
  Qed.

  Lemma extract_edge_sem_next {n m} (ee:edge (Some(S n)) m) (f0:bf(S n)) :
    reduced_bf f0 = true ->
    extract_edge(sem_next ee f0) =
      @XEdge _ (Some(S n)) m ee
        (Next(Node(fnary_evalo1 f0 false)(fnary_evalo1 f0 true))).
  Proof with curry1.
  intro.
  specialize(extract_edge_with_reduced_next ee (Next(Node(fnary_evalo1 f0 false)(fnary_evalo1 f0 true)))) as HH...
  Qed.

  Function reduced_ldd {n} (l:ldd n) : bool :=
    match l with
    | Leaf _ => true
    | Edge ee f0 f1 =>
      reduced_bf(sha_bf(sem_ldd f0)(sem_ldd f1)) &&
      reduced_ldd f0 &&
      reduced_ldd f1
    end.

  Theorem reduced_ldd_extract_ldd {n} (f:bf n) :
    reduced_ldd (extract_ldd f) = true.
  Proof with curry1.
  apply (extract_ldd_ind (fun n f l => reduced_ldd l = true))...
  specialize(reduced_bf_extract_edge pX) as PX...
  Qed.

  Lemma extract_ldd_sem_ldd_eq_same {n} (l:ldd n) :
    reduced_ldd l = true ->
    extract_ldd(sem_ldd l) = l.
  Proof with curry1.
  induction l; intros; rewrite extract_ldd_eq...
  - rewrite extract_edge_sem_term...
  - rewrite extract_edge_sem_next...
    rewrite_subst...
  Qed.

  Theorem ldd_canonical {n} (l1 l2:ldd n) :
    reduced_ldd l1 = true ->
    reduced_ldd l2 = true ->
    sem_ldd l1 = sem_ldd l2 <-> l1 = l2.
  Proof with curry1.
  split...
  specialize(apply_f extract_ldd H1) as HH.
  rewrite extract_ldd_sem_ldd_eq_same in HH...
  rewrite extract_ldd_sem_ldd_eq_same...
  Qed.

  (* Section. Packaging Using SubType Notation *)

  Definition norm {n} (l:ldd n) := reduced_ldd l.
  Hint Unfold norm : curry1_rewrite.

  Definition LDD (n:nat) : Type :=
    { l : ldd n | norm l = true }.

  Lemma norm_extract_ldd {n} (f:bf n) : norm(extract_ldd f) = true.
  Proof with curry1.
  rewrite reduced_ldd_extract_ldd...
  Qed.

  Definition extract {n} (f:bf n) : LDD n :=
    (exist _ (extract_ldd f) (norm_extract_ldd f)).

  Definition sem {n} (l:LDD n) : bf n :=
    sem_ldd(proj1_sig l).

  Definition LDD_PI {n} (l1 l2:LDD n) : l1 = l2 <-> proj1_sig l1 = proj1_sig l2.
  Proof with curry1.
  split...
  destruct l1, l2...
  Qed.

  Theorem sem_extract {n} (f:bf n) :sem(extract f) = f.
  Proof. unfold sem, extract; curry1. Qed.
  Hint Rewrite @sem_extract : curry1_rewrite.

  Theorem extract_sem {n} (l:LDD n) : extract(sem l) = l.
  Proof with curry1.
  rewrite LDD_PI...
  destruct l as [l p].
  unfold sem...
  rewrite extract_ldd_sem_ldd_eq_same...
  Qed.
  Hint Rewrite @extract_sem : curry1_rewrite.

  Theorem LDD_uniqueness {n} (l1 l2:LDD n) : sem l1 = sem l2 <-> l1 = l2.
  Proof with curry1.
  split...
  specialize(apply_f extract H)...
  Qed.

  Theorem LDD_existence {n} (f:bf n) : exists (l:LDD n), sem l = f.
  Proof. exists (extract f); curry1. Qed.

End MaxLdd1.

(*
Extraction extract_ldd.

val extract_ldd : nat -> bf -> ldd

let rec extract_ldd n f =
  let XEdge (_, m0, ee, nx) = extract_edge n f in
  match nx with
   | Term -> Leaf (m0, ee)
   | Next (_, f') ->
     let Node (n'', f'0, f'1) = f' in
     let ldd0 = extract_ldd n'' f'0 in
     let ldd1 = extract_ldd n'' f'1 in
     Edge (n'', m0, ee, ldd0, ldd1)
 *)
