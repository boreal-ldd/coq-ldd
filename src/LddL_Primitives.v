Require Import Arith .
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.

Require Import Ground.SimplProp.
Require Import Ground.SimplBool.
Require Import Ground.SimplOption.
Require Import Ground.SimplEq.
Require Import Ground.SimplFun.
Require Import Ground.SimplBoolFun.
Require Import Ground.SimplCmp.
Require Import Ground.SimplSimpl.

Require Import Ground.SimplNat.
Require Import Ground.SimplAB.

Require Import Ground.SimplList_Chap0_Standard.
Require Import Ground.SimplList_Chap1_Construct.
Require Import Ground.SimplList_Chap2_Predicate.
Require Import Ground.SimplList_Chap3_PredicateOnConstruct.
Require Import Ground.SimplList.
Require Import Ground.SimplABList.
Require Import Ground.SimplMore.
Require Import Ground.SimplDTT.
Require Import Ground.SimplLtN_Chap0_Construct.
Require Import Ground.SimplLtN_Chap1_Array_as_Function.
Require Import Ground.SimplLtN_Chap1_Sec1_Boolean_Array.
Require Import Ground.SimplLtN_Chap2_Cartesian_Operators.
Require Import Ground.SimplLtNIndexedArray.

Require Import Ground.SimplDTT.
Require Import Ground.SimplLtN.
Require Import Ground.SimplEnum.
Require Import Ground.SimplEnumCmp.
Require Import Ground.SimplEnumCount.
Require Import Ground.SimplEnumArray.

Require Import Ground.MiniBool_VAX.
Require Import Ground.MiniBool_Logic.

Require Import FiniteCurryfy.
Require Import FiniteBooleanFunction.

Require Import LddO_Primitives.
Require Import LddO_OPrim.
Require Import LddU_Primitives.

Require Import LddL_NatPrimitives.
Require Import LddL_VecPrimitives.
Require Import LddL_CountPrimitives.

(* [MOVEME] *)
Lemma nat_odd_0 : Nat.odd 0 = false. Proof. reflexivity. Qed.
Hint Rewrite nat_odd_0 : curry1_rewrite.

Definition lP_twist_bf (ucx:bool) {n} (pv:pvec(S n)) (f:bf(S n)) : bf(S n) :=
  if ucx
  then lC_twist_bf pv f
  else lUX_twist_bf pv f.

Definition fnary_evall1 (ucx:bool) {n} (pv:pvec(S n)) (f:bf(S n)) (b:bool) : bf n :=
  (fnary_evalu1 (snd(proj1_sig pv)) (lP_twist_bf ucx pv f) b).

Definition ucx_of_oprim (p:oprim) : bool :=
  match p with
  | OC _ _ => true
  | _ => false
  end.

Lemma lP_twist_bf_same_involutive b {n} (pv:pvec(S n)) f : (lP_twist_bf b pv (lP_twist_bf b pv f)) = f.
Proof. destruct b; curry1. Qed.
Hint Rewrite @lP_twist_bf_same_involutive : curry1_rewrite.

Definition intro_lP_bf (p:oprim) {n} (pv:pvec (S n)) (f:bf n) : bf (S n) :=
  lP_twist_bf (ucx_of_oprim p) pv (intro_uP_bf (oPrim_of_oprim p) (snd(proj1_sig pv)) f).

Definition elim_lP_bf (p:oprim) {n} (pv:pvec (S n)) (f:bf(S n)) : bf n :=
  elim_uP_bf (oPrim_of_oprim p) (snd(proj1_sig pv)) (lP_twist_bf (ucx_of_oprim p) pv f).

Definition is_lP_bf (p:oprim) {n} (pv:pvec (S n)) (f:bf(S n)) : bool :=
  is_uP_bf (oPrim_of_oprim p) (snd(proj1_sig pv)) (lP_twist_bf (ucx_of_oprim p) pv f).

Lemma fnary_evall1_intro_lP_bf_same p b {n} pv f :
  fnary_evall1 (ucx_of_oprim p) pv (@intro_lP_bf p n pv f) b =
    (fun x => eval_prim (oPrim_of_oprim p) b (f x)).
Proof with curry1.
unfold fnary_evall1, intro_lP_bf...
apply fnary_evalu1_intro_uP_bf_same.
Qed.
Hint Rewrite @fnary_evall1_intro_lP_bf_same : curry1_rewrite.

Lemma fnary_evall1_intro_lP_bf_rev_prim_same {n} p pv f :
  fnary_evall1 (ucx_of_oprim p) pv (@intro_lP_bf p n pv f) (rev_prim(oPrim_of_oprim p)) = f.
Proof. curry1. Qed.
Hint Rewrite @fnary_evall1_intro_lP_bf_rev_prim_same : curry1_rewrite.

Lemma elim_lP_bf_intro_lP_bf_same p {n} (pv:pvec(S n)) (f:bf n) : elim_lP_bf p pv (intro_lP_bf p pv f) = f.
Proof. unfold elim_lP_bf, intro_lP_bf; curry1. Qed.
Hint Rewrite @elim_lP_bf_intro_lP_bf_same : curry1_rewrite.

Lemma intro_lP_bf_eq_as_elim_lP_bf_and_elim_intro p n (f:bf n) g (pv:pvec(S n)) :
  intro_lP_bf p pv f = g <-> f = elim_lP_bf p pv g /\ intro_lP_bf p pv (elim_lP_bf p pv g) = g.
Proof. split; curry1. Qed.

Lemma lP_twist_bf_shift b {n} pv f1 f2 : @lP_twist_bf b n pv f1 = f2 <-> f1 = lP_twist_bf b pv f2.
Proof. split; curry1. Qed.

Lemma intro_lP_bf_elim_lP_bf_same_eq_same p {n} pv (f:bf(S n)) :
  intro_lP_bf p pv (elim_lP_bf p pv f) = f <-> is_lP_bf p pv f = true.
Proof with curry1.
unfold intro_lP_bf, elim_lP_bf, is_lP_bf...
rewrite lP_twist_bf_shift...
Qed.
Hint Rewrite @intro_lP_bf_elim_lP_bf_same_eq_same : curry1_rewrite.

Lemma intro_lP_bf_as_is_lP_bf p {n} pv f g :
  @intro_lP_bf p n pv f = g <-> is_lP_bf p pv g = true /\ f = elim_lP_bf p pv g.
Proof. rewrite intro_lP_bf_eq_as_elim_lP_bf_and_elim_intro; curry1. Qed.

(* [MOVEME] *)
Lemma fnary_evalu1_axor_input {n} k v (f:bf(S n)) b :
  fnary_evalu1 k (axor_input v f) b = axor_input (spop k v) (fnary_evalu1 k f (xorb b (v k))).
Proof with curry1.
apply functional_extensionality...
unfold fnary_evalu1, axor_input...
apply f_equal...
rewrite (array_expansion_u k)...
Qed.

Lemma rewrite_is_uU_bf_using_axor_input {n} k f :
  @is_uP_bf oU n k f = true <-> f = axor_input (array_cdirac true k) f.
Proof with curry1.
rewrite is_uU_bf_as_fnary_evalu1.
rewrite (shannon_expansion_u k)...
repeat rewrite fnary_evalu1_axor_input...
Qed.

Lemma rewrite_is_uX_bf_using_axor_input {n} k f :
  @is_uP_bf oX n k f = true <-> f = neg_bf(axor_input (array_cdirac true k) f).
Proof with curry1.
rewrite is_uX_bf_as_fnary_evalu1.
rewrite (shannon_expansion_u k)...
repeat rewrite fnary_evalu1_axor_input...
rewrite H...
Qed.

Lemma is_lP_bf_lP_twist_bf p {n} (pv:pvec(S n)) f:
  is_lP_bf p pv (lP_twist_bf (ucx_of_oprim p) pv f) = is_uP_bf (oPrim_of_oprim p) (snd(proj1_sig pv)) f.
Proof. unfold is_lP_bf; curry1. Qed.

Lemma beq_ltN_ltN_intro_both_same n k x y : beq_ltN (@ltN_intro n k x) (ltN_intro k y) = beq_ltN x y.
Proof. rewrite eq_iff_eq_true; curry1. Qed.
Hint Rewrite beq_ltN_ltN_intro_both_same : curry1_rewrite.

Lemma spop_array_cdirac {n} k1 b2 (k2:ltN(S n)) :
  spop k1 (array_cdirac b2 k2) =
    match ltN_pop k1 k2 with
    | AA _ => (fun _ => false)
    | BB k2' => array_cdirac b2 k2'
    end.
Proof with curry1.
dest_match_step...
unfold spop, array_cdirac, array_dirac.
apply functional_extensionality...
Qed.

Lemma beq_ltN_comm {n} k1 k2 : @beq_ltN n k1 k2 = beq_ltN k2 k1.
Proof. rewrite (beq_sym_of_iff_true (beq_iff_true_beq_ltN _)); reflexivity. Qed.

Lemma beq_ltN_ltN_intro_l_same {n} k1 k2 : beq_ltN (@ltN_intro n k1 k2) k1 = false.
Proof. rewrite beq_ltN_comm; curry1. Qed.
Hint Rewrite @beq_ltN_ltN_intro_l_same : cury1_rewrite.

Lemma adot_false_r n x : @adot n x (fun _ => false) = false.
Proof. rewrite adot_comm; curry1. Qed.
Hint Rewrite adot_false_r : curry1_rewrite.

Lemma array_cdirac_ltN_intro_k_eval_k {n} b k (k':ltN n) : array_cdirac b (ltN_intro k k') k = false.
Proof. unfold array_cdirac, array_dirac; curry1. Qed.
Hint Rewrite @array_cdirac_ltN_intro_k_eval_k : curry1_rewrite.

Lemma array_lin_swap_eq_same_lemma1 {n} (pv:pvec(S n)) :
  (forall x, lC_twist_array pv x = x) <->  spop (snd(proj1_sig pv)) (fst(proj1_sig pv)) = (fun _ => false).
Proof with curry1.
curry1.
split...
- destruct pv as [[v k] Hpv]...
  destruct(array_minsat(spop k v)) as [k0|] eqn:E...
  specialize(H(array_cdirac true (ltN_intro k k0)))...
  unfold lC_twist_array in *...
  rewrite (array_expansion_u k) in H...
  rewrite array_minsat_eq_Some_iff_is_min_positive in E...
  unfold is_min_positive in E...
  unfold spop in H0...
- destruct pv as [[v0 k0] Hpv]...
  apply functional_extensionality; intro k1...
  unfold lC_twist_array...
  rewrite (rewrite_adot_using_spop k0)...
Qed.

Lemma rewrite_fnary_eq_cst_bf if0 th0 {n} (k:ltN(S n)) f :
  fnary_evalu1 k f if0 = cst_bf n th0 <-> (forall x : array bool (S n), x k = if0 -> f x = th0).
Proof with curry1.
unfold fnary_evalu1.
split...
- specialize(f_equal(fun f => f(spop k x))H)...
- apply functional_extensionality...
  specialize(H(spush k if0 x))...
Qed.

Lemma rewrite_is_uC_bf if0 th0 {n} (k:ltN(S n)) f :
  is_uP_bf (oC if0 th0) k f = true <-> (forall x : array bool (S n), x k = if0 -> f x = th0).
Proof with curry1.
rewrite is_uP_bf_iff_fnary_evalu1.
apply rewrite_fnary_eq_cst_bf.
Qed.

Lemma forall_lC_twist_array {n} (pv:pvec(S n)) (P:array bool(S n) -> Prop) :
  (forall (a:array bool _), P a) <-> (forall (a:array bool _), P(lC_twist_array pv a)).
Proof with curry1.
split...
specialize(H(lC_twist_array pv a))...
Qed.

Lemma forall_lUX_twist_array {n} (pv:pvec(S n)) (P:array bool(S n) -> Prop) :
  (forall (a:array bool _), P a) <-> (forall (a:array bool _), P(lUX_twist_array pv a)).
Proof with curry1.
split...
specialize(H(lUX_twist_array pv a))...
Qed.

Lemma lC_twist_bf_lC_twist_array_same {n} pv f (a:array bool (S n)) :
  lC_twist_bf pv f (lC_twist_array pv a) = f a.
Proof. unfold lC_twist_bf; curry1. Qed.
Hint Rewrite @lC_twist_bf_lC_twist_array_same : curry1_rewrite.

Lemma rewrite_is_lC_bf if0 th0 {n} (pv:pvec(S n)) f :
  is_lP_bf (OC if0 th0) pv f = true <-> (forall x, adot (fst(proj1_sig pv)) x = if0 -> f x = th0).
Proof with curry1.
unfold is_lP_bf; simpl.
rewrite rewrite_is_uC_bf.
unfold lP_twist_bf.
rewrite (forall_lC_twist_array pv)...
split; intros H a; specialize(H a)...
Qed.

Lemma aray_lUX_twist_array_cdirac_same {n} (pv:pvec(S n)) :
  lUX_twist_array pv (array_cdirac true (snd (proj1_sig pv))) = fst (proj1_sig pv).
Proof with curry1.
unfold lUX_twist_array...
destruct pv as [[a k] Hpv]...
rewrite <- Hpv, spush_spop_same...
Qed.
Hint Rewrite @aray_lUX_twist_array_cdirac_same : curry1_rewrite.

Lemma rewrite_is_lU_bf {n} (pv:pvec(S n)) f :
  is_lP_bf OU pv f = true <-> f = axor_input (fst(proj1_sig pv)) f.
Proof with curry1.
unfold is_lP_bf.
rewrite rewrite_is_uU_bf_using_axor_input...
rewrite axor_input_lUX_twist_bf...
rewrite lUX_twist_bf_injective...
Qed.

Lemma neg_bf_lUX_twist_bf {n} pv f : neg_bf (@lUX_twist_bf n pv f) = lUX_twist_bf pv (neg_bf f).
Proof. reflexivity. Qed.

Lemma rewrite_is_lX_bf {n} (pv:pvec(S n)) f :
  is_lP_bf OX pv f = true <-> f = neg_bf (axor_input (fst(proj1_sig pv)) f).
Proof with curry1.
unfold is_lP_bf...
rewrite rewrite_is_uX_bf_using_axor_input...
rewrite axor_input_lUX_twist_bf...
rewrite neg_bf_lUX_twist_bf.
rewrite lUX_twist_bf_injective...
Qed.

Lemma same_vec_implies_same_is_lP_bf_lemma1 p {n} (pv1 pv2:pvec(S n)) f :
  fst(proj1_sig pv1) = fst(proj1_sig pv2) -> is_lP_bf p pv1 f = true -> is_lP_bf p pv2 f = true.
Proof with curry1.
destruct p...
- rewrite rewrite_is_lU_bf in *...
  rewrite_subst.
- rewrite rewrite_is_lX_bf in *...
  rewrite_subst.
- rewrite rewrite_is_lC_bf in *...
  specialize(H0 x)...
  rewrite_subst.
Qed.

Lemma same_vec_implies_same_is_lP_bf p {n} (pv1 pv2:pvec(S n)) f :
  fst(proj1_sig pv1) = fst(proj1_sig pv2) -> is_lP_bf p pv1 f = is_lP_bf p pv2 f.
Proof with curry1.
intro H1; rewrite eq_iff_eq_true; split; intro H2.
- apply (same_vec_implies_same_is_lP_bf_lemma1 _ _ _ _ H1 H2).
- apply (same_vec_implies_same_is_lP_bf_lemma1 _ _ _ _ (eq_sym H1) H2).
Qed.

Lemma cntsat_bf_lP_twist_bf b {n} pv f : cntsat_bf (@lP_twist_bf b n pv f) = cntsat_bf f.
Proof with curry1.
unfold lP_twist_bf.
unfold lC_twist_bf, lUX_twist_bf.
destruct b.
- fold (comp (lC_twist_array pv) f).
  apply cntsat_bf_absorb_involutive.
  apply functional_extensionality...
- fold (comp (lUX_twist_array pv) f).
  apply cntsat_bf_absorb_involutive.
  apply functional_extensionality...
Qed.

Lemma cntsat_bf_intro_lC_bf if0 th0 {n} pv (f:bf n) :
  cntsat_bf (intro_lP_bf (OC if0 th0) pv f) = (if th0 then (2 ^ n) else 0) + cntsat_bf f.
Proof.
unfold intro_lP_bf.
rewrite cntsat_bf_lP_twist_bf.
apply cntsat_bf_intro_uC_bf.
Qed.

Lemma axor_input_dot_bf {n} a1 b2 a2 : axor_input a1 (@dot_bf n b2 a2) = dot_bf (xorb b2 (adot a1 a2)) a2.
Proof with curry1.
apply functional_extensionality...
unfold axor_input, dot_bf...
rewrite adot_axor_r...
rewrite adot_comm...
Qed.
Hint Rewrite @axor_input_dot_bf : curry1_rewrite.

Definition beq_array_bool {n} (a1 a2:array bool n) := beq_ord (cmp_array_bool a1 a2) Eq.

Lemma beq_iff_true_beq_array_bool {n} : beq_iff_true (@beq_array_bool n).
Proof with curry1.
intros x y...
unfold beq_array_bool.
rewrite (beq_iff_true_beq_ord _ _)...
destruct (cmp_P_cmp_array_bool n) as [RB TB AB].
rewrite (RB _ _)...
Qed.

Lemma beq_array_bool_iff_true {n} x y : @beq_array_bool n x y = true <-> x = y.
Proof. apply beq_iff_true_beq_array_bool. Qed.
Hint Rewrite @beq_array_bool_iff_true : curry1_rewrite.

Lemma beq_array_bool_refl {n} x : @beq_array_bool n x x = true.
Proof. curry1. Qed.
Hint Rewrite @beq_array_bool_refl : curry1_rewrite.

Lemma dot_bf_fun_false_l {n} b a : dot_bf b a (fun _ : ltN n => false) = b.
Proof. unfold dot_bf; curry1. Qed.
Hint Rewrite @dot_bf_fun_false_l : curry1_rewrite.

Lemma dot_bf_array_cdorac_l {n} b1 (a1:array bool n) b2 k2 :
  dot_bf b1 a1 (array_cdirac b2 k2) = xorb b1 (b2 && a1 k2).
Proof. unfold dot_bf; curry1. Qed.
Hint Rewrite @dot_bf_array_cdorac_l : curry1_rewrite.

Lemma dot_bf_injective {n} b1 b2 (a1 a2:array bool n) : dot_bf b1 a1 = dot_bf b2 a2 <-> b1 = b2 /\ a1 = a2.
Proof with curry1.
split...
specialize(f_equal(fun f => f(fun _ => false))H)...
rewrite <- (beq_iff_true_beq_array_bool _ _).
unfold beq_array_bool, cmp_array_bool, cmp_array...
rewrite (beq_iff_true_beq_ord _ _)...
dest_match_step...
rename l into k.
rewrite (minsat_alternative_array_minsat _ _) in D...
unfold is_min_positive in D...
specialize(f_equal(fun f => f(array_cdirac true k))H) as Hk...
rewrite_subst.
rewrite cmp_bool_rewrite...
Qed.
Hint Rewrite @dot_bf_injective : curry1_rewrite.

Lemma beq_bf_dot_bf_both {n} b1 b2 (a1 a2:array bool n) :
  beq_bf (dot_bf b1 a1) (dot_bf b2 a2) = eqb b1 b2 && beq_array_bool a1 a2.
Proof with curry1.
rewrite eq_iff_eq_true...
rewrite andb_true_iff...
rewrite (beq_bf_iff_true _ _)...
Qed.
Hint Rewrite @beq_bf_dot_bf_both : curry1_rewrite.

Lemma is_lU_bf_dot_bf {n} v1 b2 a2 : is_lP_bf OU v1 (@dot_bf (S n) b2 a2) = negb(adot (fst (proj1_sig v1)) a2).
Proof with curry1.
rewrite eq_iff_eq_true.
rewrite rewrite_is_lU_bf...
Qed.
Hint Rewrite @is_lU_bf_dot_bf : curry1_rewrite.

Lemma elim_lU_bf_dot_bf {n} v1 b2 a2 :
  elim_lP_bf OU v1 (@dot_bf (S n) b2 a2) = dot_bf b2 (spop (snd(proj1_sig v1)) a2).
Proof with curry1.
unfold elim_lP_bf...
unfold elim_uP_bf...
apply functional_extensionality...
unfold fnary_evalu1.
unfold lUX_twist_bf...
unfold lUX_twist_array...
unfold rev_prim...
destruct v1 as [[a1 k1] Hv1]...
unfold dot_bf...
rewrite (rewrite_adot_using_spop k1)...
Qed.
Hint Rewrite @elim_lU_bf_dot_bf : curry1_rewrite.

Lemma xorb_bf_neg_bf {n} b f : xorb_bf b (@neg_bf n f) = xorb_bf (negb b) f.
Proof with curry1.
apply functional_extensionality...
unfold xorb_bf, neg_bf...
unfold uop_bf, bop_bf...
Qed.
Hint Rewrite @xorb_bf_neg_bf : curry1_rewrite.

Lemma neg_bf_dot_bf {n} b a : neg_bf (@dot_bf n b a) = dot_bf (negb b) a.
Proof. unfold dot_bf; curry1. Qed.
Hint Rewrite @neg_bf_dot_bf : curry1_rewrite.

Lemma is_lX_bf_dot_bf {n} v1 b2 a2 : is_lP_bf OX v1 (@dot_bf (S n) b2 a2) = adot (fst (proj1_sig v1)) a2.
Proof with curry1.
rewrite eq_iff_eq_true.
rewrite rewrite_is_lX_bf...
BPS.MicroBPSolver...
Qed.
Hint Rewrite @is_lX_bf_dot_bf : curry1_rewrite.

Lemma elim_lX_bf_dot_bf {n} v1 b2 a2 :
  elim_lP_bf OX v1 (@dot_bf (S n) b2 a2) = dot_bf b2 (spop (snd(proj1_sig v1)) a2).
Proof with curry1.
unfold elim_lP_bf...
unfold elim_uP_bf...
apply functional_extensionality...
unfold fnary_evalu1.
unfold lUX_twist_bf...
unfold lUX_twist_array...
unfold rev_prim...
destruct v1 as [[a1 k1] Hv1]...
unfold dot_bf...
rewrite (rewrite_adot_using_spop k1)...
Qed.
Hint Rewrite @elim_lX_bf_dot_bf : curry1_rewrite.

Lemma if_then_b_else_negb_b (x y:bool) : (if x then y else negb y) = negb(xorb x y).
Proof. BPS.MicroBPSolver; reflexivity. Qed.
Hint Rewrite if_then_b_else_negb_b : curry1_rewrite.

Lemma if_then_negb_b_else_b (x y:bool) : (if x then negb y else y) = xorb x y.
Proof. BPS.MicroBPSolver; reflexivity. Qed.
Hint Rewrite if_then_negb_b_else_b : curry1_rewrite.

Lemma forall_adot_l_eq_false {n} a0 : (forall a1, @adot n a0 a1 = false) <-> a0 = fun _ => false.
Proof with curry1.
split...
rewrite <- (beq_iff_true_beq_array_bool _ _).
unfold beq_array_bool, cmp_array_bool, cmp_array.
dest_match_step...
rename l into k.
rewrite (minsat_alternative_array_minsat _ _) in D...
specialize(H(array_cdirac true k)) as Hk...
Qed.
Hint Rewrite @forall_adot_l_eq_false : curry1_rewrite.

Lemma forall_adot_l_eq_true {n} a0 : (forall a1, @adot n a0 a1 = true) <-> False.
Proof with curry1.
split...
specialize(H(fun _ => false))...
Qed.
Hint Rewrite @forall_adot_l_eq_true : curry1_rewrite.

Lemma forall_adot_l_eq_bool {n} a0 b0 : (forall a1, @adot n a0 a1 = b0) <-> b0 = false /\ a0 = fun _ => false.
Proof. destruct b0; curry1. Qed.
Hint Rewrite @forall_adot_l_eq_bool : curry1_rewrite.

Lemma adot_false n : @adot n (fun _ => false) = cst_bf n false.
Proof. apply functional_extensionality; curry1. Qed.
Hint Rewrite @adot_false : curry1_rewrite.

Lemma dot_bf_fun_false_r b n : @dot_bf n b (fun _ => false) = cst_bf n b.
Proof. unfold dot_bf; curry1. Qed.
Hint Rewrite dot_bf_fun_false_r : curry1_rewrite.

Lemma dot_bf_eq_cst_bf b' n v' b : dot_bf b' v' = cst_bf n b <-> b = b' /\ v' = (fun _ => false).
Proof with curry1.
split...
assert(E:dot_bf false v' = cst_bf n (xorb b b')).
{
  apply functional_extensionality...
  specialize(f_equal(fun f => f x)H)...
  unfold dot_bf, cst_bf in *...
}
rewrite (@functional_extensionality_iff (array bool n) bool) in E.
unfold cst_bf, dot_bf in E. rewrite xorb_false in E. unfold id in E.
rewrite forall_adot_l_eq_bool in E...
Qed.
Hint Rewrite dot_bf_eq_cst_bf : curry1_rewrite.

Lemma cst_bf_eq_dot_bf b' n v' b : cst_bf n b = dot_bf b' v' <-> b = b' /\ v' = (fun _ => false).
Proof. rewrite <- dot_bf_eq_cst_bf; curry1. Qed.
Hint Rewrite @cst_bf_eq_dot_bf : curry1_rewrite.

Lemma elim_lP_bf_cst_bf {n} o pv b : elim_lP_bf o pv (cst_bf (S n) b) = cst_bf n b.
Proof. destruct o; curry1. Qed.
Hint Rewrite @elim_lP_bf_cst_bf : curry1_rewrite.

Lemma is_lU_bf_cst_bf {n} pv b : is_lP_bf OU pv (cst_bf (S n) b) = true.
Proof. rewrite <- dot_bf_fun_false_r, is_lU_bf_dot_bf; curry1. Qed.
Hint Rewrite @is_lU_bf_cst_bf : curry1_rewrite.

Lemma spop_fun_cst {A n} k v0 : @spop A n k (fun _ => v0) = (fun _ => v0).
Proof. reflexivity. Qed.
Hint Rewrite @spop_fun_cst : curry1_rewrite.

Lemma cntsat_bf_is_lC_bf if0 th0 {n} pv (f:bf(S n)) :
  is_lP_bf (OC if0 th0) pv f = true ->
    cntsat_bf f = (if th0 then 2^n else 0) + cntsat_bf (elim_lP_bf (OC if0 th0) pv f).
Proof with curry1.
intros H.
rewrite <- intro_lP_bf_elim_lP_bf_same_eq_same in H.
specialize(f_equal(fun f => cntsat_bf f)H) as CH.
rewrite cntsat_bf_intro_lC_bf in CH.
symmetry in CH.
assumption.
Qed.

Lemma fnary_evalo1_dot_bf b1 b2 {n} (a1:array bool (S n)) :
  fnary_evalo1 (dot_bf b1 a1) b2 = dot_bf (xorb b1 (b2 && shead a1)) (stail a1).
Proof with curry1.
apply functional_extensionality...
unfold fnary_evalo1...
unfold dot_bf...
rewrite (rewrite_adot_using_spop (ltN_of_nat 0 (S n) eq_refl))...
Qed.

Lemma array_1 {A} (a:array A 1) : a = (fun _ => a(ltN_of_nat 0 1 eq_refl)).
Proof. apply functional_extensionality; curry1. Qed.

Lemma non_false_array_fun_cst n b : @non_false_array n (fun _ => b) = (0 <? n) && b.
Proof with curry1.
unfold non_false_array...
destruct((0 <? n) && b) eqn:E0; destruct n...
Qed.
Hint Rewrite non_false_array_fun_cst : curry1_rewrite.

Lemma non_false_array_S {n} (a:array bool (S n)) : non_false_array a = shead a || non_false_array (stail a).
Proof with curry1.
unfold non_false_array...
unfold shead...
dest_match_step...
Qed.

Lemma non_false_array_0 (a:array bool 0) : non_false_array a = false.
Proof. unfold non_false_array; curry1. Qed.
Hint Rewrite non_false_array_0 : curry1_rewrite.

Lemma cntsat_bf_dot_bf {n} b (a:array bool n) :
  cntsat_bf (dot_bf b a) =
    if non_false_array a
    then (* non trivial *) 2 ^ (pred n)
    else (* constant *) if b then 2 ^ n else 0.
Proof with curry1.
generalize dependent b.
induction n...
repeat rewrite fnary_evalo1_dot_bf.
repeat rewrite IHn...
clear IHn.
rewrite non_false_array_S...
dest_match_step...
- destruct n...
- dest_match_step...
  dest_match_step...
Qed.

Lemma adot_ascal_r b {n} x y : @adot n x (ascal b y) = b && adot x y.
Proof. rewrite adot_comm, adot_ascal, adot_comm; reflexivity. Qed.

Lemma adot_spush_r {n} x k v (y:array bool n) : adot x (spush k v y) = xorb (x k && v) (adot (spop k x) y).
Proof. rewrite (rewrite_adot_using_spop k); curry1. Qed.
Hint Rewrite @adot_spush_r : curry1_rewrite.

Lemma adot_spush_l {n} x k v (y:array bool n) : adot (spush k v y) x = xorb (v && x k) (adot y (spop k x)).
Proof. rewrite (rewrite_adot_using_spop k); curry1. Qed.
Hint Rewrite @adot_spush_l : curry1_rewrite.

Lemma adot_spop_both {n} k (x y:array bool (S n)) :
  adot (spop k x) (spop k y) = xorb (x k && y k) (adot x y).
Proof. rewrite (rewrite_adot_using_spop k); curry1. Qed.

Lemma elim_lC_bf_dot_bf if0 th0 {n} (pv:pvec(S n)) b a :
  elim_lP_bf (OC if0 th0) pv (dot_bf b a) =
    let a0 := fst(proj1_sig pv) in
    let k0 := snd(proj1_sig pv) in
    dot_bf (xorb b (a k0 && negb if0)) (spop k0 (axor a (ascal (a k0) a0))).
Proof with curry1.
apply functional_extensionality...
unfold elim_lP_bf, elim_uP_bf, dot_bf...
destruct pv as [[a0 k0] H0]...
unfold lC_twist_bf, lC_twist_array, fnary_evalu1...
unfold xorb_bf, uop_bf...
unfold rev_prim...
rewrite adot_axor_l...
rewrite (adot_comm _ (spop k0 a0) _)...
BPS.MicroBPSolver...
Qed.
Hint Rewrite @elim_lC_bf_dot_bf : curry1_rewrite.

Lemma non_false_array_axor {n} (x y:array bool n) :
  non_false_array (axor x y) = negb(beq_array_bool x y).
Proof with curry1.
rewrite <- negb_injective.
rewrite eq_iff_eq_true...
unfold non_false_array...
rewrite axor_shift...
Qed.

(* [MOVEME] Ground.SimplNat *)
Lemma n_eq_n_plus_m (n m:nat) : n = n + m <-> m = 0.
Proof. lia. Qed.
Hint Rewrite n_eq_n_plus_m : curry1_rewrite.

(* [MOVEME] Ground.SimplNat *)
Lemma n_eq_m_plus_n (n m:nat) : n = m + n <-> m = 0.
Proof. lia. Qed.
Hint Rewrite n_eq_m_plus_n : curry1_rewrite.

(* [MOVEME] Ground.SimplNat *)
Lemma n_plus_m_eq_n (n m:nat) : n + m = n <-> m = 0.
Proof. lia. Qed.
Hint Rewrite n_plus_m_eq_n : curry1_rewrite.

(* [MOVEME] Ground.SimplNat *)
Lemma m_plus_n_eq_n (n m:nat) : m + n = n <-> m = 0.
Proof. lia. Qed.
Hint Rewrite m_plus_n_eq_n : curry1_rewrite.

(* [MOVEME] Ground.SimplNat *)
Lemma pow_2_n_eq_0 (n:nat) : 2 ^ n = 0 <-> False.
Proof. rewrite Nat.pow_eq_0_iff; simplnat. Qed.
Hint Rewrite pow_2_n_eq_0 : curry1_rewrite.

Lemma rewrite_non_false_array_using_spop {n} (k:ltN(S n)) a :
  non_false_array a = a k || non_false_array (spop k a).
Proof with curry1.
induction n...
- rewrite (array_1 a)...
- rewrite (non_false_array_S a).
  rewrite (non_false_array_S (spop k a)).
  destruct(ltN_pred k) as [k'|] eqn:E0...
  + rewrite (IHn k')...
    rewrite spop_ltN_S...
    unfold stail...
    BPS.MicroBPSolver...
  + destruct k...
    rewrite non_false_array_S...
Qed.

Lemma non_false_array_eq_false  n a : @non_false_array n a = false <-> a = (fun _ => false).
Proof. unfold non_false_array; curry1. Qed.
Hint Rewrite non_false_array_eq_false : curry1_rewrite.

Lemma rewrite_beq_array_bool_using_spop {n} k (x y:array bool (S n)) :
  beq_array_bool x y = (eqb(x k)(y k)) && (beq_array_bool(spop k x)(spop k y)).
Proof with curry1.
rewrite eq_iff_eq_true...
rewrite (array_expansion_u k)...
rewrite andb_true_iff...
Qed.

Lemma is_lC_bf_cst_bf if0 th0 {n} pv1 b2 : is_lP_bf (OC if0 th0) pv1 (@cst_bf (S n) b2) = eqb th0 b2.
Proof. unfold is_lP_bf; curry1. Qed.
Hint Rewrite @is_lC_bf_cst_bf : curry1_rewrite.

(* [REMOVE] *)

Lemma spop_eq_fun_cst {n} (k0:ltN(S n)) a : spop k0 a = (fun _ => false) <-> a = array_cdirac (a k0) k0.
Proof. rewrite (array_expansion_u k0); curry1. Qed.
Hint Rewrite @spop_eq_fun_cst : curry1_rewrite.

Lemma is_lC_bf_dot_bf if0 th0 {n} pv1 b2 a2 :
  is_lP_bf (OC if0 th0) pv1 (@dot_bf (S n) b2 a2) =
    let k1 := snd(proj1_sig pv1) in
    let a1 := fst(proj1_sig pv1) in
    beq_array_bool (spop k1 a2) (ascal (a2 k1) (spop k1 a1)) && (eqb th0 (xorb b2 (a2 k1 && if0))).
Proof with curry1.
rewrite eq_iff_eq_true...
destruct pv1 as [[a1 k1] H1]...
split...
- specialize(cntsat_bf_is_lC_bf _ _ _ _ H) as CH.
  rewrite cntsat_bf_dot_bf in CH...
  rewrite cntsat_bf_dot_bf in CH...
  rewrite non_false_array_axor in CH...
  rewrite (rewrite_non_false_array_using_spop k1) in CH.
  destruct(beq_array_bool (spop k1 a2) (ascal (a2 k1) (spop k1 a1)))eqn:E0...
  + apply reverse_bool_eq...
    destruct(a2 k1)eqn:E1...
    dest_match_step...
  + exfalso.
    destruct(a2 k1)eqn:E1...
    * dest_match_step...
      destruct n...
      rewrite (array_1 a1) in *...
      rewrite (array_1 a2) in *...
    * destruct(non_false_array (spop k1 a2))eqn:E2...
      dest_match_step...
      destruct n...
- destruct(beq_array_bool a1 a2)eqn:E0...
  + rewrite rewrite_is_lC_bf...
  + rewrite (rewrite_beq_array_bool_using_spop k1) in E0...
    rewrite H in E0...
    destruct(a2 k1)eqn:E1...
Qed.
Hint Rewrite @is_lC_bf_dot_bf : curry1_rewrite.

Lemma is_lP_bf_cst_bf_same_th0 if0 th0 {n} pv : is_lP_bf (OC if0 th0) pv (cst_bf (S n) th0) = true.
Proof with curry1.
rewrite <- dot_bf_fun_false_r, is_lC_bf_dot_bf...
Qed.
Hint Rewrite @is_lP_bf_cst_bf_same_th0 : curry1_rewrite.

Lemma intro_lC_bf_cst_bf if0 th0 {n} (pv:pvec _) b :
  intro_lP_bf (OC if0 th0) pv (cst_bf n b) =
      if eqb th0 b
      then cst_bf _ b
      else dot_bf (xorb if0 th0) (fst(proj1_sig pv)).
Proof with curry1.
rewrite intro_lP_bf_as_is_lP_bf...
dest_match_step...
Qed.
Hint Rewrite @intro_lC_bf_cst_bf : curry1_rewrite.

Definition intro_lP_pvec_cst_as_dot_pvec {n} (o:oprim) (pv:pvec(S n)) b : bool * array bool(S n) :=
  match o with
  | OU => (b, (fun _ => false))
  | OX => (b, (array_cdirac true (snd(proj1_sig pv))))
  | OC if0 th0 =>
    if eqb th0 b
    then (b, (fun _ => false))
    else (xorb if0 th0, (fst(proj1_sig pv)))
  end.

Lemma intro_lP_bf_cst_bf {n} (o:oprim) (pv:pvec _) b :
  intro_lP_bf o pv (cst_bf n b) =
    let t := intro_lP_pvec_cst_as_dot_pvec o pv b in
    dot_bf (fst t) (snd t).
Proof with curry1.
rewrite intro_lP_bf_as_is_lP_bf...
destruct o...
dest_match_step...
Qed.

Lemma intro_lC_bf_eq_intro_lC_bf_exclusive_case if1 th1 if2 th2 {n} pv1 pv2 (f1 f2:bf n) :
  intro_lP_bf (OC if1 th1) pv1 f1 = intro_lP_bf (OC if2 th2) pv2 f2 ->
    th1 <> th2 -> f1 = cst_bf _ th2 /\ f2 = cst_bf _ th1 /\ fst(proj1_sig pv1) = fst(proj1_sig pv2).
Proof with curry1.
intros.
assert(f1 = cst_bf n th2 /\ f2 = cst_bf n th1); [ | curry1].
specialize(f_equal (fun f => cntsat_bf f) H) as Hc.
repeat rewrite cntsat_bf_intro_lC_bf in Hc.
dest_match_step...
Qed.

Lemma is_lC_bf_intro_lC_bf_using_th0_neq_th1 if0 if1 th0 th1 {n} (pv1 pv2:pvec(S n)) (f:bf n) :
  th0 <> th1 -> is_lP_bf (OC if0 th0) pv1 (intro_lP_bf (OC if1 th1) pv2 f) = true <->
    if0 <> if1 /\ fst(proj1_sig pv1) = fst(proj1_sig pv2) /\ f = cst_bf _ (negb th1).
Proof with curry1.
curry1.
split...
- assert(f = cst_bf n (negb th1) /\ fst (proj1_sig pv1) = fst (proj1_sig pv2)).
  {
    rewrite <- intro_lP_bf_elim_lP_bf_same_eq_same in H.
    apply intro_lC_bf_eq_intro_lC_bf_exclusive_case in H...
  }
  destruct H0 as [H0 H1].
  rewrite H0 in *.
  apply(same_vec_implies_same_is_lP_bf_lemma1 _ _ _ _ H1) in H...
  apply reverse_bool_eq...
- rewrite andb_true_iff...
  destruct pv1 as [[a1 k1] H1]...
Qed.

Lemma is_lP_bf_intro_lP_bf {n} p pv f : is_lP_bf p pv (@intro_lP_bf p n pv f) = true.
Proof. unfold is_lP_bf, intro_lP_bf; curry1. Qed.
Hint Rewrite @is_lP_bf_intro_lP_bf : curry1_rewrite.

Lemma axor_input_involutive {n} (v:array bool n) f : axor_input v (axor_input v f) = f.
Proof with curry1.
apply functional_extensionality...
unfold axor_input...
Qed.
Hint Rewrite @axor_input_involutive : curry1_rewrite.

Lemma axor_input_injective {n} (v:array bool n) f1 f2 : axor_input v f1 = axor_input v f2 <-> f1 = f2.
Proof with curry1.
split...
specialize(f_equal (axor_input v) H)...
Qed.
Hint Rewrite @axor_input_injective : curry1_rewrite.

Lemma axor_input_shift {n} (v:array bool n) f1 f2 : axor_input v f1 = f2 <-> f1 = axor_input v f2.
Proof. split; curry1.  Qed.

Lemma axor_input_neg_bf {n} (v:array bool n) f : axor_input v (neg_bf f) = neg_bf (axor_input v f).
Proof. apply functional_extensionality; curry1. Qed.

Lemma axor_input_intro_lX_bf_same {n} (pv:pvec(S n)) f :
  axor_input (fst (proj1_sig pv)) (intro_lP_bf OX pv f) = neg_bf (intro_lP_bf OX pv f).
Proof with curry1.
rewrite axor_input_shift...
rewrite axor_input_neg_bf.
rewrite <- rewrite_is_lX_bf...
Qed.
Hint Rewrite @axor_input_intro_lX_bf_same : curry1_rewrite.

Lemma if_agrees_on_conquences (c1 c2:bool) {A} (v1 v0:A) :
  (if c1 then v1 else v0) = (if c2 then v1 else v0) <-> (c1 = c2 \/ v1 = v0).
Proof. dest_bool. Qed.
Hint Rewrite @if_agrees_on_conquences : curry1_rewrite.

Lemma axor_input_intro_lC_bf_same if0 th0 {n} (pv:pvec(S n)) f :
  axor_input (fst (proj1_sig pv)) (intro_lP_bf (OC if0 th0) pv f) =
    intro_lP_bf (OC (xorb if0 (asum(fst (proj1_sig pv)))) th0) pv
      (axor_input (spop (snd(proj1_sig pv)) (fst (proj1_sig pv))) f).
Proof with curry1.
apply functional_extensionality...
unfold axor_input, intro_lP_bf...
unfold lC_twist_bf...
destruct pv as [[v k] Hpv]...
unfold lC_twist_array...
unfold intro_uP_bf...
rewrite adot_axor_l...
Qed.

Lemma axor_input_intro_lU_bf_same {n} (pv:pvec(S n)) (f:bf n) :
  axor_input (fst (proj1_sig pv)) (intro_lP_bf OU pv f) = intro_lP_bf OU pv f.
Proof with curry1.
rewrite axor_input_shift...
rewrite <- rewrite_is_lU_bf...
Qed.
Hint Rewrite @axor_input_intro_lU_bf_same : curry1_rewrite.

Lemma neg_bf_lC_twist_bf {n} (pv:pvec(S n)) (f:bf(S n)) :
  neg_bf (lC_twist_bf pv f) = lC_twist_bf pv (neg_bf f).
Proof. unfold lC_twist_bf; curry1. Qed.
Hint Rewrite @neg_bf_lC_twist_bf : curry1_rewrite.

Lemma neg_bf_intro_uC_bf if0 th0 {n} k f :
  neg_bf (@intro_uP_bf (oC if0 th0) n k f) = intro_uP_bf (oC if0 (negb th0)) k (neg_bf f).
Proof with curry1.
apply functional_extensionality...
unfold intro_uP_bf, neg_bf, uop_bf...
dest_match_step...
Qed.
Hint Rewrite @neg_bf_intro_uC_bf : curry1_rewrite.

Lemma neg_bf_intro_lC_bf if0 th0 {n} pv (f:bf n) :
  neg_bf (intro_lP_bf (OC if0 th0) pv f) = intro_lP_bf (OC if0 (negb th0)) pv (neg_bf f).
Proof. unfold intro_lP_bf; curry1. Qed.
Hint Rewrite @neg_bf_intro_lC_bf : curry1_rewrite.

Lemma is_uC_bf_intro_uC_bf_same_k {n} (if1 b1 : bool) (k : ltN (S n)) (if2 b2 : bool) (f : bf n) :
  is_uP_bf (oC if1 b1) k (intro_uP_bf (oC if2 b2) k f) =
    if eqb if1 if2 then eqb b1 b2 else (beq_bf f (cst_bf _ b1)).
Proof with curry1.
rewrite eq_iff_eq_true...
rewrite is_uP_bf_iff_fnary_evalu1.
rewrite fnary_evalu1_intro_uC_bf...
dest_match_step...
Qed.
Hint Rewrite @is_uC_bf_intro_uC_bf_same_k : curry1_rewrite.

Lemma intro_lC_bf_eq_intro_lC_bf_same_pvec if0 if1 th0 th1 {n} pv (f0 f1:bf n) :
  intro_lP_bf (OC if0 th0) pv f0 = intro_lP_bf (OC if1 th1) pv f1 <->
    if eqb th0 th1
    then (f0 = f1 /\ (if0 <> if1 -> f0 = (fun _ => th0)))
    else (if0 <> if1 /\ f0 = (fun _ => negb th0) /\ f1 = (fun _ => negb th1)).
Proof with curry1.
unfold intro_lP_bf...
rewrite intro_uP_bf_eq_as_elim_uP_bf_and_elim_intro...
unfold elim_uP_bf...
unfold rev_prim...
destruct(eqb th0 th1)eqn:E0;
  destruct(eqb if0 if1)eqn:E1...
Qed.
Hint Rewrite @intro_lC_bf_eq_intro_lC_bf_same_pvec : curry1_rewrite.

Definition weirdly_lin_compatible (p1 p2:oprim) : bool :=
  match p1, p2 with
  | OU, OC _ _ => true
  | OC _ _, OU => true
  | _, _ => false
  end.

Lemma axor_input_cst_bf {n} a b : axor_input a (cst_bf n b) = cst_bf n b.
Proof. unfold axor_input; curry1. Qed.
Hint Rewrite @axor_input_cst_bf : curry1_rewrite.

Lemma is_lX_bf_intro_lC_bf_same if0 th0 {n} pv (f:bf n) :
  is_lP_bf OX pv (intro_lP_bf (OC if0 th0) pv f) = asum (fst (proj1_sig pv)) && (beq_bf f (cst_bf n (negb th0))).
Proof with curry1.
rewrite eq_iff_eq_true...
rewrite andb_true_iff...
rewrite rewrite_is_lX_bf...
rewrite axor_input_intro_lC_bf_same...
rewrite (eq_sym_iff if0)...
rewrite xorb_migration...
Qed.
Hint Rewrite @is_lX_bf_intro_lC_bf_same : curry1_rewrite.

Lemma intro_uX_bf_dot_bf {n} k b (v:array bool n) : intro_uP_bf oX k (dot_bf b v) = dot_bf b (spush k true v).
Proof with curry1.
apply functional_extensionality...
unfold intro_uP_bf, dot_bf...
Qed.
Hint Rewrite @intro_uX_bf_dot_bf : curry1_rewrite.

Lemma lUX_twist_bf_dot_bf {n} pv b (a:array bool(S n)) :
  let pv1 := proj1_sig pv in
  let v := fst pv1 in
  let k := snd pv1 in
    lUX_twist_bf pv (dot_bf b a) = dot_bf b (spush k (adot v a) (spop k a)).
Proof with curry1.
simpl...
unfold lUX_twist_bf, dot_bf...
apply functional_extensionality...
unfold lUX_twist_array...
destruct pv as [[v k] Hpv]...
repeat rewrite (rewrite_adot_using_spop k)...
destruct(x k)eqn:E0...
rewrite adot_axor_r...
rewrite (adot_comm _ (spop k a) _)...
Qed.
Hint Rewrite @lUX_twist_bf_dot_bf : curry1_rewrite.

Lemma intro_lX_bf_dot_bf {n} (pv:pvec(S n)) b (v:array bool n) :
  intro_lP_bf OX pv (dot_bf b v) =
    let pv1 := proj1_sig pv in
    let a := fst pv1 in
    let k := snd pv1 in
    dot_bf b (spush k (negb(adot (spop k a) v)) v).
Proof. unfold intro_lP_bf; curry1. Qed.
Hint Rewrite @intro_lX_bf_dot_bf : curry1_rewrite.

Lemma is_lC_bf_intro_lC_bf_same_pvec if0 th0 if1 th1 {n} (pv:pvec _) (f:bf n) :
  is_lP_bf (OC if0 th0) pv (intro_lP_bf (OC if1 th1) pv f) =
    if eqb if0 if1
    then eqb th0 th1
    else beq_bf f (cst_bf _ th0).
Proof with curry1.
rewrite eq_iff_eq_true...
rewrite <- intro_lP_bf_elim_lP_bf_same_eq_same...
destruct(eqb th0 th1)eqn:E0...
- destruct(eqb if0 if1)eqn:E0...
  split...
  rewrite H0 in H...
- destruct(eqb if0 if1)eqn:E0...
Qed.
Hint Rewrite @is_lC_bf_intro_lC_bf_same_pvec : curry1_rewrite.

Lemma ux_comm_Cc_lemma1 {n} (pvC:pvec(S (S n))) (pvc:pvec(S n)) :
  let pvC0 := proj1_sig pvC in
  let pvc0 := proj1_sig pvc in
  let aC := fst pvC0 in
  let ac := fst pvc0 in
  let kC := snd pvC0 in
  let kc := snd pvc0 in
  let kC' := ltN_intro kC kc in
  let aC0 := spush kC false ac in
  let ac0 := axor aC (ascal (aC kC') aC0) in
  let ac' := spop kC' ac0 in
  let kc' := ltN_comm_intro kc kC in
  pvec_p (ac', kc') = true.
Proof. curry1. Qed.

Definition ux_comm_Cc {n} (pvC:pvec(S (S n))) (pvc:pvec(S n)) : pvec(S n) :=
  let pvC0 := proj1_sig pvC in
  let pvc0 := proj1_sig pvc in
  let aC := fst pvC0 in
  let ac := fst pvc0 in
  let kC := snd pvC0 in
  let kc := snd pvc0 in
  let kC' := ltN_intro kC kc in
  let aC0 := spush kC false ac in
  let ac0 := axor aC (ascal (aC kC') aC0) in
  let ac' := spop kC' ac0 in
  let kc' := ltN_comm_intro kc kC in
  (exist _ (ac', kc') (ux_comm_Cc_lemma1 pvC pvc)).

Lemma ux_comm_cC_lemma0 {n} (pvC:pvec(S (S n))) (pvc:pvec(S n)) :
  let pvC0 := proj1_sig pvC in
  let pvc0 := proj1_sig pvc in
  let aC := fst pvC0 in
  let ac := fst pvc0 in
  let kC := snd pvC0 in
  let kc := snd pvc0 in
  let kC' := ltN_intro kC kc in
  let aC' := spush kC false ac in
  pvec_p (aC', kC') = true.
Proof. curry1. Qed.

Definition ux_comm_cC {n} (pvC:pvec(S (S n))) (pvc:pvec(S n)) : pvec(S (S n)) :=
  let pvC0 := proj1_sig pvC in
  let pvc0 := proj1_sig pvc in
  let aC := fst pvC0 in
  let ac := fst pvc0 in
  let kC := snd pvC0 in
  let kc := snd pvc0 in
  let kC' := ltN_intro kC kc in
  let aC' := spush kC false ac in
  (exist _ (aC', kC') (ux_comm_cC_lemma0 pvC pvc)).

Lemma ascal_spop_l b {n} k (v:array bool(S n)) : ascal b (spop k v) = spop k (ascal b v).
Proof. reflexivity. Qed.
Hint Rewrite @ascal_spop_l : curry1_rewrite.

Lemma ltN_pop_ltN_kC_kc_kV {n} kC (kc:ltN n) : ltN_pop (ltN_intro kC kc) kC = BB(ltN_comm_intro kc kC).
Proof with curry1.
destruct n...
Qed.
Hint Rewrite @ltN_pop_ltN_kC_kc_kV : curry1_rewrite.

Lemma spush_ltN_intro_kC_kc_kC {A n} kC (kc:ltN n) (v:A) x :
  spush (ltN_intro kC kc) v x kC = x (ltN_comm_intro kc kC).
Proof. unfold spush; curry1. Qed.
Hint Rewrite @spush_ltN_intro_kC_kc_kC : curry1_rewrite.

Lemma intro_lU_bf_intro_lP_bf {n} (pvC:pvec(S(S n))) (pvc:pvec(S n)) (f:bf n) :
  intro_lP_bf OU pvC (intro_lP_bf OU pvc f) =
    intro_lP_bf OU (ux_comm_cC pvC pvc) (intro_lP_bf OU (ux_comm_Cc pvC pvc) f).
Proof with curry1.
destruct pvC as [[aC kC] HC], pvc as [[ac kc] Hc]...
unfold intro_lP_bf, lUX_twist_bf, lUX_twist_array, intro_uP_bf...
unfold lUX_twist_bf, lUX_twist_array...
apply functional_extensionality; intros x.
repeat rewrite spop_spush_same.
apply f_equal...
apply functional_extensionality...
unfold spop, spush...
BPS.MicroBPSolver...
Qed.

Definition UXb (b:bool) : oprim := if b then OX else OU.

Definition UXb_comm_Cc {n} (bC bc:bool) (pvC:pvec(S(S n))) (pvc:pvec(S n)) : bool :=
  let pvC0 := proj1_sig pvC in
  let pvc0 := proj1_sig pvc in
  let aC := fst pvC0 in
  let ac := fst pvc0 in
  let kC := snd pvC0 in
  let kc := snd pvc0 in
  let kC' := ltN_intro kC kc in
  xorb bC (aC kC' && bc).

Lemma spop_eval {A n} k1 x k2 : @spop A n k1 x k2 = x (ltN_intro k1 k2).
Proof. reflexivity. Qed.

Lemma ucx_of_oprim_UXb (b:bool) : ucx_of_oprim (UXb b) = false.
Proof. destruct b; reflexivity. Qed.
Hint Rewrite ucx_of_oprim_UXb : curry1_rewrite.

Definition uxb (b:bool) : oPrim := if b then oX else oU.

Lemma oPrim_of_oprim_UXb b : LddO_OPrim.oPrim_of_oprim (UXb b) = uxb b.
Proof. destruct b; reflexivity. Qed.
Hint Rewrite oPrim_of_oprim_UXb : curry1_rewrite.

Lemma eval_prim_uxb b : eval_prim (uxb b) = (fun x y => xorb (b && x) y).
Proof. destruct b; curry1. Qed.
Hint Rewrite eval_prim_uxb : curry1_rewrite.

Lemma intro_lUX_bf_intro_lUX_bf {n} (bC bc:bool) (pvC:pvec(S(S n))) (pvc:pvec(S n)) (f:bf n) :
  intro_lP_bf (UXb bC) pvC (intro_lP_bf (UXb bc) pvc f) =
    let bC' := UXb_comm_Cc bC bc pvC pvc in
    intro_lP_bf (UXb bc) (ux_comm_cC pvC pvc) (intro_lP_bf (UXb bC') (ux_comm_Cc pvC pvc) f).
Proof with curry1.
destruct pvC as [[aC kC] HC], pvc as [[ac kc] Hc]...
unfold intro_lP_bf, lUX_twist_bf, lUX_twist_array, intro_uP_bf...
unfold lUX_twist_bf, lUX_twist_array...
unfold UXb_comm_Cc...
apply functional_extensionality; intros x.
repeat rewrite spop_spush_same.
repeat rewrite ascal_spop_l.
repeat rewrite <- spop_axor.
rewrite spop_spop_comm...
rewrite spop_eval...
match goal with
| |- context[f ?X] =>
    match goal with
    | |- context[f ?Y] =>
      match Y with
      | X => fail 1
      | _ => assert(R1:X = Y)
      end
    end
end.
{
  curry1.
  apply functional_extensionality...
  unfold spop, spush...
  BPS.MicroBPSolver...
}
rewrite R1. clear R1.
BPS.MicroBPSolver...
unfold spop...
BPS.MicroBPSolver...
Qed.

Lemma fold_orb (x1 x2:bool) {A:Type} (y1 y2:A) :
  (if x1 then y1 else (if x2 then y1 else y2)) = if (x1 || x2) then y1 else y2.
Proof. destruct x1; reflexivity. Qed.
Hint Rewrite @fold_orb : curry1_rewrite.

Lemma intro_lC_bf_intro_lC_bf {n} (ifC ifc th0:bool) (pvC:pvec(S(S n))) (pvc:pvec(S n)) (f:bf n) (xx:bool) :
  intro_lP_bf (OC ifC th0) pvC (intro_lP_bf (OC ifc th0) pvc f) =
    let ifC' := negb(UXb_comm_Cc (negb ifC) (negb ifc) pvC pvc) in
    intro_lP_bf (OC ifc th0) (ux_comm_cC pvC pvc) (intro_lP_bf (OC ifC' th0) (ux_comm_Cc pvC pvc) f).
Proof with curry1.
destruct pvC as [[aC kC] HC], pvc as [[ac kc] Hc]...
unfold intro_lP_bf, lC_twist_bf, lC_twist_array, intro_uP_bf...
unfold lC_twist_bf, lC_twist_array...
unfold UXb_comm_Cc...
apply functional_extensionality; intros x.
repeat rewrite spop_spush_same.
rewrite spop_spop_comm...
left.
repeat rewrite ascal_spop_l.
rewrite adot_axor_r...
rewrite adot_ascal_r...
repeat rewrite adot_spop_both...
BPS.MicroBPSolver...
Qed.

Lemma axor_input_intro_lC_bf (if0 th0 : bool) {n : nat} (pv1 pv2 : pvec (S n)) (f : bf n) :
  axor_input (fst (proj1_sig pv1)) (intro_lP_bf (OC if0 th0) pv2 f) =
  intro_lP_bf (OC (xorb if0 (adot (fst(proj1_sig pv1)) (fst (proj1_sig pv2)))) th0) pv2
    (axor_input (spop (snd (proj1_sig pv2)) (fst (proj1_sig pv1))) f).
Proof with curry1.
unfold axor_input, intro_lP_bf...
apply functional_extensionality...
unfold lC_twist_bf...
unfold intro_uP_bf...
unfold lC_twist_array...
left...
destruct pv1 as [[a1 k1] H1], pv2 as [[a2 k2] H2]...
rewrite adot_axor_r...
rewrite adot_comm...
Qed.

Lemma is_lX_bf_intro_lC_bf if0 th0 {n} (pv1 pv2:pvec _) (f:bf n) :
  is_lP_bf OX pv1 (intro_lP_bf (OC if0 th0) pv2 f) =
    adot (fst (proj1_sig pv1)) (fst (proj1_sig pv2)) && beq_bf f (cst_bf n (negb th0)).
Proof with curry1.
rewrite eq_iff_eq_true.
split...
rewrite rewrite_is_lX_bf in H.
rewrite axor_input_intro_lC_bf in H.
rewrite neg_bf_intro_lC_bf in H.
rewrite intro_lC_bf_eq_intro_lC_bf_same_pvec in H...
apply reverse_bool_eq...
Qed.
Hint Rewrite @is_lX_bf_intro_lC_bf : curry1_rewrite.

Lemma intro_lC_bf_eval if0 th0 {n} pv (f:bf n) x :
  intro_lP_bf (OC if0 th0) pv f x =
    if eqb(adot (fst(proj1_sig pv)) x) if0
    then th0
    else f(spop(snd(proj1_sig pv))x).
Proof with curry1.
unfold intro_lP_bf...
unfold lC_twist_bf...
unfold intro_uP_bf...
unfold lC_twist_array...
Qed.
Hint Rewrite @intro_lC_bf_eval : curry1_rewrite.

Lemma rewrite_intro_lC_bf if0 th0 {n} pv (f:bf n) :
  intro_lP_bf (OC if0 th0) pv f =
    (fun x => if eqb(adot (fst(proj1_sig pv)) x) if0 then th0 else f(spop(snd(proj1_sig pv))x)).
Proof. apply functional_extensionality, intro_lC_bf_eval. Qed.

Lemma aand_fun_true_l {n} a : aand a (fun _ : ltN n => true) = a.
Proof. apply functional_extensionality; curry1. Qed.
Hint Rewrite @aand_fun_true_l : curry1_rewrite.

Lemma adot_fun_true_l {n} a : @adot n a (fun _ => true) = asum a.
Proof. unfold adot; curry1. Qed.
Hint Rewrite @adot_fun_true_l : curry1_rewrite.

Lemma is_lU_bf_intro_lC_bf_same if0 th0 {n} pv (f:bf n) :
   is_lP_bf OU pv (intro_lP_bf (OC if0 th0) pv f) = true <->
    f = if asum (fst (proj1_sig pv))
      then (cst_bf n th0)
      else (axor_input (spop (snd (proj1_sig pv)) (fst (proj1_sig pv))) f).
Proof with curry1.
rewrite rewrite_is_lU_bf.
rewrite axor_input_intro_lC_bf_same.
unfold intro_lP_bf...
rewrite (shannon_expansion_u_gen (snd (proj1_sig pv)) if0)...
dest_match_step...
Qed.

Lemma pvec_1_unique (pv:pvec 1) : pv = (exist _ ((fun _ => true), (ltN_of_nat 0 1 eq_refl)) eq_refl).
Proof with curry1.
setoid_rewrite exist_eq_r...
destruct pv as [[a k] H]...
apply functional_extensionality...
Qed.

(* [MOVEME] Ground.SimpletNat *)
Lemma odd_S n : Nat.odd (S n) = negb(Nat.odd n).
Proof with curry1.
induction n...
rewrite IHn...
Qed.

Lemma asum_fun_true n : @asum n (fun _ => true) = Nat.odd n.
Proof with curry1.
induction n...
rewrite asum_S, odd_S...
Qed.
Hint Rewrite asum_fun_true : curry1_rewrite.

Lemma asum_pv_eq_false_implies_non_false_array_spop_k_v {n} (pv:pvec(S n)) :
  asum (fst (proj1_sig pv)) = false ->
    non_false_array (spop (snd (proj1_sig pv)) (fst (proj1_sig pv))) = true.
Proof with curry1.
intros H1.
rewrite (asum_spop _ (snd(proj1_sig pv))) in H1...
apply reverse_bool_eq...
unfold non_false_array in H...
rewrite H in H1...
Qed.

Lemma eval_prim_oPrim_of_oprim_UXb b :
  eval_prim (LddO_OPrim.oPrim_of_oprim (UXb b)) = (fun x y => xorb (b && x) y).
Proof. destruct b; compute; repeat (apply functional_extensionality; intros); curry1. Qed.
Hint Rewrite eval_prim_oPrim_of_oprim_UXb : curry1_rewrite.

Definition vec_of_array {n} (v:array bool n) : option(vec n) :=
  (if non_false_array v as b0 return non_false_array v = b0 -> option(vec n)
  then fun E => Some(exist _ v E)
  else fun _ => None) eq_refl.

Definition axor_vec {n} (v1 v2:array bool n) : option(vec n) := vec_of_array (axor v1 v2).

Definition vec_of_array_bool {n} (a:array bool n) : option(vec n) :=
  (if non_false_array a as b0 return non_false_array a = b0 -> option(vec n)
  then (fun E => Some(exist _ a E))
  else (fun _ => None)) eq_refl.

Definition is_lU_pvec_intro_lU_pvec_comm {n} (pvC pvc:pvec(S(S n))) : @AB bool (pvec(S n)) :=
  let a := spop (snd (proj1_sig pvc)) (lUX_twist_array pvc (fst (proj1_sig pvC))) in
  match vec_of_array_bool a with
  | Some vc' => BB (pvec_of_vec_min vc')
  | None => AA true
  end.

Lemma vec_of_array_bool_eq_Some {n} (a:array bool n) (v:vec n) : vec_of_array_bool a = Some v <-> a = proj1_sig v.
Proof with curry1.
unfold vec_of_array_bool.
fold(vec n).
rewrite_dtt_if...
destruct v as [v Hv]...
split...
Qed.

Lemma non_false_array_fun_false {n} : non_false_array (fun _ : ltN n => false) = false.
Proof. unfold non_false_array; curry1. Qed.
Hint Rewrite @non_false_array_fun_false : curry1_rewrite.

Lemma non_false_array_eq_true {n} a : @non_false_array n a = false <-> a = (fun _ => false).
Proof. unfold non_false_array; curry1. Qed.
Hint Rewrite @non_false_array_eq_true : curry1_rewrite.

Lemma vec_of_array_bool_eq_None {n} (a:array bool n) : vec_of_array_bool a = None <-> a = (fun _ => false).
Proof with curry1.
unfold vec_of_array_bool.
fold(vec n).
rewrite_dtt_if...
split...
Qed.
Hint Rewrite @vec_of_array_bool_eq_None : curry1_rewrite.

Lemma elim_lU_bf_axor_input {n} pv a (f:bf(S(S n))) :
  elim_lP_bf OU pv (axor_input a f) = axor_input (spop (snd (proj1_sig pv)) (lUX_twist_array pv a))
      (fnary_evalu1 (snd (proj1_sig pv)) (lUX_twist_bf pv f) (a (snd (proj1_sig pv)))).
Proof with curry1.
unfold elim_lP_bf...
unfold elim_uP_bf.
rewrite lUX_twist_bf_axor_input.
rewrite fnary_evalu1_axor_input...
setoid_rewrite axor_input_injective.
apply f_equal...
unfold lUX_twist_array...
Qed.

Lemma elim_lX_bf_eq_elim_lU_bf {n} pv (f:bf(S n)) : elim_lP_bf OX pv f = elim_lP_bf OU pv f.
Proof. reflexivity. Qed.

Lemma elim_lX_bf_axor_input {n} pv a (f:bf(S(S n))) :
  elim_lP_bf OX pv (axor_input a f) = axor_input (spop (snd (proj1_sig pv)) (lUX_twist_array pv a))
      (fnary_evalu1 (snd (proj1_sig pv)) (lUX_twist_bf pv f) (a (snd (proj1_sig pv)))).
Proof. rewrite elim_lX_bf_eq_elim_lU_bf. apply elim_lU_bf_axor_input. Qed.

Lemma lUX_twist_bf_intro_lU_bf {n} (pv:pvec _) (f:bf(S n)) :
  lUX_twist_bf pv (intro_lP_bf OU pv f) = intro_uP_bf oU (snd(proj1_sig pv)) f.
Proof. unfold intro_lP_bf; curry1. Qed.
Hint Rewrite @lUX_twist_bf_intro_lU_bf : curry1_rewrite.

Lemma lUX_twist_bf_intro_lX_bf {n} (pv:pvec _) (f:bf(S n)) :
  lUX_twist_bf pv (intro_lP_bf OX pv f) = intro_uP_bf oX (snd(proj1_sig pv)) f.
Proof. unfold intro_lP_bf; curry1. Qed.
Hint Rewrite @lUX_twist_bf_intro_lX_bf : curry1_rewrite.

Lemma axor_input_intro_lU_bf {n} a (pv:pvec _) (f:bf n) :
  axor_input a (intro_lP_bf OU pv f) =
    intro_lP_bf OU pv (axor_input (spop (snd(proj1_sig pv)) (lUX_twist_array pv a)) f).
Proof with curry1.
apply functional_extensionality...
unfold axor_input, intro_lP_bf...
unfold lUX_twist_bf...
unfold intro_uP_bf...
apply f_equal...
unfold lUX_twist_array...
apply functional_extensionality...
unfold ascal...
BPS.MicroBPSolver...
Qed.

Lemma intro_lP_bf_same_prim_injective o {n} (pv:pvec _) (f1 f2:bf n) :
  intro_lP_bf o pv f1 = intro_lP_bf o pv f2 <-> f1 = f2.
Proof. rewrite intro_lP_bf_as_is_lP_bf; curry1. Qed.
Hint Rewrite @intro_lP_bf_same_prim_injective : curry1_rewrite.

Lemma is_lU_bf_intro_lU_bf_comm {n} (pvC pvc:pvec _) (f:bf(S n)) :
  is_lP_bf OU pvC (intro_lP_bf OU pvc f) =
    match is_lU_pvec_intro_lU_pvec_comm pvC pvc with
    | AA b => b
    | BB pvC' => is_lP_bf OU pvC' f
    end.
Proof with curry1.
unfold is_lU_pvec_intro_lU_pvec_comm...
dest_match_step...
- rewrite vec_of_array_bool_eq_Some in D...
  destruct v as [v Hv]...
  rewrite eq_iff_eq_true.
  repeat rewrite rewrite_is_lU_bf...
  rewrite axor_input_intro_lU_bf...
- rewrite (same_vec_implies_same_is_lP_bf _ pvC pvc)...
  destruct pvC as [[aC kC] HC], pvc as [[ac kc] Hc]...
  symmetry in D.
  rewrite (array_expansion_u kc) in D...
  unfold lUX_twist_array in H0...
  rewrite (array_expansion_u kc)...
  symmetry in H0. rewrite axor_shift in H0...
  destruct(aC kc)eqn:E0...
  symmetry in H0...
Qed.

Definition is_lUX_pvec_intro_lUX_pvec_comm {n} (bC bc:bool) (pvC pvc:pvec(S(S n))) : @AB bool (bool * pvec(S n)) :=
  let a := spop (snd (proj1_sig pvc)) (lUX_twist_array pvc (fst (proj1_sig pvC))) in
  let b := xorb bC ( (fst (proj1_sig pvC) (snd(proj1_sig pvc))) && bc) in
  match vec_of_array_bool a with
  | Some vc' => BB (b, pvec_of_vec_min vc')
  | None => AA (eqb bC bc)
  end.

Lemma rewrite_is_lUX_bf (b:bool) {n} (pv:pvec(S n)) (f:bf(S n)) :
  is_lP_bf (UXb b) pv f = true <-> f = xorb_bf b (axor_input(fst(proj1_sig pv)) f).
Proof with curry1.
destruct b...
- apply rewrite_is_lX_bf.
- apply rewrite_is_lU_bf.
Qed.

Lemma lUX_twist_array_axor {n} (pv:pvec _) (x y:array bool(S n)) :
  lUX_twist_array pv (axor x y) = axor (lUX_twist_array pv x) (lUX_twist_array pv y).
Proof with curry1.
destruct pv as [[a k] H].
unfold lUX_twist_array...
rewrite (array_expansion_u k)...
apply functional_extensionality...
unfold ascal...
BPS.MicroBPSolver...
Qed.

(* [REPLACE by axor_input_intro_lUX_bf] *)

Lemma axor_input_intro_lX_bf {n} a (pv:pvec _) (f:bf n) :
  axor_input a (intro_lP_bf OX pv f) =
    intro_lP_bf OX pv
      (xorb_bf (a (snd(proj1_sig pv))) (axor_input (spop (snd(proj1_sig pv)) (lUX_twist_array pv a)) f)).
Proof with curry1.
apply functional_extensionality...
unfold axor_input, intro_lP_bf...
unfold lUX_twist_bf...
rewrite lUX_twist_array_axor...
unfold intro_uP_bf...
unfold xorb_bf, uop_bf...
unfold lUX_twist_array...
Qed.

Lemma axor_input_intro_lUX_bf {n} a b (pv:pvec _) (f:bf n) :
  axor_input a (intro_lP_bf (UXb b) pv f) =
    intro_lP_bf (UXb b) pv
      (xorb_bf (b && (a (snd(proj1_sig pv))))
        (axor_input (spop (snd(proj1_sig pv)) (lUX_twist_array pv a)) f)).
Proof with curry1.
apply functional_extensionality...
unfold axor_input, intro_lP_bf...
unfold lUX_twist_bf...
rewrite lUX_twist_array_axor...
unfold intro_uP_bf...
unfold xorb_bf, uop_bf...
unfold lUX_twist_array...
BPS.MicroBPSolver...
Qed.

Lemma xorb_bf_intro_lUX_bf {n} b1 b2 pv (f:bf n) :
  xorb_bf b1 (intro_lP_bf (UXb b2) pv f) = intro_lP_bf (UXb b2) pv (xorb_bf b1 f).
Proof with curry1.
destruct b1...
unfold intro_lP_bf...
rewrite neg_bf_lUX_twist_bf...
rewrite lUX_twist_bf_injective...
destruct b2...
- rewrite neg_bf_intro_uX_bf...
- rewrite neg_bf_intro_uU_bf...
Qed.

Lemma fnary_evaly1_intro_uUX_bf (b1 b2:bool) {n} (k:ltN(S n)) (f:bf n) :
  fnary_evalu1 k (intro_uP_bf (uxb b1) k f) b2 = xorb_bf (b1 && b2) f.
Proof. destruct b1; curry1. Qed.
Hint Rewrite @fnary_evaly1_intro_uUX_bf : curry1_rewrite.

Lemma is_uP_bf_uxb_intro_uP_bf_uxb_same_k (bC bc:bool) {n} (k:ltN(S n)) f :
  is_uP_bf (uxb bC) k (intro_uP_bf (uxb bc) k f) = eqb bC bc.
Proof with curry1.
rewrite eq_iff_eq_true...
split...
rewrite <- intro_uP_bf_elim_uP_bf_same_eq_same in H.
rewrite (shannon_expansion_u k) in H...
rewrite H0 in H...
Qed.
Hint Rewrite @is_uP_bf_uxb_intro_uP_bf_uxb_same_k : curry1_rewrite.

Lemma spush_k_eq_array_dirac_k {A n} k (a:array A n) (a0 x0 x1:A) :
  spush k a0 a = array_dirac k x0 x1 <-> a0 = x0 /\ a = (fun _ => x1).
Proof. rewrite (array_expansion_u k); curry1. Qed.

Lemma is_lUX_bf_intro_lUX_bf_comm {n} bC bc (pvC pvc:pvec _) (f:bf(S n)) :
  is_lP_bf (UXb bC) pvC (intro_lP_bf (UXb bc) pvc f) =
    match is_lUX_pvec_intro_lUX_pvec_comm bC bc pvC pvc with
    | AA b => b
    | BB b'_pvC' => is_lP_bf (UXb(fst b'_pvC')) (snd b'_pvC') f
    end.
Proof with curry1.
unfold is_lUX_pvec_intro_lUX_pvec_comm...
dest_match_step...
- rewrite vec_of_array_bool_eq_Some in D...
  destruct v as [v Hv]...
  rewrite eq_iff_eq_true.
  repeat rewrite rewrite_is_lUX_bf...
  rewrite axor_input_intro_lUX_bf...
  rewrite xorb_bf_intro_lUX_bf...
  rewrite andb_comm...
- rewrite (same_vec_implies_same_is_lP_bf _ pvC pvc)...
  + unfold is_lP_bf, intro_lP_bf...
  + setoid_rewrite spush_k_eq_array_dirac_k in D...
    rewrite axor_shift in H0...
    destruct pvC as [[aC kC] HC], pvc as [[ac kc] Hc]...
    unfold lUX_twist_array in *...
    rewrite (array_expansion_u kc)...
    destruct(aC kc)eqn:E0...
    symmetry in H0...
Qed.

Definition UX_C : Type := @AB bool (bool * bool).

Definition UX_C_of_oprim (o:oprim) : UX_C :=
  match o with
  | OU => AA false
  | OX => AA true
  | OC if0 th0 => BB(if0, th0)
  end.

Definition oprim_of_UX_C (o:UX_C) : oprim :=
  match o with
  | AA b => if b then OX else OU
  | BB p => OC (fst p) (snd p)
  end.

Lemma UX_C_iff_oprim o1 o2 : UX_C_of_oprim o1 = o2 <-> o1 = oprim_of_UX_C o2.
Proof with curry1.
compute...
dest_match; curry1; try solve_by_invert.
split...
inversion H...
Qed.

Lemma merge_lC_twist_array_comm_Cc_eqK_lemma0 {n}  (pvC pvc:pvec (S n)) :
    let kC := snd(proj1_sig pvC) in
    let aC := fst(proj1_sig pvC) in
    let kc := snd(proj1_sig pvc) in
    let ac := fst(proj1_sig pvc) in
    let ac0 := spush kC false (spop kC ac) in
    pvec_p (axor ac0 aC, kC) = true.
Proof. curry1. Qed.

Definition merge_lC_twist_array_comm_Cc_eqK {n}  (pvC pvc:pvec (S n)) : pvec(S n) :=
    let kC := snd(proj1_sig pvC) in
    let aC := fst(proj1_sig pvC) in
    let kc := snd(proj1_sig pvc) in
    let ac := fst(proj1_sig pvc) in
    let ac0 := spush kC false (spop kC ac) in
    (exist _ (axor ac0 aC, kC) (merge_lC_twist_array_comm_Cc_eqK_lemma0 pvC pvc)).

Lemma merge_lC_twist_array_comm_Cc_eqK_spec0 {n} (pvC pvc:pvec(S n)) (x:array bool(S n)) :
  snd(proj1_sig pvC) = snd(proj1_sig pvc) ->
    lC_twist_array pvC (lC_twist_array pvc x) =
      lC_twist_array (merge_lC_twist_array_comm_Cc_eqK pvc pvC) x.
Proof with curry1.
destruct pvC as [[aC kC] HC], pvc as [[ac kc] Hc]...
unfold lC_twist_array, merge_lC_twist_array_comm_Cc_eqK ...
repeat rewrite adot_spop_both.
repeat rewrite adot_axor_r.
rewrite (array_expansion_u kc)...
repeat rewrite adot_spop_both...
Qed.

Lemma spop_k_spush_ltN_intro_k_k' {n} k k' {A} v (x:array A (S n)) :
  spop k (spush (ltN_intro k k') v x) = spush k' v (spop (ltN_comm_intro k' k) x).
Proof with curry1.
apply functional_extensionality...
unfold spop, spush...
rewrite ltN_pop_ltN_intro_both_same_l.
dest_match_step...
Qed.

Definition can_comm_lC_twist_array {n} (pvC pvc:pvec (S n)) : bool :=
    let kC := snd(proj1_sig pvC) in
    let aC := fst(proj1_sig pvC) in
    let kc := snd(proj1_sig pvc) in
    let ac := fst(proj1_sig pvc) in
    negb(aC kc) && negb(ac kC).

Lemma lC_twist_array_comm {n} (pvC pvc:pvec(S n)) (x:array bool(S n)) :
  can_comm_lC_twist_array pvC pvc = true ->
  lC_twist_array pvC (lC_twist_array pvc x) =
    lC_twist_array pvc (lC_twist_array pvC x).
Proof with curry1.
destruct pvC as [[aC kC] HC], pvc as [[ac kc] Hc]...
unfold lC_twist_array, can_comm_lC_twist_array in *...
destruct(ltN_pop kC kc) as [|kc'] eqn:E1...
repeat rewrite adot_spop_both.
rewrite (array_expansion_u kC)...
destruct n...
repeat rewrite spop_k_spush_ltN_intro_k_k'...
Qed.

Lemma can_comm_lC_twist_array_comm {n} (pvC pvc:pvec(S n)) :
  can_comm_lC_twist_array pvC pvc = can_comm_lC_twist_array pvc pvC.
Proof. unfold can_comm_lC_twist_array; curry1. Qed.

Lemma intro_uP_bf_injective o {n} k (f1 f2:bf n) : intro_uP_bf o k f1 = intro_uP_bf o k f2 <-> f1 = f2.
Proof. rewrite intro_uP_bf_as_is_uP_bf; curry1. Qed.
Hint Rewrite @intro_uP_bf_injective : curry1_rewrite.

Lemma generalize_array_maxsat_true {n} (p:ltN n -> bool) H (CTX:ltN n -> Prop) :
  CTX(array_maxsat_true p H) <-> (forall v, array_maxsat p = Some v -> CTX v).
Proof with curry1.
unfold array_maxsat_true.
fold(ltN n).
pattern(match array_maxsat p as mp return (array_maxsat p = mp -> ltN n) with
   | Some k => fun _ : array_maxsat p = Some k => k
   | None => fun E : array_maxsat p = None =>
    match array_maxsat_true_lemma1 p H E return (ltN n) with end
   end eq_refl).
rewrite match_option_dtt...
split...
dest_match_step...
Qed.

Lemma rewrite_is_lC_bf_v2 if0 th0 {n} pv (f:bf(S n)) :
  is_lP_bf (OC if0 th0) pv f =
    is_cst_bf true (fun x => impb((eqb if0 (adot(fst(proj1_sig pv)) x)))(eqb th0 (f x))).
Proof with curry1.
rewrite eq_iff_eq_true.
rewrite rewrite_is_lC_bf.
unfold is_cst_bf.
rewrite (beq_bf_iff_true _ _).
rewrite (@functional_extensionality_iff (array bool (S n)) bool).
split; intros H x; specialize(H x)...
destruct(xorb if0 (adot (fst (proj1_sig pv)) x))eqn:E0...
Qed.

Lemma pvec_of_array_max_lemma0 {n} (a:array bool n) k :
  array_maxsat a = Some k -> pvec_p (a, k) = true.
Proof with curry1.
rewrite (maxsat_alternative_array_maxsat _).
unfold is_max_positive...
Qed.

Definition pvec_of_array_max {n} (a:array bool n) : option(pvec n) :=
match array_maxsat a as r return array_maxsat a = r -> option(pvec n) with
| Some k => fun E => @Some (pvec n) (exist _ (a, k) (pvec_of_array_max_lemma0 a k E))
| None   => fun _ => None
end eq_refl.

Definition is_lC_pvec_intro_lC_pvec_comm_same_th0
  {n} (ifC ifc:bool) (pvC pvc:pvec(S(S n))) : @AB bool (bool * pvec(S n)) :=
  let aC := fst(proj1_sig pvC) in
  let kc := snd(proj1_sig pvc) in
  let ac := fst(proj1_sig pvc) in
  match pvec_of_array_max (spop kc (axor aC (ascal (aC kc) ac))) with
  | Some pvC' => (
    let ifC' := xorb ifC ( aC kc && (negb ifc) ) in
    @BB bool (bool * (pvec(S n))) (ifC', pvC')
  )
  | None => (@AA bool (bool * (pvec(S n))) (eqb ifC ifc))
  end.

Lemma pvec_of_array_max_eq_Some {n} (a:array bool n) (pv:pvec n) :
  pvec_of_array_max a = Some pv <->
    fst(proj1_sig pv) = a /\ is_max_positive cmp_ltN a (snd(proj1_sig pv)).
Proof with curry1.
unfold pvec_of_array_max.
unfold pvec.
pattern(match array_maxsat a as r return (array_maxsat a = r -> option {ak : array bool n * ltN n | pvec_p ak = true}) with
| Some k =>
    fun E : array_maxsat a = Some k =>
    Some (exist (fun ak : array bool n * ltN n => pvec_p ak = true) (a, k) (pvec_of_array_max_lemma0 a k E))
| None => fun _ : array_maxsat a = None => None
end eq_refl).
rewrite match_option_dtt.
split...
- rewrite (maxsat_alternative_array_maxsat _ _) in p...
  unfold is_max_positive in H0...
  specialize(f_equal(fun f => f(snd(proj1_sig pv)))p)...
- rewrite (maxsat_alternative_array_maxsat _ _) in p...
  rename v into k.
  destruct pv as [[a' k'] Hpv]...
  rewrite eq_sym_iff...
  split...
  apply(is_max_positive_unique _ (cmp_P_cmp_ltN _) _ _ _ p H).
Qed.

Lemma pvec_of_array_max_eq_None {n} (a:array bool n) : pvec_of_array_max a = None <-> a = (fun _ => false).
Proof with curry1.
unfold pvec_of_array_max.
unfold pvec.
pattern(match array_maxsat a as r return (array_maxsat a = r -> option {ak : array bool n * ltN n | pvec_p ak = true}) with
| Some k =>
    fun E : array_maxsat a = Some k =>
    Some (exist (fun ak : array bool n * ltN n => pvec_p ak = true) (a, k) (pvec_of_array_max_lemma0 a k E))
| None => fun _ : array_maxsat a = None => None
end eq_refl).
rewrite match_option_dtt.
split...
- rewrite (maxsat_alternative_array_maxsat _) in p...
- rewrite (maxsat_alternative_array_maxsat _) in p...
Qed.

Lemma is_lC_pvec_intro_lC_pvec_comm_same_th0_eq_AA {n} (ifC ifc:bool) (pvC pvc:pvec(S(S n))) b :
  is_lC_pvec_intro_lC_pvec_comm_same_th0 ifC ifc pvC pvc = AA b <->
    fst(proj1_sig pvC) = fst(proj1_sig pvc) /\ b = eqb ifC ifc.
Proof with curry1.
unfold is_lC_pvec_intro_lC_pvec_comm_same_th0.
destruct pvC as [[aC kC] HC], pvc as [[ac kc] Hc]...
dest_match_step...
- rewrite pvec_of_array_max_eq_Some in D...
- rewrite pvec_of_array_max_eq_None in D...
  rewrite axor_shift in D...
  rewrite (eq_sym_iff _ _)...
  rewrite (array_expansion_u kc)...
  destruct(aC kc)eqn:E0...
  destruct(ltN_pop kc kC) as [|kC'] eqn:E1...
  specialize(f_equal(fun f => f kC')D)...
  unfold spop in H...
Qed.

Lemma is_lC_pvec_intro_lC_pvec_comm_same_th0_eq_BB {n} (ifC ifc:bool) (pvC pvc:pvec(S(S n))) (out:(bool*(pvec(S n)))) :
  let ifC' := fst out in
  let pvC' := snd out in
  let aC' := fst(proj1_sig pvC') in
  let kC' := snd(proj1_sig pvC') in
  let aC := fst(proj1_sig pvC) in
  let kc := snd(proj1_sig pvc) in
  let ac := fst(proj1_sig pvc) in
  is_lC_pvec_intro_lC_pvec_comm_same_th0 ifC ifc pvC pvc = BB out <->
    aC' = spop kc (axor aC (ascal (aC kc) ac)) /\
    ifC' = xorb ifC (aC kc && negb ifc) /\
    is_max_positive cmp_ltN aC' kC'.
Proof with curry1.
unfold is_lC_pvec_intro_lC_pvec_comm_same_th0.
destruct out as [ifC' pvC'].
destruct pvC as [[aC kC] HC], pvc as [[ac kc] Hc], pvC' as [[aC' kC'] HC'].
simpl.
dest_match_step...
- rewrite pvec_of_array_max_eq_Some in D...
  destruct p as [[ap kp] Hp]...
  split...
  apply(is_max_positive_unique _ (cmp_P_cmp_ltN _) _ _ _ H0 H1).
- rewrite pvec_of_array_max_eq_None in D...
  unfold is_max_positive in H0...
  specialize(f_equal(fun f => f kC')D)...
  rewrite H in HC'...
Qed.

(* $2021-06-25 : done intro_lU_bf_intro_lU_bf *)
(* $2021-06-30 : done intro_lC_bf_intro_lC_bf_same_th0 *)
Lemma is_lC_bf_intro_lC_bf_comm_same_th0 ifC ifc th0 {n} (pvC pvc:pvec _) (f:bf(S n)) :
  is_lP_bf (OC ifC th0) pvC (intro_lP_bf (OC ifc th0) pvc f) =
    match extract_cst f with
    | Some f0 => (eqb th0 f0) || (eqb ifC ifc && beq_array_bool (fst(proj1_sig pvC)) (fst(proj1_sig pvc)))
    | None =>
      match is_lC_pvec_intro_lC_pvec_comm_same_th0 ifC ifc pvC pvc with
      | AA b => b
      | BB b'_pvC' => is_lP_bf (OC (fst b'_pvC') th0) (snd b'_pvC') f
      end
    end.
Proof with curry1.
dest_match_step...
- dest_match_step...
  destruct pvC as [[aC kC] HC], pvc as [[ac kc] Hc]...
  rewrite eq_iff_eq_true...
  repeat rewrite andb_true_iff...
  rewrite (array_expansion_u kC)...
  destruct(ac kC)eqn:E0...
  rewrite eq_sym_iff...
  BPS.MicroBPSolver...
- dest_match_step...
  + rewrite is_lC_pvec_intro_lC_pvec_comm_same_th0_eq_AA in D0...
    rewrite (same_vec_implies_same_is_lP_bf _ _ _ _ H)...
    rewrite VAX.andb_eq_l...
    apply reverse_bool_eq...
  + rewrite is_lC_pvec_intro_lC_pvec_comm_same_th0_eq_BB in D0...
    destruct pvC as [[aC kC] HC], pvc as [[ac kc] Hc]...
    destruct p as [ifC' [[aC' kC'] HC']]...
    unfold axor, ascal in HC'...
    rewrite eq_iff_eq_true...
    repeat rewrite rewrite_is_lC_bf...
    repeat rewrite spop_eval in HC'...
    split...
    * rewrite adot_axor_l in H0...
      rewrite adot_ascal in H0...
      specialize(H(spush kc false x)) as H0x...
      specialize(H(spush kc true  x)) as H1x...
      apply reverse_bool_eq...
      rewrite H2 in *...
      generalize dependent H0x.
      generalize dependent H1x.
      generalize dependent H0.
      BPS.MicroBPSolver...
    * dest_match_step...
      specialize(H(spop kc x)) as Hx...
      rewrite adot_axor_l, adot_ascal in Hx...
      apply reverse_bool_eq...
      rewrite H0 in *...
      generalize dependent Hx.
      repeat rewrite adot_spop_both.
      BPS.MicroBPSolver...
      destruct(aC kc)eqn:E0...
Qed.
