
type unit0 =
| Tt

type bool =
| True
| False

(** val xorb : bool -> bool -> bool **)

let xorb b1 b2 =
  match b1 with
  | True -> (match b2 with
             | True -> False
             | False -> True)
  | False -> b2

(** val negb : bool -> bool **)

let negb = function
| True -> False
| False -> True

type nat =
| O
| S of nat

type 'a option =
| Some of 'a
| None

type ('a, 'b) prod =
| Pair of 'a * 'b

(** val fst : ('a1, 'a2) prod -> 'a1 **)

let fst = function
| Pair (x, _) -> x

(** val snd : ('a1, 'a2) prod -> 'a2 **)

let snd = function
| Pair (_, y) -> y

type 'a sig0 = 'a
  (* singleton inductive, whose constructor was exist *)

(** val pred : nat -> nat **)

let pred n = match n with
| O -> n
| S u -> u

(** val eqb : bool -> bool -> bool **)

let eqb b1 b2 =
  match b1 with
  | True -> b2
  | False -> (match b2 with
              | True -> False
              | False -> True)

module Nat =
 struct
  (** val eqb : nat -> nat -> bool **)

  let rec eqb n m =
    match n with
    | O -> (match m with
            | O -> True
            | S _ -> False)
    | S n' -> (match m with
               | O -> False
               | S m' -> eqb n' m')

  (** val leb : nat -> nat -> bool **)

  let rec leb n m =
    match n with
    | O -> True
    | S n' -> (match m with
               | O -> False
               | S m' -> leb n' m')

  (** val ltb : nat -> nat -> bool **)

  let ltb n m =
    leb (S n) m
 end

(** val solution_left : 'a1 -> 'a2 -> 'a1 -> 'a2 **)

let solution_left _ x _ =
  x

(** val comp : ('a1 -> 'a2) -> ('a2 -> 'a3) -> 'a1 -> 'a3 **)

let comp f g x =
  g (f x)

(** val opmap : ('a1 -> 'a2) -> 'a1 option -> 'a2 option **)

let opmap f = function
| Some a -> Some (f a)
| None -> None

(** val isSome : 'a1 option -> bool **)

let isSome = function
| Some _ -> True
| None -> False

(** val isNone : 'a1 option -> bool **)

let isNone o =
  negb (isSome o)

type ord =
| Lt
| Eq
| Gt

type 'a cmp = 'a -> 'a -> ord

(** val cmp_comp : ord -> ord -> ord **)

let cmp_comp x1 x2 =
  match x1 with
  | Eq -> x2
  | x -> x

(** val cmp_bool : bool cmp **)

let cmp_bool b1 b2 =
  match b1 with
  | True -> (match b2 with
             | True -> Eq
             | False -> Gt)
  | False -> (match b2 with
              | True -> Lt
              | False -> Eq)

(** val cmp_prod :
    'a1 cmp -> 'a2 cmp -> ('a1, 'a2) prod -> ('a1, 'a2) prod -> ord **)

let cmp_prod cA cB x y =
  cmp_comp (cA (fst x) (fst y)) (cB (snd x) (snd y))

type 'a minsat_t = ('a -> bool) -> 'a option

(** val minsat_prod :
    'a1 minsat_t -> 'a2 minsat_t -> (('a1, 'a2) prod -> bool) -> ('a1, 'a2)
    prod option **)

let minsat_prod minsat_A minsat_B p =
  match minsat_A (fun a -> isSome (minsat_B (fun b -> p (Pair (a, b))))) with
  | Some a0 ->
    (match minsat_B (fun b -> p (Pair (a0, b))) with
     | Some b0 -> Some (Pair (a0, b0))
     | None -> None)
  | None -> None

(** val cmp_sig : 'a1 cmp -> 'a1 cmp **)

let cmp_sig cmpA =
  cmpA

(** val cmp_nat : nat -> nat -> ord **)

let rec cmp_nat x y =
  match x with
  | O -> (match y with
          | O -> Eq
          | S _ -> Lt)
  | S x0 -> (match y with
             | O -> Gt
             | S y0 -> cmp_nat x0 y0)

type ('a, 'b) aB =
| AA of 'a
| BB of 'b

type ltN = nat

(** val ltN_of_nat : nat -> nat -> ltN **)

let ltN_of_nat k _ =
  k

(** val ltN_S : nat -> ltN -> ltN **)

let ltN_S n k =
  ltN_of_nat (S k) (S n)

(** val ltN_intro : nat -> ltN -> ltN -> ltN **)

let ltN_intro n k x =
  match Nat.ltb x k with
  | True -> ltN_of_nat x (S n)
  | False -> ltN_of_nat (S x) (S n)

(** val ltN_pop : nat -> ltN -> ltN -> (unit0, ltN) aB **)

let ltN_pop n k x =
  match Nat.eqb x k with
  | True -> AA Tt
  | False ->
    BB
      (match Nat.ltb x k with
       | True -> ltN_of_nat x n
       | False -> ltN_of_nat (pred x) n)

(** val ltN_pred : nat -> ltN -> ltN option **)

let ltN_pred n = function
| O -> None
| S k0 -> Some (ltN_of_nat k0 n)

(** val cmp_ltN : nat -> ltN cmp **)

let cmp_ltN _ =
  cmp_sig cmp_nat

type 'a array = ltN -> 'a

(** val sempty : 'a1 array **)

let sempty _ =
  assert false (* absurd case *)

(** val scons : nat -> 'a1 -> 'a1 array -> 'a1 array **)

let scons n a s k =
  match ltN_pred n k with
  | Some k' -> s k'
  | None -> a

(** val stail : nat -> 'a1 array -> 'a1 array **)

let stail n s k =
  s (ltN_S n k)

(** val spop : nat -> ltN -> 'a1 array -> 'a1 array **)

let spop n k s x =
  s (ltN_intro n k x)

(** val spush : nat -> ltN -> 'a1 -> 'a1 array -> 'a1 array **)

let spush n k v s x =
  match ltN_pop n k x with
  | AA _ -> v
  | BB x0 -> s x0

(** val array_minsat : nat -> bool array -> ltN option **)

let rec array_minsat n a =
  match n with
  | O -> None
  | S n0 ->
    let k0 = ltN_of_nat O (S n0) in
    (match a k0 with
     | True -> Some k0
     | False -> opmap (ltN_S n0) (array_minsat n0 (stail n0 a)))

(** val minsat_map : ('a1 -> 'a2) -> 'a1 minsat_t -> 'a2 minsat_t **)

let minsat_map map minsatA p =
  opmap map (minsatA (comp map p))

type ('a, 'b) fnary = 'a array -> 'b

(** val fnary_evalo1 : nat -> ('a1, 'a2) fnary -> 'a1 -> ('a1, 'a2) fnary **)

let fnary_evalo1 n f a s =
  f (scons n a s)

(** val fnary_evalu1 :
    nat -> ltN -> ('a1, 'a2) fnary -> 'a1 -> ('a1, 'a2) fnary **)

let fnary_evalu1 n k f v a =
  f (spush n k v a)

type bf = (bool, bool) fnary

(** val beq_bf : nat -> bf -> bf -> bool **)

let rec beq_bf n f4 f5 =
  match n with
  | O -> eqb (f4 sempty) (f5 sempty)
  | S n0 ->
    (match beq_bf n0 (fnary_evalo1 n0 f4 False) (fnary_evalo1 n0 f5 False) with
     | True -> beq_bf n0 (fnary_evalo1 n0 f4 True) (fnary_evalo1 n0 f5 True)
     | False -> False)

(** val bop_bf : (bool -> bool -> bool) -> nat -> bf -> bf -> bf **)

let bop_bf m _ f4 f5 a =
  m (f4 a) (f5 a)

(** val cst_bf : nat -> bool -> bf **)

let cst_bf _ b _ =
  b

(** val is_cst_bf : bool -> nat -> bf -> bool **)

let is_cst_bf b n f =
  beq_bf n f (cst_bf n b)

(** val piN : nat -> ltN -> bool -> bf **)

let piN _ k b x =
  xorb b (x k)

type 'edge ldd =
| Leaf of nat * 'edge
| Edge of nat * nat * 'edge * 'edge ldd * 'edge ldd

type 'f node =
| Node of nat * 'f * 'f

type 'f next =
| Term
| Next of nat * 'f

type ('edge, 'f) xedge =
| XEdge of nat option * nat * 'edge * 'f node next

(** val xedge_projN : nat -> ('a1, 'a2) xedge -> nat option **)

let xedge_projN _ = function
| XEdge (opn, _, _, _) -> opn

(** val xedge_projE : nat -> ('a1, 'a2) xedge -> 'a1 **)

let xedge_projE _ = function
| XEdge (_, _, ee, _) -> ee

(** val xedge_projF : nat -> ('a1, 'a2) xedge -> 'a2 node next **)

let xedge_projF _ = function
| XEdge (_, _, _, nx) -> nx

(** val extract_ldd :
    (nat -> bf -> ('a1, bf) xedge) -> nat -> bf -> 'a1 ldd **)

let rec extract_ldd extract_edge0 n f =
  let XEdge (_, m0, ee, nx) = extract_edge0 n f in
  (match nx with
   | Term -> Leaf (m0, ee)
   | Next (_, f') ->
     let Node (n'', f'0, f'1) = f' in
     let ldd0 = extract_ldd extract_edge0 n'' f'0 in
     let ldd4 = extract_ldd extract_edge0 n'' f'1 in
     Edge (n'', m0, ee, ldd0, ldd4))

type 'edge lDD = 'edge ldd

(** val extract :
    (nat -> bf -> ('a1, bf) xedge) -> (nat option -> nat -> 'a1 -> bf next ->
    bf) -> nat -> bf -> 'a1 lDD **)

let extract extract_edge0 _ n f =
  extract_ldd extract_edge0 n f

type oPrim = ((bool, bool) prod, bool) prod

(** val oU : oPrim **)

let oU =
  Pair ((Pair (True, True)), False)

(** val oX : oPrim **)

let oX =
  Pair ((Pair (True, True)), True)

(** val oC : bool -> bool -> oPrim **)

let oC if0 th0 =
  Pair ((Pair ((negb if0), if0)), th0)

(** val detect_prim : oPrim -> bool -> bool -> bool **)

let detect_prim p x0 x1 =
  eqb
    (xorb (match fst (fst p) with
           | True -> x0
           | False -> False)
      (match snd (fst p) with
       | True -> x1
       | False -> False)) (snd p)

(** val eval_prim : oPrim -> bool -> bool -> bool **)

let eval_prim p x y =
  let Pair (p0, py) = p in
  let Pair (px0, px1) = p0 in
  (match xorb px0 px1 with
   | True -> (match eqb x px1 with
              | True -> py
              | False -> y)
   | False -> xorb y (match x with
                      | True -> py
                      | False -> False))

(** val rev_prim : oPrim -> bool **)

let rev_prim = function
| Pair (p0, _) ->
  let Pair (px0, px1) = p0 in
  (match xorb px0 px1 with
   | True -> negb px1
   | False -> False)

type oprim =
| OU
| OX
| OC of bool * bool

(** val oPrim_of_oprim : oprim -> oPrim **)

let oPrim_of_oprim = function
| OU -> oU
| OX -> oX
| OC (if0, th0) -> oC if0 th0

(** val pi_oprim : ltN -> oprim **)

let pi_oprim = function
| O -> OU
| S n ->
  (match n with
   | O -> OX
   | S n0 ->
     (match n0 with
      | O -> OC (False, False)
      | S n1 ->
        (match n1 with
         | O -> OC (True, False)
         | S n2 ->
           (match n2 with
            | O -> OC (False, True)
            | S _ -> OC (True, True)))))

(** val cmp_oprim : oprim -> oprim -> ord **)

let cmp_oprim x y =
  match x with
  | OU -> (match y with
           | OU -> Eq
           | _ -> Lt)
  | OX -> (match y with
           | OU -> Gt
           | OX -> Eq
           | OC (_, _) -> Lt)
  | OC (if0, th0) ->
    (match y with
     | OC (if1, th1) -> cmp_comp (cmp_bool th0 th1) (cmp_bool if0 if1)
     | _ -> Gt)

(** val minsat_oprim : oprim minsat_t **)

let minsat_oprim =
  minsat_map pi_oprim (array_minsat (S (S (S (S (S (S O)))))))

type ('term, 'prim, 'f) xprim =
| XTerm of nat * 'term
| XPrim of nat * 'prim * 'f
| XSha of nat * 'f * 'f

type ('term, 'prim) trans =
| ETerm of nat * 'term
| EPrim of nat option * nat * 'prim * ('term, 'prim) trans
| ENext of nat

(** val sem_trans :
    (nat -> 'a1 -> bf) -> (nat -> 'a2 -> bf -> bf) -> nat option -> nat ->
    ('a1, 'a2) trans -> bf next -> bf **)

let rec sem_trans sem_term0 sem_prim0 _ _ e f0 =
  match e with
  | ETerm (n, t) -> sem_term0 n t
  | EPrim (o, n, p, e0) ->
    sem_prim0 n p (sem_trans sem_term0 sem_prim0 o n e0 f0)
  | ENext n ->
    (match f0 with
     | Term -> assert false (* absurd case *)
     | Next (n0, f) -> solution_left (S n) (fun f4 -> f4) n0 f)

type ('term, 'prim, 'f) xtrans = (('term, 'prim) trans, 'f) xedge

(** val xTrans :
    nat option -> nat -> ('a1, 'a2) trans -> 'a3 node next -> (('a1, 'a2)
    trans, 'a3) xedge **)

let xTrans opn m ee nx =
  XEdge (opn, m, ee, nx)

(** val xTPrim :
    nat -> 'a2 -> ('a1, 'a2, 'a3) xtrans -> ('a1, 'a2, 'a3) xtrans **)

let xTPrim n p xe =
  xTrans (xedge_projN n xe) (S n) (EPrim ((xedge_projN n xe), n, p,
    (xedge_projE n xe))) (xedge_projF n xe)

(** val extract_trans :
    (nat -> bf -> ('a1, 'a2, bf) xprim) -> nat -> bf -> ('a1, 'a2, bf) xtrans **)

let rec extract_trans extract_xprim0 n f =
  match extract_xprim0 n f with
  | XTerm (n0, t) -> xTrans None n0 (ETerm (n0, t)) Term
  | XPrim (n0, p, xf) -> xTPrim n0 p (extract_trans extract_xprim0 n0 xf)
  | XSha (n0, f0, f4) ->
    xTrans (Some (S n0)) (S n0) (ENext n0) (Next ((S n0), (Node (n0, f0,
      f4))))

type ('term, 'prim) edge = ('term, 'prim) trans

type ('term, 'prim, 'f) xedge0 = (('term, 'prim) edge, 'f) xedge

(** val xEdge :
    (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> (nat -> 'a2 -> bf ->
    bf) -> (nat -> 'a2 -> bf -> bf option) -> (nat -> 'a2 -> 'a2 -> ord
    option) -> (nat -> bf -> bool) -> nat option -> nat -> ('a1, 'a2) edge ->
    'a3 node next -> (('a1, 'a2) edge, 'a3) xedge **)

let xEdge _ _ _ _ _ _ opn m ee nx =
  XEdge (opn, m, ee, nx)

(** val extract_edge :
    (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> (bf -> 'a1) -> (nat ->
    'a2 -> bf -> bf) -> (nat -> 'a2 -> bf -> bf option) -> (nat -> 'a2 -> 'a2
    -> ord option) -> (nat -> bf -> bool) -> (nat -> bf -> ('a1, 'a2, bf)
    xprim) -> nat -> bf -> ('a1, 'a2, bf) xedge0 **)

let extract_edge sem_term0 extract_term0 _ sem_prim0 extract_prim0 pcmp_prim0 is_prim_free0 extract_xprim0 n f =
  let xt = extract_trans extract_xprim0 n f in
  xEdge sem_term0 extract_term0 sem_prim0 extract_prim0 pcmp_prim0
    is_prim_free0 (xedge_projN n xt) n (xedge_projE n xt) (xedge_projF n xt)

(** val sem_edge :
    (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> (nat -> 'a2 -> bf ->
    bf) -> (nat -> 'a2 -> bf -> bf option) -> (nat -> 'a2 -> 'a2 -> ord
    option) -> (nat -> bf -> bool) -> nat option -> nat -> ('a1, 'a2) edge ->
    bf next -> bf **)

let sem_edge sem_term0 _ sem_prim0 _ _ _ opn m e nx =
  sem_trans sem_term0 sem_prim0 opn m e nx

type ('term, 'prim) lDD0 = ('term, 'prim) edge lDD

(** val extract0 :
    (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> (bf -> 'a1) -> (nat ->
    'a2 -> bf -> bf) -> (nat -> 'a2 -> bf -> bf option) -> (nat -> 'a2 -> 'a2
    -> ord option) -> (nat -> bf -> bool) -> (nat -> bf -> ('a1, 'a2, bf)
    xprim) -> nat -> bf -> ('a1, 'a2) lDD0 **)

let extract0 sem_term0 extract_term0 term_of_bool0 sem_prim0 extract_prim0 pcmp_prim0 is_prim_free0 extract_xprim0 =
  extract
    (extract_edge sem_term0 extract_term0 term_of_bool0 sem_prim0
      extract_prim0 pcmp_prim0 is_prim_free0 extract_xprim0)
    (sem_edge sem_term0 extract_term0 sem_prim0 extract_prim0 pcmp_prim0
      is_prim_free0)

(** val intro_uP_bf : oPrim -> nat -> ltN -> bf -> bf **)

let intro_uP_bf p n k f a =
  eval_prim p (a k) (f (spop n k a))

(** val elim_uP_bf : oPrim -> nat -> ltN -> bf -> bf **)

let elim_uP_bf p n k f =
  fnary_evalu1 n k f (rev_prim p)

(** val is_uP_bf : oPrim -> nat -> ltN -> bf -> bool **)

let is_uP_bf p n k f =
  is_cst_bf True n
    (bop_bf (detect_prim p) n (fnary_evalu1 n k f False)
      (fnary_evalu1 n k f True))

(** val extract_cst : nat -> bf -> bool option **)

let extract_cst n f =
  let f0 = f (fun _ -> False) in
  (match is_cst_bf f0 n f with
   | True -> Some f0
   | False -> None)

(** val extract_piN_bool : nat -> bool -> bf -> ltN option **)

let extract_piN_bool n b f =
  array_minsat n (fun k -> beq_bf n (piN n k b) f)

(** val extract_piN : nat -> bf -> (ltN, bool) prod option **)

let extract_piN n f =
  let f0 = f (fun _ -> False) in
  (match extract_piN_bool n f0 f with
   | Some k -> Some (Pair (k, f0))
   | None -> None)

type term =
| Cst of nat * bool
| PiN of nat * ltN * bool

(** val sem_term : nat -> term -> bf **)

let sem_term _ = function
| Cst (n, b) -> cst_bf n b
| PiN (n, k, b) -> piN n k b

(** val extract_term : nat -> bf -> term option **)

let extract_term n f =
  match extract_cst n f with
  | Some b -> Some (Cst (n, b))
  | None ->
    (match extract_piN n f with
     | Some kb -> Some (PiN (n, (fst kb), (snd kb)))
     | None -> None)

(** val term_of_bool : bf -> term **)

let term_of_bool f =
  Cst (O, (f (fun _ -> False)))

type prim = (oprim, ltN) prod

(** val sem_prim : nat -> prim -> bf -> bf **)

let sem_prim n p f =
  intro_uP_bf (oPrim_of_oprim (fst p)) n (snd p) f

(** val detect_prim0 : nat -> prim -> bf -> bool **)

let detect_prim0 n p f =
  is_uP_bf (oPrim_of_oprim (fst p)) n (snd p) f

(** val elim_prim : nat -> prim -> bf -> bf **)

let elim_prim n p f =
  elim_uP_bf (oPrim_of_oprim (fst p)) n (snd p) f

(** val extract_prim : nat -> prim -> bf -> bf option **)

let extract_prim n p f =
  match detect_prim0 n p f with
  | True -> Some (elim_prim n p f)
  | False -> None

(** val cmp_prim : nat -> prim -> prim -> ord **)

let cmp_prim n =
  cmp_prod cmp_oprim (cmp_ltN n)

(** val pcmp_prim : nat -> prim -> prim -> ord option **)

let pcmp_prim n x y =
  Some (cmp_prim n x y)

(** val minsat_prim : nat -> (prim -> bool) -> prim option **)

let minsat_prim n p =
  minsat_prod minsat_oprim (array_minsat n) p

(** val minsat_prim_uP_bf : nat -> bf -> prim option **)

let minsat_prim_uP_bf n f =
  minsat_prim (S n) (fun p -> detect_prim0 n p f)

(** val is_prim_free : nat -> bf -> bool **)

let is_prim_free n f =
  match n with
  | O -> True
  | S n' -> isNone (minsat_prim_uP_bf n' f)

type 'f xprim0 = (term, prim, 'f) xprim

(** val extract_xprim : nat -> bf -> bf xprim0 **)

let extract_xprim n f =
  match n with
  | O -> XTerm (O, (term_of_bool f))
  | S n' ->
    (match extract_term (S n') f with
     | Some t -> XTerm ((S n'), t)
     | None ->
       (match minsat_prim_uP_bf n' f with
        | Some p -> XPrim (n', p, (elim_prim n' p f))
        | None ->
          XSha (n', (fnary_evalo1 n' f False), (fnary_evalo1 n' f True))))

type lDD1 = (term, prim) lDD0

(** val extract1 : nat -> bf -> lDD1 **)

let extract1 =
  extract0 sem_term extract_term term_of_bool sem_prim extract_prim pcmp_prim
    is_prim_free extract_xprim

(** val f1 : bf **)

let f1 x =
  let x0 = x (ltN_of_nat O (S (S (S (S O))))) in
  let x1 = x (ltN_of_nat (S O) (S (S (S (S O))))) in
  let x2 = x (ltN_of_nat (S (S O)) (S (S (S (S O))))) in
  let x3 = x (ltN_of_nat (S (S (S O))) (S (S (S (S O))))) in
  xorb x0 (xorb x1 (xorb x2 x3))

(** val ldd1 : lDD1 **)

let ldd1 =
  extract1 (S (S (S (S O)))) f1

(** val f2 : bf **)

let f2 x =
  let x0 = x (ltN_of_nat O (S (S (S (S (S (S (S (S (S (S O))))))))))) in
  let x1 = x (ltN_of_nat (S O) (S (S (S (S (S (S (S (S (S (S O))))))))))) in
  let x2 = x (ltN_of_nat (S (S O)) (S (S (S (S (S (S (S (S (S (S O)))))))))))
  in
  let x3 =
    x (ltN_of_nat (S (S (S O))) (S (S (S (S (S (S (S (S (S (S O)))))))))))
  in
  let x4 =
    x (ltN_of_nat (S (S (S (S O)))) (S (S (S (S (S (S (S (S (S (S O)))))))))))
  in
  let x5 =
    x
      (ltN_of_nat (S (S (S (S (S O))))) (S (S (S (S (S (S (S (S (S (S
        O)))))))))))
  in
  let x6 =
    x
      (ltN_of_nat (S (S (S (S (S (S O)))))) (S (S (S (S (S (S (S (S (S (S
        O)))))))))))
  in
  let x7 =
    x
      (ltN_of_nat (S (S (S (S (S (S (S O))))))) (S (S (S (S (S (S (S (S (S (S
        O)))))))))))
  in
  let x8 =
    x
      (ltN_of_nat (S (S (S (S (S (S (S (S O)))))))) (S (S (S (S (S (S (S (S
        (S (S O)))))))))))
  in
  let x9 =
    x
      (ltN_of_nat (S (S (S (S (S (S (S (S (S O))))))))) (S (S (S (S (S (S (S
        (S (S (S O)))))))))))
  in
  xorb x0
    (xorb x1
      (xorb x2 (xorb x3 (xorb x4 (xorb x5 (xorb x6 (xorb x7 (xorb x8 x9))))))))

(** val ldd2 : lDD1 **)

let ldd2 =
  extract1 (S (S (S (S (S (S (S (S (S (S O)))))))))) f2

(** val f3 : bf **)

let f3 x =
  let x1 = x (ltN_of_nat (S O) (S (S (S (S (S (S (S (S (S (S O))))))))))) in
  let x2 = x (ltN_of_nat (S (S O)) (S (S (S (S (S (S (S (S (S (S O)))))))))))
  in
  let x3 =
    x (ltN_of_nat (S (S (S O))) (S (S (S (S (S (S (S (S (S (S O)))))))))))
  in
  let x4 =
    x (ltN_of_nat (S (S (S (S O)))) (S (S (S (S (S (S (S (S (S (S O)))))))))))
  in
  (match negb x1 with
   | True -> xorb x3 (match x2 with
                      | True -> x4
                      | False -> False)
   | False -> False)

(** val ldd3 : lDD1 **)

let ldd3 =
  extract1 (S (S (S (S (S (S (S (S (S (S O)))))))))) f3
