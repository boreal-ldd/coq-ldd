Require Import Arith .
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.

Require Import Ground.SimplProp.
Require Import Ground.SimplBool.
Require Import Ground.SimplEq.
Require Import Ground.SimplCmp.
Require Import Ground.SimplSimpl.

Require Import Ground.SimplNat.
Require Import Ground.SimplAB.
Require Import Ground.SimplList.
Require Import Ground.SimplABList.
Require Import Ground.SimplMore.

Require Import MiniBool_Logic.

Require Import Ground.SimplDTT.
Require Import FiniteCurryfy.
Require Import FiniteBooleanFunction.

Require Import LddO_Primitives.

Section LddO.

  Definition has_oCY_bf (th0:bool) {n} (f:bf(S n)) : bool :=
    is_oC_bf false th0 f || is_oC_bf true th0 f.

  Definition has_oC_bf {n} (f:bf(S n)) : bool :=
    has_oCY_bf false f || has_oCY_bf true f.

  Definition has_o_bf {n} (f:bf(S n)) : bool :=
    is_oU_bf f || is_oX_bf f || has_oC_bf f.

  Definition has_cst_bf {n} (f:bf n) : bool :=
    is_cst_bf false f || is_cst_bf true f.

  Inductive prim :=
  | OU : prim
  | OX : prim
  | OC (if0 th0:bool) : prim.

  Definition intro_prim_bf (p:prim) {n} (f:bf n) : bf(S n) :=
  match p with
  | OU => intro_oU_bf f
  | OX => intro_oX_bf f
  | OC if0 th0 => intro_oC_bf if0 th0 f
  end.

  Inductive decomp : nat -> Type :=
  | DCst n (b:bool) : decomp n
  | DPi0 n (b:bool) : decomp (S n)
  | DPrim n (p:prim) : decomp n -> decomp (S n)
  | DNode n (f:bf(S n)) : decomp (S n).

  Fixpoint sem_decomp_bf {n} (d:decomp n) {struct d} : bf n :=
  match d with
  | DCst n0 b => ((fun _ => b) : bf n0)
  | DPi0 n0 b => pi0 n0 b
  | DPrim n0 p d0 => intro_prim_bf p (sem_decomp_bf d0)
  | DNode _ f => f
  end.

  Definition extract_cst_bf {n} (f:bf n) : option bool :=
  if is_cst_bf false f then Some false
  else if is_cst_bf true f then Some true
  else None.

  Definition is_pi0_bf (b:bool) {n} (f:bf(S n)) : bool :=
    is_cst_bf       b  (fnary_evalo1 f false) &&
    is_cst_bf (negb b) (fnary_evalo1 f true ).

  Definition has_pi0_bf {n} (f:bf(S n)) : bool :=
    is_pi0_bf false f || is_pi0_bf true f.

  Definition extract_pi0_bf {n} (f:bf(S n)) : option bool :=
  if is_pi0_bf false f
  then Some false
  else if is_pi0_bf true f
    then Some true
    else None.

  Definition extract_oCY_bf (th0:bool) {n} (f:bf(S n)) : option (bool * bf n) :=
  if is_oC_bf false th0 f
  then (Some(false, elim_oC_bf false th0 f))
  else if is_oC_bf true th0 f
  then (Some(true, elim_oC_bf true th0 f))
  else None.

  Definition extract_oC_bf {n} (f:bf(S n)) : option ((bool * bool) * bf n) :=
  match extract_oCY_bf false f with
  | Some (if0, g) => Some((if0, false), g)
  | None => match extract_oCY_bf true f with
    | Some (if0, g) => Some((if0, true), g)
    | None => None
    end
  end.

  Definition extract_prim_bf {n} (f:bf(S n)) : option (prim * bf n) :=
  if is_oU_bf f
  then Some(OU, elim_oU_bf f)
  else if is_oX_bf f
    then Some(OX, elim_oX_bf f)
    else
      match extract_oC_bf f with
      | Some((if0, th0), g) => Some(OC if0 th0, g)
      | None => None
      end.

  Fixpoint extract_decomp_bf {n} (f:bf n) : decomp n :=
  match n as n0 return (bf n0 -> decomp n0) with
  | 0 => fun f0 : bf 0 => DCst 0 (f0 sempty)
  | S n0 =>
      fun f0 : bf (S n0) =>
      match extract_cst_bf f0 with
      | Some b => DCst (S n0) b
      | None =>
          match extract_pi0_bf f0 with
          | Some b => DPi0 n0 b
          | None =>
              match extract_prim_bf f0 with
              | Some (p, g) => DPrim n0 p (extract_decomp_bf g)
              | None => DNode n0 f0
              end
          end
      end
  end f.

  Lemma extract_cst_bf_cst_bf n b :
    @extract_cst_bf n (fun _ => b) = Some b.
  Proof with curry1.
  unfold extract_cst_bf...
  dest_match_step...
  Qed.
  Hint Rewrite extract_cst_bf_cst_bf : curry1_rewrite.

  Lemma extract_cst_bf_eq_Some n f b :
    @extract_cst_bf n f = Some b <-> f = (fun _ => b).
  Proof with curry1.
  split...
  unfold extract_cst_bf in H...
  dest_match_step...
  dest_match_step...
  Qed.
  Hint Rewrite extract_cst_bf_eq_Some : curry1_rewrite.

  Lemma is_pi0_bf_iff_true b n f : @is_pi0_bf b n f = true <-> f = pi0 n b.
  Proof with curry1.
  unfold is_pi0_bf.
  rewrite andb_true_iff...
  unfold fnary_evalo1, pi0...
  split...
  - rewrite shannon_expansion...
    unfold fnary_evalo1...
    rewrite H...
    apply functional_extensionality...
  - split...
    apply functional_extensionality...
  Qed.
  Hint Rewrite is_pi0_bf_iff_true : curry1_rewrite.

  Lemma pi0_eq_pi0 n b1 b2 : pi0 n b1 = pi0 n b2 <-> b1 = b2.
  Proof with curry1.
  split...
  apply_both H ((fun _ => false):ba (S n))...
  unfold pi0 in *...
  Qed.
  Hint Rewrite pi0_eq_pi0 : curry1_rewrite.

  Lemma is_pi0_bf_pi0 n b1 b2 : is_pi0_bf b1 (pi0 n b2) = eqb b1 b2.
  Proof with curry1.
  rewrite bool_eq_iff_iff...
  split...
  Qed.
  Hint Rewrite is_pi0_bf_pi0 : curry1_rewrite.

  Lemma extract_pi0_bf_pi0_bf n b :
    extract_pi0_bf(pi0 n b) = Some b.
  Proof with curry1.
  unfold extract_pi0_bf.
  dest_match_step...
  Qed.
  Hint Rewrite extract_pi0_bf_pi0_bf : curry1_rewrite.

  Lemma extract_pi0_bf_eq_Some n f b :
    extract_pi0_bf f = Some b <-> f = pi0 n b.
  Proof with curry1.
  split...
  unfold extract_pi0_bf in H...
  dest_match_step...
  dest_match_step...
  Qed.
  Hint Rewrite extract_pi0_bf_eq_Some : curry1_rewrite.

  Lemma has_cst_bf_cst_bf n b : @has_cst_bf n (fun _ => b) = true.
  Proof with curry1.
  unfold has_cst_bf, is_cst_bf.
  rewrite orb_true_iff...
  destruct b...
  Qed.
  Hint Rewrite has_cst_bf_cst_bf : curry1_rewrite.

  Lemma is_cst_bf_comp_negb b n f :
    @is_cst_bf b n (comp f negb) = is_cst_bf (negb b) f.
  Proof with curry1.
  setoid_rewrite is_cst_bf_negb_bf...
  Qed.
  Hint Rewrite is_cst_bf_comp_negb : curry1_rewrite.

  Lemma has_cst_bf_comp_negb n (f:bf n) :
    has_cst_bf (comp f negb) = has_cst_bf f.
  Proof with curry1.
  unfold has_cst_bf...
  rewrite andb_comm...
  Qed.
  Hint Rewrite has_cst_bf_comp_negb : curry1_rewrite.

  Lemma extract_oCY_bf_intro_oC_bf th0 if1 th1 n f1 : has_cst_bf f1 = false ->
    @extract_oCY_bf th0 n (intro_oC_bf if1 th1 f1) =
      if eqb th0 th1 then Some (if1, f1) else None.
  Proof with curry1.
  unfold extract_oCY_bf.
  dest_match_step...
  - dest_match_step...
  - dest_match_step...
    dest_match_step...
  Qed.
  (* [NO] Hint Rewrite extract_oCY_bf_intro_oC_bf : curry1_rewrite. *)

  Lemma extract_oC_bf_intro_oC_bf if0 th0 n f0 : has_cst_bf f0 = false ->
    @extract_oC_bf n (intro_oC_bf if0 th0 f0) = Some (if0, th0, f0).
  Proof with curry1.
  unfold extract_oC_bf...
  rewrite extract_oCY_bf_intro_oC_bf...
  rewrite extract_oCY_bf_intro_oC_bf...
  dest_match_step...
  Qed.
  (* [NO] Hint Rewrite extract_oC_bf_intro_oC_bf : curry1_rewrite. *)

  Lemma extract_prim_bf_intro_prim_bf n p g : has_cst_bf g = false ->
    @extract_prim_bf n (intro_prim_bf p g) = Some (p, g).
  Proof with curry1.
  unfold extract_prim_bf, intro_prim_bf...
  destruct p...
  rewrite extract_oC_bf_intro_oC_bf...
  dest_match_step...
  dest_match_step...
  Qed.
  (* [NO] Hint Rewrite extract_prim_bf_intro_prim_bf : curry1_rewrite. *)

  Lemma is_oC_bf_cst_bf if0 th0 b1 n :
    @is_oC_bf if0 th0 n (fun _ => b1) = eqb th0 b1.
  Proof with curry1.
  unfold is_oC_bf, is_cst_bf, fnary_evalo1...
  ring.
  Qed.
  Hint Rewrite is_oC_bf_cst_bf : curry1_rewrite.

  Lemma elim_oC_bf_cst_bf if0 th0 b1 n :
    @elim_oC_bf if0 th0 n (fun _ => b1) = (fun _ => b1).
  Proof. reflexivity. Qed.
  Hint Rewrite elim_oC_bf_cst_bf : curry1_rewrite.

  Lemma intro_oC_bf_eq_cst_bf if0 th0 n f0 b1 :
    @intro_oC_bf if0 th0 n f0 = (fun _ => b1) <->
      th0 = b1 /\ f0 = (fun _ => b1).
  Proof. curry1. Qed.
  Hint Rewrite intro_oC_bf_eq_cst_bf : curry1_rewrite.

  Lemma has_cst_bf_intro_oC_bf if0 th0 n f0 :
    has_cst_bf (intro_oC_bf if0 th0 f0) = @is_cst_bf th0 n f0.
  Proof with curry1.
  rewrite bool_eq_iff_iff...
  split...
  - unfold has_cst_bf in H...
    rewrite andb_false_iff in H...
    destruct H...
    + setoid_rewrite intro_oC_bf_eq_cst_bf in H...
    + setoid_rewrite intro_oC_bf_eq_cst_bf in H...
  - unfold has_cst_bf...
    rewrite andb_false_iff...
    setoid_rewrite intro_oC_bf_eq_cst_bf...
    destruct th0...
  Qed.
  Hint Rewrite has_cst_bf_intro_oC_bf : curry1_rewrite.

  Lemma is_pi0_bf_intro_oC_bf b0 if1 th1 n f1 :
    is_pi0_bf b0 (@intro_oC_bf if1 th1 n f1) =
      is_cst_bf (negb th1) f1 && (eqb b0 (xorb if1 th1)).
  Proof with curry1.
  unfold is_pi0_bf...
  dest_match_step...
  - destruct(xorb b0 th1)eqn:E...
  - destruct(xorb b0 th1)eqn:E...
  Qed.
  Hint Rewrite is_pi0_bf_intro_oC_bf : curry1_rewrite.

  Lemma has_pi0_bf_intro_oC_bf if0 th0 n f0 :
    has_pi0_bf (@intro_oC_bf if0 th0 n f0) = is_cst_bf (negb th0) f0.
  Proof with curry1.
  unfold has_pi0_bf...
  destruct(eqb if0 th0)eqn:E...
  Qed.
  Hint Rewrite has_pi0_bf_intro_oC_bf : curry1_rewrite.

  Lemma reverse_has_cst_bf_true n f :
    @has_cst_bf n f = true <-> f = (fun _ => f(fun _ => false)).
  Proof with curry1.
  split...
  - unfold has_cst_bf in H...
    rewrite andb_false_iff in H...
    destruct H...
  - rewrite H...
  Qed.

  Lemma extract_oCY_bf_eq_Some n f if0 th0 f0 :
    has_cst_bf f = false ->
    has_pi0_bf f = false ->
    @extract_oCY_bf th0 n f = Some (if0, f0) <-> f = intro_oC_bf if0 th0 f0.
  Proof with curry1.
  split...
  - symmetry.
    unfold extract_oCY_bf in H1...
    dest_match_step...
    + setoid_rewrite intro_elim_eq_same_oC_bf...
    + dest_match_step...
      setoid_rewrite intro_elim_eq_same_oC_bf...
  - rewrite extract_oCY_bf_intro_oC_bf...
    unfold has_cst_bf; destruct th0...
  Qed.

  Lemma extract_oC_bf_eq_Some n f if0 th0 f0 :
    has_cst_bf f = false ->
    has_pi0_bf f = false ->
      @extract_oC_bf n f = Some (if0, th0, f0) <-> f = intro_oC_bf if0 th0 f0.
  Proof with curry1.
  split...
  - unfold extract_oC_bf in *...
    dest_match_step...
    + rewrite extract_oCY_bf_eq_Some in D...
    + dest_match_step...
      rewrite extract_oCY_bf_eq_Some in D0...
  - rewrite extract_oC_bf_intro_oC_bf...
    unfold has_cst_bf; destruct th0...
  Qed.

  Lemma is_oU_bf_pi0 n b : is_oU_bf (pi0 n b) = false.
  Proof. unfold is_oU_bf; curry1. Qed.
  Hint Rewrite is_oU_bf_pi0 : curry1_rewrite.

  Lemma is_pi0_bf_intro_oU_bf b n f : is_pi0_bf b (@intro_oU_bf n f) = false.
  Proof with curry1.
  destruct(is_pi0_bf b (intro_oU_bf f))eqn:E...
  setoid_rewrite intro_oU_bf_as_is_oU_bf in E...
  Qed.
  Hint Rewrite is_pi0_bf_intro_oU_bf : curry1_rewrite.

  Lemma has_pi0_bf_intro_oU_bf n f : has_pi0_bf (@intro_oU_bf n f) = false.
  Proof. unfold has_pi0_bf; curry1. Qed.
  Hint Rewrite has_pi0_bf_intro_oU_bf : curry1_rewrite.

  Lemma is_cst_intro_oU_bf b n f :
    is_cst_bf b (@intro_oU_bf n f) = is_cst_bf b f.
  Proof with curry1.
  rewrite bool_eq_iff_iff...
  split...
  setoid_rewrite intro_oU_bf_as_is_oU_bf in H...
  Qed.
  Hint Rewrite is_cst_intro_oU_bf : curry1_rewrite.

  Lemma has_cst_intro_oU_bf n f :
    has_cst_bf (@intro_oU_bf n f) = has_cst_bf f.
  Proof. unfold has_cst_bf; curry1. Qed.
  Hint Rewrite has_cst_intro_oU_bf : curry1_rewrite.

  Lemma is_cst_bf_intro_oX_bf b n f : is_cst_bf b (@intro_oX_bf n f) = false.
  Proof with curry1.
  unfold is_cst_bf...
  unfold fnary_evalo1...
  destruct(is_cst_bf b f)eqn:E...
  Qed.
  Hint Rewrite is_cst_bf_intro_oX_bf : curry1_rewrite.

  Lemma has_cst_bf_intro_oX_bf n f : has_cst_bf (@intro_oX_bf n f) = false.
  Proof. unfold has_cst_bf; curry1. Qed.
  Hint Rewrite has_cst_bf_intro_oX_bf : curry1_rewrite.

  Lemma is_pi0_bf_intro_oX_bf b n f : is_pi0_bf b (@intro_oX_bf n f) = is_cst_bf b f.
  Proof with curry1.
  unfold is_pi0_bf...
  Qed.
  Hint Rewrite is_pi0_bf_intro_oX_bf : curry1_rewrite.

  Lemma has_pi0_bf_intro_oX_bf n f : has_pi0_bf (@intro_oX_bf n f) = has_cst_bf f.
  Proof. unfold has_pi0_bf, has_cst_bf; curry1. Qed.
  Hint Rewrite has_pi0_bf_intro_oX_bf : curry1_rewrite.

  Lemma extract_prim_bf_eq_Some n f p g :
    has_cst_bf f = false ->
    has_pi0_bf f = false ->
      @extract_prim_bf n f = Some (p, g) <-> f = intro_prim_bf p g.
  Proof with curry1.
  split...
  - symmetry.
    unfold extract_prim_bf in *...
    dest_match_step...
    + setoid_rewrite intro_elim_eq_same_oU_bf...
    + dest_match_step...
      * setoid_rewrite intro_elim_eq_same_oX_bf...
      * dest_match_step...
        rewrite extract_oC_bf_eq_Some in D1...
  - rewrite extract_prim_bf_intro_prim_bf...
    destruct p...
    unfold has_cst_bf; destruct th0...
  Qed.

  Lemma extract_cst_bf_eq_None n f :
    @extract_cst_bf n f = None <-> has_cst_bf f = false.
  Proof with curry1.
  unfold extract_cst_bf, has_cst_bf.
  dest_match_step...
  dest_match_step...
  Qed.
  Hint Rewrite extract_cst_bf_eq_None : curry1_rewrite.

  Lemma extract_pi0_bf_eq_None n f :
    @extract_pi0_bf n f = None <-> has_pi0_bf f = false.
  Proof with curry1.
  unfold extract_pi0_bf, has_pi0_bf.
  dest_match_step...
  dest_match_step...
  Qed.
  Hint Rewrite extract_pi0_bf_eq_None : curry1_rewrite.

  Lemma sem_extract_decomp_bf n (f:bf n) :
    sem_decomp_bf (extract_decomp_bf f) = f.
  Proof with curry1.
  induction n...
  - apply functional_extensionality...
    specialize(sempty_unique x)...
  - dest_match_step...
    dest_match_step...
    dest_match_step...
    rewrite extract_prim_bf_eq_Some in D1...
    rewrite IHn...
  Qed.

  Inductive reduced : forall n, bf n -> forall m, bf m -> Type :=
  | RCst n (b:bool) : reduced n (fun _ => b) n (fun _ => b)
  | RPi0 n (b:bool) : reduced _ (pi0 n b) _ (pi0 n b)
  | RNode n (f:bf(S n)): has_o_bf f = false -> reduced _ f _ f
  | RPrim n m f0 f1 p :
    has_cst_bf f1 = false ->
    reduced n f0 m f1 -> reduced _ f0 _ (intro_prim_bf p f1).

  Lemma reduced_unique n fr0 ff m

End LddO.
