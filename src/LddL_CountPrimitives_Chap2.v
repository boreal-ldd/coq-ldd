(* This File as been moved to Ground *)
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Nat.
Require Import Bool Ring.
Require Import FunInd.
Require Import Recdef.

From Ground Require Import Ground.SimplProp.
From Ground Require Import Ground.SimplBool.
From Ground Require Import Ground.SimplOption.
From Ground Require Import Ground.SimplEq.
From Ground Require Import Ground.SimplCmp.
From Ground Require Import Ground.SimplSimpl.

From Ground Require Import Ground.SimplNat.
From Ground Require Import Ground.SimplAB.

From Ground Require Import Ground.SimplList_Chap0_Standard.
From Ground Require Import Ground.SimplList_Chap1_Construct.
From Ground Require Import Ground.SimplList_Chap2_Predicate.
From Ground Require Import Ground.SimplList_Chap3_PredicateOnConstruct.
From Ground Require Import Ground.SimplList.
From Ground Require Import Ground.SimplABList.
From Ground Require Import Ground.SimplMore.

From Ground Require Import MiniBool_VAX.
From Ground Require Import MiniBool_Logic.

From Ground Require Import Ground.SimplDTT.
From Ground Require Import Ground.SimplLtN.
From Ground Require Import Ground.SimplArray.

From Ground Require Import FiniteCurryfy.
From Ground Require Import FiniteBooleanFunction.

From Ground Require Import Ground.SimplEnum.
From Ground Require Import Ground.SimplEnumCount.
From Ground Require Import Ground.SimplEnumArray.

From Ground Require Import FiniteBooleanFunction_Chap2_EnumCount.

Require Import LddO_Primitives.
Require Import LddO_OPrim.

Require Import LddU_Primitives.


Lemma cntsat_bf_intro_uU_bf {n} k (f:bf n) : cntsat_bf (intro_uP_bf oU k f) = 2*(cntsat_bf f).
Proof. rewrite(rewrite_cnsat_bf_using_fnary_evalu1 k); curry1. Qed.

Lemma cntsat_bf_intro_uX_bf {n} k (f:bf n) : cntsat_bf (intro_uP_bf oX k f) = 2 ^ n.
Proof with curry1.
rewrite(rewrite_cnsat_bf_using_fnary_evalu1 k)...
apply cntsat_bf_le_pow_2_n.
Qed.
#[export] Hint Rewrite @cntsat_bf_intro_uX_bf : curry1_rewrite.

Lemma cntsat_bf_intro_uC_bf if0 th0 {n} k (f:bf n) :
  cntsat_bf (intro_uP_bf (oC if0 th0) k f) = (if th0 then (2 ^ n) else 0) + cntsat_bf f.
Proof. rewrite (rewrite_cnsat_bf_using_fnary_evalu1_gen k if0); curry1. Qed.

Lemma intro_uC_bf_eq_intro_uC_bf_exclusive_case if1 th1 if2 th2 {n} k1 k2 (f1 f2:bf n) :
  intro_uP_bf (oC if1 th1) k1 f1 = intro_uP_bf (oC if2 th2) k2 f2 ->
    th1 <> th2 -> f1 = cst_bf _ th2 /\ f2 = cst_bf _ th1.
Proof with curry1.
intros.
specialize(f_equal (fun f => cntsat_bf f) H) as Hc.
repeat rewrite cntsat_bf_intro_uC_bf in Hc.
dest_match_step...
Qed.