
type bool =
| True
| False

val xorb : bool -> bool -> bool

val negb : bool -> bool

type nat =
| O
| S of nat

type 'a option =
| Some of 'a
| None

type 'a sig0 = 'a
  (* singleton inductive, whose constructor was exist *)

val eqb : bool -> bool -> bool

val solution_left : 'a1 -> 'a2 -> 'a1 -> 'a2

val comp : ('a1 -> 'a2) -> ('a2 -> 'a3) -> 'a1 -> 'a3

type ltN = nat

val ltN_of_nat : nat -> nat -> ltN

val ltN_S : nat -> ltN -> ltN

type 'a array = ltN -> 'a

val sempty : 'a1 array

val scons : nat -> 'a1 -> 'a1 array -> 'a1 array

val shead : nat -> 'a1 array -> 'a1

val stail : nat -> 'a1 array -> 'a1 array

type ('a, 'b) fnary = 'a array -> 'b

val fnary_evalo1 : nat -> ('a1, 'a2) fnary -> 'a1 -> ('a1, 'a2) fnary

type bf = (bool, bool) fnary

val beq_bf : nat -> bf -> bf -> bool

val uop_bf : (bool -> bool) -> nat -> bf -> bf

val neg_bf : nat -> bf -> bf

val cst_bf : nat -> bool -> bf

val is_cst_bf : bool -> nat -> bf -> bool

val pi0 : nat -> bool -> bf

type 'edge ldd =
| Leaf of nat * 'edge
| Edge of nat * nat * 'edge * 'edge ldd * 'edge ldd

type 'f node =
| Node of nat * 'f * 'f

type 'f next =
| Term
| Next of nat * 'f

type ('edge, 'f) xedge =
| XEdge of nat option * nat * 'edge * 'f node next

val xedge_projN : nat -> ('a1, 'a2) xedge -> nat option

val xedge_projE : nat -> ('a1, 'a2) xedge -> 'a1

val xedge_projF : nat -> ('a1, 'a2) xedge -> 'a2 node next

val extract_ldd : (nat -> bf -> ('a1, bf) xedge) -> nat -> bf -> 'a1 ldd

type 'edge lDD = 'edge ldd

val extract :
  (nat -> bf -> ('a1, bf) xedge) -> (nat -> 'a1 -> bf) -> (nat -> nat -> 'a1
  -> bf -> bf) -> nat -> bf -> 'a1 lDD

type ('term, 'prim, 'f) xprim =
| XTerm of nat * 'term
| XPrim of nat * 'prim * 'f
| XSha of nat * 'f * 'f

type ('term, 'prim) trans =
| ETerm of nat * 'term
| EPrim of nat option * nat * 'prim * ('term, 'prim) trans
| ENext of nat

type ('term, 'prim, 'f) xtrans = (('term, 'prim) trans, 'f) xedge

val xTrans :
  nat option -> nat -> ('a1, 'a2) trans -> 'a3 node next -> (('a1, 'a2)
  trans, 'a3) xedge

val xTPrim : 'a2 -> nat -> ('a1, 'a2, 'a3) xtrans -> ('a1, 'a2, 'a3) xtrans

val extract_trans :
  (nat -> bf -> ('a1, 'a2, bf) xprim) -> nat -> bf -> ('a1, 'a2, bf) xtrans

val sem_trans :
  (nat -> 'a1 -> bf) -> ('a2 -> nat -> bf -> bf) -> nat option -> nat ->
  ('a1, 'a2) trans -> bf next -> bf

type ('term, 'prim) edge = ('term, 'prim) trans

type ('term, 'prim, 'f) xedge0 = (('term, 'prim) edge, 'f) xedge

val xEdge :
  (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> ('a2 -> nat -> bf -> bf)
  -> nat option -> nat -> ('a1, 'a2) edge -> 'a3 node next -> (('a1, 'a2)
  edge, 'a3) xedge

val extract_edge :
  (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> (bf -> 'a1) -> ('a2 ->
  nat -> bf -> bf) -> ('a2 -> nat -> bf -> bf option) -> (nat -> bf -> bool)
  -> (nat -> bf -> ('a1, 'a2, bf) xprim) -> nat -> bf -> ('a1, 'a2, bf) xedge0

val sem_edge :
  (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> ('a2 -> nat -> bf -> bf)
  -> nat option -> nat -> ('a1, 'a2) edge -> bf next -> bf

val sem_Term :
  (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> ('a2 -> nat -> bf -> bf)
  -> nat -> ('a1, 'a2) edge -> bf

val sem_Next :
  (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> ('a2 -> nat -> bf -> bf)
  -> nat -> nat -> ('a1, 'a2) edge -> bf -> bf

type ('term, 'prim) lDD0 = ('term, 'prim) edge lDD

val extract0 :
  (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> (bf -> 'a1) -> ('a2 ->
  nat -> bf -> bf) -> ('a2 -> nat -> bf -> bf option) -> (nat -> bf -> bool)
  -> (nat -> bf -> ('a1, 'a2, bf) xprim) -> nat -> bf -> ('a1, 'a2) lDD0

val intro_oP_bf : (bool -> bool -> bool) -> nat -> bf -> bool array -> bool

val oU : bool -> bool -> bool

val oX : bool -> bool -> bool

val oC : bool -> bool -> bool -> bool -> bool

val intro_oU_bf : nat -> bf -> bool array -> bool

val intro_oX_bf : nat -> bf -> bool array -> bool

val intro_oC_bf : bool -> bool -> nat -> bf -> bool array -> bool

val rev_oU : bool

val rev_oX : bool

val rev_oC : bool -> bool -> bool

val elim_oP_bf : bool -> nat -> bf -> bf

val elim_oU_bf : nat -> bf -> bf

val elim_oX_bf : nat -> bf -> bf

val elim_oC_bf : bool -> bool -> nat -> bf -> bf

val is_oU_bf : nat -> bf -> bool

val is_oX_bf : nat -> bf -> bool

val is_oC_bf : bool -> bool -> nat -> bf -> bool

type term =
| Cst of nat * bool
| Pi0 of nat * bool

val sem_term : nat -> term -> bf

val extract_cst : nat -> bf -> term option

val extract_pi0 : nat -> bf -> term option

val extract_term : nat -> bf -> term option

val term_of_bool : bf -> term

type prim =
| OU
| OX
| OC of bool * bool

val sem_prim : prim -> nat -> bf -> bf

val detect_prim : prim -> nat -> bf -> bool

val elim_prim : prim -> nat -> bf -> bf

val extract_prim : prim -> nat -> bf -> bf option

val is_prim_free : nat -> bf -> bool

type 'f xprim0 = (term, prim, 'f) xprim

val extract_xprim : nat -> bf -> bf xprim0

type lDD1 = (term, prim) lDD0

val extract1 : nat -> bf -> lDD1

val f1 : bf

val ldd1 : lDD1

val f2 : bf

val ldd2 : lDD1
