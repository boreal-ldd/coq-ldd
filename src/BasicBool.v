Require Import Arith .
Require Import Bool.
Require Import Coq.Program.Equality.
Require Import Bool Ring.
Require Import Ground.SimplProp.
Require Import Ground.SimplBool.

Lemma ifF (b0:bool) {A B:Type} (f1 f2:A -> B) x :
  (if b0 then f1 else f2) x = if b0 then (f1 x) else (f2 x).
Proof. dest_bool. Qed.

Lemma ifV (b0:bool) {A B:Set} (f:A -> B) x1 x2 :
  f(if b0 then x1 else x2) = if b0 then (f x1) else (f x2).
Proof. dest_bool. Qed.

Lemma ifif (b1 x1 x2:bool) {A:Type} (y1 y2:A) :
  (if (if b1 then x1 else x2) then y1 else y2) =
    if b1 then (if x1 then y1 else y2) else (if x2 then y1 else y2).
Proof. dest_bool. Qed.

Lemma ifP (f1 f2:bool -> Prop) :
  (forall (b0:bool), if b0 then (f1 b0) else (f2 b0)) <->
    (forall (b0:bool), if b0 then (f1 true) else (f2 false)).
Proof.
split; simplbool; autospec; dest_if.
Qed.

Lemma ifP' {A:Type} (f1 f2:bool -> A) (X:A) (E:bool->Prop):
(forall (b:bool), ((X = if b then (f1 b) else (f2 b)) -> E b)) <->
(forall (b:bool), ((X = if b then (f1 true) else (f2 false)) -> E b)).
Proof.
split; simplbool; autospec; dest_if.
Qed.

Lemma ifN (b0:bool) {A:Type} (x1 x2:A) :
  (if (negb b0) then x1 else x2) = if b0 then x2 else x1.
Proof. dest_bool. Qed.

Lemma ifU (b0:bool) {A:Type} (x:A) : (if b0 then x else x) = x.
Proof. dest_bool. Qed.

Ltac print_goal :=
  match goal with
    | H : ?E |- _ => idtac "[H] "H ":" E; fail
    | |- ?E => idtac "|- "E
  end.

Ltac ifR1 b E1 E2 :=
  let X := fresh "X" in
  let EQif := fresh "EQif" in
  remember (if b then E1 else E2) as X eqn:EQif;
  move EQif at top;
  generalize dependent b;
  rewrite (ifP' _ _ X _);
  intros; subst; simpl;
  idtac "ifR: "b
.

Ltac ifR1' b E1 E2 H :=
  let X := fresh "X" in
  let EQif := fresh "EQif" in
  remember (if b then E1 else E2) as X eqn:EQif in H;
  move EQif at top;
  generalize dependent b;
  rewrite (ifP' _ _ X _);
  intros; subst; simpl;
  idtac "ifR: "b
.

Ltac subst_tac E X Y := match E with
  | context G [X] =>
    let E' := context G[Y] in
    subst_tac E' X Y
  | _ => E
end.

Ltac ifR2 := match goal with
| b0 : bool |- ?E =>
  match E with context [if b0 then ?E1 else ?E2] =>
    (match E1 with
    | context [b0] => idtac
    | _ => match E2 with
      | context [b0] => idtac
      end
    end);
    ifR1 b0 E1 E2
  end
| |- ?E =>
  match E with context [if ?b0 then ?E1 else ?E2] =>
    (match E1 with
    | context [b0] => idtac
    | _ => match E2 with
      | context [b0] => idtac
      end
    end);
    let b := fresh "b" in
    idtac "?" b ":=" b0;
    remember b0 as b eqn:EQ;
    generalize dependent EQ;
    let E1' := subst_tac E1 b0 b in
    let E2' := subst_tac E2 b0 b in
    ifR1 b E1' E2'
  end
| b0 : bool, H : ?E |- _ =>
  match E with context [if b0 then ?E1 else ?E2] =>
    (match E1 with
    | context [b0] => idtac
    | _ => match E2 with
      | context [b0] => idtac
      end
    end);
    ifR1' b0 E1 E2 H
  end
| H : ?E |- _ =>
  match E with context [if ?b0 then ?E1 else ?E2] =>
    (match E1 with
    | context [b0] => idtac
    | _ => match E2 with
      | context [b0] => idtac
      end
    end);
    let b := fresh "b" in
    idtac "?" b ":=" b0;
    remember b0 as b eqn:EQ in H;
    generalize dependent EQ;
    let E1' := subst_tac E1 b0 b in
    let E2' := subst_tac E2 b0 b in
    ifR1' b E1' E2' H
  end
end.

Ltac crazIf_step := match goal with
| H : context [(if ?b then ?f1 else ?f2) ?x] |- _ =>
  rewrite (ifF b f1 f2 x) in H
| |- context [(if ?b then ?f1 else ?f2) ?x] =>
  rewrite (ifF b f1 f2 x)
| H : context [if (negb ?b) then ?x1 else ?x2] |- _ =>
  rewrite (ifN b x1 x2)
| |- context [if (negb ?b) then ?x1 else ?x2] =>
  rewrite (ifN b x1 x2)
| H : context [?f(if ?b0 then ?x1 else ?x2)] |- _ =>
  rewrite (ifV b0 f x1 x2) in H
| |- context [?f(if ?b0 then ?x1 else ?x2)] =>
  rewrite (ifV b0 f x1 x2)
| H : context [if (if ?b1 then ?x1 else ?x2) then ?y1 else ?y2] |- _ =>
  rewrite (ifif b1 x1 x2 y1 y2) in H
| |- context [if (if ?b1 then ?x1 else ?x2) then ?y1 else ?y2] =>
  rewrite (ifif b1 x1 x2 y1 y2)
| H : context [if ?b then ?x else ?x] |- _ =>
  rewrite (ifU b x) in H
| |- context [if ?b then ?x else ?x] =>
  rewrite (ifU b x)
| H : context [if (negb ?b) then ?x1 else ?x2] |- _ =>
  rewrite (ifN b x1 x2) in H
| |- context [if (negb ?b) then ?x1 else ?x2] =>
  rewrite (ifN b x1 x2)
| _ => ifR2
end.

Ltac crazIf := repeat((repeat crazIf_step); simplbool).

(*
Require Import ssreflect ssrbool.
About ifP.
*)

Lemma Cxy_to_circuit (x0 if0 th0 fx:bool) :
  (if (eqb x0 if0) then th0 else fx) =
    (if th0 then orb else andb) (xorb x0 (xorb if0 th0)) fx.
Proof.
rewrite <- eqb_true_iff.
crazIf.
Qed.

Lemma Cxy_to_circuit2 (x0 if0 th0 fx:bool) :
  (if (eqb x0 if0) then th0 else fx) =
    (if th0 then orb  (xorb (negb if0) x0) fx
            else andb (xorb (     if0) x0) fx
    ).
Proof.
dest_bool.
Qed.

Example example_000 (a b:bool) : (if a then a else b) = a || b.
Proof. dest_bool. Qed.
