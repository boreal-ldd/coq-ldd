
type unit0 =
| Tt

type bool =
| True
| False

val xorb : bool -> bool -> bool

val negb : bool -> bool

type nat =
| O
| S of nat

type 'a option =
| Some of 'a
| None

type ('a, 'b) prod =
| Pair of 'a * 'b

val fst : ('a1, 'a2) prod -> 'a1

val snd : ('a1, 'a2) prod -> 'a2

type 'a sig0 = 'a
  (* singleton inductive, whose constructor was exist *)

val pred : nat -> nat

val eqb : bool -> bool -> bool

module Nat :
 sig
  val eqb : nat -> nat -> bool

  val leb : nat -> nat -> bool

  val ltb : nat -> nat -> bool
 end

val solution_left : 'a1 -> 'a2 -> 'a1 -> 'a2

val comp : ('a1 -> 'a2) -> ('a2 -> 'a3) -> 'a1 -> 'a3

val opmap : ('a1 -> 'a2) -> 'a1 option -> 'a2 option

val isSome : 'a1 option -> bool

val isNone : 'a1 option -> bool

type ord =
| Lt
| Eq
| Gt

type 'a cmp = 'a -> 'a -> ord

val cmp_comp : ord -> ord -> ord

val cmp_bool : bool cmp

val cmp_prod : 'a1 cmp -> 'a2 cmp -> ('a1, 'a2) prod -> ('a1, 'a2) prod -> ord

type 'a minsat_t = ('a -> bool) -> 'a option

val minsat_prod :
  'a1 minsat_t -> 'a2 minsat_t -> (('a1, 'a2) prod -> bool) -> ('a1, 'a2)
  prod option

val cmp_sig : 'a1 cmp -> 'a1 cmp

val cmp_nat : nat -> nat -> ord

type ('a, 'b) aB =
| AA of 'a
| BB of 'b

type ltN = nat

val ltN_of_nat : nat -> nat -> ltN

val ltN_S : nat -> ltN -> ltN

val ltN_intro : nat -> ltN -> ltN -> ltN

val ltN_pop : nat -> ltN -> ltN -> (unit0, ltN) aB

val ltN_pred : nat -> ltN -> ltN option

val cmp_ltN : nat -> ltN cmp

type 'a array = ltN -> 'a

val sempty : 'a1 array

val scons : nat -> 'a1 -> 'a1 array -> 'a1 array

val stail : nat -> 'a1 array -> 'a1 array

val spop : nat -> ltN -> 'a1 array -> 'a1 array

val spush : nat -> ltN -> 'a1 -> 'a1 array -> 'a1 array

val array_minsat : nat -> bool array -> ltN option

val minsat_map : ('a1 -> 'a2) -> 'a1 minsat_t -> 'a2 minsat_t

type ('a, 'b) fnary = 'a array -> 'b

val fnary_evalo1 : nat -> ('a1, 'a2) fnary -> 'a1 -> ('a1, 'a2) fnary

val fnary_evalu1 : nat -> ltN -> ('a1, 'a2) fnary -> 'a1 -> ('a1, 'a2) fnary

type bf = (bool, bool) fnary

val beq_bf : nat -> bf -> bf -> bool

val bop_bf : (bool -> bool -> bool) -> nat -> bf -> bf -> bf

val cst_bf : nat -> bool -> bf

val is_cst_bf : bool -> nat -> bf -> bool

val piN : nat -> ltN -> bool -> bf

type 'edge ldd =
| Leaf of nat * 'edge
| Edge of nat * nat * 'edge * 'edge ldd * 'edge ldd

type 'f node =
| Node of nat * 'f * 'f

type 'f next =
| Term
| Next of nat * 'f

type ('edge, 'f) xedge =
| XEdge of nat option * nat * 'edge * 'f node next

val xedge_projN : nat -> ('a1, 'a2) xedge -> nat option

val xedge_projE : nat -> ('a1, 'a2) xedge -> 'a1

val xedge_projF : nat -> ('a1, 'a2) xedge -> 'a2 node next

val extract_ldd : (nat -> bf -> ('a1, bf) xedge) -> nat -> bf -> 'a1 ldd

type 'edge lDD = 'edge ldd

val extract :
  (nat -> bf -> ('a1, bf) xedge) -> (nat option -> nat -> 'a1 -> bf next ->
  bf) -> nat -> bf -> 'a1 lDD

type oPrim = ((bool, bool) prod, bool) prod

val oU : oPrim

val oX : oPrim

val oC : bool -> bool -> oPrim

val detect_prim : oPrim -> bool -> bool -> bool

val eval_prim : oPrim -> bool -> bool -> bool

val rev_prim : oPrim -> bool

type oprim =
| OU
| OX
| OC of bool * bool

val oPrim_of_oprim : oprim -> oPrim

val pi_oprim : ltN -> oprim

val cmp_oprim : oprim -> oprim -> ord

val minsat_oprim : oprim minsat_t

type ('term, 'prim, 'f) xprim =
| XTerm of nat * 'term
| XPrim of nat * 'prim * 'f
| XSha of nat * 'f * 'f

type ('term, 'prim) trans =
| ETerm of nat * 'term
| EPrim of nat option * nat * 'prim * ('term, 'prim) trans
| ENext of nat

val sem_trans :
  (nat -> 'a1 -> bf) -> (nat -> 'a2 -> bf -> bf) -> nat option -> nat ->
  ('a1, 'a2) trans -> bf next -> bf

type ('term, 'prim, 'f) xtrans = (('term, 'prim) trans, 'f) xedge

val xTrans :
  nat option -> nat -> ('a1, 'a2) trans -> 'a3 node next -> (('a1, 'a2)
  trans, 'a3) xedge

val xTPrim : nat -> 'a2 -> ('a1, 'a2, 'a3) xtrans -> ('a1, 'a2, 'a3) xtrans

val extract_trans :
  (nat -> bf -> ('a1, 'a2, bf) xprim) -> nat -> bf -> ('a1, 'a2, bf) xtrans

type ('term, 'prim) edge = ('term, 'prim) trans

type ('term, 'prim, 'f) xedge0 = (('term, 'prim) edge, 'f) xedge

val xEdge :
  (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> (nat -> 'a2 -> bf -> bf)
  -> (nat -> 'a2 -> bf -> bf option) -> (nat -> 'a2 -> 'a2 -> ord option) ->
  (nat -> bf -> bool) -> nat option -> nat -> ('a1, 'a2) edge -> 'a3 node
  next -> (('a1, 'a2) edge, 'a3) xedge

val extract_edge :
  (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> (bf -> 'a1) -> (nat ->
  'a2 -> bf -> bf) -> (nat -> 'a2 -> bf -> bf option) -> (nat -> 'a2 -> 'a2
  -> ord option) -> (nat -> bf -> bool) -> (nat -> bf -> ('a1, 'a2, bf)
  xprim) -> nat -> bf -> ('a1, 'a2, bf) xedge0

val sem_edge :
  (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> (nat -> 'a2 -> bf -> bf)
  -> (nat -> 'a2 -> bf -> bf option) -> (nat -> 'a2 -> 'a2 -> ord option) ->
  (nat -> bf -> bool) -> nat option -> nat -> ('a1, 'a2) edge -> bf next -> bf

type ('term, 'prim) lDD0 = ('term, 'prim) edge lDD

val extract0 :
  (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> (bf -> 'a1) -> (nat ->
  'a2 -> bf -> bf) -> (nat -> 'a2 -> bf -> bf option) -> (nat -> 'a2 -> 'a2
  -> ord option) -> (nat -> bf -> bool) -> (nat -> bf -> ('a1, 'a2, bf)
  xprim) -> nat -> bf -> ('a1, 'a2) lDD0

val intro_uP_bf : oPrim -> nat -> ltN -> bf -> bf

val elim_uP_bf : oPrim -> nat -> ltN -> bf -> bf

val is_uP_bf : oPrim -> nat -> ltN -> bf -> bool

val extract_cst : nat -> bf -> bool option

val extract_piN_bool : nat -> bool -> bf -> ltN option

val extract_piN : nat -> bf -> (ltN, bool) prod option

type term =
| Cst of nat * bool
| PiN of nat * ltN * bool

val sem_term : nat -> term -> bf

val extract_term : nat -> bf -> term option

val term_of_bool : bf -> term

type prim = (oprim, ltN) prod

val sem_prim : nat -> prim -> bf -> bf

val detect_prim0 : nat -> prim -> bf -> bool

val elim_prim : nat -> prim -> bf -> bf

val extract_prim : nat -> prim -> bf -> bf option

val cmp_prim : nat -> prim -> prim -> ord

val pcmp_prim : nat -> prim -> prim -> ord option

val minsat_prim : nat -> (prim -> bool) -> prim option

val minsat_prim_uP_bf : nat -> bf -> prim option

val is_prim_free : nat -> bf -> bool

type 'f xprim0 = (term, prim, 'f) xprim

val extract_xprim : nat -> bf -> bf xprim0

type lDD1 = (term, prim) lDD0

val extract1 : nat -> bf -> lDD1

val f1 : bf

val ldd1 : lDD1

val f2 : bf

val ldd2 : lDD1

val f3 : bf

val ldd3 : lDD1
