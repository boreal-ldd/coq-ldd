Require Import Arith .
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.

Require Import Ground.SimplProp.
Require Import Ground.SimplBool.
Require Import Ground.SimplOption.
Require Import Ground.SimplEq.
Require Import Ground.SimplFun.
Require Import Ground.SimplBoolFun.
Require Import Ground.SimplCmp.
Require Import Ground.SimplSimpl.

Require Import Ground.SimplNat.
Require Import Ground.SimplAB.

Require Import Ground.SimplList_Chap0_Standard.
Require Import Ground.SimplList_Chap1_Construct.
Require Import Ground.SimplList_Chap2_Predicate.
Require Import Ground.SimplList_Chap3_PredicateOnConstruct.
Require Import Ground.SimplList.
Require Import Ground.SimplABList.
Require Import Ground.SimplMore.
Require Import Ground.SimplDTT.
Require Import Ground.SimplLtN_Chap0_Construct.
Require Import Ground.SimplLtN_Chap1_Array_as_Function.
Require Import Ground.SimplLtN_Chap1_Sec1_Boolean_Array.
Require Import Ground.SimplLtNIndexedArray.

Require Import Ground.SimplDTT.
Require Import Ground.SimplLtN.
Require Import Ground.SimplEnum.

Require Import Ground.MiniBool_VAX.
Require Import Ground.MiniBool_Logic.

Require Import FiniteCurryfy.
Require Import FiniteBooleanFunction.
Require Import MaxLddGen.
Require Import PrimOGen.

Require Import LddO_Primitives.
Require Import LddO_OPrim.

(* Section 1. Definition if Terminal and Primitives *)
(* SubSection 1. Defining Terminals *)
Inductive term : nat -> Type :=
| Cst (n:nat) (b:bool) : term n
| Pi0 (n:nat) (b:bool) : term (S n).

Lemma Cst_eq_Cst n b1 b2 : Cst n b1 = Cst n b2 <-> b1 = b2. Proof. split; solve_by_invert. Qed.
Lemma Pi0_eq_Pi0 n b1 b2 : Pi0 n b1 = Pi0 n b2 <-> b1 = b2. Proof. split; solve_by_invert. Qed.
Lemma Cst_eq_Pi0 n b1 b2 : Cst _ b1 = Pi0 n b2 <-> False. Proof. solve_by_invert. Qed.
Lemma Pi0_eq_Cst n b1 b2 : Pi0 n b1 = Cst _ b2 <-> False. Proof. solve_by_invert. Qed.

Hint Rewrite Cst_eq_Cst : curry1_rewrite.
Hint Rewrite Pi0_eq_Pi0 : curry1_rewrite.
Hint Rewrite Cst_eq_Pi0 : curry1_rewrite.
Hint Rewrite Pi0_eq_Cst : curry1_rewrite.

Definition sem_term {n} (t:term n) : bf n :=
match t with
| Cst n b => cst_bf n b
| Pi0 n b => pi0 n b
end.

Definition extract_cst {n} (f:bf n) : option(term n) :=
let f0 := f(fun _ => false) in
if is_cst_bf f0 f then (Some(Cst _ f0)) else None.

Definition extract_pi0 {n} (f:bf n) : option(term n) :=
match n as n0 return bf n0 -> option(term n0) with
| 0 => (fun _ => None)
| S n' => (fun f =>
  let f0 := f(fun _ => false) in
  if beq_bf (pi0 n' f0) f
  then Some(Pi0 n' f0)
  else None)
end f.

Definition extract_term {n} (f:bf n) : option(term n) :=
match extract_cst f with
| Some t => Some t
| None =>
  match extract_pi0 f with
  | Some t => Some t
  | None => None
  end
end.

Lemma extract_cst_eq_Some {n} (f:bf n) t :
  extract_cst f = Some t <->
    t = Cst _ (f(fun _ => false)) /\ f = sem_term t.
Proof with curry1.
unfold extract_cst.
dest_match_step...
- rewrite (eq_sym_iff (Cst n _ ) t)...
- rewrite H0 in D...
Qed.
Hint Rewrite @extract_cst_eq_Some : curry1_rewrite.

Lemma extract_pi0_eq_Some n f t :
  extract_pi0 f = Some t <->
    match n as n0 return bf n0 -> term n0 -> Prop with
    | 0 => (fun _ _ => False)
    | S n' => (fun f t =>
      t = Pi0 n' (f(fun _ => false)) /\ f = sem_term t)
    end f t.
Proof with curry1.
destruct n...
rewrite andb_true_iff...
rewrite shannon_expansion...
split...
Qed.
Hint Rewrite extract_pi0_eq_Some : curry1_rewrite.

Lemma extract_cst_cst_bf n b : extract_cst (cst_bf n b) = Some(Cst n b).
Proof. unfold extract_cst; curry1. Qed.
Hint Rewrite extract_cst_cst_bf : curry1_rewrite.

Lemma pi0_cst n b (a:ba (S n)) : pi0 n b a = xorb b (shead a).
Proof. reflexivity. Qed.
Hint Rewrite pi0_cst : curry1_rewrite.

Lemma extract_term_correct {n} f t : @extract_term n f = Some t <-> sem_term t = f.
Proof with curry1.
unfold extract_term, sem_term.
dest_match_step...
- rewrite H0...
  split...
  dest_match_step...
- dependent destruction t...
  + dest_match_step...
  + split...
Qed.

(* [NOTE] we enforce constantes to be part of terminals *)
Definition term_of_bool (f:bf 0) : term 0 :=
  Cst 0 (f(fun _ => false)).

Lemma extract_term_cst_bf n b :
  extract_term (cst_bf n b) = Some(Cst n b).
Proof with curry1.
unfold extract_term...
Qed.
Hint Rewrite extract_term_cst_bf : curry1_rewrite.

Lemma term_of_bool_cst_bf b :
  term_of_bool (cst_bf 0 b) = Cst 0 b.
Proof. reflexivity. Qed.
Hint Rewrite term_of_bool_cst_bf : curry1_rewrite.

Lemma extract_term_0 (f:bf 0) : extract_term f = Some(term_of_bool f).
Proof with curry1.
rewrite (bf0 f)...
Qed.

Definition is_term {n} (f:bf n) : bool :=
  @PrimOGen.is_term term (@extract_term) n f.

(* SubSection 2. Defining Primitives *)
Definition prim := oprim .

Definition sem_prim (p:prim) {n} (f:bf n) : bf (S n) := intro_oP_bf (oPrim_of_oprim p) f.
Definition detect_prim (p:prim) {n} (f:bf(S n)) : bool := is_oP_bf (oPrim_of_oprim p) f.
Definition elim_prim (p:prim) {n} (f:bf(S n)) : bf n := elim_oP_bf (oPrim_of_oprim p) f.

Definition extract_prim (p:prim) {n} (f:bf(S n)) : option(bf n) :=
if detect_prim p f then Some(elim_prim p f) else None.

Definition is_prim (p:prim) {n} (f:bf(S n)) :=
  @PrimOGen.is_prim prim (@extract_prim) p n f.

Definition is_prim' (p:prim) {n} (f:bf n) :=
  @PrimOGen.is_prim' prim (@extract_prim) p n f.

Lemma sem_prim_as_detect_prim p {n} f g :
  @sem_prim p n f = g <-> detect_prim p g = true /\ f = elim_prim p g.
Proof with curry1.
unfold detect_prim, elim_prim, sem_prim.
rewrite (intro_oP_bf_as_is_oP_bf (oPrim_of_oprim p) _ f g)...
Qed.

Lemma extract_prim_iff_sem_prim {n} f p xf :
  @extract_prim p n f = Some xf <-> sem_prim p xf = f.
Proof with curry1.
unfold extract_prim...
rewrite sem_prim_as_detect_prim...
Qed.
Hint Rewrite @extract_prim_iff_sem_prim.

Lemma intro_oC_bf_cst_bf if0 th0 n cst0 :
  intro_oC_bf if0 th0 (cst_bf n cst0) =
    if eqb cst0 th0 then (cst_bf _ cst0) else (pi0 _ (xorb if0 th0)).
Proof with curry1.
setoid_rewrite (shannon_expansion_gen if0)...
dest_match_step...
Qed.
Hint Rewrite intro_oC_bf_cst_bf : curry1_rewrite.

Lemma is_term_cst_bf n c : is_term (cst_bf n c) = true.
Proof with curry1.
unfold is_term, PrimOGen.is_term...
Qed.
Hint Rewrite is_term_cst_bf : curry1_rewrite.

Lemma is_cst_bf_pi0 b1 n b2 : is_cst_bf b1 (pi0 n b2) = false.
Proof with curry1.
destruct(is_cst_bf b1 (pi0 n b2))eqn:E...
Qed.
Hint Rewrite is_cst_bf_pi0 : curry1_rewrite.

Lemma extract_cst_pi0 n b :
  extract_cst (pi0 n b) = None.
Proof. unfold extract_cst; curry1. Qed.
Hint Rewrite extract_cst_pi0 : curry1_rewrite.

Lemma extract_term_pi0 n b :
  extract_term (pi0 n b) = Some(Pi0 n b).
Proof. unfold extract_term; curry1. Qed.
Hint Rewrite extract_term_pi0 : curry1_rewrite.

Lemma is_term_pi0 n b : is_term (pi0 n b) = true.
Proof. unfold is_term, PrimOGen.is_term; curry1. Qed.
Hint Rewrite is_term_pi0 : curry1_rewrite.

Lemma is_term_intro_oC_bf if0 th0 n cst0 :
  is_term (intro_oC_bf if0 th0 (cst_bf n cst0)) = true.
Proof with curry1.
simpl...
dest_match_step...
Qed.

Lemma intro_oU_bf_cst_bf n b : intro_oU_bf(cst_bf n b) = cst_bf _ b.
Proof. unfold intro_oU_bf; curry1. Qed.
Hint Rewrite intro_oU_bf_cst_bf : curry1_rewrite.

Lemma intro_oX_bf_cst_bf n b : intro_oX_bf (cst_bf n b) = pi0 n b.
Proof. setoid_rewrite shannon_expansion; curry1. Qed.
Hint Rewrite intro_oX_bf_cst_bf : curry1_rewrite.

Lemma elim_prim_cst_bf p n b : elim_prim p (cst_bf (S n) b) = cst_bf n b.
Proof. unfold elim_prim; curry1. Qed.
Hint Rewrite elim_prim_cst_bf : curry1_rewrite.

Lemma sem_prim_OU {n} f: @sem_prim OU n f = intro_oU_bf f.
Proof. reflexivity. Qed.
Hint Rewrite @sem_prim_OU : curry1_rewrite.

Lemma sem_prim_OX {n} f: @sem_prim OX n f = intro_oX_bf f.
Proof. reflexivity. Qed.
Hint Rewrite @sem_prim_OX : curry1_rewrite.

Lemma sem_prim_OC if0 th0 {n} f: @sem_prim (OC if0 th0) n f = intro_oC_bf if0 th0 f.
Proof. reflexivity. Qed.
Hint Rewrite @sem_prim_OC : curry1_rewrite.

Lemma eval_prim_false_fun_false p b :
  eval_prim (oPrim_of_oprim p) false b =
    match p with OC false th0 => th0 | _ => b end.
Proof with curry1.
destruct p...
BPS.MicroBPSolver...
Qed.

Lemma sem_prim_fun_false p {n} f :
  @sem_prim p n f (fun _  => false) =
    match p with
    | OC false th0 => th0
    | _ => f(fun _ => false)
    end.
Proof with curry1.
unfold sem_prim, intro_oP_bf...
rewrite eval_prim_false_fun_false...
Qed.

Lemma is_term_sem_prim p {n} (f:bf n): is_term (sem_prim p f) = isSome(extract_cst f).
Proof with curry1.
unfold is_term, PrimOGen.is_term...
destruct(extract_cst f)eqn:E...
- rewrite H0...
  destruct p...
  dest_match_step...
- destruct(extract_term(sem_prim p f))eqn:E0...
  rewrite extract_term_correct in E0...
  unfold sem_prim in E0.
  symmetry in E0.
  rewrite (intro_oP_bf_as_is_oP_bf (oPrim_of_oprim p) _ f) in E0...
  unfold elim_oP_bf in E...
  dependent destruction t...
Qed.
Hint Rewrite is_term_sem_prim : curry1_rewrite.

Lemma is_oU_bf_cst_bf n b : @is_oU_bf n (cst_bf _ b) = true.
Proof. unfold is_oU_bf; curry1. Qed.
Hint Rewrite is_oU_bf_cst_bf : curry1_rewrite.

Lemma is_oU_bf_pi0 n b : @is_oU_bf n (pi0 _ b) = false.
Proof. unfold is_oU_bf; curry1. Qed.
Hint Rewrite is_oU_bf_pi0 : curry1_rewrite.

Lemma is_oX_bf_cst_bf n b : @is_oX_bf n (cst_bf _ b) = false.
Proof. unfold is_oX_bf; curry1. Qed.
Hint Rewrite is_oX_bf_cst_bf : curry1_rewrite.

Lemma is_oX_bf_pi0 n b : @is_oX_bf n (pi0 _ b) = true.
Proof. unfold is_oX_bf; curry1. Qed.
Hint Rewrite is_oX_bf_pi0 : curry1_rewrite.

Lemma is_oC_bf_cst_bf if0 th0 n b :
  @is_oC_bf if0 th0 n (cst_bf _ b) = eqb th0 b.
Proof. unfold is_oC_bf; curry1. Qed.
Hint Rewrite is_oC_bf_cst_bf : curry1_rewrite.

Lemma is_oC_bf_pi0 if0 th0 n b :
  @is_oC_bf if0 th0 n (pi0 _ b) = eqb th0 (xorb if0 b).
Proof. unfold is_oC_bf; curry1. Qed.
Hint Rewrite is_oC_bf_pi0 : curry1_rewrite.

Lemma detect_prim_oC if0 th0 {n} f : @detect_prim (OC if0 th0) n f = is_oC_bf if0 th0 f.
Proof. reflexivity. Qed.
Hint Rewrite detect_prim_oC : curry1_rewrite.

Lemma detect_prim_sem_prim p1 p2 {n} (f2:bf n) :
  detect_prim p1 (sem_prim p2 f2) = true ->
    p1 = p2 \/ (is_term(sem_prim p2 f2) = true).
Proof with curry1.
destruct(is_term (sem_prim p2 f2))eqn:E...
destruct p1, p2...
dest_match_step...
Qed.

Lemma elim_prim_sem_prim p {n} (f:bf n) :
  elim_prim p (sem_prim p f) = f.
Proof. unfold elim_prim, sem_prim; curry1. Qed.
Hint Rewrite @elim_prim_sem_prim : curry1_rewrite.

Lemma sem_prim_injective {n p1 f1} :
  @is_term (S n) (sem_prim p1 f1) = false ->
  forall p2 f2,
    (sem_prim p1 f1 = sem_prim p2 f2 <-> p1 = p2 /\ f1 = f2).
Proof with curry1.
split...
symmetry in H0.
rewrite sem_prim_as_detect_prim in H0...
specialize(detect_prim_sem_prim p2 p1 f1)...
Qed.

Definition is_prim_free {n} (f:bf n) : bool :=
  match n as n0 return bf n0 -> bool with
  | 0 => (fun _ => true)
  | S n' => (fun f =>
    negb(detect_prim OU f) &&
    negb(detect_prim OX f) &&
    negb(detect_prim (OC false false) f) &&
    negb(detect_prim (OC false true ) f) &&
    negb(detect_prim (OC true  false) f) &&
    negb(detect_prim (OC true  true ) f))
  end f.

Lemma is_prim_free_correct {n} (f:bf n) :
  is_prim_free f = true <-> forall p, is_prim' p f = false.
Proof with curry1.
unfold is_prim_free...
destruct n...
repeat rewrite andb_true_iff.
unfold is_prim, PrimOGen.is_prim...
unfold extract_prim...
split...
- destruct p...
  dest_bool.
- specialize(H OU) as HH...
  specialize(H OX) as HH...
  specialize(H (OC false false)) as HH...
  specialize(H (OC false true )) as HH...
  specialize(H (OC true  false)) as HH...
  specialize(H (OC true  true )) as HH...
Qed.

Definition xprim {F} : nat -> Type :=
  @PrimOGen.xprim term prim F.

Definition extract_xprim {n} (f:bf n) : @xprim bf n :=
match n as n0 return bf n0 -> @xprim bf n0 with
| O => (fun f => XTerm _ _ (term_of_bool f))
| S n' => (fun f =>
  match extract_term f with
  | Some t => XTerm _ _ t
  | None =>
  match extract_prim OU f with
  | Some f' => XPrim _ _ OU f'
  | None =>
  match extract_prim OX f with
  | Some f' => XPrim _ _ OX f'
  | None =>
  match extract_prim (OC false false) f with
  | Some f' => XPrim _ _ (OC false false) f'
  | None =>
  match extract_prim (OC false true ) f with
  | Some f' => XPrim _ _ (OC false true ) f'
  | None =>
  match extract_prim (OC true  false) f with
  | Some f' => XPrim _ _ (OC true  false) f'
  | None =>
  match extract_prim (OC true  true ) f with
  | Some f' => XPrim _ _ (OC true true ) f'
  | None => (XSha _ _ (fnary_evalo1 f false) (fnary_evalo1 f true))
  end
  end
  end
  end
  end
  end
  end)
end f.

Definition sem_xprim {n} (xp:@xprim bf n) : bf n :=
  @PrimOGen.sem_xprim term (@sem_term) prim (@sem_prim) n xp.

Lemma sem_xprim_extract_single {n} (f:bf n) :
  sem_xprim(extract_xprim f) = f.
Proof with curry1.
unfold sem_xprim, PrimOGen.sem_xprim, extract_xprim.
dest_match...
- rewrite bf0...
  rewrite (sempty_unique (fun _ => false))...
- rewrite extract_term_correct in D...
- rewrite extract_prim_iff_sem_prim in D0...
- rewrite extract_prim_iff_sem_prim in D1...
- rewrite extract_prim_iff_sem_prim in D2...
- rewrite extract_prim_iff_sem_prim in D3...
- rewrite extract_prim_iff_sem_prim in D4...
- rewrite extract_prim_iff_sem_prim in D5...
Qed.

Lemma extract_prim_eq_None p {n} f : @extract_prim p n f = None <-> detect_prim p f = false.
Proof. unfold extract_prim; curry1. Qed.

Lemma extract_xprim_minimal {n} (f:bf n) :
  match extract_xprim f with
  | XTerm _ _ _ => True
  | XPrim  _ _ _ _ => is_term f = false
  | XSha _ _ _ _ => is_term f = false /\ is_prim_free f = true
  end.
Proof with curry1.
unfold extract_xprim.
dest_match_step...
dest_match_step...
assert(is_term f = false).
{ unfold is_term, PrimOGen.is_term... }
dest_match...
rewrite extract_prim_eq_None in D0, D1, D2, D3, D4, D5...
Qed.

(* describe transformations (that is non-normalized trans) *)
Definition trans : option nat -> nat -> Type :=
  PrimOGen.trans term prim.

Lemma is_term_is_prim_with_is_term_false {n} (f0:bf n) :
  is_term f0 = false -> forall p, is_term(sem_prim p f0) = false.
Proof with curry1.
intros...
destruct(extract_cst f0)eqn:E...
rewrite H1 in H...
Qed.

Definition LDD : nat -> Type :=
  (PrimOGen.LDD
    term (@sem_term) (@extract_term) (@extract_term_correct)
  term_of_bool extract_term_0 prim (@sem_prim) (@extract_prim)
  (@extract_prim_iff_sem_prim)
  (@sem_prim_injective)
  (@is_prim_free)
  (@is_prim_free_correct)
  (@extract_xprim)
  (@sem_xprim_extract_single)
  (@extract_xprim_minimal)).

Definition extract : forall {n}, bf n -> LDD n :=
  (@PrimOGen.extract term (@sem_term) (@extract_term) (@extract_term_correct)
  term_of_bool extract_term_0 prim (@sem_prim) (@extract_prim)
  (@extract_prim_iff_sem_prim)
  (@sem_prim_injective)
  (@is_prim_free)
  (@is_prim_free_correct)
  (@extract_xprim)
  (@sem_xprim_extract_single)
  (@extract_xprim_minimal)).

Definition sem : forall {n}, LDD n -> bf n :=
  (@PrimOGen.sem term (@sem_term) (@extract_term) (@extract_term_correct)
  term_of_bool extract_term_0 prim (@sem_prim) (@extract_prim)
  (@extract_prim_iff_sem_prim)
  (@sem_prim_injective)
  (@is_prim_free)
  (@is_prim_free_correct)
  (@extract_xprim)
  (@sem_xprim_extract_single)
  (@extract_xprim_minimal)).

Theorem sem_extract {n} (f:bf n) : sem(extract f) = f.
Proof. apply PrimOGen.sem_extract. Qed.

Theorem extract_sem {n} (l:LDD n) : extract(sem l) = l.
Proof.
apply PrimOGen.extract_sem.
apply (@is_term_is_prim_with_is_term_false).
Qed.

Theorem LDD_uniqueness {n} (l1 l2:LDD n) : sem l1 = sem l2 <-> l1 = l2.
Proof.
apply PrimOGen.LDD_uniqueness.
apply (@is_term_is_prim_with_is_term_false).
Qed.

Theorem LDD_existence {n} (f:bf n) : exists (l:LDD n), sem l = f.
Proof.
apply PrimOGen.LDD_existence.
Qed.

(*
(* TESTS *)

Definition f1 : bf 4 := (fun (x:ba 4) =>
  let x0 := x(ltN_of_nat 0 4 (eq_refl _)) in
  let x1 := x(ltN_of_nat 1 4 (eq_refl _)) in
  let x2 := x(ltN_of_nat 2 4 (eq_refl _)) in
  let x3 := x(ltN_of_nat 3 4 (eq_refl _)) in
  xorb x0 (xorb x1 (xorb x2 x3))).

Definition ldd1 : LDD 4 := extract f1.

Definition f2 : bf 10 := (fun (x:ba 10) =>
  let x0 := x(ltN_of_nat 0 10 (eq_refl _)) in
  let x1 := x(ltN_of_nat 1 10 (eq_refl _)) in
  let x2 := x(ltN_of_nat 2 10 (eq_refl _)) in
  let x3 := x(ltN_of_nat 3 10 (eq_refl _)) in
  let x4 := x(ltN_of_nat 4 10 (eq_refl _)) in
  let x5 := x(ltN_of_nat 5 10 (eq_refl _)) in
  let x6 := x(ltN_of_nat 6 10 (eq_refl _)) in
  let x7 := x(ltN_of_nat 7 10 (eq_refl _)) in
  let x8 := x(ltN_of_nat 8 10 (eq_refl _)) in
  let x9 := x(ltN_of_nat 9 10 (eq_refl _)) in
  xorb x0 (xorb x1 (xorb x2 (xorb x3 (xorb x4
 (xorb x5 (xorb x6 (xorb x7 (xorb x8 x9))))))))).

Definition ldd2 : LDD 10 := extract f2.

Extraction "ldd_o_ucx_tests" extract ldd1 ldd2.

(* When Evaluated in OCaml using OCaml's toplevel.
val f1 : (nat -> bool) -> bool = <fun>
val ldd1 : (term, prim) trans ldd =
  Leaf (S (S (S (S O))),
   EPrim (None, S (S (S O)), OX,
    EPrim (None, S (S O), OX,
     EPrim (None, S O, OX, ETerm (S O, Pi0 (O, False))))))
val f2 : (nat -> bool) -> bool = <fun>
val ldd2 : (term, prim) trans ldd =
  Leaf (S (S (S (S (S (S (S (S (S (S O))))))))),
   EPrim (None, S (S (S (S (S (S (S (S (S O)))))))), OX,
    EPrim (None, S (S (S (S (S (S (S (S O))))))), OX,
     EPrim (None, S (S (S (S (S (S (S O)))))), OX,
      EPrim (None, S (S (S (S (S (S O))))), OX,
       EPrim (None, S (S (S (S (S O)))), OX,
        EPrim (None, S (S (S (S O))), OX,
         EPrim (None, S (S (S O)), OX,
          EPrim (None, S (S O), OX,
           EPrim (None, S O, OX, ETerm (S O, Pi0 (O, False))))))))))))
 *)
*)

(* Extra *)

