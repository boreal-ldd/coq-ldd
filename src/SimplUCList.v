Require Import Arith .
Require Import Psatz.
Require Import Bool.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Bool Ring.
Require Import Ground.SimplProp.
Require Import Ground.SimplBool.
Require Import Ground.SimplEq.
Require Import Ground.SimplList.

(* Section 2. uclist *)

(** Ultimately Constant List **)
(* defines an infinite list made of two components :
  - a finite list
  - and an element which is infinetely replicated *)
Definition uclist (A:Type) : Type := (list A) * A.

(* The [uclist_nth] accessor :
  - allows to access to the [n]-th element of the list
  - matches the verbose definition of [uclist]s *)
Definition uclist_nth {A} (l:uclist A) (n:nat) : A.
Proof.
destruct l as [l t].
apply (List.nth n l t).
Defined.

Definition uclist_length {A} (l:uclist A) := List.length (fst l).

Definition uclist_map {A B:Type} (f:A->B) (l:uclist A) : uclist B.
Proof.
destruct l.
apply (List.map f l, f a).
Defined.

Fixpoint uclist_map2_rec {A B C} (f:A->B->C) (lA:list A) (lB:list B) (tA:A) (tB:B)
  : list C :=
match lA, lB with
| nil, nil => nil
| nil, _   => List.map (fun x => f tA x) lB
| _  , nil => List.map (fun x => f x tB) lA
| cons xA lA, cons xB lB => cons (f xA xB) (uclist_map2_rec f lA lB tA tB)
end.

Definition uclist_map2 {A B C} (f:A->B->C) (lA:uclist A) (lB:uclist B) : uclist C.
Proof.
destruct lA as [lA tA], lB as [lB tB].
apply (uclist_map2_rec f lA lB tA tB, f tA tB).
Defined.

Definition uclist_cst {A} (x:A) : uclist A := (nil, x).

Definition uclist_cons {A} (x:A) (l:uclist A) : uclist A :=
  match l with (l, t) => (cons x l, t) end.

Definition uclist_normalized {A} (eq:@beqt A) (uc:uclist A) : bool :=
let (ll, lt) := uc in
match list_last_error ll with
| Some xx => negb(eq lt xx)
| None    => true
end.

Definition uclist_normalized_P {A} (uc:uclist A) : Prop :=
  match list_last_error (fst uc) with
  | Some last => ~(last = (snd uc))
  | None      => True
  end.

(* Extension of the [List.cons] operator to [uclist] :
  [@uclist_cons_safe A eq x uc = uc']
  such that if [uclist_normalized_P uc] then [uclist_normalized_P uc']
  with [eq] the decidable equality other [A] *)
Definition uclist_cons_safe {A} (eq:A->A->bool) (x:A) (l:uclist A) : uclist A :=
  match l with
  | (nil, t) => if eq x t then (nil, t) else (cons x nil, t)
  | (l, t) => (cons x l, t)
  end.

Lemma uclist_normalized_P_uclist_cons_safe {A} x uc eq (H:@beq_iff_true A eq) :
  uclist_normalized_P (uclist_cons_safe eq x uc) <-> uclist_normalized_P uc.
Proof with simpllist1.
unfold uclist_normalized_P, uclist_cons_safe...
dest_match...
simpleq_auto.
Qed.

Lemma uclist_map2_rec_nil {A B C} f lA lB tA tB :
  @uclist_map2_rec A B C f lA lB tA tB = nil <-> lA = nil /\ lB = nil.
Proof with simpllist1.
generalize dependent lB.
destruct lA, lB...
Qed.

Lemma uclist_map2_nil {A B C} f lA lB t :
  @uclist_map2 A B C f lA lB = (nil, t) <->
    match lA, lB with
    | (lA, tA), (lB, tB) => lA = nil /\ lB = nil /\ t = f tA tB
    end.
Proof with simpllist1.
simpl...
rewrite uclist_map2_rec_nil...
firstorder.
Qed.

Lemma uclist_map2_rec_map_l {A A' B C} (f2:A'->B->C) (f:A->A') lA lB tA tB :
@uclist_map2_rec A' B C f2 (List.map f lA) lB (f tA) tB =
  @uclist_map2_rec A B C (fun x y => f2 (f x) y) lA lB tA tB.
Proof with simpllist1.
generalize dependent lB.
induction lA; destruct lB...
Qed.

Lemma uclist_map2_rec_map_r {A B B' C} (f2:A->B'->C) (f:B->B') lA lB tA tB :
@uclist_map2_rec A B' C f2 lA (List.map f lB) tA (f tB) =
  @uclist_map2_rec A B C (fun x y => f2 x (f y)) lA lB tA tB.
Proof with simpllist1.
generalize dependent lB.
induction lA; destruct lB...
Qed.

Lemma map_uclist_map2_rec {A B C C'} (f2:A->B->C) (f:C->C') lA lB tA tB :
List.map f (@uclist_map2_rec A B C f2 lA lB tA tB) =
  @uclist_map2_rec A B C' (fun x y => f(f2 x y)) lA lB tA tB.
Proof with simpllist1.
generalize dependent lB.
induction lA; destruct lB; simpllist1;
  try rewrite map_map; simpllist1.
Qed.

Lemma uclist_length_uclist_map2_rec {A B C} f l1 l2 t1 t2 :
  List.length (@uclist_map2_rec A B C f l1 l2 t1 t2) = max(List.length l1)(List.length l2).
Proof with simpllist1.
generalize dependent l2.
induction l1; destruct l2; simpllist1...
Qed.

Lemma uclist_length_uclist_map2 {A B C} f ld1 ld2 :
  uclist_length (@uclist_map2 A B C f ld1 ld2) = max(uclist_length ld1)(uclist_length ld2).
Proof with simpllist1.
destruct ld1 as [l1 t1], ld2 as [l2 t2]...
apply uclist_length_uclist_map2_rec.
Qed.

Lemma uclist_map2_rec_nil_l {A B C} f lB tA tB :
  @uclist_map2_rec A B C f nil lB tA tB = List.map (fun x => f tA x) lB.
Proof. destruct lB; reflexivity. Qed.

Lemma uclist_map2_rec_nil_r {A B C} f lA tA tB :
  @uclist_map2_rec A B C f lA nil tA tB = List.map (fun x => f x tB) lA.
Proof. destruct lA; reflexivity. Qed.

Lemma forallb_uclist_map2_rec_with_forallb {A B C} pA pB pC (f:A->B->C)
  (HC:forall (a:A) (b:B), pA a = true -> pB b = true -> pC(f a b) = true )
  lA tA lB tB
  (TA:pA tA = true)              (TB:pB tB = true)
  (HA:List.forallb pA lA = true) (HB:List.forallb pB lB = true) :
  List.forallb pC (uclist_map2_rec f lA lB tA tB) = true.
Proof with simpllist1.
generalize dependent lB.
induction lA; destruct lB; simpllist1;
  rewrite HC; simpl in *; simpllist1;
  rewrite forallb_map; simpl in *...
- rewrite (forallb_with_forallb pB (fun x : B => pC (f tA x)))...
- rewrite (forallb_with_forallb pA (fun x : A => pC (f x tB)))...
Qed.

Lemma uclist_nth_uclist_map2 {A B C} f uA uB i :
  uclist_nth (@uclist_map2 A B C f uA uB) i = f (uclist_nth uA i) (uclist_nth uB i).
Proof with simpllist1.
destruct uA as [lA tA], uB as [lB tB]...
generalize dependent lB.
generalize dependent lA.
induction i; destruct lA, lB...
- rewrite nth_map.
- rewrite nth_map, nth_to_nth_error.
  dest_match.
Qed.

Definition uclist_beq {A} (beq:A->A->bool) (u1 u2:uclist A) :=
  let (l, t) := uclist_map2 beq u1 u2 in
  List.forallb id l && t.

Lemma uclist_map2_rec_same {A B} (f:A->A->B) l t0 t1 :
  uclist_map2_rec f l l t0 t1 = List.map (fun x => f x x) l.
Proof with simpllist1.
induction l...
Qed.

Lemma ld_eq_rewrite {A} (ld1 ld2:uclist A) :
  ld1 = ld2 <-> uclist_length ld1 = uclist_length ld2 /\ (forall i, uclist_nth ld1 i = uclist_nth ld2 i).
Proof with simpllist12.
destruct ld1, ld2...
split...
rewrite list_eq_rewrite.
unfold uclist_length in H...
split...
- specialize(H0 i).
  repeat rewrite nth_to_nth_error in H0.
  dest_match...
  + rewrite <- H in D0...
  + rewrite H in D...
- specialize(H0(length l)).
  repeat rewrite nth_to_nth_error in H0.
  assert(R1:length l <= length l)...
  assert(R2:length l0 <= length l)...
  rewrite_subst...
Qed.

Definition uclist_normalize {A} (eq:@beqt A) (uc:uclist A) : uclist A :=
  let (ll, lt) := uc in (rm_trail(eq lt)ll, lt).

Lemma uclist_normalized_normalize {A} (eq:@beqt A) (uc:uclist A) :
  uclist_normalized eq (uclist_normalize eq uc) = true.
Proof with simpllist12.
destruct uc...
specialize(list_last_error_rm_trail(eq a)l)...
dest_match...
Qed.

Lemma list_last_error_uclist_map2_rec {A B C} f lA lB tA tB :
list_last_error (@uclist_map2_rec A B C f lA lB tA tB) =
  match list_last_error lA, list_last_error lB with
  | Some xA, Some xB => Some(if length lA =? length lB
    then f xA xB
    else if length lA <=? length lB
      then f tA xB
      else f xA tB)
  | Some xA, None    => Some(f xA tB)
  | None   , Some xB => Some(f tA xB)
  | None   , None    => None
  end.
Proof with simpllist12.
generalize dependent lB.
induction lA; destruct lB...
- rewrite list_last_error_map.
  dest_match...
- rewrite list_last_error_map.
  dest_match...
- rewrite (IHlA lB)... clear IHlA.
  dest_match_step...
  + dest_match_step...
    rewrite list_last_error_eq_None in D0...
    dest_if...
    simpl in *...
  + rewrite list_last_error_eq_None in D...
    dest_match...
    * simpl in *...
    * rewrite list_last_error_eq_None in D...
Qed.

Lemma uclist_normalize_normalized {A eq uc} (H:@uclist_normalized A eq uc = true) :
  uclist_normalize eq uc = uc.
Proof with simpllist12.
destruct uc...
induction l...
simpl in *...
gen_dest_match simpllist12; simpl in *...
Qed.

Lemma uclist_nth_uclist_normalize {A} (eq:@beqt A) (EQ:beq_iff_true eq) uc i :
  uclist_nth (uclist_normalize eq uc) i = uclist_nth uc i.
Proof with simpllist12; simpleq_auto.
destruct uc as [ll lt]...
generalize dependent ll.
induction i; destruct ll...
- gen_dest_match simpllist12...
- specialize(IHi ll)...
  gen_dest_match simpllist12...
Qed.

Lemma ld_eq_rewrite_using_normalized {A} (eq:@beqt A) (EQ:beq_iff_true eq)
  ld1 (H1:uclist_normalized eq ld1 = true)
  ld2 (H2:uclist_normalized eq ld2 = true) :
  ld1 = ld2 <-> (forall i, uclist_nth ld1 i = uclist_nth ld2 i).
Proof with simpllist12.
rewrite (ld_eq_rewrite ld1 ld2).
split...
destruct ld1 as [ll1 lt1], ld2 as [ll2 lt2]; simpl in *...
specialize(list_nth_last_eq _ _ _ _ H)...
unfold uclist_length...
dest_match...
- destruct(Nat.leb (S(length ll1)) (length ll2)) eqn:E0;
  destruct(Nat.leb (S(length ll2)) (length ll1)) eqn:E1.
  + rewrite Nat.leb_le in E0, E1. omega.
  + rewrite Nat.leb_le in E0. clear E1.
    specialize(H(pred(length ll2))).
    rewrite list_last_error_vs_nth_error in D...
    rewrite list_last_error_vs_nth_error in D0...
    repeat rewrite nth_to_nth_error in H...
    destruct(length ll2) eqn:EN; simpl in *...
    simpleq_auto.
  + rewrite Nat.leb_le in E1. clear E0.
    specialize(H(pred(length ll1))).
    rewrite list_last_error_vs_nth_error in D...
    rewrite list_last_error_vs_nth_error in D0...
    repeat rewrite nth_to_nth_error in H...
    destruct(length ll1) eqn:EN; simpl in *...
    simpleq_auto.
  + rewrite Nat.leb_gt in *.
    omega.
- rewrite list_last_error_eq_None in D0...
  simpl in *...
  specialize(H(pred(length ll2)))...
  rewrite list_last_error_vs_nth_error in D...
  repeat rewrite nth_to_nth_error in H...
  dest_match; simpleq_auto.
- rewrite list_last_error_eq_None in D...
  simpl in *...
  specialize(H(pred(length ll1))).
  rewrite list_last_error_vs_nth_error in D0...
  repeat rewrite nth_to_nth_error in H...
  dest_match; simpleq_auto.
- rewrite list_last_error_eq_None in D...
  rewrite list_last_error_eq_None in D0...
Qed.

Definition uclist_forallb {A} (p:A->bool) (uc:uclist A) :=
  List.forallb p (fst uc) && p (snd uc).

Lemma nth_error_uclist_map2_rec {A B C} f lA lB tA tB i :
  List.nth_error (@uclist_map2_rec A B C f lA lB tA tB) i =
    if max(List.length lA)(List.length lB) <=? i
    then None
    else Some(f (List.nth i lA tA) (List.nth i lB tB)).
Proof with simpllist14.
generalize dependent lB.
generalize dependent lA.
induction i; destruct lA, lB...
- rewrite nth_to_nth_error.
  dest_match...
- rewrite nth_to_nth_error.
  dest_match...
Qed.

Lemma uclist_subset_inter {A} (subset:A->A->bool) (inter:A->A->A)
  (SI:forall x y z, subset x (inter y z) = (subset x y) && (subset x z)) ld ld1 ld2 :
    uclist_forallb id (uclist_map2 subset ld (uclist_map2 inter ld1 ld2)) =
      uclist_forallb id (uclist_map2 subset ld ld1) && uclist_forallb id (uclist_map2 subset ld ld2).
Proof with simpllist14.
destruct ld1 as [l1 d1], ld2 as [l2 d2], ld as [ll dd]...
unfold uclist_forallb...
repeat rewrite SI...
destruct(subset dd d1)eqn:E1...
destruct(subset dd d2)eqn:E2...
rewrite eq_iff_eq_true.
rewrite andb_true_iff.
repeat rewrite list_forallb_rewrite_nth_error.
repeat split...
- specialize(H i).
  rewrite nth_error_uclist_map2_rec.
  rewrite nth_error_uclist_map2_rec in H.
  rewrite nth_to_nth_error in H.
  rewrite nth_to_nth_error in H.
  rewrite nth_error_uclist_map2_rec in H.
  rewrite uclist_length_uclist_map2_rec in H.
  repeat rewrite max_leb in *.
  repeat rewrite nth_to_nth_error...
  repeat rewrite nth_to_nth_error in H...
  dest_match; rewrite SI in *...
- specialize(H i).
  rewrite nth_error_uclist_map2_rec.
  rewrite nth_error_uclist_map2_rec in H.
  rewrite nth_to_nth_error in H.
  rewrite nth_to_nth_error in H.
  rewrite nth_error_uclist_map2_rec in H.
  rewrite uclist_length_uclist_map2_rec in H.
  repeat rewrite max_leb in *.
  repeat rewrite nth_to_nth_error in *.
  dest_match; rewrite SI in H...
- specialize(H i). specialize(H0 i).
  rewrite nth_error_uclist_map2_rec.
  rewrite nth_error_uclist_map2_rec in H, H0.
  repeat rewrite nth_to_nth_error.
  rewrite nth_error_uclist_map2_rec.
  repeat rewrite nth_to_nth_error in *.
  rewrite uclist_length_uclist_map2_rec.
  repeat rewrite max_leb in *.
  dest_match; rewrite SI...
Qed.

Lemma uclist_map2_rec_map_rm_trail {A B C} ff f p l d1 d2 :
  @uclist_map2_rec _ _ C ff (@List.map A B f (rm_trail p l)) l d1 d2 =
  List.app (List.map (fun x => ff (f x) x) (rm_trail p l))
           (List.map (fun x => ff d1 x) (ntll (length(rm_trail p l)) l)).
Proof with simpllist1.
induction l...
dest_match_step...
dest_match_step...
Qed.

Lemma uclist_forallb_cons {A} p x l d :
  @uclist_forallb A p (cons x l, d) = p x && (uclist_forallb p (l, d)).
Proof with simpllist1.
unfold uclist_forallb...
rewrite andb_assoc...
Qed.

