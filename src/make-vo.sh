#coqc=~/.opam/system/bin/coqc
coqc=coqc
#coqc=~/.opam/4.08.1/bin/coqc

echo -e

echo "$1" >> files.fof
echo "§§ SET FKEY $1" > $1.out
echo "§§ SET DONE FALSE" >> $1.out
time -p $coqc $1 2>&1 | tee -a $1.out
#$coqc $1
echo "§§ SET DONE TRUE" >> $1.out
