
type bool =
| True
| False

(** val xorb : bool -> bool -> bool **)

let xorb b1 b2 =
  match b1 with
  | True -> (match b2 with
             | True -> False
             | False -> True)
  | False -> b2

(** val negb : bool -> bool **)

let negb = function
| True -> False
| False -> True

type nat =
| O
| S of nat

type 'a option =
| Some of 'a
| None

type 'a sig0 = 'a
  (* singleton inductive, whose constructor was exist *)

(** val eqb : bool -> bool -> bool **)

let eqb b1 b2 =
  match b1 with
  | True -> b2
  | False -> (match b2 with
              | True -> False
              | False -> True)

(** val solution_left : 'a1 -> 'a2 -> 'a1 -> 'a2 **)

let solution_left _ x _ =
  x

(** val comp : ('a1 -> 'a2) -> ('a2 -> 'a3) -> 'a1 -> 'a3 **)

let comp f g x =
  g (f x)

type ltN = nat

(** val ltN_of_nat : nat -> nat -> ltN **)

let ltN_of_nat k _ =
  k

(** val ltN_S : nat -> ltN -> ltN **)

let ltN_S n k =
  ltN_of_nat (S k) (S n)

type 'a array = ltN -> 'a

(** val sempty : 'a1 array **)

let sempty _ =
  assert false (* absurd case *)

(** val scons : nat -> 'a1 -> 'a1 array -> 'a1 array **)

let scons n a s = function
| O -> a
| S k0 -> s (ltN_of_nat k0 n)

(** val shead : nat -> 'a1 array -> 'a1 **)

let shead n s =
  s (ltN_of_nat O (S n))

(** val stail : nat -> 'a1 array -> 'a1 array **)

let stail n s k =
  s (ltN_S n k)

type ('a, 'b) fnary = 'a array -> 'b

(** val fnary_evalo1 : nat -> ('a1, 'a2) fnary -> 'a1 -> ('a1, 'a2) fnary **)

let fnary_evalo1 n f a s =
  f (scons n a s)

type bf = (bool, bool) fnary

(** val beq_bf : nat -> bf -> bf -> bool **)

let rec beq_bf n f3 f4 =
  match n with
  | O -> eqb (f3 sempty) (f4 sempty)
  | S n0 ->
    (match beq_bf n0 (fnary_evalo1 n0 f3 False) (fnary_evalo1 n0 f4 False) with
     | True -> beq_bf n0 (fnary_evalo1 n0 f3 True) (fnary_evalo1 n0 f4 True)
     | False -> False)

(** val uop_bf : (bool -> bool) -> nat -> bf -> bf **)

let uop_bf m _ f =
  comp f m

(** val neg_bf : nat -> bf -> bf **)

let neg_bf n f =
  uop_bf negb n f

(** val cst_bf : nat -> bool -> bf **)

let cst_bf _ b _ =
  b

(** val is_cst_bf : bool -> nat -> bf -> bool **)

let is_cst_bf b n f =
  beq_bf n f (cst_bf n b)

(** val pi0 : nat -> bool -> bf **)

let pi0 n b x =
  xorb b (shead n x)

type 'edge ldd =
| Leaf of nat * 'edge
| Edge of nat * nat * 'edge * 'edge ldd * 'edge ldd

type 'f node =
| Node of nat * 'f * 'f

type 'f next =
| Term
| Next of nat * 'f

type ('edge, 'f) xedge =
| XEdge of nat option * nat * 'edge * 'f node next

(** val xedge_projN : nat -> ('a1, 'a2) xedge -> nat option **)

let xedge_projN _ = function
| XEdge (opn, _, _, _) -> opn

(** val xedge_projE : nat -> ('a1, 'a2) xedge -> 'a1 **)

let xedge_projE _ = function
| XEdge (_, _, ee, _) -> ee

(** val xedge_projF : nat -> ('a1, 'a2) xedge -> 'a2 node next **)

let xedge_projF _ = function
| XEdge (_, _, _, nx) -> nx

(** val extract_ldd :
    (nat -> bf -> ('a1, bf) xedge) -> nat -> bf -> 'a1 ldd **)

let rec extract_ldd extract_edge0 n f =
  let XEdge (_, ee, xf, nx) = extract_edge0 n f in
  (match nx with
   | Term -> Leaf (ee, xf)
   | Next (_, f3) ->
     let Node (n1, f4, f5) = f3 in
     Edge (n1, ee, xf, (extract_ldd extract_edge0 n1 f4),
     (extract_ldd extract_edge0 n1 f5)))

type 'edge lDD = 'edge ldd

(** val extract :
    (nat -> bf -> ('a1, bf) xedge) -> (nat -> 'a1 -> bf) -> (nat -> nat ->
    'a1 -> bf -> bf) -> nat -> bf -> 'a1 lDD **)

let extract extract_edge0 _ _ n f =
  extract_ldd extract_edge0 n f

type ('term, 'prim, 'f) xprim =
| XTerm of nat * 'term
| XPrim of nat * 'prim * 'f
| XSha of nat * 'f * 'f

type ('term, 'prim) trans =
| ETerm of nat * 'term
| EPrim of nat option * nat * 'prim * ('term, 'prim) trans
| ENext of nat

type ('term, 'prim, 'f) xtrans = (('term, 'prim) trans, 'f) xedge

(** val xTrans :
    nat option -> nat -> ('a1, 'a2) trans -> 'a3 node next -> (('a1, 'a2)
    trans, 'a3) xedge **)

let xTrans opn m ee nx =
  XEdge (opn, m, ee, nx)

(** val xTPrim :
    'a2 -> nat -> ('a1, 'a2, 'a3) xtrans -> ('a1, 'a2, 'a3) xtrans **)

let xTPrim p n xe =
  xTrans (xedge_projN n xe) (S n) (EPrim ((xedge_projN n xe), n, p,
    (xedge_projE n xe))) (xedge_projF n xe)

(** val extract_trans :
    (nat -> bf -> ('a1, 'a2, bf) xprim) -> nat -> bf -> ('a1, 'a2, bf) xtrans **)

let rec extract_trans extract_xprim0 n f =
  match extract_xprim0 n f with
  | XTerm (n0, t) -> xTrans None n0 (ETerm (n0, t)) Term
  | XPrim (n0, p, xf) -> xTPrim p n0 (extract_trans extract_xprim0 n0 xf)
  | XSha (n0, f0, f3) ->
    xTrans (Some (S n0)) (S n0) (ENext n0) (Next ((S n0), (Node (n0, f0,
      f3))))

(** val sem_trans :
    (nat -> 'a1 -> bf) -> ('a2 -> nat -> bf -> bf) -> nat option -> nat ->
    ('a1, 'a2) trans -> bf next -> bf **)

let rec sem_trans sem_term0 sem_prim0 _ _ e f0 =
  match e with
  | ETerm (n, t) -> sem_term0 n t
  | EPrim (o, n, p, e0) ->
    sem_prim0 p n (sem_trans sem_term0 sem_prim0 o n e0 f0)
  | ENext n ->
    (match f0 with
     | Term -> assert false (* absurd case *)
     | Next (n0, f) -> solution_left (S n) (fun f3 -> f3) n0 f)

type ('term, 'prim) edge = ('term, 'prim) trans

type ('term, 'prim, 'f) xedge0 = (('term, 'prim) edge, 'f) xedge

(** val xEdge :
    (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> ('a2 -> nat -> bf ->
    bf) -> nat option -> nat -> ('a1, 'a2) edge -> 'a3 node next -> (('a1,
    'a2) edge, 'a3) xedge **)

let xEdge _ _ _ opn m ee nx =
  XEdge (opn, m, ee, nx)

(** val extract_edge :
    (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> (bf -> 'a1) -> ('a2 ->
    nat -> bf -> bf) -> ('a2 -> nat -> bf -> bf option) -> (nat -> bf ->
    bool) -> (nat -> bf -> ('a1, 'a2, bf) xprim) -> nat -> bf -> ('a1, 'a2,
    bf) xedge0 **)

let extract_edge sem_term0 extract_term0 _ sem_prim0 _ _ extract_xprim0 n f =
  let xt = extract_trans extract_xprim0 n f in
  xEdge sem_term0 extract_term0 sem_prim0 (xedge_projN n xt) n
    (xedge_projE n xt) (xedge_projF n xt)

(** val sem_edge :
    (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> ('a2 -> nat -> bf ->
    bf) -> nat option -> nat -> ('a1, 'a2) edge -> bf next -> bf **)

let sem_edge sem_term0 _ sem_prim0 opn m e nx =
  sem_trans sem_term0 sem_prim0 opn m e nx

(** val sem_Term :
    (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> ('a2 -> nat -> bf ->
    bf) -> nat -> ('a1, 'a2) edge -> bf **)

let sem_Term sem_term0 extract_term0 sem_prim0 m e =
  sem_edge sem_term0 extract_term0 sem_prim0 None m e Term

(** val sem_Next :
    (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> ('a2 -> nat -> bf ->
    bf) -> nat -> nat -> ('a1, 'a2) edge -> bf -> bf **)

let sem_Next sem_term0 extract_term0 sem_prim0 n m e f =
  sem_edge sem_term0 extract_term0 sem_prim0 (Some n) m e (Next (n, f))

type ('term, 'prim) lDD0 = ('term, 'prim) edge lDD

(** val extract0 :
    (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> (bf -> 'a1) -> ('a2 ->
    nat -> bf -> bf) -> ('a2 -> nat -> bf -> bf option) -> (nat -> bf ->
    bool) -> (nat -> bf -> ('a1, 'a2, bf) xprim) -> nat -> bf -> ('a1, 'a2)
    lDD0 **)

let extract0 sem_term0 extract_term0 term_of_bool0 sem_prim0 extract_prim0 is_prim_free0 extract_xprim0 =
  extract
    (extract_edge sem_term0 extract_term0 term_of_bool0 sem_prim0
      extract_prim0 is_prim_free0 extract_xprim0)
    (sem_Term sem_term0 extract_term0 sem_prim0)
    (sem_Next sem_term0 extract_term0 sem_prim0)

(** val intro_oP_bf :
    (bool -> bool -> bool) -> nat -> bf -> bool array -> bool **)

let intro_oP_bf p n f a =
  p (shead n a) (f (stail n a))

(** val oU : bool -> bool -> bool **)

let oU _ fx =
  fx

(** val oX : bool -> bool -> bool **)

let oX =
  xorb

(** val oC : bool -> bool -> bool -> bool -> bool **)

let oC if0 th0 x0 fx =
  match eqb x0 if0 with
  | True -> th0
  | False -> fx

(** val intro_oU_bf : nat -> bf -> bool array -> bool **)

let intro_oU_bf n f =
  intro_oP_bf oU n f

(** val intro_oX_bf : nat -> bf -> bool array -> bool **)

let intro_oX_bf n f =
  intro_oP_bf oX n f

(** val intro_oC_bf : bool -> bool -> nat -> bf -> bool array -> bool **)

let intro_oC_bf if0 th0 n f =
  intro_oP_bf (oC if0 th0) n f

(** val rev_oU : bool **)

let rev_oU =
  False

(** val rev_oX : bool **)

let rev_oX =
  False

(** val rev_oC : bool -> bool -> bool **)

let rev_oC if0 _ =
  negb if0

(** val elim_oP_bf : bool -> nat -> bf -> bf **)

let elim_oP_bf r n f =
  fnary_evalo1 n f r

(** val elim_oU_bf : nat -> bf -> bf **)

let elim_oU_bf n f =
  elim_oP_bf rev_oU n f

(** val elim_oX_bf : nat -> bf -> bf **)

let elim_oX_bf n f =
  elim_oP_bf rev_oX n f

(** val elim_oC_bf : bool -> bool -> nat -> bf -> bf **)

let elim_oC_bf if0 th0 n f =
  elim_oP_bf (rev_oC if0 th0) n f

(** val is_oU_bf : nat -> bf -> bool **)

let is_oU_bf n f =
  beq_bf n (fnary_evalo1 n f False) (fnary_evalo1 n f True)

(** val is_oX_bf : nat -> bf -> bool **)

let is_oX_bf n f =
  beq_bf n (fnary_evalo1 n f False) (neg_bf n (fnary_evalo1 n f True))

(** val is_oC_bf : bool -> bool -> nat -> bf -> bool **)

let is_oC_bf if0 th0 n f =
  is_cst_bf th0 n (fnary_evalo1 n f if0)

type term =
| Cst of nat * bool
| Pi0 of nat * bool

(** val sem_term : nat -> term -> bf **)

let sem_term _ = function
| Cst (n, b) -> cst_bf n b
| Pi0 (n, b) -> pi0 n b

(** val extract_cst : nat -> bf -> term option **)

let extract_cst n f =
  match is_cst_bf False n f with
  | True -> Some (Cst (n, False))
  | False ->
    (match is_cst_bf True n f with
     | True -> Some (Cst (n, True))
     | False -> None)

(** val extract_pi0 : nat -> bf -> term option **)

let extract_pi0 n f =
  match n with
  | O -> None
  | S n' ->
    (match beq_bf (S n') (pi0 n' False) f with
     | True -> Some (Pi0 (n', False))
     | False ->
       (match beq_bf (S n') (pi0 n' True) f with
        | True -> Some (Pi0 (n', True))
        | False -> None))

(** val extract_term : nat -> bf -> term option **)

let extract_term n f =
  match extract_cst n f with
  | Some t -> Some t
  | None -> extract_pi0 n f

(** val term_of_bool : bf -> term **)

let term_of_bool f =
  Cst (O, (f (fun _ -> False)))

type prim =
| OU
| OX
| OC of bool * bool

(** val sem_prim : prim -> nat -> bf -> bf **)

let sem_prim p n f =
  match p with
  | OU -> intro_oU_bf n f
  | OX -> intro_oX_bf n f
  | OC (if0, th0) -> intro_oC_bf if0 th0 n f

(** val detect_prim : prim -> nat -> bf -> bool **)

let detect_prim p n f =
  match p with
  | OU -> is_oU_bf n f
  | OX -> is_oX_bf n f
  | OC (if0, th0) -> is_oC_bf if0 th0 n f

(** val elim_prim : prim -> nat -> bf -> bf **)

let elim_prim p n f =
  match p with
  | OU -> elim_oU_bf n f
  | OX -> elim_oX_bf n f
  | OC (if0, th0) -> elim_oC_bf if0 th0 n f

(** val extract_prim : prim -> nat -> bf -> bf option **)

let extract_prim p n f =
  match detect_prim p n f with
  | True -> Some (elim_prim p n f)
  | False -> None

(** val is_prim_free : nat -> bf -> bool **)

let is_prim_free n f =
  match n with
  | O -> True
  | S n' ->
    (match match match match match negb (detect_prim OU n' f) with
                             | True -> negb (detect_prim OX n' f)
                             | False -> False with
                       | True -> negb (detect_prim (OC (False, False)) n' f)
                       | False -> False with
                 | True -> negb (detect_prim (OC (False, True)) n' f)
                 | False -> False with
           | True -> negb (detect_prim (OC (True, False)) n' f)
           | False -> False with
     | True -> negb (detect_prim (OC (True, True)) n' f)
     | False -> False)

type 'f xprim0 = (term, prim, 'f) xprim

(** val extract_xprim : nat -> bf -> bf xprim0 **)

let extract_xprim n f =
  match n with
  | O -> XTerm (O, (term_of_bool f))
  | S n' ->
    (match extract_term (S n') f with
     | Some t -> XTerm ((S n'), t)
     | None ->
       (match extract_prim OU n' f with
        | Some f' -> XPrim (n', OU, f')
        | None ->
          (match extract_prim OX n' f with
           | Some f' -> XPrim (n', OX, f')
           | None ->
             (match extract_prim (OC (False, False)) n' f with
              | Some f' -> XPrim (n', (OC (False, False)), f')
              | None ->
                (match extract_prim (OC (False, True)) n' f with
                 | Some f' -> XPrim (n', (OC (False, True)), f')
                 | None ->
                   (match extract_prim (OC (True, False)) n' f with
                    | Some f' -> XPrim (n', (OC (True, False)), f')
                    | None ->
                      (match extract_prim (OC (True, True)) n' f with
                       | Some f' -> XPrim (n', (OC (True, True)), f')
                       | None ->
                         XSha (n', (fnary_evalo1 n' f False),
                           (fnary_evalo1 n' f True)))))))))

type lDD1 = (term, prim) lDD0

(** val extract1 : nat -> bf -> lDD1 **)

let extract1 =
  extract0 sem_term extract_term term_of_bool sem_prim extract_prim
    is_prim_free extract_xprim

(** val f1 : bf **)

let f1 x =
  let x0 = x (ltN_of_nat O (S (S (S (S O))))) in
  let x1 = x (ltN_of_nat (S O) (S (S (S (S O))))) in
  let x2 = x (ltN_of_nat (S (S O)) (S (S (S (S O))))) in
  let x3 = x (ltN_of_nat (S (S (S O))) (S (S (S (S O))))) in
  xorb x0 (xorb x1 (xorb x2 x3))

(** val ldd1 : lDD1 **)

let ldd1 =
  extract1 (S (S (S (S O)))) f1

(** val f2 : bf **)

let f2 x =
  let x0 = x (ltN_of_nat O (S (S (S (S (S (S (S (S (S (S O))))))))))) in
  let x1 = x (ltN_of_nat (S O) (S (S (S (S (S (S (S (S (S (S O))))))))))) in
  let x2 = x (ltN_of_nat (S (S O)) (S (S (S (S (S (S (S (S (S (S O)))))))))))
  in
  let x3 =
    x (ltN_of_nat (S (S (S O))) (S (S (S (S (S (S (S (S (S (S O)))))))))))
  in
  let x4 =
    x (ltN_of_nat (S (S (S (S O)))) (S (S (S (S (S (S (S (S (S (S O)))))))))))
  in
  let x5 =
    x
      (ltN_of_nat (S (S (S (S (S O))))) (S (S (S (S (S (S (S (S (S (S
        O)))))))))))
  in
  let x6 =
    x
      (ltN_of_nat (S (S (S (S (S (S O)))))) (S (S (S (S (S (S (S (S (S (S
        O)))))))))))
  in
  let x7 =
    x
      (ltN_of_nat (S (S (S (S (S (S (S O))))))) (S (S (S (S (S (S (S (S (S (S
        O)))))))))))
  in
  let x8 =
    x
      (ltN_of_nat (S (S (S (S (S (S (S (S O)))))))) (S (S (S (S (S (S (S (S
        (S (S O)))))))))))
  in
  let x9 =
    x
      (ltN_of_nat (S (S (S (S (S (S (S (S (S O))))))))) (S (S (S (S (S (S (S
        (S (S (S O)))))))))))
  in
  xorb x0
    (xorb x1
      (xorb x2 (xorb x3 (xorb x4 (xorb x5 (xorb x6 (xorb x7 (xorb x8 x9))))))))

(** val ldd2 : lDD1 **)

let ldd2 =
  extract1 (S (S (S (S (S (S (S (S (S (S O)))))))))) f2
