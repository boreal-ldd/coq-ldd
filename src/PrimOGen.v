Require Import Arith .
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.

Require Import Ground.SimplProp.
Require Import Ground.SimplBool.
Require Import Ground.SimplOption.
Require Import Ground.SimplEq.
Require Import Ground.SimplFun.
Require Import Ground.SimplBoolFun.
Require Import Ground.SimplCmp.
Require Import Ground.SimplSimpl.

Require Import Ground.SimplNat.
Require Import Ground.SimplAB.

Require Import Ground.SimplList_Chap0_Standard.
Require Import Ground.SimplList_Chap1_Construct.
Require Import Ground.SimplList_Chap2_Predicate.
Require Import Ground.SimplList_Chap3_PredicateOnConstruct.
Require Import Ground.SimplList.
Require Import Ground.SimplABList.
Require Import Ground.SimplMore.
Require Import Ground.SimplDTT.
Require Import Ground.SimplLtN_Chap0_Construct.
Require Import Ground.SimplLtN_Chap1_Array_as_Function.
Require Import Ground.SimplLtN_Chap1_Sec1_Boolean_Array.
Require Import Ground.SimplLtNIndexedArray.

Require Import Ground.SimplDTT.
Require Import Ground.SimplLtN.
Require Import Ground.SimplEnum.

Require Import Ground.MiniBool_VAX.
Require Import Ground.MiniBool_Logic.

Require Import FiniteCurryfy.
Require Import FiniteBooleanFunction.
Require Import MaxLddGen.

Section PrimOGen1.

  (* Section 1. Definition if Terminal and Primitives *)
  (* SubSection 1. Defining Terminals *)
  Variable term : nat -> Type.
  Variable sem_term : forall {n}, term n -> bf n.
  Variable extract_term : forall {n}, bf n -> option(term n).
  Definition is_term {n} (f:bf n) : bool := isSome(extract_term f).

  Variable extract_term_correct : forall {n} f t,
    @extract_term n f = Some t <-> sem_term t = f.

  (* [NOTE] we enforce constantes to be part of terminals *)
  Variable term_of_bool : bf 0 -> term 0.
  Variable extract_term_0 : forall (f:bf 0), extract_term f = Some(term_of_bool f).

  (* SubSection 2. Defining Primitives *)
  Variable prim : Type.

  Variable sem_prim  : prim -> forall {n}, bf n    -> bf (S n).
  Variable extract_prim : prim -> forall {n}, bf(S n) -> option(bf n).
  Definition is_prim (p:prim) {n} (f:bf(S n)) := isSome(extract_prim p f).

  Definition is_prim' (p:prim) {n} (f:bf n) : bool :=
    match n as n0 return bf n0 -> bool with
    | 0 => fun _ => false
    | S m => fun f => is_prim p f
    end f.

  Variable extract_prim_iff_sem_prim : forall {n} f p xf,
    @extract_prim p n f = Some xf <-> sem_prim p xf = f.

  Variable sem_prim_injective : forall {n p1 f1},
    forall (H:@is_term (S n) (sem_prim p1 f1) = false) p2 f2,
      (sem_prim p1 f1 = sem_prim p2 f2 <-> p1 = p2 /\ f1 = f2).

  Variable is_prim_free : forall {n} (f:bf n), bool.

  Variable is_prim_free_correct : forall {n} (f:bf n),
    is_prim_free f = true <-> forall p, is_prim' p f = false.

  Inductive xprim {F:nat -> Type} : nat -> Type :=
  | XTerm {n:nat} (t:term n) : xprim n
  | XPrim {n:nat} (p:prim) (xf:F n) : xprim (S n)
  | XSha  {n:nat} (f0 f1:F n) : xprim (S n).

  Variable extract_xprim : forall {n}, bf n -> @xprim bf n.

  Definition sem_xprim {n} (xp:@xprim bf n) : bf n :=
  match xp with
  | XTerm t    => sem_term t
  | XPrim p xf => sem_prim p xf
  | XSha f0 f1 => sha_bf f0 f1
  end.

  Variable sem_xprim_extract_single : forall {n} (f:bf n),
    sem_xprim(extract_xprim f) = f.

  Variable extract_xprim_minimal : forall {n} (f:bf n),
    match extract_xprim f with
    | XTerm _ => True
    | XPrim  _ _ => is_term f = false
    | XSha _ _ => is_term f = false /\ is_prim_free f = true
    end.

  (* SubSection. Single Step : Extract o Sem *)

  Lemma extract_term_sem_term {n} (t:term n) : extract_term (sem_term t) = Some t.
  Proof with curry1.
  rewrite extract_term_correct...
  Qed.
  Hint Rewrite @extract_term_sem_term : curry1_rewrite.

  Lemma sem_term_injective {n} (t1 t2:term n) : sem_term t1 = sem_term t2 <-> t2 = t1.
  Proof. rewrite <- extract_term_correct; curry1. Qed.
  Hint Rewrite @sem_term_injective : curry1_rewrite.

  Lemma sem_term_term_of_bool (f:bf 0) : sem_term (term_of_bool f) = f.
  Proof.
  specialize(extract_term_0 f) as H0.
  rewrite extract_term_correct in H0.
  assumption.
  Qed.
  Hint Rewrite sem_term_term_of_bool : curry1_rewrite.

  Lemma is_term_sem_term {n} (t:term n) :
    is_term (sem_term t) = true.
  Proof with curry1.
  unfold is_term, isSome...
  Qed.
  Hint Rewrite @is_term_sem_term : curry1_rewrite.

  Lemma extract_xprim_sem_term {n} (p:term n) : extract_xprim (sem_term p) = XTerm p.
  Proof with curry1.
  specialize(extract_xprim_minimal (sem_term p)) as H0...
  specialize(sem_xprim_extract_single (sem_term p)) as H1...
  dest_match_step...
  Qed.
  Hint Rewrite @extract_xprim_sem_term : curry1_rewrite.

  Lemma is_term0 (f:bf 0) : is_term f = true.
  Proof with curry1.
  unfold is_term.
  rewrite extract_term_0...
  Qed.
  Hint Rewrite is_term0 : curry1_rewrite.

  Lemma extract_prim_sem_prim p {n} (f:bf n) :
    extract_prim p (sem_prim p f) = Some f.
  Proof with curry1.
  rewrite extract_prim_iff_sem_prim...
  Qed.
  Hint Rewrite @extract_prim_sem_prim: curry1_rewrite.

  Lemma is_prim_free_sem_prim p {n} (f:bf n) :
    is_prim_free (sem_prim p f) = false.
  Proof with curry1.
  destruct(is_prim_free(sem_prim p f))eqn:E...
  rewrite is_prim_free_correct in E...
  specialize(E p)...
  unfold is_prim in E...
  Qed.
  Hint Rewrite @is_prim_free_sem_prim : curry1_rewrite.

  Lemma extract_xprim_sem_prim p {n} (f:bf n) :
    is_term(sem_prim p f) = false -> extract_xprim (sem_prim p f) = XPrim p f.
  Proof with curry1.
  specialize(extract_xprim_minimal (sem_prim p f)) as HH...
  specialize(sem_xprim_extract_single (sem_prim p f)) as H1...
  rewrite <- H1 in H.
  dtt_dest_match_step...
  rewrite sem_prim_injective in H1...
  Qed.

  Lemma extract_xprim_sha_bf {n} (f0 f1:bf n) :
    is_term (sha_bf f0 f1) = false -> is_prim_free (sha_bf f0 f1) = true ->
      extract_xprim (sha_bf f0 f1) = XSha f0 f1.
  Proof with curry1.
  specialize(extract_xprim_minimal (sha_bf f0 f1)) as HH0...
  specialize(sem_xprim_extract_single (sha_bf f0 f1)) as HH1...
  rewrite <- HH1 in H, H0.
  dtt_dest_match_step...
  Qed.

  (* SubSection. Extract XPrim equality with constructor rewriting *)

  Lemma extract_xprim_eq_XTerm {n} f t :
    @extract_xprim n f = XTerm t <-> f = sem_term t.
  Proof with curry1.
  split...
  specialize(sem_xprim_extract_single f) as H0; rewrite_subst.
  Qed.
  Hint Rewrite @extract_xprim_eq_XTerm : curry1_rewrite.

  Lemma extract_xprim_eq_XPrim n f0 p (xf:bf n) :
    extract_xprim f0 = XPrim p xf <-> is_term f0 = false /\ f0 = sem_prim p xf.
  Proof with curry1.
  split...
  - specialize (extract_xprim_minimal f0) as H1.
    specialize(sem_xprim_extract_single f0) as H0; rewrite_subst.
  - rewrite extract_xprim_sem_prim...
  Qed.
  Hint Rewrite @extract_xprim_eq_XPrim : curry1_rewrite.

  Lemma extract_xprim_eq_XSha {n} f (f0 f1:bf n) :
    extract_xprim f = XSha f0 f1 <->
      is_term f = false /\ is_prim_free f = true /\ f = sha_bf f0 f1.
  Proof with curry1.
  split...
  - specialize (extract_xprim_minimal f) as H1.
    specialize(sem_xprim_extract_single f) as H0; rewrite_subst.
  - rewrite extract_xprim_sha_bf...
  Qed.
  Hint Rewrite @extract_xprim_eq_XSha : curry1_rewrite.

  (* describe transformations (that is non-normalized transs) *)
  Inductive trans : option nat -> nat -> Type :=
  | ETerm {n} (t:term n) : trans None n
  | EPrim {o n} (p:prim) (e0:trans o n) : trans o (S n)
  | ENext n : trans (Some(S n)) (S n).

  Variable is_term_is_prim_with_is_term_false : forall {n} (f0:bf n),
    is_term f0 = false -> forall p, is_term(sem_prim p f0) = false.

  Definition prim_term_not_term p {opn m} (t:trans opn m) : bool :=
    match t with
    | @ETerm n0 t1 => negb (is_term (sem_prim p (sem_term t1)))
    | _ => true
    end.

  Fixpoint trans_normalized {opn m} (t:trans opn m) : bool :=
    match t with
    | EPrim p t0 => prim_term_not_term p t0 && trans_normalized t0
    | _ => true
    end.

  Lemma trans_le {n m} (t:trans (Some n) m) : n <= m.
  Proof with curry1.
  remember (Some n) as SomeN eqn:E.
  induction t...
  Qed.

  Definition xtrans {F:nat -> Type} : nat -> Type := @MaxLddGen.xedge trans F.
  Definition XTrans {F:nat -> Type} {opn:option nat} {m:nat} (ee:trans opn m) (nx:@next (@node F) opn) :=
    @MaxLddGen.XEdge trans F opn m ee nx.

  Definition XTPrim (p:prim) {n F} (xe:@xtrans F n) : @xtrans F (S n) :=
    XTrans (EPrim p (xedge_projE _ xe)) (xedge_projF _ xe).

  (* [TODO] use 'structural' induction on natural *)

  Definition extract_trans {n} (f:bf n) : @xtrans bf n :=
    Fix lt_wf (fun n : nat => bf n -> xtrans n)
    (fun (n : nat) (extract : forall y : nat, y < n -> bf y -> xtrans y) (f : bf n) =>
   match extract_xprim f in (xprim n0)
     return ((forall y : nat, y < n0 -> bf y -> xtrans y) -> xtrans n0)
   with
   | @XTerm _ n0 t =>
       fun _ =>
       XTrans (ETerm t) Term
   | @XPrim _ n0 p xf =>
       fun (extract0 : forall y : nat, y < S n0 -> bf y -> xtrans y) =>
        XTPrim p (extract0 n0 (Nat.lt_succ_diag_r n0) xf)
   | @XSha _ n0 f0 f1 =>
       fun _ =>
       XTrans (ENext n0) (Next (Node f0 f1))
   end extract) n f.

  Lemma extract_trans_eq {n} (f:bf n) :
    extract_trans f =
    match extract_xprim f in (xprim n0) return (xtrans n0)
     with
     | XTerm t    => XTrans (ETerm t) Term
     | XPrim p xf => XTPrim p (extract_trans xf)
     | XSha f0 f1 => XTrans (ENext _) (Next (Node f0 f1))
     end.
  Proof with curry1.
  unfold extract_trans.
  rewrite Fix_eq; fold (@extract_trans)...
  - dest_match_step...
  - apply functional_extensionality...
    dest_match_step...
    rewrite H...
  Qed.

  Lemma extract_trans_ind (P:forall n, bf n -> @xtrans bf n -> Prop)
    (PTerm:forall n (f:bf n),
           forall t (D:extract_xprim f = XTerm t),
           P n f (XTrans (ETerm t) Term))
    (PPrim:forall n (f:bf(S n)),
           forall p xf (D:extract_xprim f = XPrim p xf),
           P _ xf (extract_trans xf) ->
           P _ f (XTPrim p (extract_trans xf)))
    (PSha: forall n (f:bf(S n)),
           forall f0 f1 (D:extract_xprim f = XSha f0 f1),
           P _ f (XTrans (ENext _) (Next (Node f0 f1)))) :
      forall n (f:bf n), P n f (extract_trans f).
  Proof with curry1.
  intro n.
  induction (lt_wf n)...
  rewrite extract_trans_eq.
  dest_match_step...
  Qed.

  Fixpoint sem_trans {opn m} (e:trans opn m) (f0:@next bf opn) : bf m.
  Proof.
  destruct e.
  - apply (sem_term t).
  - apply (sem_prim p (sem_trans _ _ e f0)).
  - dependent destruction f0.
    apply f.
  Defined.

  Lemma XTrans_eq_XTrans F n opm0 ee0 nx0 opm1 ee1 nx1 :
    @XTrans F opm0 n ee0 nx0 = @XTrans F opm1 n ee1 nx1 <->
      opm0 = opm1 /\ ee0 =J ee1 /\ nx0 =J nx1.
  Proof with curry1.
  unfold XTrans...
  rewrite XEdge_eq_XEdge...
  Qed.
  Hint Rewrite XTrans_eq_XTrans : curry1_rewrite.

  Definition sem_next_node {opn} (nx:@next (@node bf) opn) : @next bf opn :=
    @MaxLddGen.sem_next_node opn nx.

  Lemma XTrans_xedge_projE_xedge_projF {F n} (xt:@xtrans F n) :
    XTrans (xedge_projE trans xt) (xedge_projF trans xt) = xt.
  Proof. destruct xt; curry1. Qed.
  Hint Rewrite @XTrans_xedge_projE_xedge_projF : curry1_rewrite.

  Definition sem_xtrans {m} (xe:@xtrans bf m) : bf m :=
    @MaxLddGen.sem_xedge trans (@sem_trans) m xe.

  Lemma sem_xtrans_XTPrim p {n} (xt:@xtrans bf n) :
    sem_xtrans (XTPrim p xt) = sem_prim p (sem_xtrans xt).
  Proof. reflexivity. Qed.
  Hint Rewrite @sem_xtrans_XTPrim : curry1_rewrite.

  Theorem sem_xtrans_extract_trans : forall {m} (f:bf m), sem_xtrans(extract_trans f) = f.
  Proof with curry1.
  apply (extract_trans_ind (fun n f e => sem_xtrans e = f))...
  rewrite_subst...
  Qed.
  Hint Rewrite @sem_xtrans_extract_trans : curry1_rewrite.

  Lemma extract_xprim0 (f:bf 0) :
    extract_xprim f = XTerm (term_of_bool f).
  Proof. curry1. Qed.
  Hint Rewrite extract_xprim0 : curry1_rewrite.

  Lemma extract_trans0 (f:bf 0) :
    extract_trans f = XTrans (ETerm(term_of_bool f)) Term.
  Proof. rewrite extract_trans_eq; curry1. Qed.
  Hint Rewrite extract_trans0 : curry1_rewrite.

  (* SubSection. Normalizing Transform as Edges *)

  Definition edge opn m : Type := {t:trans opn m | trans_normalized t = true}.

  Lemma edge_PI opn m (e1 e2:edge opn m) : e1 = e2 <-> proj1_sig e1 = proj1_sig e2.
  Proof with curry1.
  split...
  destruct e1, e2...
  Qed.

  Lemma trans_normalized_extract_trans {n} (f:bf n) :
    trans_normalized(xedge_projE _ (extract_trans f)) = true.
  Proof with curry1.
  induction n...
  rewrite extract_trans_eq.
  dtt_dest_match_step...
  specialize(IHn xf)...
  symmetry in RE...
  unfold prim_term_not_term...
  unfold xedge_projE.
  specialize(sem_xtrans_extract_trans xf) as HH2.
  dtt_dest_match_step...
  dtt_dest_match_step...
  Qed.

  Definition xedge {F:nat -> Type} : nat -> Type := @MaxLddGen.xedge edge F.
  Definition XEdge {F:nat -> Type} {opn:option nat} {m:nat} (ee:edge opn m) (nx:@next (@node F) opn) :=
    @MaxLddGen.XEdge edge F opn m ee nx.

  Lemma XEdge_eq_XEdge F n opm0 ee0 nx0 opm1 ee1 nx1 :
    @XEdge F opm0 n ee0 nx0 = @XEdge F opm1 n ee1 nx1 <->
      opm0 = opm1 /\ ee0 =J ee1 /\ nx0 =J nx1.
  Proof with curry1.
  unfold XEdge...
  rewrite XEdge_eq_XEdge...
  Qed.
  Hint Rewrite XEdge_eq_XEdge : curry1_rewrite.

  Definition extract_edge {n} (f:bf n) : @xedge bf n.
  Proof.
  pose (extract_trans f) as xt.
  refine (XEdge (exist _ (xedge_projE _ xt) _) (xedge_projF _ xt)).
  apply trans_normalized_extract_trans.
  Defined.

  Lemma edge_le {n m} (e:edge (Some n) m) : n <= m.
  Proof. destruct e. apply (trans_le x). Qed.

  Definition sem_edge {opn m} (e:edge opn m) (nx:next opn) : bf m :=
    sem_trans (proj1_sig e) nx.

  Definition sem_Term {m} (ee:edge None m) : bf m :=
    @MaxLddGen.sem_term edge (@sem_edge) m ee.

  Definition sem_Next {n m} (ee:edge(Some n) m) (f:bf n) : bf m :=
    @MaxLddGen.sem_next edge (@sem_edge) n m ee f.

  Definition sem_xedge {m} (xe:@xedge bf m) : bf m :=
    @MaxLddGen.sem_xedge edge (@sem_edge) m xe.

  Theorem sem_xedge_extract_edge {m} (f:bf m) : sem_xedge(extract_edge f) = f.
  Proof. apply (sem_xtrans_extract_trans f). Qed.

  Definition id_trans n : trans (Some(S n))(S n) := ENext n.
  Definition id_edge n : edge (Some(S n))(S n) :=
    (exist _ (id_trans n) (eq_refl _)).

  Lemma sem_id_trans n : sem_Next(id_edge n) = id.
  Proof. reflexivity. Qed.

  Lemma rewrite_xedge_eq {n F} (xe1 xe2:@xedge F n) :
    xe1 = xe2 <->
      xedge_projN _ xe1 = xedge_projN _ xe2 /\ xedge_projE _ xe1 =J xedge_projE _ xe2 /\ xedge_projF _ xe1 =J xedge_projF _ xe2.
  Proof with curry1.
  split...
  dependent destruction xe1...
  dependent destruction xe2...
  Qed.

  Lemma rewrite_xedge_eq_proj1_sig {n F} (xe1 xe2:@xedge F n) :
    xe1 = xe2 <->
      xedge_projN _ xe1 = xedge_projN _ xe2 /\ proj1_sig (xedge_projE _ xe1) =J proj1_sig (xedge_projE _ xe2) /\ xedge_projF _ xe1 =J xedge_projF _ xe2.
  Proof with curry1.
  split...
  dependent destruction xe1...
  dependent destruction xe2...
  rewrite (edge_PI _ _ ee0 ee)...
  Qed.

  Lemma rewrite_extract_edge {opn m} (f:bf m) ee (nx:next opn) :
    extract_edge f = XEdge ee nx <->
      extract_trans f = XTrans (proj1_sig ee) nx.
  Proof with curry1.
  rewrite rewrite_xedge_eq_proj1_sig...
  dependent destruction ee...
  split...
  rewrite H...
  Qed.

  (* Section. Extending Edge Extraction to LDD Extraction *)

  Definition ldd : nat -> Type := MaxLddGen.ldd edge.

  Definition sem_ldd {n} (l:ldd n) : bf n :=
    sem_ldd edge (@sem_edge) l.

  Definition extract_ldd {n} (f:bf n) : ldd n :=
    extract_ldd edge (@edge_le) (@extract_edge) f.

  Theorem sem_ldd_extract_ldd {n} (f:bf n) :
    sem_ldd(extract_ldd f) = f.
  Proof with curry1.
  apply (@sem_ldd_extract_ldd edge).
  apply (@sem_xedge_extract_edge).
  Qed.
  Hint Rewrite @sem_ldd_extract_ldd : curry1_rewrite.

  (* Section. Extract o Semantic *)

  Definition reduced_bf {n} (f:bf n) : bool :=
    @MaxLddGen.reduced_bf edge (@extract_edge) n f.

  Lemma is_prim_sem_prim_same p {n} (f:bf n) :
    is_prim p (sem_prim p f) = true.
  Proof with curry1.
  unfold is_prim...
  Qed.
  Hint Rewrite @is_prim_sem_prim_same : curry1_rewrite.

  Lemma rewrite_extract_xprim_with_is_term_is_prim_free {n} (f:bf(S n)) :
   is_term f = false -> is_prim_free f = true ->
    extract_xprim f = XSha (fnary_evalo1 f false) (fnary_evalo1 f true).
  Proof with curry1.
  simpl...
  Qed.

  Lemma rewrite_extract_trans_with_is_term_and_is_prim_free :
    forall n (f:bf(S n)),
    is_term f = false -> is_prim_free f = true ->
    extract_trans f = XTrans (ENext _)
      (Next(Node(fnary_evalo1 f false)(fnary_evalo1 f true))).
  Proof with curry1.
  intros n f.
  rewrite extract_trans_eq...
  rewrite rewrite_extract_xprim_with_is_term_is_prim_free...
  Qed.

  Lemma rewrite_extract_edge_with_is_term_and_is_prim_free :
    forall n (f:bf(S n)),
    is_term f = false -> is_prim_free f = true ->
    extract_edge f = XEdge (id_edge _)
      (Next(Node(fnary_evalo1 f false)(fnary_evalo1 f true))).
  Proof with curry1.
  intros...
  rewrite rewrite_extract_edge...
  rewrite rewrite_extract_trans_with_is_term_and_is_prim_free...
  Qed.

  Lemma rewrite_reduced_bf_as_trans {n} (f:bf n) :
    reduced_bf f = @MaxLddGen.reduced_bf trans (@extract_trans) _ f.
  Proof with curry1.
  unfold reduced_bf, extract_edge, MaxLddGen.reduced_bf...
  dest_match_step...
  Qed.

  Lemma rewrite_trans_same n (ee:trans(Some(S n)) (S n)) : ee = ENext n.
  Proof with curry1.
  dependent destruction ee...
  specialize (trans_le ee)...
  Qed.

  Lemma extract_trans_eq_ENext n f nx :
    extract_trans f = MaxLddGen.XEdge trans (ENext n) nx
      <-> is_term f = false /\ is_prim_free f = true /\
         nx = (Next(Node(fnary_evalo1 f false)(fnary_evalo1 f true))).
  Proof with curry1.
  split...
  - rewrite extract_trans_eq in H.
    dtt_dest_match_step...
    * symmetry in RE...
      unfold XTPrim in H...
      unfold xedge_projN in H.
      dtt_dest_match_step...
      inv H1...
    * symmetry in RE...
  - rewrite rewrite_extract_trans_with_is_term_and_is_prim_free...
  Qed.
  Hint Rewrite extract_trans_eq_ENext : curry1_rewrite.

  Lemma rewrite_reduced_bf n (f:bf n) :
    reduced_bf f = true <-> is_term f = false /\ is_prim_free f = true.
  Proof with curry1.
  split...
  - rewrite rewrite_reduced_bf_as_trans in H...
    unfold MaxLddGen.reduced_bf in H...
    dest_match_step...
    dest_match_step...
    dependent destruction ee...
    specialize(trans_le ee)...
  - unfold reduced_bf.
    destruct n...
    unfold MaxLddGen.reduced_bf.
    rewrite (rewrite_extract_edge_with_is_term_and_is_prim_free _ f)...
  Qed.

  Definition reduced_next {opn} (nx:@next bf opn) : bool :=
    @MaxLddGen.reduced_next edge (@extract_edge) opn nx.

  Theorem reduced_next_projF_extract_trans : forall {n} f,
    reduced_next(sem_next_node(xedge_projF _ (@extract_trans n f))) = true.
  Proof with curry1.
  apply (extract_trans_ind (fun n f xf =>
    reduced_next(sem_next_node(xedge_projF _ xf)) = true))...
  rewrite rewrite_reduced_bf...
  Qed.

  Theorem reduced_next_projF_extract_edge {n} f :
    reduced_next(sem_next_node(xedge_projF _ (@extract_edge n f))) = true.
  Proof. specialize(reduced_next_projF_extract_trans f); curry1. Qed.

  Lemma is_term_sem_prim_eq_false p {n} (f:bf n) :
    is_term (sem_prim p f) = false <->
      (is_term (sem_prim p f) = false /\ is_term f = true) \/
      (is_term f = false).
  Proof with curry1.
  intros.
  destruct(is_term f)eqn:E...
  Qed.

  Definition trans_is_ETerm {opn m} (t:trans opn m) : bool :=
    match t with
    | ETerm _ => true
    | _ => false
    end.

  Theorem is_term_sem_trans_with_normalized {opn m} (t:trans opn m) (nx:next opn) :
    reduced_next(sem_next_node nx) = true -> trans_normalized t = true ->
    is_term (sem_trans t (sem_next_node nx)) = trans_is_ETerm t.
  Proof with curry1.
  induction t...
  - specialize(IHt _  H (eq_refl _))...
    dependent destruction t...
  - dependent destruction nx...
    dependent destruction f...
    rewrite rewrite_reduced_bf in H...
  Qed.

  Theorem extract_trans_sem_trans {opn m} (ee:trans opn m) (nx:next opn) :
    reduced_next(sem_next_node nx) = true -> trans_normalized ee = true ->
    extract_trans (sem_trans ee (sem_next_node nx)) = XTrans ee nx.
  Proof with curry1.
  induction ee...
  - rewrite extract_trans_eq...
    dependent destruction nx...
  - specialize (IHee nx)...
    rewrite extract_trans_eq...
    specialize(is_term_sem_trans_with_normalized (EPrim p ee) nx) as HH...
    rewrite extract_xprim_sem_prim...
    unfold XTPrim...
    rewrite IHee...
  - dependent destruction nx...
    dependent destruction f...
    rewrite rewrite_reduced_bf in H...
  Qed.

  Lemma trans_normalized_edge {opn m} (ee:edge opn m) :
    trans_normalized (proj1_sig ee) = true.
  Proof with curry1.
  dependent destruction ee...
  Qed.
  Hint Rewrite @trans_normalized_edge : curry1_rewrite.

  Theorem extract_edge_sem_edge {opn m} (ee:edge opn m) (nx:next opn) :
    reduced_next(sem_next_node nx) = true ->
    extract_edge (sem_edge ee (sem_next_node nx)) = XEdge ee nx.
  Proof with curry1.
  rewrite rewrite_extract_edge...
  unfold sem_edge...
  rewrite extract_trans_sem_trans...
  Qed.

  Definition LDD (n:nat) : Type :=
    MaxLddGen.LDD edge (@extract_edge) (@sem_edge) n.

  Definition extract : forall {n}, bf n -> LDD n :=
    @MaxLddGen.extract edge
    (@edge_le)
    (@extract_edge)
    (@sem_edge)
    (@sem_xedge_extract_edge)
    (@reduced_next_projF_extract_edge).

  Definition sem : forall {n}, LDD n -> bf n :=
    @MaxLddGen.sem edge (@extract_edge) (@sem_edge).

  Theorem sem_extract {n} (f:bf n) : sem(extract f) = f.
  Proof. apply MaxLddGen.sem_extract. Qed.

  Theorem extract_sem {n} (l:LDD n) : extract(sem l) = l.
  Proof.
  apply MaxLddGen.extract_sem.
  - apply (@extract_edge_sem_edge).
  Qed.

  Theorem LDD_uniqueness {n} (l1 l2:LDD n) : sem l1 = sem l2 <-> l1 = l2.
  Proof.
  apply (MaxLddGen.LDD_uniqueness edge
    (@edge_le)
    (@extract_edge)
    (@sem_edge)
    (@sem_xedge_extract_edge)
    (@reduced_next_projF_extract_edge)
    (@extract_edge_sem_edge)).
  Qed.

  Theorem LDD_existence {n} (f:bf n) : exists (l:LDD n), sem l = f.
  Proof.
  apply (MaxLddGen.LDD_existence edge
    (@edge_le)
    (@extract_edge)
    (@sem_edge)
    (@sem_xedge_extract_edge)
    (@reduced_next_projF_extract_edge)).
  Qed.

End PrimOGen1.
