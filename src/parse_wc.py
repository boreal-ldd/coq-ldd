import sys

FileRName = sys.argv[1]
FileR = open(FileRName)
TXT = FileR.read().split('\n')
FileR.close()

M = [[w for w in line.split(' ') if w != ''] for line in TXT if line!='']

D = dict()

for line in M :
	D[line[3]] = {
		'line-count' : int(line[0]),
		'word-count' : int(line[1]),
		'char-count' : int(line[2])
	}

print(D)
