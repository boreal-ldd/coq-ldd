Require Import Arith .
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.

Require Import Ground.SimplProp.
Require Import Ground.SimplBool.
Require Import Ground.SimplOption.
Require Import Ground.SimplEq.
Require Import Ground.SimplFun.
Require Import Ground.SimplBoolFun.
Require Import Ground.SimplCmp.
Require Import Ground.SimplSimpl.

Require Import Ground.SimplNat.
Require Import Ground.SimplAB.

Require Import Ground.SimplList_Chap0_Standard.
Require Import Ground.SimplList_Chap1_Construct.
Require Import Ground.SimplList_Chap2_Predicate.
Require Import Ground.SimplList_Chap3_PredicateOnConstruct.
Require Import Ground.SimplList.
Require Import Ground.SimplABList.
Require Import Ground.SimplMore.
Require Import Ground.SimplDTT.
Require Import Ground.SimplLtN_Chap0_Construct.
Require Import Ground.SimplLtN_Chap1_Array_as_Function.
Require Import Ground.SimplLtN_Chap1_Sec1_Boolean_Array.
Require Import Ground.SimplLtNIndexedArray.

Require Import Ground.SimplDTT.
Require Import Ground.SimplLtN.
Require Import Ground.SimplEnum.

Require Import Ground.MiniBool_VAX.
Require Import Ground.MiniBool_Logic.

Require Import FiniteCurryfy.
Require Import FiniteBooleanFunction.

(* Section. Definition of Ordered Primitives : Intro/Elim/Detect *)

Definition pPrim (x:(bool * bool * bool)) : bool := (fst(fst x)) || (snd(fst x)).

Lemma rewrite_pPrim (x0 x1 y:bool) : pPrim (x0, x1, y) = true <-> x0 = true \/ x1 = true.
Proof. dest_bool. Qed.

Definition oPrim : Type := { x : (bool * bool * bool) | pPrim x = true }.

Definition oU : oPrim := (exist _ (true, true, false) eq_refl ).
Definition oX : oPrim := (exist _ (true, true, true ) eq_refl ).

Lemma oC_lemma if0 th0 : pPrim (negb if0, if0, th0) = true.
Proof. dest_bool. Qed.

Definition oC (if0 th0:bool) : oPrim := (exist _ (negb if0, if0, th0 ) (oC_lemma if0 th0)).

Definition detect_prim (p:oPrim) (x0 x1:bool) : bool :=
  let p := proj1_sig p in
  eqb (xorb ((fst(fst p)) && x0) ((snd(fst p)) && x1)) (snd p).

Lemma detect_prim_oU x0 x1 : detect_prim oU x0 x1 = (eqb x0 x1).
Proof. compute; BPS.MicroBPSolver; simplbool. Qed.
Lemma detect_prim_oX x0 x1 : detect_prim oX x0 x1 = (xorb x0 x1).
Proof. compute; BPS.MicroBPSolver; simplbool. Qed.
Lemma detect_prim_oC if0 th0 x0 x1 : detect_prim (oC if0 th0) x0 x1 = eqb th0 (if if0 then x1 else x0).
Proof. compute; BPS.MicroBPSolver; simplbool. Qed.
Hint Rewrite detect_prim_oU : curry1_rewrite.
Hint Rewrite detect_prim_oX : curry1_rewrite.
Hint Rewrite detect_prim_oC : curry1_rewrite.

Lemma detect_prim_oU_fun : detect_prim oU = eqb.
Proof. (repeat (apply functional_extensionality; intro)); curry1. Qed.
Lemma detect_prim_oX_fun : detect_prim oX = xorb.
Proof. (repeat (apply functional_extensionality; intro)); curry1. Qed.
Lemma detect_prim_oC_fun if0 th0 : detect_prim (oC if0 th0) = fun x0 x1 => eqb th0 (if if0 then x1 else x0).
Proof. (repeat (apply functional_extensionality; intro)); curry1. Qed.

Hint Rewrite detect_prim_oU_fun : curry1_rewrite.
Hint Rewrite detect_prim_oX_fun : curry1_rewrite.
Hint Rewrite detect_prim_oC_fun : curry1_rewrite.

Definition eval_prim (p:oPrim) (x y:bool) : bool :=
  match proj1_sig p with
  | (px0, px1, py) =>
    if xorb px0 px1
    then (* C *)
      (if eqb x px1 then py else y)
    else (* px0 = px1 = true -> U | X *)
      xorb y (x && py)
  end.

Lemma eval_prim_oU x y : eval_prim oU x y = y.
Proof. dest_bool. Qed.
Lemma eval_prim_oX x y : eval_prim oX x y = xorb x y.
Proof. dest_bool. Qed.
Lemma eval_prim_oC if0 th0 x y : eval_prim (oC if0 th0) x y = if eqb x if0 then th0 else y.
Proof. dest_bool. Qed.
Hint Rewrite eval_prim_oU : curry1_rewrite.
Hint Rewrite eval_prim_oX : curry1_rewrite.
Hint Rewrite eval_prim_oC : curry1_rewrite.

Lemma eval_prim_oU_fun : eval_prim oU = (fun x y => y).
Proof. (repeat (apply functional_extensionality; intro)); curry1. Qed.
Lemma eval_prim_oX_fun : eval_prim oX = xorb.
Proof. (repeat (apply functional_extensionality; intro)); curry1. Qed.
Lemma eval_prim_oC_fun if0 th0 : eval_prim (oC if0 th0) = (fun x y => if eqb x if0 then th0 else y).
Proof. (repeat (apply functional_extensionality; intro)); curry1. Qed.
Hint Rewrite eval_prim_oU_fun : curry1_rewrite.
Hint Rewrite eval_prim_oX_fun : curry1_rewrite.
Hint Rewrite eval_prim_oC_fun : curry1_rewrite.

Definition rev_prim (p:oPrim) : bool :=
  match proj1_sig p with
  | (px0, px1, py) =>
    if xorb px0 px1 then (negb px1) else false
  end.

Definition rev_intro (p:bool -> bool -> bool) (r:bool) := p r = id.

Lemma rev_intro_oPrim_explcit (p:oPrim) : eval_prim p (rev_prim p) = id.
Proof with curry1.
destruct p...
apply functional_extensionality...
compute...
compute in e...
dest_bool.
Qed.
Hint Rewrite rev_intro_oPrim_explcit : curry1_rewrite.

Lemma rev_intro_oPrim (p:oPrim) : rev_intro (eval_prim p) (rev_prim p).
Proof. unfold rev_intro; apply rev_intro_oPrim_explcit. Qed.

Definition intro_oP_bf (p:oPrim) {n} (f:bf n) := (fun a => eval_prim p (shead a) (f(stail a))).

Definition intro_oU_bf {n} f := @intro_oP_bf oU n f.
Definition intro_oX_bf {n} f := @intro_oP_bf oX n f.
Definition intro_oC_bf if0 th0 {n} f:= @intro_oP_bf (oC if0 th0) n f.

Definition elim_oP_bf (p:oPrim) {n} (f:bf(S n)) : bf n := fnary_evalo1 f (rev_prim p).

Definition elim_oU_bf {n} f := @elim_oP_bf oU n f.
Definition elim_oX_bf {n} f := @elim_oP_bf oX n f.
Definition elim_oC_bf if0 th0 {n} f := @elim_oP_bf (oC if0 th0) n f.

Lemma elim_oP_bf_intro_oP_bf_same p n (f:bf n) : elim_oP_bf p (intro_oP_bf p f) = f.
Proof with curry1.
apply functional_extensionality...
unfold elim_oP_bf, intro_oP_bf...
unfold fnary_evalo1...
Qed.
Hint Rewrite elim_oP_bf_intro_oP_bf_same : curry1_rewrite.

Lemma elim_intro_oU_bf n (f:bf n) : elim_oU_bf (intro_oU_bf f) = f.
Proof. apply elim_oP_bf_intro_oP_bf_same. Qed.
Lemma elim_intro_oX_bf n (f:bf n) : elim_oX_bf (intro_oX_bf f) = f.
Proof. apply elim_oP_bf_intro_oP_bf_same. Qed.
Lemma elim_intro_oC_bf if0 th0 n (f:bf n) :
  elim_oC_bf if0 th0 (intro_oC_bf if0 th0 f) = f.
Proof. apply elim_oP_bf_intro_oP_bf_same. Qed.
Hint Rewrite elim_intro_oU_bf : curry1_rewrite.
Hint Rewrite elim_intro_oX_bf : curry1_rewrite.
Hint Rewrite elim_intro_oC_bf : curry1_rewrite.

Definition is_oP_bf (p:oPrim) {n} (f:bf(S n)) : bool :=
  is_cst_bf true (bop_bf (detect_prim p) (fnary_evalo1 f false) (fnary_evalo1 f true)).

Definition is_oU_bf {n} (f:bf(S n)) : bool := is_oP_bf oU f.
Definition is_oX_bf {n} (f:bf(S n)) : bool := is_oP_bf oX f.
Definition is_oC_bf (if0 th0:bool) {n} (f:bf(S n)) : bool := is_oP_bf (oC if0 th0) f.

Lemma is_oP_bf_oU_rewrite {n} (f:bf(S n)) :
  is_oP_bf oU f = beq_bf (fnary_evalo1 f false) (fnary_evalo1 f true).
Proof. unfold is_oP_bf; curry1. Qed.
Lemma is_oP_bf_oX_rewrite {n} (f:bf(S n)) :
  is_oP_bf oX f = beq_bf (fnary_evalo1 f false) (neg_bf (fnary_evalo1 f true)).
Proof. unfold is_oP_bf; curry1. Qed.
Lemma is_oP_bf_oC_rewrite if0 th0 {n} (f:bf(S n)) :
  is_oP_bf (oC if0 th0) f = is_cst_bf th0 (fnary_evalo1 f if0).
Proof with curry1.
unfold is_oP_bf...
dest_if...
- rewrite bop_bf_x1_only...
  dest_bool...
- rewrite bop_bf_x0_only...
  dest_bool...
Qed.
Hint Rewrite @is_oP_bf_oU_rewrite : curry1_rewrite.
Hint Rewrite @is_oP_bf_oX_rewrite : curry1_rewrite.
Hint Rewrite @is_oP_bf_oC_rewrite : curry1_rewrite.

(* Section 2. Single Evaluation of Ordered Primitives *)

Lemma fnary_evalo1_intro_oP_bf n p (f:bf n) b :
  fnary_evalo1 (intro_oP_bf p f) b = (fun a => eval_prim p b (f a)).
Proof with curry1.
apply functional_extensionality...
unfold fnary_evalo1, intro_oP_bf...
Qed.

Lemma fnary_evalo1_intro_oU_bf n (f:bf n) b : fnary_evalo1 (intro_oU_bf f) b = f.
Proof with curry1.
unfold intro_oU_bf.
rewrite fnary_evalo1_intro_oP_bf...
Qed.
Hint Rewrite fnary_evalo1_intro_oU_bf : curry1_rewrite.

Lemma fnary_evalo1_intro_oX_bf n (f:bf n) b : fnary_evalo1 (intro_oX_bf f) b = xorb_bf b f.
Proof with curry1.
unfold intro_oX_bf.
rewrite fnary_evalo1_intro_oP_bf...
Qed.
Hint Rewrite fnary_evalo1_intro_oX_bf : curry1_rewrite.

Lemma fnary_evalo1_intro_oC_bf if0 th0 n (f:bf n) b :
  fnary_evalo1 (intro_oC_bf if0 th0 f) b =
    if eqb b if0 then (fun _ => th0) else f.
Proof with curry1.
unfold intro_oC_bf.
rewrite fnary_evalo1_intro_oP_bf...
dest_if...
Qed.
Hint Rewrite fnary_evalo1_intro_oC_bf : curry1_rewrite.

(* Section. Reduction of : Detect o Intro *)

(* SubSection. Reflexive Case is_A(intro_A f) *)
Lemma is_oP_bf_intro_oP_bf_same p n (f:bf n) : is_oP_bf p (intro_oP_bf p f) = true.
Proof with curry1.
unfold is_oP_bf, intro_oP_bf...
apply functional_extensionality...
destruct p...
unfold pPrim in e.
unfold bop_bf, cst_bf, detect_prim, eval_prim, fnary_evalo1...
generalize dependent e.
BPS.MicroBPSolver...
Qed.
Hint Rewrite is_oP_bf_intro_oP_bf_same : curry1_rewrite.

Lemma is_oU_bf_intro_oU_bf n (f:bf n) : is_oU_bf (intro_oU_bf f) = true.
Proof. apply is_oP_bf_intro_oP_bf_same. Qed.
Hint Rewrite is_oU_bf_intro_oU_bf : curry1_rewrite.

Lemma is_oX_bf_intro_oX_bf n (f:bf n) : is_oX_bf(intro_oX_bf f) = true.
Proof. apply is_oP_bf_intro_oP_bf_same. Qed.
Hint Rewrite is_oX_bf_intro_oX_bf : curry1_rewrite.

Lemma is_oC_bf_intro_oC_bf_same if0 th0 n (f:bf n) : is_oC_bf if0 th0(intro_oC_bf if0 th0 f) = true.
Proof. apply is_oP_bf_intro_oP_bf_same. Qed.
Hint Rewrite is_oC_bf_intro_oC_bf_same : curry1_rewrite.

(* SubSection. Asymetric Case *)
Lemma is_oU_bf_intro_oX_bf n (f:bf n) : is_oU_bf (intro_oX_bf f) = false.
Proof. unfold is_oU_bf, is_oP_bf; curry1. Qed.
Hint Rewrite is_oU_bf_intro_oX_bf : curry1_rewrite.

Lemma is_oU_bf_intro_oC_bf if0 th0 n (f:bf n) :
  is_oU_bf(intro_oC_bf if0 th0 f) = is_cst_bf th0 f.
Proof with curry1.
unfold is_oU_bf...
dest_match_step...
Qed.
Hint Rewrite is_oU_bf_intro_oC_bf : curry1_rewrite.

Lemma is_oX_intro_oU_bf n (f:bf n) :
  is_oX_bf (intro_oU_bf f) = false.
Proof. unfold is_oX_bf; curry1. Qed.
Hint Rewrite is_oX_intro_oU_bf : curry1_rewrite.

Lemma is_oX_intro_oC_bf if0 th0 n (f:bf n) :
  is_oX_bf (intro_oC_bf if0 th0 f) = is_cst_bf (negb th0) f.
Proof with curry1.
unfold is_oX_bf...
dest_match_step...
Qed.
Hint Rewrite is_oX_intro_oC_bf : curry1_rewrite.

Lemma is_oC_intro_oU_bf if0 th0 n (f:bf n) :
  is_oC_bf if0 th0 (intro_oU_bf f) = is_cst_bf th0 f.
Proof. unfold is_oC_bf; curry1. Qed.
Hint Rewrite is_oC_intro_oU_bf : curry1_rewrite.

Lemma is_oC_intro_oX_bf if0 th0 n (f:bf n) :
  is_oC_bf if0 th0 (intro_oX_bf f) = is_cst_bf (xorb if0 th0) f.
Proof.
unfold is_oC_bf; curry1.
rewrite xorb_comm; reflexivity.
Qed.
Hint Rewrite is_oC_intro_oX_bf : curry1_rewrite.

Lemma is_oC_bf_intro_oC_bf if0 th0 if1 th1 n (f:bf n) :
  is_oC_bf if0 th0(intro_oC_bf if1 th1 f) =
    if eqb if0 if1
    then eqb th0 th1
    else is_cst_bf th0 f.
Proof with curry1.
unfold is_oC_bf, elim_oC_bf, elim_oP_bf, is_cst_bf...
dest_match_step...
Qed.
Hint Rewrite is_oC_bf_intro_oC_bf : curry1_rewrite.

(* Section. Reduction of : Intro o Elim (Asymmetric Case) *)

Lemma elim_oP_bf_intro_oP_bf p1 p2 n (f:bf n) :
  elim_oP_bf p2 (intro_oP_bf p1 f) = (fun x => eval_prim p1 (rev_prim p2) (f x)).
Proof with curry1.
apply functional_extensionality...
unfold elim_oP_bf, intro_oP_bf, fnary_evalo1...
Qed.
Hint Rewrite elim_oP_bf_intro_oP_bf : curry1_rewrite.

Lemma elim_oU_bf_intro_oX_bf n (f:bf n) : elim_oU_bf(intro_oX_bf f) = f.
Proof. curry1. Qed.
Hint Rewrite elim_oU_bf_intro_oX_bf : curry1_rewrite.

Lemma elim_oU_bf_intro_oC_bf if0 th0 n (f:bf n) :
  elim_oU_bf(intro_oC_bf if0 th0 f) = if if0 then f else (fun _ => th0).
Proof with curry1.
unfold elim_oU_bf, elim_oP_bf...
dest_match_step...
Qed.
Hint Rewrite elim_oU_bf_intro_oC_bf : curry1_rewrite.

Lemma elim_oX_bf_intro_oU_bf n (f:bf n) : elim_oX_bf(intro_oU_bf f) = f.
Proof. curry1. Qed.
Hint Rewrite elim_oX_bf_intro_oU_bf : curry1_rewrite.

Lemma elim_oX_bf_intro_oC_bf if0 th0 n (f:bf n) :
  elim_oX_bf(intro_oC_bf if0 th0 f) = if if0 then f else (fun _ => th0).
Proof with curry1.
unfold elim_oX_bf, intro_oC_bf...
dest_match_step...
Qed.
Hint Rewrite elim_oX_bf_intro_oC_bf : curry1_rewrite.

Lemma elim_oC_bf_intro_oU_bf if0 th0 n (f:bf n) :
  elim_oC_bf if0 th0 (intro_oU_bf f) = f.
Proof with curry1.
unfold elim_oC_bf, intro_oU_bf...
Qed.
Hint Rewrite elim_oC_bf_intro_oU_bf : curry1_rewrite.

Lemma elim_oC_bf_intro_oX_bf if0 th0 n (f:bf n) :
  elim_oC_bf if0 th0 (intro_oX_bf f) = xorb_bf (negb if0) f.
Proof with curry1.
unfold elim_oC_bf, intro_oX_bf...
unfold rev_prim...
Qed.
Hint Rewrite elim_oC_bf_intro_oX_bf : curry1_rewrite.

Lemma elim_oC_bf_intro_oC_bf if0 th0 if1 th1 n (f:bf n) :
  elim_oC_bf if0 th0 (intro_oC_bf if1 th1 f) =
    if xorb if0 if1
    then (fun _ => th1)
    else f.
Proof with curry1.
unfold elim_oC_bf, intro_oC_bf...
unfold rev_prim...
dest_match_step...
Qed.
Hint Rewrite elim_oC_bf_intro_oC_bf : curry1_rewrite.

(* Section. Triade Intro/Elim/Detect *)

(* SubSection. Symmetric Case *)
Lemma intro_oP_bf_elim_oP_bf_same_eq_same p {n} (f:bf(S n)) :
  intro_oP_bf p (elim_oP_bf p f) = f <-> is_oP_bf p f = true.
Proof with curry1.
unfold intro_oP_bf, elim_oP_bf, is_oP_bf...
destruct p...
unfold pPrim in e...
unfold fnary_evalo1...
unfold eval_prim, rev_prim, detect_prim...
unfold bop_bf.
split; curry1; apply functional_extensionality...
- rewrite <- H...
  generalize dependent e.
  BPS.MicroBPSolver...
- specialize(equal_f H (stail x))... clear H.
  repeat rewrite (eval_bf_S_n _ f)...
  generalize dependent e.
  BPS.MicroBPSolver...
Qed.
Hint Rewrite intro_oP_bf_elim_oP_bf_same_eq_same : curry1_rewrite.

Lemma intro_elim_eq_same_oU_bf {n} (f:bf(S n)) : intro_oU_bf (elim_oU_bf f) = f <-> is_oU_bf f = true.
Proof. apply intro_oP_bf_elim_oP_bf_same_eq_same. Qed.
Hint Rewrite @intro_elim_eq_same_oU_bf : curry1_rewrite.

Lemma intro_elim_eq_same_oX_bf {n} (f:bf(S n)) : intro_oX_bf (elim_oX_bf f) = f <-> is_oX_bf f = true.
Proof. apply intro_oP_bf_elim_oP_bf_same_eq_same. Qed.
Hint Rewrite @intro_elim_eq_same_oX_bf : curry1_rewrite.

Lemma intro_elim_eq_same_oC_bf if0 th0 {n} (f:bf(S n)) :
  intro_oC_bf if0 th0 (elim_oC_bf if0 th0 f) = f <-> is_oC_bf if0 th0 f = true.
Proof. apply intro_oP_bf_elim_oP_bf_same_eq_same. Qed.
Hint Rewrite @intro_elim_eq_same_oC_bf : curry1_rewrite.

(* SubSection. Asymmetric Rewriting of Intro into Detect /\ Elim *)

Lemma intro_oP_bf_as_is_oP_bf p n f g :
  @intro_oP_bf p n f = g <-> (is_oP_bf p g = true /\ f = elim_oP_bf p g).
Proof. split; curry1. Qed.

Lemma intro_oU_bf_as_is_oU_bf n f g : @intro_oU_bf n f = g <-> (is_oU_bf g = true /\ f = elim_oU_bf g).
Proof. apply intro_oP_bf_as_is_oP_bf. Qed.
Hint Rewrite intro_oU_bf_as_is_oU_bf : curry1_rewrite.

Lemma intro_oX_bf_as_is_oX_bf n f g :
  @intro_oX_bf n f = g <-> (is_oX_bf g = true /\ f = elim_oX_bf g).
Proof. apply intro_oP_bf_as_is_oP_bf. Qed.
Hint Rewrite intro_oX_bf_as_is_oX_bf : curry1_rewrite.

Lemma intro_oC_bf_as_is_oC_bf if0 th0 n f g :
  @intro_oC_bf if0 th0 n f = g <-> (is_oC_bf if0 th0 g = true /\ f = elim_oC_bf if0 th0 g).
Proof. apply intro_oP_bf_as_is_oP_bf. Qed.
Hint Rewrite intro_oC_bf_as_is_oC_bf : curry1_rewrite.

(* Section. Intro eq Intro *)

(* SubSection. Symmetric Case *)

Lemma intro_oU_bf_eq_intro_oU_bf n (f1 f2:bf n) : intro_oU_bf f1 = intro_oU_bf f2 <-> f1 = f2.
Proof. curry1. Qed.

Lemma intro_oX_bf_eq_intro_oX_bf n (f1 f2:bf n) : intro_oX_bf f1 = intro_oX_bf f2 <-> f1 = f2.
Proof. curry1. Qed.

Lemma intro_oC_bf_eq_intro_oC_bf_same if0 th0 n (f1 f2:bf n) :
  intro_oC_bf if0 th0 f1 = intro_oC_bf if0 th0 f2 <-> f1 = f2.
Proof. curry1. Qed.

(* SubSection. Asymmetric Case *)

Lemma intro_oU_bf_eq_intro_oX_bf n f1 f2 :
  @intro_oU_bf n f1 = intro_oX_bf f2 <-> False.
Proof. curry1. Qed.
Hint Rewrite intro_oU_bf_eq_intro_oX_bf : curry1_rewrite.

Lemma intro_oU_bf_eq_intro_oC_bf n f1 if0 th0 f2 :
  @intro_oU_bf n f1 = intro_oC_bf if0 th0 f2 <->
    f2 = f1 /\ f1 = (fun _ => th0).
Proof. curry1; split; curry1. Qed.
Hint Rewrite intro_oU_bf_eq_intro_oC_bf : curry1_rewrite.

Lemma intro_oX_bf_eq_intro_oU_bf n f1 f2 :
  @intro_oX_bf n f1 = intro_oU_bf f2 <-> False.
Proof. curry1. Qed.
Hint Rewrite intro_oX_bf_eq_intro_oU_bf : curry1_rewrite.

Lemma intro_oX_bf_eq_intro_oC_bf n f1 if0 th0 f2 :
  @intro_oX_bf n f1 = intro_oC_bf if0 th0 f2 <->
    f2 = (fun _ => negb th0) /\
    f1 = xorb_bf (negb if0) f2.
Proof with curry1.
simpl...
split; curry1; dest_if...
Qed.
Hint Rewrite intro_oX_bf_eq_intro_oC_bf : curry1_rewrite.

Lemma intro_oC_bf_eq_intro_oC_bf n if0 th0 f0 if1 th1 f1 :
  @intro_oC_bf if0 th0 n f0 = intro_oC_bf if1 th1 f1 <->
    (if1 = if0 -> (th0 = th1 /\ f0 = f1)) /\
    (if1 = negb if0 -> (f1 = (fun _ => th0) /\ f0 = (fun _ => th1))).
Proof with curry1.
simpl...
dest_match_step...
Qed.
