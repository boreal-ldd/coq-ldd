Require Import Arith .
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.

Require Import Ground.SimplProp.
Require Import Ground.SimplBool.
Require Import Ground.SimplOption.
Require Import Ground.SimplEq.
Require Import Ground.SimplFun.
Require Import Ground.SimplBoolFun.
Require Import Ground.SimplCmp.
Require Import Ground.SimplSimpl.

Require Import Ground.SimplNat.
Require Import Ground.SimplAB.

Require Import Ground.SimplList_Chap0_Standard.
Require Import Ground.SimplList_Chap1_Construct.
Require Import Ground.SimplList_Chap2_Predicate.
Require Import Ground.SimplList_Chap3_PredicateOnConstruct.
Require Import Ground.SimplList.
Require Import Ground.SimplABList.
Require Import Ground.SimplMore.
Require Import Ground.SimplDTT.
Require Import Ground.SimplLtN_Chap0_Construct.
Require Import Ground.SimplLtN_Chap1_Array_as_Function.
Require Import Ground.SimplLtN_Chap1_Sec1_Boolean_Array.
Require Import Ground.SimplLtNIndexedArray.

Require Import Ground.SimplDTT.
Require Import Ground.SimplLtN.
Require Import Ground.SimplEnum.
Require Import Ground.SimplEnumCmp.

Require Import Ground.MiniBool_VAX.
Require Import Ground.MiniBool_Logic.

Require Import FiniteCurryfy.
Require Import FiniteBooleanFunction.

Require Import Ground.SimplEnum.

Require Import LddU_Primitives.
(* Require Import LddL_NatPrimitives. *)

(* we define vec as a non-false Boolean array *)

Definition non_false_array {n} (a:array bool n) : bool := isSome(array_minsat a).

Definition vec (n:nat) : Type := { a : array bool n | non_false_array a = true }.

(* we define pvec as a non-false Boolean array, with fixed pivot *)

Definition pvec_p {n} (ak : (array bool n * ltN n)) : bool := (fst ak)(snd ak).

Definition pvec (n:nat) : Type := { ak : (array bool n * ltN n) | pvec_p ak = true }.

Lemma vec_of_pvec_lemma1 n a k : @pvec_p n (a, k) = true -> non_false_array a = true.
Proof with curry1.
unfold pvec_p, non_false_array...
apply reverse_bool_eq...
Qed.

Definition vec_of_pvec {n} (pv:pvec n) : vec n :=
  (exist _ (fst(proj1_sig pv)) (vec_of_pvec_lemma1 _ _ _ (proj2_sig pv))).

Definition pvec_of_vec {n} (v:vec n) (k:ltN n) (H:(proj1_sig v) k = true) : pvec n :=
  (exist _ (proj1_sig v, k) H).

Lemma pvec_of_vec_min_lemma1 {n} (v:vec n) : proj1_sig v <> (fun _ => false).
Proof with curry1.
destruct v as [v Hv]...
unfold non_false_array in Hv...
Qed.

Lemma pvec_of_vec_min_lemma2 {n} (v:vec n) :
  pvec_p (proj1_sig v, array_minsat_true (proj1_sig v) (pvec_of_vec_min_lemma1 v)) = true.
Proof with curry1.
unfold pvec_p...
remember(array_minsat_true (proj1_sig v) (pvec_of_vec_min_lemma1 v)) as x eqn:E.
symmetry in E. rewrite array_minsat_iff_is_min_positive in E.
unfold is_min_positive in E...
Qed.

Definition pvec_of_vec_min {n} (v:vec n) : pvec n :=
  (exist _
    (proj1_sig v, array_minsat_true (proj1_sig v) (pvec_of_vec_min_lemma1 v))
    (pvec_of_vec_min_lemma2 v)
  ).

Lemma maxsat_alternative_array_maxsat {n} : maxsat_alternative (@cmp_ltN n) array_maxsat.
Proof with curry1.
rewrite <- maxsat_positive_iff_maxsat_alternative.
apply maxsat_positive_array_maxsat.
Qed.

Lemma array_maxsat_true_lemma1 {n} (p:ltN n -> bool) :
  p <> (fun _ => false) -> array_maxsat p = None -> False.
Proof. intros H1 H2; rewrite (maxsat_alternative_array_maxsat _) in H2; simplprop. Qed.

Definition array_maxsat_true {n} (p : ltN n -> bool) (P : p <> (fun _ : ltN n => false)) : ltN n :=
match array_maxsat p as mp return (array_maxsat p = mp -> ltN n) with
| Some k => fun _  => k
| None => fun E => match array_maxsat_true_lemma1 p P E return (ltN n) with end
end eq_refl.

Lemma array_maxsat_iff_is_min_positive {n} a p k : @array_maxsat_true n a p = k <-> is_max_positive cmp_ltN a k.
Proof with curry1.
unfold array_maxsat_true...
pattern (match array_maxsat a as mp return (array_maxsat a = mp -> ltN n) with
| Some k0 => fun _ : array_maxsat a = Some k0 => k0
| None => fun E : array_maxsat a = None => match array_maxsat_true_lemma1 a p E return (ltN n) with
                                         end
end eq_refl).
rewrite match_option_dtt...
split...
- dest_match_step...
- rewrite (maxsat_alternative_array_maxsat _) in p0...
  split...
  refine (is_max_positive_unique _ (@cmp_P_cmp_ltN _) _ _ _ p0 H)...
Qed.

Lemma pvec_of_vec_max_lemma2 {n} (v:vec n) :
  pvec_p (proj1_sig v, array_maxsat_true (proj1_sig v) (pvec_of_vec_min_lemma1 v)) = true.
Proof with curry1.
unfold pvec_p...
remember(array_maxsat_true (proj1_sig v) (pvec_of_vec_min_lemma1 v)) as x eqn:E.
symmetry in E. rewrite array_maxsat_iff_is_min_positive in E.
unfold is_max_positive in E...
Qed.

Definition pvec_of_vec_max {n} (v:vec n) : pvec n :=
  (exist _
    (proj1_sig v, array_maxsat_true (proj1_sig v) (pvec_of_vec_min_lemma1 v))
    (pvec_of_vec_max_lemma2 v)
  ).

Lemma minsat_alternative_minsat_bf n : minsat_alternative cmp_array_bool (@minsat_bf n).
Proof with curry1.
rewrite <- minsat_positive_iff_minsat_alternative.
apply minsat_positive_minsat_bf.
Qed.

Lemma minsat_bf_vec_lemma1 {n} (f:bf n) a0 :
  let f' : bf n := (fun a => non_false_array a && f a) in
  minsat_bf f' = Some a0 ->  non_false_array a0 = true.
Proof with curry1.
curry1.
rewrite (minsat_alternative_minsat_bf _ _) in H...
unfold is_min_positive in H...
Qed.

Definition minsat_bf_vec {n} (f:bf n) : option(vec n) :=
  let f' : bf n := (fun a => non_false_array a && f a) in
  match minsat_bf f' as v0 return minsat_bf f' = v0 -> option(vec n) with
  | Some a => fun E => Some ((exist _ a (minsat_bf_vec_lemma1 f a E)):vec n)
  | None   => fun _ => None
  end eq_refl.

Definition minsat_bf_pvec_min {n} (f:bf n) : option(pvec n) :=
  opmap pvec_of_vec_min (minsat_bf_vec f).

Definition minsat_bf_pvec_max {n} (f:bf n) : option(pvec n) :=
  opmap pvec_of_vec_max (minsat_bf_vec f).

Definition lC_twist_array {n} (pv:pvec(S n)) (a:array bool(S n)) : array bool (S n) :=
  let pv0 := proj1_sig pv in
  let v := fst pv0 in
  let k := snd pv0 in
  (spush k (adot a v) (spop k a)).

Lemma lC_twist_array_involutive {n} (pv:pvec(S n)) a : lC_twist_array pv (lC_twist_array pv a) = a.
Proof with curry1.
destruct pv as [[v k] Hpv]...
unfold lC_twist_array...
rewrite (array_expansion_u k)...
rewrite (rewrite_adot_using_spop k)...
rewrite (rewrite_adot_using_spop k)...
Qed.
Hint Rewrite @lC_twist_array_involutive : curry1_rewrite.

Lemma lC_twist_array_eval_pivot {n} (pv:pvec(S n)) a :
  (lC_twist_array pv a) (snd(proj1_sig pv)) = adot (fst(proj1_sig pv)) a.
Proof with curry1.
destruct pv as [[v k] Hpv]...
unfold lC_twist_array...
rewrite adot_comm...
Qed.
Hint Rewrite @lC_twist_array_eval_pivot : curry1_rewrite.

Definition lC_twist_bf {n} (pv:pvec(S n)) (f:bf(S n)) : bf(S n) :=
  (fun a => f(lC_twist_array pv a)).

Lemma lC_twist_bf_lC_twist_bf_same {n} pv f : @lC_twist_bf n pv (lC_twist_bf pv f) = f.
Proof. unfold lC_twist_bf; apply functional_extensionality; curry1. Qed.
Hint Rewrite @lC_twist_bf_lC_twist_bf_same : curry1_rewrite.

Lemma lC_twist_array_shift {n} pv (a1 a2:array _ (S n)) :
  lC_twist_array pv a1 = a2 <-> a1 = lC_twist_array pv a2.
Proof. split; curry1. Qed.

Lemma lC_twist_bf_shift {n} (f g:bf(S n)) pv : lC_twist_bf pv f = g <-> f = lC_twist_bf pv g.
Proof. split; curry1. Qed.

Lemma lC_twist_bf_cst_bf {n} pv b : lC_twist_bf pv (cst_bf (S n) b) = cst_bf _ b.
Proof. reflexivity. Qed.
Hint Rewrite @lC_twist_bf_cst_bf : curry1_rewrite.

Lemma lC_twist_bf_injective {n} pv (f1 f2:bf(S n)) : lC_twist_bf pv f1 = lC_twist_bf pv f2 <-> f1 = f2.
Proof with curry1.
split...
specialize(f_equal(fun f => lC_twist_bf pv f)H)...
Qed.
Hint Rewrite @lC_twist_bf_injective : curry1_rewrite.

Lemma lC_twist_bf_axor_input {n} (pv:pvec(S n)) (v1:array bool(S n)) f :
  let pv0 := proj1_sig pv in
  let k := snd pv0 in
  let v := fst pv0 in
  lC_twist_bf pv (axor_input v1 f) =
    axor_input (axor v1 (array_cdirac (xorb(v1 k)(adot v v1)) k)) (lC_twist_bf pv f).
Proof with curry1.
unfold lC_twist_bf...
apply functional_extensionality...
unfold lC_twist_array, axor_input...
apply f_equal...
rewrite axor_shift...
destruct pv as [[v0 k] Hpv]...
rewrite (array_expansion_u k)...
repeat rewrite adot_axor_l...
rewrite (adot_comm _ v1 v0)...
ring.
Qed.

Lemma lC_twist_bf_axor_input_same {n} (pv:pvec(S n)) f :
  let pv0 := proj1_sig pv in
  let v := fst pv0 in
  let k := snd pv0 in
  lC_twist_bf pv (axor_input v f) =
    axor_input (axor v (array_cdirac (negb (asum v)) k)) (lC_twist_bf pv f).
Proof with curry1.
simpl; rewrite lC_twist_bf_axor_input...
destruct pv as [[v k] Hpv]...
apply apply_intro...
Qed.

Lemma array_dirac_same {A n} k a0 : @array_dirac A n k a0 a0 = (fun _ => a0).
Proof with curry1.
apply functional_extensionality...
unfold array_dirac...
Qed.
Hint Rewrite @array_dirac_same : curry1_rewrite.

Lemma array_cdirac_false n : @array_cdirac false n = fun _ _ => false.
Proof with curry1.
unfold array_cdirac...
apply functional_extensionality...
Qed.
Hint Rewrite @array_cdirac_false : curry1_rewrite.

Lemma array_cdirac_eq_false b n k : @array_cdirac b n k = (fun _ => false) <-> b = false.
Proof with curry1.
destruct b...
specialize (f_equal(fun f => f k)H)...
Qed.
Hint Rewrite array_cdirac_eq_false : curry1_rewrite.

Lemma false_eq_array_cdirac n b k : (fun _ => false) = @array_cdirac b n k <-> b = false.
Proof. split; intro H; curry1; symmetry in H; rewrite (array_cdirac_eq_false b n k) in H; curry1. Qed.
Hint Rewrite false_eq_array_cdirac : curry1_rewrite.

Lemma array_cdirac_eval_eq_true {n} b1 k1 k2 : @array_cdirac b1 n k1 k2 = true <-> b1 = true /\ k1 = k2.
Proof with curry1.
unfold array_cdirac, array_dirac...
rewrite andb_true_iff...
Qed.
Hint Rewrite @array_cdirac_eval_eq_true : curry1_rewrite.

Lemma array_cdirac_injective {n} b1 b2 k1 k2 :
  @array_cdirac b1 n k1 = array_cdirac b2 k2 <-> b1 = b2 /\ (b1 = false \/ k1 = k2).
Proof with curry1.
destruct n...
rewrite (array_expansion_u k1)...
rewrite (eq_sym_iff b1)...
destruct b1...
- destruct b2...
  rewrite (eq_sym_iff k2)...
- split...
  symmetry in H0...
Qed.
Hint Rewrite @array_cdirac_injective : curry1_rewrite.

Lemma adot_axor_r {n} (x y1 y2:array _ n) : adot x (axor y1 y2) = xorb (adot x y1) (adot x y2).
Proof with curry1.
rewrite adot_comm, adot_axor_l, adot_comm...
rewrite adot_comm...
Qed.

Lemma adot_array_dirac_r n v b k : @adot n v (array_cdirac b k) = b && (v k).
Proof. rewrite adot_comm; curry1. Qed.
Hint Rewrite adot_array_dirac_r : curry1_rewrite.

Lemma axor_input_lC_twist_bf {n}  (pv:pvec(S n)) (v1:array bool(S n)) f :
  let pv0 := proj1_sig pv in
  let k := snd pv0 in
  let v := fst pv0 in
  axor_input v1 (lC_twist_bf pv f) =
    lC_twist_bf pv (axor_input (axor v1 (array_cdirac (xorb(v1 k)(adot v v1)) k)) f).
Proof with curry1.
simpl.
rewrite (lC_twist_bf_axor_input pv)...
apply apply_intro...
apply apply_intro...
symmetry; repeat rewrite axor_shift...
destruct pv as [[v k] Hpv]...
rewrite adot_axor_r...
Qed.

Lemma axor_input_false {n} : @axor_input n (fun _ => false) = id.
Proof. unfold axor_input; curry1. Qed.
Hint Rewrite @axor_input_false : curry1_rewrite.

Lemma axor_array_dirac_same_pvec b1 b2 {n} (k:ltN n) :
  axor (array_cdirac b1 k) (array_cdirac b2 k) = array_cdirac (xorb b1 b2) k.
Proof with curry1.
destruct n...
rewrite (array_expansion_u k)...
Qed.
Hint Rewrite @axor_array_dirac_same_pvec : curry1_rewrite.

Lemma eval_pvec {n} (pv:pvec n) : fst (proj1_sig pv) (snd (proj1_sig pv)) = true.
Proof. destruct pv as [[v k] Hpv]; curry1. Qed.
Hint Rewrite @eval_pvec : curry1_rewrite.

Definition lUX_twist_array {n} (pv:pvec(S n)) (a:array bool(S n)) : array bool (S n) :=
  let pv0 := proj1_sig pv in
  let v := fst pv0 in
  let k := snd pv0 in
  (spush k (a k) (axor (spop k a) (ascal (a k) (spop k v)))).

Lemma lUX_twist_array_same_involutive {n} (pv:pvec(S n)) a : lUX_twist_array pv (lUX_twist_array pv a) = a.
Proof with curry1.
destruct pv as [[v k] Hpv]...
unfold lUX_twist_array...
Qed.
Hint Rewrite @lUX_twist_array_same_involutive : curry1_rewrite.

Lemma lUX_twist_array_eval_pivot {n} (pv:pvec(S n)) a :
  (lUX_twist_array pv a) (snd(proj1_sig pv)) =  a (snd(proj1_sig pv)).
Proof with curry1.
destruct pv as [[v k] Hpv]...
unfold lUX_twist_array...
Qed.
Hint Rewrite @lC_twist_array_eval_pivot : curry1_rewrite.

Definition lUX_twist_bf {n} (pv:pvec(S n)) (f:bf(S n)) : bf(S n) :=
  (fun a => f(lUX_twist_array pv a)).

Lemma lUX_twist_bf_same_involutive {n} pv f : @lUX_twist_bf n pv (lUX_twist_bf pv f) = f.
Proof. unfold lUX_twist_bf; apply functional_extensionality; curry1. Qed.
Hint Rewrite @lUX_twist_bf_same_involutive : curry1_rewrite.

Lemma lUX_twist_array_shift {n} pv (a1 a2:array _ (S n)) :
  lUX_twist_array pv a1 = a2 <-> a1 = lUX_twist_array pv a2.
Proof. split; curry1. Qed.

Lemma lUX_twist_bf_shift {n} (f g:bf(S n)) pv : lUX_twist_bf pv f = g <-> f = lUX_twist_bf pv g.
Proof. split; curry1. Qed.

Lemma lUX_twist_bf_cst_bf {n} pv b : lUX_twist_bf pv (cst_bf (S n) b) = cst_bf _ b.
Proof. reflexivity. Qed.
Hint Rewrite @lC_twist_bf_cst_bf : curry1_rewrite.

Lemma lUX_twist_bf_injective {n} pv (f1 f2:bf(S n)) : lUX_twist_bf pv f1 = lUX_twist_bf pv f2 <-> f1 = f2.
Proof with curry1.
split...
specialize(f_equal(fun f => lUX_twist_bf pv f)H)...
Qed.
Hint Rewrite @lC_twist_bf_injective : curry1_rewrite.

Lemma lUX_twist_bf_axor_input {n} (pv:pvec(S n)) (v1:array bool(S n)) f :
  let pv0 := proj1_sig pv in
  let k := snd pv0 in
  let v := fst pv0 in
  lUX_twist_bf pv (axor_input v1 f) =
    axor_input (lUX_twist_array pv v1) (lUX_twist_bf pv f).
Proof with curry1.
unfold lUX_twist_bf...
apply functional_extensionality...
unfold lUX_twist_array, axor_input...
apply f_equal...
destruct pv as [[v0 k] Hpv]...
rewrite (array_expansion_u k)...
rewrite (axor_comm _ (axor (spop k x) _)), axor_assoc...
destruct(eqb (x k) (v1 k))eqn:E0; curry1; rewrite E0...
Qed.

Lemma lUX_twist_array_same {n} (pv:pvec(S n)) :
  let pv0 := proj1_sig pv in
  let v := fst pv0 in
  let k := snd pv0 in
  lUX_twist_array pv v = spush k (v k) (ascal (negb(v k)) (spop k v)).
Proof. unfold lUX_twist_array; curry1. Qed.

Lemma lUX_twist_bf_axor_input_same {n} (pv:pvec(S n)) f :
  let pv0 := proj1_sig pv in
  let v := fst pv0 in
  let k := snd pv0 in
  lUX_twist_bf pv (axor_input v f) =
    axor_input (lUX_twist_array pv v) (lUX_twist_bf pv f).
Proof. simpl; rewrite lUX_twist_bf_axor_input; reflexivity. Qed.

Lemma axor_input_lUX_twist_bf {n}  (pv:pvec(S n)) (v1:array bool(S n)) f :
  let pv0 := proj1_sig pv in
  let k := snd pv0 in
  let v := fst pv0 in
  axor_input v1 (lUX_twist_bf pv f) =
    lUX_twist_bf pv (axor_input (lUX_twist_array pv v1) f).
Proof. simpl; rewrite (lUX_twist_bf_axor_input pv); curry1. Qed.
