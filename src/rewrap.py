import sys

Tag = sys.argv[1]
FileRNameL = sys.argv[2:]

DD = dict()

for FileRName in FileRNameL :
	FileR = open(FileRName)
	DR = eval(FileR.read())
	FileR.close()
	V = DR[Tag]
	del DR[Tag]
	assert(V not in DD)
	DD[V] = DR

print(DD)
