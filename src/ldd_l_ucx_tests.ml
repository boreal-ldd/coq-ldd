
type unit0 =
| Tt

type bool =
| True
| False

(** val xorb : bool -> bool -> bool **)

let xorb b1 b2 =
  match b1 with
  | True -> (match b2 with
             | True -> False
             | False -> True)
  | False -> b2

(** val negb : bool -> bool **)

let negb = function
| True -> False
| False -> True

type nat =
| O
| S of nat

type 'a option =
| Some of 'a
| None

type ('a, 'b) prod =
| Pair of 'a * 'b

(** val fst : ('a1, 'a2) prod -> 'a1 **)

let fst = function
| Pair (x, _) -> x

(** val snd : ('a1, 'a2) prod -> 'a2 **)

let snd = function
| Pair (_, y) -> y

type 'a list =
| Nil
| Cons of 'a * 'a list

type 'a sig0 = 'a
  (* singleton inductive, whose constructor was exist *)

(** val pred : nat -> nat **)

let pred n = match n with
| O -> n
| S u -> u

(** val eqb : bool -> bool -> bool **)

let eqb b1 b2 =
  match b1 with
  | True -> b2
  | False -> (match b2 with
              | True -> False
              | False -> True)

module Nat =
 struct
  (** val eqb : nat -> nat -> bool **)

  let rec eqb n m =
    match n with
    | O -> (match m with
            | O -> True
            | S _ -> False)
    | S n' -> (match m with
               | O -> False
               | S m' -> eqb n' m')

  (** val leb : nat -> nat -> bool **)

  let rec leb n m =
    match n with
    | O -> True
    | S n' -> (match m with
               | O -> False
               | S m' -> leb n' m')

  (** val ltb : nat -> nat -> bool **)

  let ltb n m =
    leb (S n) m
 end

(** val nth_error : 'a1 list -> nat -> 'a1 option **)

let rec nth_error l = function
| O -> (match l with
        | Nil -> None
        | Cons (x, _) -> Some x)
| S n0 -> (match l with
           | Nil -> None
           | Cons (_, l0) -> nth_error l0 n0)

(** val solution_left : 'a1 -> 'a2 -> 'a1 -> 'a2 **)

let solution_left _ x _ =
  x

(** val id : 'a1 -> 'a1 **)

let id x =
  x

(** val comp : ('a1 -> 'a2) -> ('a2 -> 'a3) -> 'a1 -> 'a3 **)

let comp f g x =
  g (f x)

(** val opmap : ('a1 -> 'a2) -> 'a1 option -> 'a2 option **)

let opmap f = function
| Some a -> Some (f a)
| None -> None

(** val isSome : 'a1 option -> bool **)

let isSome = function
| Some _ -> True
| None -> False

(** val isNone : 'a1 option -> bool **)

let isNone o =
  negb (isSome o)

(** val unop : 'a1 option -> 'a1 **)

let unop = function
| Some a -> a
| None -> assert false (* absurd case *)

type ord =
| Lt
| Eq
| Gt

type 'a cmp = 'a -> 'a -> ord

(** val cmp_comp : ord -> ord -> ord **)

let cmp_comp x1 x2 =
  match x1 with
  | Eq -> x2
  | x -> x

(** val cmp_bool : bool cmp **)

let cmp_bool b1 b2 =
  match b1 with
  | True -> (match b2 with
             | True -> Eq
             | False -> Gt)
  | False -> (match b2 with
              | True -> Lt
              | False -> Eq)

(** val beq_ord : ord -> ord -> bool **)

let beq_ord o1 o2 =
  match o1 with
  | Lt -> (match o2 with
           | Lt -> True
           | _ -> False)
  | Eq -> (match o2 with
           | Eq -> True
           | _ -> False)
  | Gt -> (match o2 with
           | Gt -> True
           | _ -> False)

(** val cmp_prod :
    'a1 cmp -> 'a2 cmp -> ('a1, 'a2) prod -> ('a1, 'a2) prod -> ord **)

let cmp_prod cA cB x y =
  cmp_comp (cA (fst x) (fst y)) (cB (snd x) (snd y))

type 'a minsat_t = ('a -> bool) -> 'a option

(** val minsat_prod :
    'a1 minsat_t -> 'a2 minsat_t -> (('a1, 'a2) prod -> bool) -> ('a1, 'a2)
    prod option **)

let minsat_prod minsat_A minsat_B p =
  match minsat_A (fun a -> isSome (minsat_B (fun b -> p (Pair (a, b))))) with
  | Some a0 ->
    (match minsat_B (fun b -> p (Pair (a0, b))) with
     | Some b0 -> Some (Pair (a0, b0))
     | None -> None)
  | None -> None

(** val cmp_sig : 'a1 cmp -> 'a1 cmp **)

let cmp_sig cmpA =
  cmpA

type ('a, 'b) aB =
| AA of 'a
| BB of 'b

(** val cmp_list : 'a1 cmp -> 'a1 list -> 'a1 list -> ord **)

let rec cmp_list cA l1 l2 =
  match l1 with
  | Nil -> (match l2 with
            | Nil -> Eq
            | Cons (_, _) -> Lt)
  | Cons (h1, t1) ->
    (match l2 with
     | Nil -> Gt
     | Cons (h2, t2) -> (match cA h1 h2 with
                         | Eq -> cmp_list cA t1 t2
                         | x -> x))

type ltN = nat

(** val ltN_of_nat : nat -> nat -> ltN **)

let ltN_of_nat k _ =
  k

(** val ltN_S : nat -> ltN -> ltN **)

let ltN_S n k =
  ltN_of_nat (S k) (S n)

(** val ltN_intro : nat -> ltN -> ltN -> ltN **)

let ltN_intro n k x =
  match Nat.ltb x k with
  | True -> ltN_of_nat x (S n)
  | False -> ltN_of_nat (S x) (S n)

(** val ltN_pop : nat -> ltN -> ltN -> (unit0, ltN) aB **)

let ltN_pop n k x =
  match Nat.eqb x k with
  | True -> AA Tt
  | False ->
    BB
      (match Nat.ltb x k with
       | True -> ltN_of_nat x n
       | False -> ltN_of_nat (pred x) n)

(** val ltN_pred : nat -> ltN -> ltN option **)

let ltN_pred n = function
| O -> None
| S k0 -> Some (ltN_of_nat k0 n)

type 'a array = ltN -> 'a

(** val sempty : 'a1 array **)

let sempty _ =
  assert false (* absurd case *)

(** val scons : nat -> 'a1 -> 'a1 array -> 'a1 array **)

let scons n a s k =
  match ltN_pred n k with
  | Some k' -> s k'
  | None -> a

(** val shead : nat -> 'a1 array -> 'a1 **)

let shead n s =
  s (ltN_of_nat O (S n))

(** val stail : nat -> 'a1 array -> 'a1 array **)

let stail n s k =
  s (ltN_S n k)

(** val fold_left_ltN_comp : nat -> ('a1 -> 'a1) array -> 'a1 -> 'a1 **)

let rec fold_left_ltN_comp n a =
  match n with
  | O -> id
  | S n0 -> comp (fold_left_ltN_comp n0 (stail n0 a)) (shead n0 a)

(** val fold_left_ltN :
    nat -> ('a1 -> 'a2 -> 'a2) -> 'a2 -> 'a1 array -> 'a2 **)

let fold_left_ltN n f b0 a =
  fold_left_ltN_comp n (fun k -> f (a k)) b0

(** val spop : nat -> ltN -> 'a1 array -> 'a1 array **)

let spop n k s x =
  s (ltN_intro n k x)

(** val spush : nat -> ltN -> 'a1 -> 'a1 array -> 'a1 array **)

let spush n k v s x =
  match ltN_pop n k x with
  | AA _ -> v
  | BB x0 -> s x0

(** val array_minsat : nat -> bool array -> ltN option **)

let rec array_minsat n a =
  match n with
  | O -> None
  | S n0 ->
    let k0 = ltN_of_nat O (S n0) in
    (match a k0 with
     | True -> Some k0
     | False -> opmap (ltN_S n0) (array_minsat n0 (stail n0 a)))

(** val array_maxsat : nat -> bool array -> ltN option **)

let rec array_maxsat n a =
  match n with
  | O -> None
  | S n0 ->
    (match array_maxsat n0 (stail n0 a) with
     | Some k -> Some (ltN_S n0 k)
     | None ->
       let k0 = ltN_of_nat O (S n0) in
       (match a k0 with
        | True -> Some k0
        | False -> None))

(** val cmp_array : 'a1 cmp -> nat -> 'a1 array -> 'a1 array -> ord **)

let cmp_array cA n a1 a2 =
  let ca = fun k -> cA (a1 k) (a2 k) in
  (match array_minsat n (fun k -> negb (beq_ord (ca k) Eq)) with
   | Some k -> ca k
   | None -> Eq)

(** val array_minsat_true : nat -> (ltN -> bool) -> ltN **)

let array_minsat_true n p =
  unop (array_minsat n p)

(** val cmp_array_bool : nat -> bool array -> bool array -> ord **)

let cmp_array_bool n a1 a2 =
  cmp_array cmp_bool n a1 a2

(** val axor : nat -> bool array -> bool array -> bool array **)

let axor _ a1 a2 k =
  xorb (a1 k) (a2 k)

(** val ascal : nat -> bool -> bool array -> bool array **)

let ascal _ b a k =
  match b with
  | True -> a k
  | False -> False

(** val asum : nat -> bool array -> bool **)

let asum n a =
  fold_left_ltN n xorb False a

(** val aand : nat -> bool array -> bool array -> bool array **)

let aand _ a1 a2 k =
  match a1 k with
  | True -> a2 k
  | False -> False

(** val adot : nat -> bool array -> bool array -> bool **)

let adot n a1 a2 =
  asum n (aand n a1 a2)

type 'a slist = 'a list

(** val list_of_array : nat -> 'a1 array -> 'a1 list **)

let rec list_of_array n a =
  match n with
  | O -> Nil
  | S n0 -> Cons ((a (ltN_of_nat O (S n0))), (list_of_array n0 (stail n0 a)))

(** val slist_of_array : nat -> 'a1 array -> 'a1 slist **)

let slist_of_array =
  list_of_array

(** val slist_nth : nat -> 'a1 slist -> ltN -> 'a1 **)

let slist_nth _ l k =
  match nth_error l k with
  | Some a -> a
  | None -> assert false (* absurd case *)

(** val array_of_slist : nat -> 'a1 slist -> 'a1 array **)

let array_of_slist =
  slist_nth

(** val cmp_slist : 'a1 cmp -> nat -> 'a1 slist cmp **)

let cmp_slist cmpA _ =
  cmp_sig (cmp_list cmpA)

(** val cmp_slist_bool : nat -> bool slist -> bool slist -> ord **)

let cmp_slist_bool n s1 s2 =
  cmp_slist cmp_bool n s1 s2

(** val minsat_map : ('a1 -> 'a2) -> 'a1 minsat_t -> 'a2 minsat_t **)

let minsat_map map minsatA p =
  opmap map (minsatA (comp map p))

(** val sig_map :
    ('a1 -> bool) -> ('a1 -> 'a2) -> ('a1 -> 'a2) -> 'a1 -> 'a2 **)

let sig_map pA f5 f0 a =
  match pA a with
  | True -> f5 a
  | False -> f0 a

(** val minsat_sig :
    ('a1 -> bool) -> 'a1 cmp -> (('a1 -> bool) -> 'a1 option) -> 'a1 minsat_t **)

let minsat_sig pA _ minsat p =
  let opv = minsat (sig_map pA p (fun _ -> False)) in
  (match opv with
   | Some a -> Some a
   | None -> None)

type ('a, 'b) fnary = 'a array -> 'b

(** val fnary_evalo1 : nat -> ('a1, 'a2) fnary -> 'a1 -> ('a1, 'a2) fnary **)

let fnary_evalo1 n f a s =
  f (scons n a s)

(** val fnary_evalu1 :
    nat -> ltN -> ('a1, 'a2) fnary -> 'a1 -> ('a1, 'a2) fnary **)

let fnary_evalu1 n k f v a =
  f (spush n k v a)

type bf = (bool, bool) fnary

(** val beq_bf : nat -> bf -> bf -> bool **)

let rec beq_bf n f5 f6 =
  match n with
  | O -> eqb (f5 sempty) (f6 sempty)
  | S n0 ->
    (match beq_bf n0 (fnary_evalo1 n0 f5 False) (fnary_evalo1 n0 f6 False) with
     | True -> beq_bf n0 (fnary_evalo1 n0 f5 True) (fnary_evalo1 n0 f6 True)
     | False -> False)

(** val bop_bf : (bool -> bool -> bool) -> nat -> bf -> bf -> bf **)

let bop_bf m _ f5 f6 a =
  m (f5 a) (f6 a)

(** val cst_bf : nat -> bool -> bf **)

let cst_bf _ b _ =
  b

(** val is_cst_bf : bool -> nat -> bf -> bool **)

let is_cst_bf b n f =
  beq_bf n f (cst_bf n b)

(** val minsat_bf : nat -> bf -> bool array option **)

let rec minsat_bf n f =
  match n with
  | O -> (match f sempty with
          | True -> Some sempty
          | False -> None)
  | S n0 ->
    (match minsat_bf n0 (fnary_evalo1 n0 f False) with
     | Some a -> Some (scons n0 False a)
     | None ->
       (match minsat_bf n0 (fnary_evalo1 n0 f True) with
        | Some a -> Some (scons n0 True a)
        | None -> None))

(** val dot_bf : nat -> bool -> bool array -> bf **)

let dot_bf n b a0 a1 =
  xorb b (adot n a0 a1)

type oPrim = ((bool, bool) prod, bool) prod

(** val oU : oPrim **)

let oU =
  Pair ((Pair (True, True)), False)

(** val oX : oPrim **)

let oX =
  Pair ((Pair (True, True)), True)

(** val oC : bool -> bool -> oPrim **)

let oC if0 th0 =
  Pair ((Pair ((negb if0), if0)), th0)

(** val detect_prim : oPrim -> bool -> bool -> bool **)

let detect_prim p x0 x1 =
  eqb
    (xorb (match fst (fst p) with
           | True -> x0
           | False -> False)
      (match snd (fst p) with
       | True -> x1
       | False -> False)) (snd p)

(** val eval_prim : oPrim -> bool -> bool -> bool **)

let eval_prim p x y =
  let Pair (p0, py) = p in
  let Pair (px0, px1) = p0 in
  (match xorb px0 px1 with
   | True -> (match eqb x px1 with
              | True -> py
              | False -> y)
   | False -> xorb y (match x with
                      | True -> py
                      | False -> False))

(** val rev_prim : oPrim -> bool **)

let rev_prim = function
| Pair (p0, _) ->
  let Pair (px0, px1) = p0 in
  (match xorb px0 px1 with
   | True -> negb px1
   | False -> False)

type oprim =
| OU
| OX
| OC of bool * bool

(** val oPrim_of_oprim : oprim -> oPrim **)

let oPrim_of_oprim = function
| OU -> oU
| OX -> oX
| OC (if0, th0) -> oC if0 th0

(** val pi_oprim : ltN -> oprim **)

let pi_oprim = function
| O -> OU
| S n ->
  (match n with
   | O -> OX
   | S n0 ->
     (match n0 with
      | O -> OC (False, False)
      | S n1 ->
        (match n1 with
         | O -> OC (True, False)
         | S n2 ->
           (match n2 with
            | O -> OC (False, True)
            | S _ -> OC (True, True)))))

(** val cmp_oprim : oprim -> oprim -> ord **)

let cmp_oprim x y =
  match x with
  | OU -> (match y with
           | OU -> Eq
           | _ -> Lt)
  | OX -> (match y with
           | OU -> Gt
           | OX -> Eq
           | OC (_, _) -> Lt)
  | OC (if0, th0) ->
    (match y with
     | OC (if1, th1) -> cmp_comp (cmp_bool th0 th1) (cmp_bool if0 if1)
     | _ -> Gt)

(** val minsat_oprim : oprim minsat_t **)

let minsat_oprim =
  minsat_map pi_oprim (array_minsat (S (S (S (S (S (S O)))))))

(** val intro_uP_bf : oPrim -> nat -> ltN -> bf -> bf **)

let intro_uP_bf p n k f a =
  eval_prim p (a k) (f (spop n k a))

(** val elim_uP_bf : oPrim -> nat -> ltN -> bf -> bf **)

let elim_uP_bf p n k f =
  fnary_evalu1 n k f (rev_prim p)

(** val is_uP_bf : oPrim -> nat -> ltN -> bf -> bool **)

let is_uP_bf p n k f =
  is_cst_bf True n
    (bop_bf (detect_prim p) n (fnary_evalu1 n k f False)
      (fnary_evalu1 n k f True))

(** val non_false_array : nat -> bool array -> bool **)

let non_false_array n a =
  isSome (array_minsat n a)

type vec = bool array

type pvec = (bool array, ltN) prod

(** val pvec_of_vec_min : nat -> vec -> pvec **)

let pvec_of_vec_min n v =
  Pair (v, (array_minsat_true n v))

(** val array_maxsat_true : nat -> (ltN -> bool) -> ltN **)

let array_maxsat_true n p =
  match array_maxsat n p with
  | Some k -> k
  | None -> assert false (* absurd case *)

(** val pvec_of_vec_max : nat -> vec -> pvec **)

let pvec_of_vec_max n v =
  Pair (v, (array_maxsat_true n v))

(** val lC_twist_array : nat -> pvec -> bool array -> bool array **)

let lC_twist_array n pv a =
  let v = fst pv in let k = snd pv in spush n k (adot (S n) a v) (spop n k a)

(** val lC_twist_bf : nat -> pvec -> bf -> bf **)

let lC_twist_bf n pv f a =
  f (lC_twist_array n pv a)

(** val lUX_twist_array : nat -> pvec -> bool array -> bool array **)

let lUX_twist_array n pv a =
  let v = fst pv in
  let k = snd pv in
  spush n k (a k) (axor n (spop n k a) (ascal n (a k) (spop n k v)))

(** val lUX_twist_bf : nat -> pvec -> bf -> bf **)

let lUX_twist_bf n pv f a =
  f (lUX_twist_array n pv a)

(** val lP_twist_bf : bool -> nat -> pvec -> bf -> bf **)

let lP_twist_bf ucx n pv f =
  match ucx with
  | True -> lC_twist_bf n pv f
  | False -> lUX_twist_bf n pv f

(** val ucx_of_oprim : oprim -> bool **)

let ucx_of_oprim = function
| OC (_, _) -> True
| _ -> False

(** val intro_lP_bf : oprim -> nat -> pvec -> bf -> bf **)

let intro_lP_bf p n pv f =
  lP_twist_bf (ucx_of_oprim p) n pv
    (intro_uP_bf (oPrim_of_oprim p) n (snd pv) f)

(** val elim_lP_bf : oprim -> nat -> pvec -> bf -> bf **)

let elim_lP_bf p n pv f =
  elim_uP_bf (oPrim_of_oprim p) n (snd pv)
    (lP_twist_bf (ucx_of_oprim p) n pv f)

(** val is_lP_bf : oprim -> nat -> pvec -> bf -> bool **)

let is_lP_bf p n pv f =
  is_uP_bf (oPrim_of_oprim p) n (snd pv) (lP_twist_bf (ucx_of_oprim p) n pv f)

type 'edge ldd =
| Leaf of nat * 'edge
| Edge of nat * nat * 'edge * 'edge ldd * 'edge ldd

type 'f node =
| Node of nat * 'f * 'f

type 'f next =
| Term
| Next of nat * 'f

type ('edge, 'f) xedge =
| XEdge of nat option * nat * 'edge * 'f node next

(** val xedge_projN : nat -> ('a1, 'a2) xedge -> nat option **)

let xedge_projN _ = function
| XEdge (opn, _, _, _) -> opn

(** val xedge_projE : nat -> ('a1, 'a2) xedge -> 'a1 **)

let xedge_projE _ = function
| XEdge (_, _, ee, _) -> ee

(** val xedge_projF : nat -> ('a1, 'a2) xedge -> 'a2 node next **)

let xedge_projF _ = function
| XEdge (_, _, _, nx) -> nx

(** val extract_ldd :
    (nat -> bf -> ('a1, bf) xedge) -> nat -> bf -> 'a1 ldd **)

let rec extract_ldd extract_edge0 n f =
  let XEdge (_, m0, ee, nx) = extract_edge0 n f in
  (match nx with
   | Term -> Leaf (m0, ee)
   | Next (_, f') ->
     let Node (n'', f'0, f'1) = f' in
     let ldd0 = extract_ldd extract_edge0 n'' f'0 in
     let ldd5 = extract_ldd extract_edge0 n'' f'1 in
     Edge (n'', m0, ee, ldd0, ldd5))

type 'edge lDD = 'edge ldd

(** val extract :
    (nat -> bf -> ('a1, bf) xedge) -> (nat option -> nat -> 'a1 -> bf next ->
    bf) -> nat -> bf -> 'a1 lDD **)

let extract extract_edge0 _ n f =
  extract_ldd extract_edge0 n f

type ('term, 'prim, 'f) xprim =
| XTerm of nat * 'term
| XPrim of nat * 'prim * 'f
| XSha of nat * 'f * 'f

type ('term, 'prim) trans =
| ETerm of nat * 'term
| EPrim of nat option * nat * 'prim * ('term, 'prim) trans
| ENext of nat

(** val sem_trans :
    (nat -> 'a1 -> bf) -> (nat -> 'a2 -> bf -> bf) -> nat option -> nat ->
    ('a1, 'a2) trans -> bf next -> bf **)

let rec sem_trans sem_term0 sem_prim0 _ _ e f0 =
  match e with
  | ETerm (n, t) -> sem_term0 n t
  | EPrim (o, n, p, e0) ->
    sem_prim0 n p (sem_trans sem_term0 sem_prim0 o n e0 f0)
  | ENext n ->
    (match f0 with
     | Term -> assert false (* absurd case *)
     | Next (n0, f) -> solution_left (S n) (fun f5 -> f5) n0 f)

type ('term, 'prim, 'f) xtrans = (('term, 'prim) trans, 'f) xedge

(** val xTrans :
    nat option -> nat -> ('a1, 'a2) trans -> 'a3 node next -> (('a1, 'a2)
    trans, 'a3) xedge **)

let xTrans opn m ee nx =
  XEdge (opn, m, ee, nx)

(** val xTPrim :
    nat -> 'a2 -> ('a1, 'a2, 'a3) xtrans -> ('a1, 'a2, 'a3) xtrans **)

let xTPrim n p xe =
  xTrans (xedge_projN n xe) (S n) (EPrim ((xedge_projN n xe), n, p,
    (xedge_projE n xe))) (xedge_projF n xe)

(** val extract_trans :
    (nat -> bf -> ('a1, 'a2, bf) xprim) -> nat -> bf -> ('a1, 'a2, bf) xtrans **)

let rec extract_trans extract_xprim0 n f =
  match extract_xprim0 n f with
  | XTerm (n0, t) -> xTrans None n0 (ETerm (n0, t)) Term
  | XPrim (n0, p, xf) -> xTPrim n0 p (extract_trans extract_xprim0 n0 xf)
  | XSha (n0, f0, f5) ->
    xTrans (Some (S n0)) (S n0) (ENext n0) (Next ((S n0), (Node (n0, f0,
      f5))))

type ('term, 'prim) edge = ('term, 'prim) trans

type ('term, 'prim, 'f) xedge0 = (('term, 'prim) edge, 'f) xedge

(** val xEdge :
    (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> (nat -> 'a2 -> bf ->
    bf) -> (nat -> 'a2 -> bf -> bf option) -> (nat -> 'a2 -> 'a2 -> ord
    option) -> (nat -> bf -> bool) -> nat option -> nat -> ('a1, 'a2) edge ->
    'a3 node next -> (('a1, 'a2) edge, 'a3) xedge **)

let xEdge _ _ _ _ _ _ opn m ee nx =
  XEdge (opn, m, ee, nx)

(** val extract_edge :
    (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> (bf -> 'a1) -> (nat ->
    'a2 -> bf -> bf) -> (nat -> 'a2 -> bf -> bf option) -> (nat -> 'a2 -> 'a2
    -> ord option) -> (nat -> bf -> bool) -> (nat -> bf -> ('a1, 'a2, bf)
    xprim) -> nat -> bf -> ('a1, 'a2, bf) xedge0 **)

let extract_edge sem_term0 extract_term0 _ sem_prim0 extract_prim0 pcmp_prim0 is_prim_free0 extract_xprim0 n f =
  let xt = extract_trans extract_xprim0 n f in
  xEdge sem_term0 extract_term0 sem_prim0 extract_prim0 pcmp_prim0
    is_prim_free0 (xedge_projN n xt) n (xedge_projE n xt) (xedge_projF n xt)

(** val sem_edge :
    (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> (nat -> 'a2 -> bf ->
    bf) -> (nat -> 'a2 -> bf -> bf option) -> (nat -> 'a2 -> 'a2 -> ord
    option) -> (nat -> bf -> bool) -> nat option -> nat -> ('a1, 'a2) edge ->
    bf next -> bf **)

let sem_edge sem_term0 _ sem_prim0 _ _ _ opn m e nx =
  sem_trans sem_term0 sem_prim0 opn m e nx

type ('term, 'prim) lDD0 = ('term, 'prim) edge lDD

(** val extract0 :
    (nat -> 'a1 -> bf) -> (nat -> bf -> 'a1 option) -> (bf -> 'a1) -> (nat ->
    'a2 -> bf -> bf) -> (nat -> 'a2 -> bf -> bf option) -> (nat -> 'a2 -> 'a2
    -> ord option) -> (nat -> bf -> bool) -> (nat -> bf -> ('a1, 'a2, bf)
    xprim) -> nat -> bf -> ('a1, 'a2) lDD0 **)

let extract0 sem_term0 extract_term0 term_of_bool0 sem_prim0 extract_prim0 pcmp_prim0 is_prim_free0 extract_xprim0 =
  extract
    (extract_edge sem_term0 extract_term0 term_of_bool0 sem_prim0
      extract_prim0 pcmp_prim0 is_prim_free0 extract_xprim0)
    (sem_edge sem_term0 extract_term0 sem_prim0 extract_prim0 pcmp_prim0
      is_prim_free0)

type term = (bool, bool slist) prod

(** val sem_term : nat -> term -> bf **)

let sem_term n t =
  dot_bf n (fst t) (array_of_slist n (snd t))

(** val extract_term_bool : nat -> bool -> bf -> bool array option **)

let extract_term_bool n b f =
  minsat_bf n (fun a -> beq_bf n (dot_bf n b a) f)

(** val extract_term : nat -> bf -> term option **)

let extract_term n f =
  let f0 = f (fun _ -> False) in
  (match extract_term_bool n f0 f with
   | Some a -> Some (Pair (f0, (slist_of_array n a)))
   | None -> None)

(** val term_of_bool : bf -> term **)

let term_of_bool f =
  Pair ((f (fun _ -> False)), (slist_of_array O (fun _ -> False)))

type svec = bool slist

(** val cmp_svec : nat -> svec -> svec -> ord **)

let cmp_svec n v1 v2 =
  cmp_sig (cmp_slist_bool n) v1 v2

type prim = (oprim, svec) prod

(** val vec_of_svec : nat -> svec -> vec **)

let vec_of_svec =
  array_of_slist

(** val svec_of_vec : nat -> vec -> svec **)

let svec_of_vec =
  slist_of_array

(** val sem_svec : oprim -> nat -> svec -> pvec **)

let sem_svec op n s =
  match op with
  | OC (_, _) -> pvec_of_vec_max n (vec_of_svec n s)
  | _ -> pvec_of_vec_min n (vec_of_svec n s)

(** val sem_prim : nat -> prim -> bf -> bf **)

let sem_prim n p f =
  intro_lP_bf (fst p) n (sem_svec (fst p) (S n) (snd p)) f

(** val detect_prim0 : nat -> prim -> bf -> bool **)

let detect_prim0 n p f =
  is_lP_bf (fst p) n (sem_svec (fst p) (S n) (snd p)) f

(** val elim_prim : nat -> prim -> bf -> bf **)

let elim_prim n p f =
  elim_lP_bf (fst p) n (sem_svec (fst p) (S n) (snd p)) f

(** val extract_prim : nat -> prim -> bf -> bf option **)

let extract_prim n p f =
  match detect_prim0 n p f with
  | True -> Some (elim_prim n p f)
  | False -> None

(** val cmp_prim : nat -> prim -> prim -> ord **)

let cmp_prim n =
  cmp_prod cmp_oprim (cmp_svec n)

(** val pcmp_of_cmp : 'a1 cmp -> 'a1 -> 'a1 -> ord option **)

let pcmp_of_cmp cA x y =
  Some (cA x y)

(** val pcmp_prim : nat -> prim -> prim -> ord option **)

let pcmp_prim n x y =
  pcmp_of_cmp (cmp_prim n) x y

(** val minsat_bf_vec : nat -> vec minsat_t **)

let minsat_bf_vec n =
  minsat_sig (non_false_array n) (cmp_array_bool n) (minsat_bf n)

(** val minsat_bf_svec : nat -> svec minsat_t **)

let minsat_bf_svec n =
  minsat_map (svec_of_vec n) (minsat_bf_vec n)

(** val minsat_prim : nat -> (prim -> bool) -> prim option **)

let minsat_prim n p =
  minsat_prod minsat_oprim (minsat_bf_svec n) p

(** val minsat_prim_uP_bf : nat -> bf -> prim option **)

let minsat_prim_uP_bf n f =
  minsat_prim (S n) (fun p -> detect_prim0 n p f)

(** val is_prim_free : nat -> bf -> bool **)

let is_prim_free n f =
  match n with
  | O -> True
  | S n' -> isNone (minsat_prim_uP_bf n' f)

type 'f xprim0 = (term, prim, 'f) xprim

(** val extract_xprim : nat -> bf -> bf xprim0 **)

let extract_xprim n f =
  match n with
  | O -> XTerm (O, (term_of_bool f))
  | S n' ->
    (match extract_term (S n') f with
     | Some t -> XTerm ((S n'), t)
     | None ->
       (match minsat_prim_uP_bf n' f with
        | Some p -> XPrim (n', p, (elim_prim n' p f))
        | None ->
          XSha (n', (fnary_evalo1 n' f False), (fnary_evalo1 n' f True))))

type lDD1 = (term, prim) lDD0

(** val extract1 : nat -> bf -> lDD1 **)

let extract1 =
  extract0 sem_term extract_term term_of_bool sem_prim extract_prim pcmp_prim
    is_prim_free extract_xprim

(** val f1 : bf **)

let f1 x =
  let x0 = x (ltN_of_nat O (S (S (S (S O))))) in
  let x1 = x (ltN_of_nat (S O) (S (S (S (S O))))) in
  let x2 = x (ltN_of_nat (S (S O)) (S (S (S (S O))))) in
  let x3 = x (ltN_of_nat (S (S (S O))) (S (S (S (S O))))) in
  xorb x0 (xorb x1 (xorb x2 x3))

(** val ldd1 : lDD1 **)

let ldd1 =
  extract1 (S (S (S (S O)))) f1

(** val f2 : bf **)

let f2 x =
  let x0 = x (ltN_of_nat O (S (S (S (S (S (S (S (S (S (S O))))))))))) in
  let x1 = x (ltN_of_nat (S O) (S (S (S (S (S (S (S (S (S (S O))))))))))) in
  let x2 = x (ltN_of_nat (S (S O)) (S (S (S (S (S (S (S (S (S (S O)))))))))))
  in
  let x3 =
    x (ltN_of_nat (S (S (S O))) (S (S (S (S (S (S (S (S (S (S O)))))))))))
  in
  let x4 =
    x (ltN_of_nat (S (S (S (S O)))) (S (S (S (S (S (S (S (S (S (S O)))))))))))
  in
  let x5 =
    x
      (ltN_of_nat (S (S (S (S (S O))))) (S (S (S (S (S (S (S (S (S (S
        O)))))))))))
  in
  let x6 =
    x
      (ltN_of_nat (S (S (S (S (S (S O)))))) (S (S (S (S (S (S (S (S (S (S
        O)))))))))))
  in
  let x7 =
    x
      (ltN_of_nat (S (S (S (S (S (S (S O))))))) (S (S (S (S (S (S (S (S (S (S
        O)))))))))))
  in
  let x8 =
    x
      (ltN_of_nat (S (S (S (S (S (S (S (S O)))))))) (S (S (S (S (S (S (S (S
        (S (S O)))))))))))
  in
  let x9 =
    x
      (ltN_of_nat (S (S (S (S (S (S (S (S (S O))))))))) (S (S (S (S (S (S (S
        (S (S (S O)))))))))))
  in
  xorb x0
    (xorb x1
      (xorb x2 (xorb x3 (xorb x4 (xorb x5 (xorb x6 (xorb x7 (xorb x8 x9))))))))

(** val ldd2 : lDD1 **)

let ldd2 =
  extract1 (S (S (S (S (S (S (S (S (S (S O)))))))))) f2

(** val f3 : bf **)

let f3 x =
  let x1 = x (ltN_of_nat (S O) (S (S (S (S (S (S (S (S (S (S O))))))))))) in
  let x2 = x (ltN_of_nat (S (S O)) (S (S (S (S (S (S (S (S (S (S O)))))))))))
  in
  let x3 =
    x (ltN_of_nat (S (S (S O))) (S (S (S (S (S (S (S (S (S (S O)))))))))))
  in
  let x4 =
    x (ltN_of_nat (S (S (S (S O)))) (S (S (S (S (S (S (S (S (S (S O)))))))))))
  in
  (match negb x1 with
   | True -> xorb x3 (match x2 with
                      | True -> x4
                      | False -> False)
   | False -> False)

(** val ldd3 : lDD1 **)

let ldd3 =
  extract1 (S (S (S (S (S (S (S (S (S (S O)))))))))) f3

(** val f4 : bf **)

let f4 x =
  let x0 = x (ltN_of_nat O (S (S (S (S (S (S (S (S (S (S O))))))))))) in
  let x1 = x (ltN_of_nat (S O) (S (S (S (S (S (S (S (S (S (S O))))))))))) in
  let x2 = x (ltN_of_nat (S (S O)) (S (S (S (S (S (S (S (S (S (S O)))))))))))
  in
  let x3 =
    x (ltN_of_nat (S (S (S O))) (S (S (S (S (S (S (S (S (S (S O)))))))))))
  in
  let x4 =
    x (ltN_of_nat (S (S (S (S O)))) (S (S (S (S (S (S (S (S (S (S O)))))))))))
  in
  let x5 =
    x
      (ltN_of_nat (S (S (S (S (S O))))) (S (S (S (S (S (S (S (S (S (S
        O)))))))))))
  in
  let x6 =
    x
      (ltN_of_nat (S (S (S (S (S (S O)))))) (S (S (S (S (S (S (S (S (S (S
        O)))))))))))
  in
  let x7 =
    x
      (ltN_of_nat (S (S (S (S (S (S (S O))))))) (S (S (S (S (S (S (S (S (S (S
        O)))))))))))
  in
  let x8 =
    x
      (ltN_of_nat (S (S (S (S (S (S (S (S O)))))))) (S (S (S (S (S (S (S (S
        (S (S O)))))))))))
  in
  let x9 =
    x
      (ltN_of_nat (S (S (S (S (S (S (S (S (S O))))))))) (S (S (S (S (S (S (S
        (S (S (S O)))))))))))
  in
  (match match match xorb x0 (xorb x3 x6) with
               | True -> xorb x1 (xorb x4 x7)
               | False -> False with
         | True -> xorb x2 (xorb x5 x8)
         | False -> False with
   | True -> negb x9
   | False -> False)

(** val ldd4 : lDD1 **)

let ldd4 =
  extract1 (S (S (S (S (S (S (S (S (S (S O)))))))))) f4
