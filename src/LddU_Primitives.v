Require Import Arith .
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.

Require Import Ground.SimplProp.
Require Import Ground.SimplBool.
Require Import Ground.SimplOption.
Require Import Ground.SimplEq.
Require Import Ground.SimplFun.
Require Import Ground.SimplBoolFun.
Require Import Ground.SimplCmp.
Require Import Ground.SimplSimpl.

Require Import Ground.SimplNat.
Require Import Ground.SimplAB.

Require Import Ground.SimplList_Chap0_Standard.
Require Import Ground.SimplList_Chap1_Construct.
Require Import Ground.SimplList_Chap2_Predicate.
Require Import Ground.SimplList_Chap3_PredicateOnConstruct.
Require Import Ground.SimplList.
Require Import Ground.SimplABList.
Require Import Ground.SimplMore.
Require Import Ground.SimplDTT.
Require Import Ground.SimplLtN_Chap0_Construct.
Require Import Ground.SimplLtN_Chap1_Array_as_Function.
Require Import Ground.SimplLtN_Chap1_Sec1_Boolean_Array.
Require Import Ground.SimplLtNIndexedArray.

Require Import Ground.SimplDTT.
Require Import Ground.SimplLtN.
Require Import Ground.SimplEnum.
Require Import Ground.SimplEnumCmp.

Require Import Ground.MiniBool_VAX.
Require Import Ground.MiniBool_Logic.

Require Import FiniteCurryfy.
Require Import FiniteBooleanFunction.

Require Import LddO_Primitives.

Definition uPrim (n:nat) : Type := oPrim * ltN n.

Definition intro_uP_bf (p:oPrim) {n} (k:ltN (S n)) (f:bf n) : bf (S n) :=
  (fun a => eval_prim p (a k) (f(spop k a))).

Definition elim_uP_bf (p:oPrim) {n} (k:ltN(S n)) (f:bf(S n)) : bf n := fnary_evalu1 k f (rev_prim p).

Definition is_uP_bf (p:oPrim) {n} (k:ltN(S n)) (f:bf(S n)) : bool :=
  is_cst_bf true (bop_bf (detect_prim p) (fnary_evalu1 k f false) (fnary_evalu1 k f true)).

Lemma fnary_evalu1_intro_uP_bf_same p b {n} k f :
  fnary_evalu1 k (@intro_uP_bf p n k f) b = (fun x => eval_prim p b (f x)).
Proof with curry1.
unfold fnary_evalu1, intro_uP_bf...
apply functional_extensionality...
Qed.

Lemma fnary_evalu1_intro_uP_bf_rev_prim_same {n} p k f : fnary_evalu1 k (@intro_uP_bf p n k f) (rev_prim p) = f.
Proof with curry1.
unfold fnary_evalu1, intro_uP_bf...
apply functional_extensionality...
Qed.
Hint Rewrite @fnary_evalu1_intro_uP_bf_rev_prim_same : curry1_rewrite.

Lemma elim_uP_bf_intro_uP_bf_same p {n} (k:ltN(S n)) (f:bf n) :
  elim_uP_bf p k (intro_uP_bf p k f) = f.
Proof with curry1.
unfold elim_uP_bf.
rewrite fnary_evalu1_intro_uP_bf_same...
Qed.
Hint Rewrite @elim_uP_bf_intro_uP_bf_same : curry1_rewrite.

Lemma ltN_pop_ltN_intro_l_same_l  {n} (k1':ltN n) k2 :
  ltN_pop (ltN_intro k2 k1') k2 = BB(ltN_comm_intro k1' k2).
Proof. destruct n; curry1. Qed.
Hint Rewrite @ltN_pop_ltN_intro_l_same_l : curry1_rewrite.

Lemma spush_ltN_intro_k_eval_k {A n} k1 k2 a0 (a:array A n) :
  spush (ltN_intro k1 k2) a0 a k1 = a (ltN_comm_intro k2 k1).
Proof. unfold spush; curry1. Qed.
Hint Rewrite @spush_ltN_intro_k_eval_k : curry1_rewrite.

Lemma elim_uP_bf_intro_uP_bf p n (f:bf(S n)) (k1 k2:ltN(S (S n))) :
  elim_uP_bf p k1 (intro_uP_bf p k2 f) =
  match ltN_pop k2 k1 with
  | AA _ => f
  | BB k1' =>
    let k2' := ltN_comm_intro k1' k2 in
    intro_uP_bf p k2' (elim_uP_bf p k1' f)
end.
Proof with curry1.
unfold elim_uP_bf, intro_uP_bf, fnary_evalu1...
apply functional_extensionality...
rewrite spop_spush.
destruct(ltN_pop k2 k1)as [|k1']eqn:E0...
Qed.

Lemma intro_uP_bf_elim_uP_bf p n (f:bf(S n)) (k1 k2:ltN(S n)) :
  intro_uP_bf p k2 (elim_uP_bf p k1 f) =
    elim_uP_bf p (ltN_rev_intro k2 k1) (intro_uP_bf p (ltN_rev_elim k1 k2) f).
Proof with curry1.
simpl.
unfold intro_uP_bf, elim_uP_bf, fnary_evalu1...
apply functional_extensionality...
Qed.

Lemma intro_uP_bf_eq_as_elim_uP_bf_and_elim_intro p n (f:bf n) g (k:ltN(S n)) :
  intro_uP_bf p k f = g <-> f = elim_uP_bf p k g /\ intro_uP_bf p k (elim_uP_bf p k g) = g.
Proof. split; curry1. Qed.

Lemma comm_prim p x1 x2 y :
  eval_prim p x1 (eval_prim p x2 y) = eval_prim p x2 (eval_prim p x1 y).
Proof with curry1.
destruct (eqb x1 (rev_prim p))eqn:E0...
destruct (eqb x2 (rev_prim p))eqn:E0...
Qed.

Lemma spush_kC_eval_ltN_intro_kC_kc {A n} kC vC kc (ac:array A n): spush kC vC ac (ltN_intro kC kc) = ac kc.
Proof. unfold spush; curry1. Qed.
Hint Rewrite @spush_kC_eval_ltN_intro_kC_kc : curry1_rewrite.

Lemma spop_spop_comm {A n} kc kC (x:array A (S(S n))) :
  spop (ltN_comm_intro kc kC) (spop (ltN_intro kC kc) x) = spop kc (spop kC x).
Proof with curry1.
unfold spop...
apply functional_extensionality...
rewrite <- ltN_intro_ltN_intro_comm...
Qed.
Hint Rewrite @spop_spop_comm : curry1_rewrite.

Lemma spop_ltN_intro_eval_eval_ltN_comm_intro {A n} kC kc (x:array A(S(S n))) :
  spop (ltN_intro kC kc) x (ltN_comm_intro kc kC) = x kC.
Proof with curry1.
unfold spop.
apply f_equal.
rewrite (ltN_intro_ltN_intro_ltN_comm_intro kc kC).
reflexivity.
Qed.
Hint Rewrite @spop_ltN_intro_eval_eval_ltN_comm_intro : curry1_rewrite.

Lemma comm_intro_uP_bf_same p n (f:bf n) k1 k2 :
  intro_uP_bf p k2 (intro_uP_bf p k1 f) =
    intro_uP_bf p (ltN_intro k2 k1) (intro_uP_bf p (ltN_comm_intro k1 k2) f).
Proof with curry1.
unfold intro_uP_bf...
apply functional_extensionality...
rewrite comm_prim...
Qed.

Lemma is_uP_bf_intro_uP_bf_same p n k f : is_uP_bf p k (@intro_uP_bf p n k f) = true.
Proof with curry1.
unfold is_uP_bf, intro_uP_bf...
apply functional_extensionality...
destruct p...
unfold pPrim in e.
unfold bop_bf, cst_bf, detect_prim, eval_prim, fnary_evalu1...
generalize dependent e.
BPS.MicroBPSolver...
Qed.
Hint Rewrite is_uP_bf_intro_uP_bf_same : curry1_rewrite.

Lemma intro_uP_bf_elim_uP_bf_same_eq_same p {n} k (f:bf(S n)) :
  intro_uP_bf p k (elim_uP_bf p k f) = f <-> is_uP_bf p k f = true.
Proof with curry1.
unfold intro_uP_bf, elim_uP_bf, is_uP_bf...
destruct p...
unfold pPrim in e...
unfold fnary_evalu1...
unfold eval_prim, rev_prim, detect_prim...
unfold bop_bf.
split; curry1; apply functional_extensionality...
- rewrite <- H...
  generalize dependent e.
  BPS.MicroBPSolver...
- specialize(equal_f H (spop k x))... clear H.
  repeat rewrite (eval_bf_S_n_u k f)...
  generalize dependent e.
  BPS.MicroBPSolver...
Qed.
Hint Rewrite intro_uP_bf_elim_uP_bf_same_eq_same : curry1_rewrite.

Lemma intro_uP_bf_as_is_uP_bf (p:oPrim) {n} k f g :
  @intro_uP_bf p n k f = g <-> is_uP_bf p k g = true /\ f = elim_uP_bf p k g.
Proof. split; curry1. Qed.

(* Annexe (En Vrac) *)

Definition extract_cst {n} (f:bf n) : option bool :=
let f0 := f(fun _ => false) in
if is_cst_bf f0 f then Some f0 else None.

Definition extract_piN_bool {n} (b:bool) (f:bf n) : option(ltN n) :=
  array_minsat (fun k => beq_bf (piN n k b) f).

Definition extract_piN {n} (f:bf n) : option(ltN n * bool) :=
  let f0 := f(fun _ => false) in
  match extract_piN_bool f0 f with
  | Some k => Some(k, f0)
  | None => None
  end.

Lemma extract_cst_eq_Some {n} (f:bf n) b :
  extract_cst f = Some b <-> f = cst_bf n b.
Proof with curry1.
unfold extract_cst.
dest_match_step...
rewrite D...
Qed.
Hint Rewrite @extract_cst_eq_Some : curry1_rewrite.

Lemma piN_array_dirac n k1 b1 k2 b2 :
  piN n k1 b1 (array_dirac k2 b2 (negb b2)) = xorb (beq_ltN k1 k2) (eqb b1 b2).
Proof. unfold piN, array_dirac; dest_match_step; curry1. Qed.
Hint Rewrite piN_array_dirac : curry1_rewrite.

Lemma fnary_evalu1_piN {n} k1 b1 k2 b2 :
  fnary_evalu1 k1 (piN (S n) k2 b2) b1 =
    match ltN_pop k1 k2 with
    | AA _ => cst_bf _ (xorb b1 b2)
    | BB k2' => piN n k2' b2
    end.
Proof with curry1.
unfold fnary_evalu1, piN...
apply functional_extensionality...
unfold spush...
dest_match_step...
Qed.
Hint Rewrite @fnary_evalu1_piN : curry1_rewrite.

Lemma fnary_evalu1_cst_bf n b1 k2 b2 : fnary_evalu1 k2 (cst_bf (S n) b1) b2 = cst_bf n b1.
Proof. reflexivity. Qed.
Hint Rewrite fnary_evalu1_cst_bf : curry1_rewrite.

Lemma cst_bf_eq_piN n b1 k2 b2 : cst_bf n b1 = piN n k2 b2 <-> False.
Proof with curry1.
simpl...
destruct n...
specialize (f_equal (fun f => @fnary_evalu1 bool bool n k2 f (negb(xorb b2 b1))) H)...
Qed.
Hint Rewrite cst_bf_eq_piN : curry1_rewrite.

Lemma piN_eq_piN n k1 b1 k2 b2 : piN n k1 b1 = piN n k2 b2 <-> k1 = k2 /\ b1 = b2.
Proof with curry1.
destruct n...
split...
specialize (f_equal (fun f => @fnary_evalu1 bool bool n k1 f b1) H)...
dest_match_step...
Qed.
Hint Rewrite piN_eq_piN : curry1_rewrite.

Lemma extract_piN_bool_eq_Some n b f k :
  extract_piN_bool b f = Some k <-> f = piN n k b.
Proof with curry1.
unfold extract_piN_bool...
rewrite array_minsat_eq_Some_iff_is_min_positive...
unfold is_min_positive...
rewrite (beq_bf_iff_true _ _)...
split...
rewrite (beq_bf_iff_true _ _) in H...
Qed.
Hint Rewrite extract_piN_bool_eq_Some : curry1_rewrite.

Lemma beq_bf_refl {n} (f:bf n) : beq_bf f f = true.
Proof. rewrite (beq_bf_iff_true _ _); reflexivity. Qed.
Hint Rewrite @beq_bf_refl : curry1_rewrite.

Lemma extract_piN_bool_piN b1 n k2 b2 :
  extract_piN_bool b1 (piN n k2 b2) = if eqb b1 b2 then Some k2 else None.
Proof with curry1.
unfold extract_piN_bool...
dest_match_step...
- rewrite array_minsat_eq_Some_iff_is_min_positive.
  unfold is_min_positive...
  rewrite (beq_bf_iff_true _ _) in H...
- apply functional_extensionality...
  apply reverse_bool_eq...
  rewrite (beq_bf_iff_true _ _) in H...
Qed.
Hint Rewrite extract_piN_bool_piN : curry1_rewrite.

Lemma piN_cst n k b1 b2 : piN n k b1 (fun _ => b2) = xorb b1 b2.
Proof. reflexivity. Qed.
Hint Rewrite piN_cst : curry1_rewrite.

Lemma extract_piN_eq_Some n f p :
  @extract_piN n f = Some p <-> f = piN n (fst p) (snd p).
Proof with curry1.
destruct p as [k b]...
unfold extract_piN...
unfold opmap.
dest_match_step...
rewrite D...
Qed.
Hint Rewrite extract_piN_eq_Some : curry1_rewrite.

Lemma piN_eq_cst_bf n k1 b1 b2 : piN n k1 b1 = cst_bf n b2 <-> False.
Proof. curry1; symmetry in H; curry1. Qed.
Hint Rewrite piN_eq_cst_bf : curry1_rewrite.

Lemma extract_cst_cst_bf n b :
  extract_cst (cst_bf n b) = Some b.
Proof. unfold extract_cst; curry1. Qed.
Hint Rewrite extract_cst_cst_bf : curry1_rewrite.

Lemma is_cst_bf_piN {n} k b : is_cst_bf b (piN n k b) = false.
Proof. apply reverse_bool_eq; curry1. Qed.
Hint Rewrite @is_cst_bf_piN : curry1_rewrite.

Lemma extract_cst_piN {n} k b : extract_cst (piN n k b) = None.
Proof. unfold extract_cst; curry1. Qed.
Hint Rewrite @extract_cst_piN : curry1_rewrite.

Lemma extract_piN_piN {n} k b : extract_piN (piN n k b) = Some(k, b).
Proof. curry1. Qed.
Hint Rewrite @extract_piN_piN : curry1_rewrite.

Lemma fnary_evalu1_intro_uC_bf if0 th0 {n} (k:ltN(S n)) b f :
  fnary_evalu1 k (intro_uP_bf (oC if0 th0) k f) b = if eqb b if0 then (fun _ => th0) else f.
Proof with curry1.
unfold fnary_evalu1, intro_uP_bf...
apply functional_extensionality...
dest_match_step...
Qed.
Hint Rewrite @fnary_evalu1_intro_uC_bf : curry1_rewrite.

Lemma fnary_evalu1_intro_uU_bf {n} (k:ltN(S n)) b f :
  fnary_evalu1 k (intro_uP_bf oU k f) b = f.
Proof with curry1.
unfold fnary_evalu1, intro_uP_bf...
apply functional_extensionality...
Qed.
Hint Rewrite @fnary_evalu1_intro_uU_bf : curry1_rewrite.

Lemma fnary_evalu1_intro_uX_bf {n} (k:ltN(S n)) b f :
  fnary_evalu1 k (intro_uP_bf oX k f) b = xorb_bf b f.
Proof with curry1.
unfold fnary_evalu1, intro_uP_bf...
apply functional_extensionality...
Qed.
Hint Rewrite @fnary_evalu1_intro_uX_bf : curry1_rewrite.

Lemma intro_uC_bf_cst_bf if0 th0 n k cst0 :
  intro_uP_bf (oC if0 th0) k (cst_bf n cst0) =
    if eqb cst0 th0 then (cst_bf _ cst0) else (piN _ k (xorb if0 th0)).
Proof with curry1.
rewrite (shannon_expansion_u_gen k if0)...
dest_match_step...
Qed.
Hint Rewrite intro_uC_bf_cst_bf : curry1_rewrite.

Lemma intro_uU_bf_cst_bf n k b : intro_uP_bf oU k (cst_bf n b) = cst_bf _ b.
Proof. rewrite (shannon_expansion_u k); curry1. Qed.
Hint Rewrite intro_uU_bf_cst_bf : curry1_rewrite.

Lemma intro_uX_bf_cst_bf n k b : intro_uP_bf oX k (cst_bf n b) = piN (S n) k b.
Proof. rewrite (shannon_expansion_u k); curry1. Qed.
Hint Rewrite intro_uX_bf_cst_bf : curry1_rewrite.

Lemma is_cst_bf_intro_uU_bf {n} k f b : is_cst_bf b (@intro_uP_bf oU n k f) = is_cst_bf b f.
Proof with curry1.
rewrite eq_iff_eq_true...
rewrite (shannon_expansion_u k)...
Qed.
Hint Rewrite @is_cst_bf_intro_uU_bf : curry1_rewrite.

Lemma is_cst_bf_intro_uX_bf {n} k f b : is_cst_bf b (@intro_uP_bf oX n k f) = false.
Proof with curry1.
rewrite eq_iff_eq_true...
rewrite (shannon_expansion_u k) in H...
Qed.
Hint Rewrite @is_cst_bf_intro_uX_bf : curry1_rewrite.

Lemma is_cst_bf_intro_uC_bf if0 th0 {n} k f b :
  is_cst_bf b (@intro_uP_bf (oC if0 th0) n k f) = eqb th0 b && is_cst_bf b f.
Proof with curry1.
rewrite eq_iff_eq_true...
rewrite (shannon_expansion_u_gen k if0)...
rewrite andb_true_iff...
Qed.
Hint Rewrite @is_cst_bf_intro_uC_bf : curry1_rewrite.

Lemma intro_uP_bf_fun_cst {n} p k f b : @intro_uP_bf p n k f (fun _ => b) = eval_prim p b (f(fun _ => b)).
Proof. reflexivity. Qed.
Hint Rewrite @intro_uP_bf_fun_cst : curry1_rewrite.

Lemma is_uU_bf_cst_bf n k b : @is_uP_bf oU n k (cst_bf _ b) = true.
Proof with curry1.
unfold is_uP_bf, cst_bf.
unfold LddO_Primitives.detect_prim...
unfold bop_bf, cst_bf...
Qed.
Hint Rewrite is_uU_bf_cst_bf : curry1_rewrite.

Lemma fnary_evalu1_xorb_bf {n} k2 b1 (f:bf(S n)) b2 :
  fnary_evalu1 k2 (xorb_bf b1 f) b2 = xorb_bf b1 (fnary_evalu1 k2 f b2).
Proof. reflexivity. Qed.
Hint Rewrite @fnary_evalu1_xorb_bf : curry1_rewrite.

Lemma fold_piN_false n k : (fun x : array bool n => x k) = piN n k false.
Proof. unfold piN; curry1. Qed.
Hint Rewrite @fold_piN_false : curry1_rewrite.

Lemma is_uU_bf_piN n k1 k2 b2 : @is_uP_bf oU n k1 (piN _ k2 b2) = (negb(proj1_sig k1 =? proj1_sig k2)).
Proof with curry1.
unfold is_uP_bf...
dest_match_step...
dest_match_step...
lia.
Qed.
Hint Rewrite is_uU_bf_piN : curry1_rewrite.

Lemma is_uX_bf_piN n k1 k2 b2 : @is_uP_bf oX n k1 (piN _ k2 b2) = (proj1_sig k1 =? proj1_sig k2).
Proof with curry1.
unfold is_uP_bf...
dest_match_step...
apply reverse_bool_eq...
dest_match_step...
lia.
Qed.
Hint Rewrite is_uX_bf_piN : curry1_rewrite.

Lemma is_uX_bf_cst_bf n k b : @is_uP_bf oX n k (cst_bf _ b) = false.
Proof. unfold is_uP_bf; curry1. Qed.
Hint Rewrite is_uX_bf_cst_bf : curry1_rewrite.

Lemma is_uC_bf_cst_bf if0 th0 n k b : @is_uP_bf (oC if0 th0) n k (cst_bf _ b) = eqb th0 b.
Proof with curry1.
unfold is_uP_bf...
rewrite eq_iff_eq_true...
unfold bop_bf...
split...
- specialize(f_equal (fun f => f(fun _ => false)) H) as H0...
- apply functional_extensionality...
Qed.
Hint Rewrite is_uC_bf_cst_bf : curry1_rewrite.

(* [MOVEME] SimplLtN *)
Lemma proj1_sig_LtN_eq_proj1_sig_ltN_same {n} (x y:ltN n) : proj1_sig x = proj1_sig y <-> x = y.
Proof. destruct x, y; curry1. Qed.
Hint Rewrite @proj1_sig_LtN_eq_proj1_sig_ltN_same : curry1_rewrite.

Lemma is_uC_bf_pi0 if0 th0 n k1 k2 b2 :
  @is_uP_bf (oC if0 th0) n k1 (piN _ k2 b2) = (proj1_sig k1 =? proj1_sig k2) && eqb th0 (xorb if0 b2).
Proof with curry1.
unfold is_uP_bf...
rewrite eq_iff_eq_true...
unfold bop_bf...
split...
- specialize(f_equal (fun f => f(fun _ => false)) H) as H0...
  specialize(f_equal (fun f => f(fun _ => true)) H) as H1...
  destruct(ltN_pop k1 k2)eqn:E0...
- apply functional_extensionality...
Qed.
Hint Rewrite is_uC_bf_pi0 : curry1_rewrite.

Lemma functional_extensionality_iff {A B:Type} (f g:A -> B) :
  f = g <-> forall a, f a = g a.
Proof with simplprop.
split...
apply functional_extensionality...
Qed.

Lemma is_uP_bf_iff_fnary_evalu1 if0 th0 {n} k (f:bf(S n)) :
  is_uP_bf (oC if0 th0) k f = true <-> fnary_evalu1 k f if0 = cst_bf _ th0.
Proof with curry1.
unfold is_uP_bf, bop_bf, fnary_evalu1...
repeat rewrite (@functional_extensionality_iff (array bool n) bool)...
split; intros H x; specialize(H x); destruct if0...
Qed.

Lemma fnary_evalu1_same {A B n} k f b : @fnary_evalu1 A B n k f b (fun _ => b) = f (fun _ => b).
Proof. unfold fnary_evalu1; curry1. Qed.
Hint Rewrite @fnary_evalu1_same : curry1_rewrite.

Lemma is_uC_bf_negb_cond if0 th0 {n} k (f:bf(S n)) :
  is_uP_bf (oC if0 th0) k f = (eqb th0 (f(fun _ => if0))) && is_uP_bf (oC if0 (f(fun _ => if0))) k f.
Proof with curry1.
destruct(eqb th0 (f (fun _ : ltN (S n) => if0)))eqn:E...
apply reverse_bool_eq...
rewrite is_uP_bf_iff_fnary_evalu1 in H...
specialize(f_equal(fun f => f (fun _ => if0)) H)...
Qed.

Lemma fnary_evlau1_intro_uP_bf_with_ltN_pop_eq_B {n} k1 k1'  k2 p2 (f:bf(S n)) b1 :
  ltN_pop k2 k1 = BB k1' ->
    fnary_evalu1 k1 (intro_uP_bf p2 k2 f) b1 =
      intro_uP_bf p2 (ltN_comm_intro k1' k2) (fnary_evalu1 k1' f b1).
Proof with curry1.
curry1.
apply functional_extensionality...
unfold fnary_evalu1, intro_uP_bf...
rewrite spop_spush...
Qed.

Lemma intro_uP_bf_dirac_same {n} if0 th0 k f b :
  intro_uP_bf (oC if0 th0) k f (@array_dirac _ (S n) k (negb b) b) =
    if xorb b if0 then th0 else f (fun _ => b).
Proof. unfold intro_uP_bf; curry1. Qed.
Hint Rewrite @intro_uP_bf_dirac_same : curry1_rewrite.

Definition corev_prim (p:oPrim) (fx:bool) : bool := eval_prim p (negb (rev_prim p)) fx.
Definition corev_prim_fun (p:oPrim) {n} (f:bf n) : bf n := uop_bf (corev_prim p) f.

Lemma eval_prim_as_if_eqb_rev_prim p b y :
  eval_prim p b y = if eqb b (rev_prim p) then y else (corev_prim p y).
Proof. dest_match_step; curry1. Qed.

Lemma fnary_evalu1_intro_uP_bf {n} k1 b1 p2 k2 (f:bf(S n)) :
  fnary_evalu1 k1 (intro_uP_bf p2 k2 f) b1 =
    match ltN_pop k2 k1 with
    | AA _ => (if eqb b1 (rev_prim p2) then f else corev_prim_fun p2 f)
    | BB k1' => let k2' := ltN_comm_intro k1' k2 in
      intro_uP_bf p2 k2' (fnary_evalu1 k1' f b1)
    end.
Proof with curry1.
apply functional_extensionality...
unfold fnary_evalu1, intro_uP_bf...
rewrite spop_spush...
destruct(ltN_pop k2 k1) as [|k1'] eqn:E0...
rewrite eval_prim_as_if_eqb_rev_prim...
dest_match_step...
Qed.

Lemma intro_uC_bf_eq_cst_bf if0 th0 {n} k (f:bf n) b :
  intro_uP_bf (oC if0 th0) k f = cst_bf _ b <-> th0 = b /\ f = cst_bf _ b.
Proof. rewrite (shannon_expansion_u_gen k if0); curry1. Qed.
Hint Rewrite @intro_uC_bf_eq_cst_bf : curry1_rewrite.

Lemma corev_prim_fun_oC n if0 th0 f : @corev_prim_fun (oC if0 th0) n f = (fun _ => th0).
Proof. unfold corev_prim_fun, corev_prim, uop_bf, rev_prim; curry1. Qed.
Hint Rewrite corev_prim_fun_oC : curry1_rewrite.

Lemma is_uC_bf_intro_uC_bf {n} if1 b1 k1 if2 b2 k2 f : if1 <> if2 ->
  is_uP_bf (oC if1 b1) k1 (intro_uP_bf (oC if2 b2) k2 f) = true <->
    (b1 = b2 /\ (is_uP_bf (oC if1 b2) k1 (intro_uP_bf (oC if2 b2) k2 f) = true)) \/
    (b2 = negb b1 /\ k2 = k1 /\ f = cst_bf n b1) .
Proof with curry1.
intros...
destruct(eqb b1 b2)eqn:E0...
rewrite is_uP_bf_iff_fnary_evalu1...
destruct n...
rewrite fnary_evalu1_intro_uP_bf...
dest_match_step...
unfold rev_prim...
Qed.

Lemma is_uP_bf_false_and_true {n} (f:bf(S n)) k0 k1 :
  is_uP_bf (oC false (f (fun _ => false))) k0 f = true ->
  is_uP_bf (oC true  (f (fun _ => true ))) k1 f = true ->
    (f(fun _ => false)) = (f(fun _ => true)) \/
    ((f(fun _ => false)) <> (f(fun _ => true)) /\  k0 = k1 /\ f = piN _ k0 (f(fun _ => false))).
Proof with curry1.
intros H0 H1.
destruct(eqb(f(fun _ => false))(f(fun _ => true)))eqn:E0...
rewrite E0 in *...
rewrite <- intro_uP_bf_elim_uP_bf_same_eq_same in H1.
rewrite <- H1 in H0...
rewrite is_uC_bf_intro_uC_bf in H0...
rewrite (shannon_expansion_u k0)...
rewrite is_uP_bf_iff_fnary_evalu1 in H1...
Qed.

Lemma is_uU_bf_as_fnary_evalu1 {n} k (f:bf(S n)) :
  is_uP_bf oU k f = true <-> fnary_evalu1 k f false = fnary_evalu1 k f true.
Proof with curry1.
unfold is_uP_bf...
rewrite (beq_bf_iff_true _ _)...
Qed.

Lemma is_uX_bf_as_fnary_evalu1 {n} k (f:bf(S n)) :
  is_uP_bf oX k f = true <-> fnary_evalu1 k f false = neg_bf(fnary_evalu1 k f true).
Proof with curry1.
unfold is_uP_bf...
rewrite (beq_bf_iff_true _ _)...
Qed.

Lemma intro_uP_bf_same_injective {n} p k (f1 f2:bf n):
  intro_uP_bf p k f1 = intro_uP_bf p k f2 <-> f1 = f2.
Proof. rewrite intro_uP_bf_as_is_uP_bf; curry1. Qed.
Hint Rewrite @intro_uP_bf_same_injective : curry1_rewrite.

Lemma fnary_evalu1_neg_bf {n} k (f:bf(S n)) b : fnary_evalu1 k (neg_bf f) b = neg_bf(fnary_evalu1 k f b).
Proof. unfold fnary_evalu1, neg_bf; curry1. Qed.
Hint Rewrite @fnary_evalu1_neg_bf : curry1_rewrite.

Lemma neg_bf_intro_uU_bf {n} k (f:bf n) : neg_bf (intro_uP_bf oU k f) = intro_uP_bf oU k (neg_bf f).
Proof. rewrite (shannon_expansion_u k); curry1. Qed.

Lemma neg_bf_intro_uX_bf {n} k (f:bf n) : neg_bf (intro_uP_bf oX k f) = intro_uP_bf oX k (neg_bf f).
Proof. rewrite (shannon_expansion_u k); curry1. Qed.

Lemma intro_uU_bf_eq_cst_bf {n} k f b : intro_uP_bf oU k f = cst_bf (S n) b <-> f = cst_bf _ b.
Proof. rewrite (shannon_expansion_u k); curry1. Qed.
Hint Rewrite @intro_uU_bf_eq_cst_bf : curry1_rewrite.

Lemma neg_bf_intro_uC_bf if0 th0 {n} k (f:bf n) :
  neg_bf (intro_uP_bf (oC if0 th0) k f) = intro_uP_bf (oC if0 (negb th0)) k (neg_bf f).
Proof. rewrite (shannon_expansion_u_gen k if0); curry1. Qed.

Lemma intro_uX_bf_eq_cst_bf {n} k f b : intro_uP_bf oX k f = cst_bf (S n) b <-> False.
Proof. rewrite (shannon_expansion_u k); curry1. Qed.
Hint Rewrite @intro_uX_bf_eq_cst_bf : curry1_rewrite.

