import sys

FileRNameL = sys.argv[1:]

D = dict()

def merge(D1, D2) :
	if (type(D1) == dict().__class__) and (type(D2) == dict().__class__) :
		D2 = D2.copy()
		DD = dict()
		for (k1, v1) in D1.items():
			if k1 in D2 :
				v2 = D2[k1]
				del D2[k1]
				DD[k1] = merge(v1, v2)
			else :
				DD[k1] = v1
		for (k2, v2) in D2.items():
			DD[k2] = v2
		return DD
	else:
		return {D1, D2}

def mergel(*DL) :
	if len(DL) == 0 :
		return dict()
	else:
		D = DL[0]
		for DR in DL[1:] :
			D = merge(D, DR)
		return D

for FileRName in FileRNameL :
	FileR = open(FileRName)
	DR = eval(FileR.read())
	FileR.close()
	D = merge(D, DR)

print(D)
