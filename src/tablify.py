import sys

FileRName = sys.argv[1]
FileR = open(FileRName)
TXT = FileR.read()
FileR.close()

DR = eval(TXT)

KEYS1 = list(DR.keys())
KEYS1.sort()

def union(*sets):
	SS = set()
	for aset in sets:
		SS = SS.union(aset)
	return SS

KEYS2 = list(union(*list(set(line.keys()) if type(line) == dict().__class__ else set() for line in DR.values())))
KEYS2.sort()

TXT = '\n'.join('\t, '.join(map(str, [k1]+[DR[k1].get(k2, '') for k2 in KEYS2])) for k1 in KEYS1)

print('\t, '.join(['']+list(map(str, KEYS2))))
print(TXT)
