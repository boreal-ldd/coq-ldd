Require Import Arith .
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.

Require Import Ground.SimplProp.
Require Import Ground.SimplBool.
Require Import Ground.SimplOption.
Require Import Ground.SimplEq.
Require Import Ground.SimplFun.
Require Import Ground.SimplBoolFun.
Require Import Ground.SimplCmp.
Require Import Ground.SimplSimpl.

Require Import Ground.SimplNat.
Require Import Ground.SimplAB.

Require Import Ground.SimplList_Chap0_Standard.
Require Import Ground.SimplList_Chap1_Construct.
Require Import Ground.SimplList_Chap2_Predicate.
Require Import Ground.SimplList_Chap3_PredicateOnConstruct.
Require Import Ground.SimplList.
Require Import Ground.SimplABList.
Require Import Ground.SimplMore.
Require Import Ground.SimplDTT.
Require Import Ground.SimplLtN_Chap0_Construct.
Require Import Ground.SimplLtN_Chap1_Array_as_Function.
Require Import Ground.SimplLtN_Chap1_Sec1_Boolean_Array.
Require Import Ground.SimplLtNIndexedArray.

Require Import Ground.SimplDTT.
Require Import Ground.SimplLtN.
Require Import Ground.SimplEnum.

Require Import Ground.MiniBool_VAX.
Require Import Ground.MiniBool_Logic.

Require Import FiniteCurryfy.
Require Import FiniteBooleanFunction.
Require Import LddO_Primitives.

Require Import Ground.SimplEnum.
Require Import Ground.SimplEnumCmp.

(* SubSection 2. Defining Primitives *)
Inductive oprim :=
| OU : oprim
| OX : oprim
| OC (if0 th0:bool) : oprim.

Definition oPrim_of_oprim (p:oprim) : oPrim :=
  match p with OU => oU | OX => oX | OC if0 th0 => oC if0 th0 end.

Definition idx_oprim (p:oprim) : ltN 6 :=
match p with
| OU => ltN_of_nat 0 6 eq_refl
| OX => ltN_of_nat 1 6 eq_refl
| OC false false => ltN_of_nat 2 6 eq_refl
| OC true  false => ltN_of_nat 3 6 eq_refl
| OC false true  => ltN_of_nat 4 6 eq_refl
| OC true  true  => ltN_of_nat 5 6 eq_refl
end.

Definition pi_oprim (k:ltN 6) : oprim :=
  match proj1_sig k with
  | 0 => OU
  | 1 => OX
  | 2 => OC false false
  | 3 => OC true false
  | 4 => OC false true
  | _ => OC true true
  end.

Definition cmp_oprim (x y:oprim) : ord :=
match x, y with
| OU, OU => Eq
| OU, _  => Lt
| _, OU  => Gt
| OX, OX => Eq
| OX, _  => Lt
| _, OX  => Gt
| OC if0 th0, OC if1 th1 =>
  cmp_comp(cmp_bool th0 th1)(cmp_bool if0 if1)
end.

Lemma cmp_P_cmp_prod_cmp_bool_cmp_bool : cmp_P(cmp_prod cmp_bool cmp_bool).
Proof. apply cmp_P_cmp_prod; apply cmp_P_bool. Qed.

Lemma cmp_P_cmp_oprim : cmp_P cmp_oprim.
Proof with curry1.
destruct cmp_P_cmp_prod_cmp_bool_cmp_bool as [RB2 TB2 AB2].
constructor.
- intro...
  destruct x, y; try solve_by_invert...
  rewrite (RB2 (th0, if0) (th1, if1))...
  split; solve_by_invert.
- intro...
  destruct x, y, z...
  specialize (TB2 (th0, if0) (th1, if1) (th2, if2))...
- intro...
  destruct x, y...
  specialize (AB2 (th0, if0) (th1, if1))...
Qed.

Lemma cmp_Eq_iff_eq_cmp_oprim : cmp_Eq_iff_eq cmp_oprim.
Proof. destruct cmp_P_cmp_oprim; curry1. Qed.

Lemma cmp_oprim_refl x : cmp_oprim x x = Eq.
Proof. rewrite (cmp_Eq_iff_eq_cmp_oprim x x); curry1. Qed.
Hint Rewrite cmp_oprim_refl : curry1_rewrite.

Lemma cmp_asym_cmp_oprim : cmp_asym cmp_oprim.
Proof. destruct cmp_P_cmp_oprim; curry1. Qed.

Lemma cmp_Lt_trans_cmp_oprim : cmp_Lt_trans cmp_oprim.
Proof. destruct cmp_P_cmp_oprim; curry1. Qed.

Lemma idx_oprim_is_cmp_oprim_compatible : idx_is_cmp_compatible cmp_oprim idx_oprim.
Proof with curry1.
unfold idx_is_cmp_compatible, is_cmp_compatible, cmp_oprim...
dest_match.
Qed.

Lemma idx_oprim_pi_oprim k : idx_oprim (pi_oprim k) = k.
Proof with curry1.
unfold pi_oprim.
dest_match...
destruct k...
Qed.
Hint Rewrite idx_oprim_pi_oprim : curry1_rewrite.

Lemma comp_pi_oprim_idx_oprim : comp pi_oprim idx_oprim = id.
Proof. apply functional_extensionality; curry1. Qed.
Hint Rewrite comp_pi_oprim_idx_oprim : curry1_rewrite.

Lemma pi_oprim_idx_oprim p : pi_oprim(idx_oprim p) = p.
Proof with curry1.
unfold idx_oprim.
dest_match...
Qed.
Hint Rewrite pi_oprim_idx_oprim : curry1_rewrite.

Lemma comp_idx_oprim_pi_oprim : comp idx_oprim pi_oprim = id.
Proof. apply functional_extensionality; curry1. Qed.
Hint Rewrite comp_idx_oprim_pi_oprim : curry1_rewrite.

Lemma pi_oprim_iff_idx_oprim k v : pi_oprim k = v <-> k = idx_oprim v.
Proof. split; curry1. Qed.

Lemma pi_oprim_is_cmp_oprim_compatible : pi_is_cmp_compatible cmp_oprim pi_oprim.
Proof with curry1.
intros k1 k2.
specialize(idx_oprim_is_cmp_oprim_compatible (pi_oprim k1) (pi_oprim k2))...
Qed.

Lemma explicit_finite_constr_oprim : explicit_finite_constr _ _ idx_oprim pi_oprim.
Proof with curry1.
constructor.
- apply comp_idx_oprim_pi_oprim.
- apply comp_pi_oprim_idx_oprim.
Qed.

Definition minsat_oprim : minsat_t oprim := minsat_map pi_oprim array_minsat.

(* [MOVEME] *)

Lemma explicit_bijection_constr_comm {A B} map rec :
  @explicit_bijection_constr A B map rec <-> explicit_bijection_constr rec map.
Proof. unfold explicit_bijection_constr; simplprop. Qed.

Lemma minsat_positive_minsat_oprim : minsat_positive cmp_oprim minsat_oprim.
Proof with curry1.
unfold minsat_oprim.
refine (minsat_positive_minsat_map_using_is_cmp_compatible_and_bijection
  pi_oprim idx_oprim _ cmp_ltN cmp_oprim _ _ (@minsat_positive_array_minsat _)).
- rewrite explicit_bijection_constr_comm.
  apply explicit_finite_constr_oprim.
- apply pi_oprim_is_cmp_oprim_compatible.
Qed.

Lemma minsat_alternative_minsat_oprim : minsat_alternative cmp_oprim minsat_oprim.
Proof with curry1.
rewrite <- minsat_positive_iff_minsat_alternative.
apply minsat_positive_minsat_oprim.
Qed.

Lemma minsat_oprim_eq_Some_iff_is_min_positive p op :
  minsat_oprim p = Some op <-> is_min_positive cmp_oprim p op.
Proof. rewrite (minsat_alternative_minsat_oprim p); curry1. Qed.

Lemma minsat_oprim_eq_None p :
  minsat_oprim p = None <-> p = (fun _ => false).
Proof. rewrite (minsat_alternative_minsat_oprim p); curry1. Qed.

Definition oprim_of_oPrim (p:oPrim) : oprim :=
  match proj1_sig p with
  | (x0, x1, y) =>
    if x0 && x1
    then if y then OX else OU
    else (OC x1 y)
  end.

Lemma oprim_of_oPrim_of_oprim p : oprim_of_oPrim (oPrim_of_oprim p) = p.
Proof. destruct p; compute; curry1. Qed.
Hint Rewrite oprim_of_oPrim_of_oprim : curry1_rewrite.

Lemma oPrim_of_oprim_of_oPrim p : oPrim_of_oprim (oprim_of_oPrim p) = p.
Proof with curry1.
destruct p as [[[x0 x1] y] Hp]...
unfold oPrim_of_oprim, oprim_of_oPrim...
unfold oU, oX, oC...
unfold pPrim in Hp...
dest_match_step...
- dependent destruction Hp...
  dest_match_step...
- generalize dependent Hp.
  generalize dependent D.
  BPS.MicroBPSolver...
Qed.
Hint Rewrite oPrim_of_oprim_of_oPrim : curry1_rewrite.

Lemma oprim_of_oPrim_eq_iff op oP : oprim_of_oPrim oP = op <-> oP = oPrim_of_oprim op.
Proof. split; curry1. Qed.

Lemma not_neq_oprim (x y:oprim) : (x <> y -> False) <-> x = y.
Proof. apply (not_neq_using_cmp_P cmp_oprim cmp_P_cmp_oprim). Qed.
Hint Rewrite not_neq_oprim : curry1_rewrite.

(* returns [true] iff p1 and p2 are compatible (usefull for LDD-U-NUCX and higher) *)
Definition oprim_compatible (p1 p2:oprim) : bool :=
  match p1, p2 with
  | OU, _  => true
  | _ , OU => true
  | OX, OX => true
  | OC if0 th0, OC if1 th1 => eqb th0 th1
  | _ , _ => false
  end.
