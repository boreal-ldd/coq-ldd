Require Import Arith .
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
Require Import Arith .
Require Import Psatz.
Require Import Coq.Program.Equality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.ProofIrrelevance.
From Coq Require Extraction.
Require Import Bool.
Require Import Bool Ring.

Require Import Ground.SimplProp.
Require Import Ground.SimplBool.
Require Import Ground.SimplOption.
Require Import Ground.SimplEq.
Require Import Ground.SimplFun.
Require Import Ground.SimplBoolFun.
Require Import Ground.SimplCmp.
Require Import Ground.SimplSimpl.

Require Import Ground.SimplNat.
Require Import Ground.SimplAB.

Require Import Ground.SimplList_Chap0_Standard.
Require Import Ground.SimplList_Chap1_Construct.
Require Import Ground.SimplList_Chap2_Predicate.
Require Import Ground.SimplList_Chap3_PredicateOnConstruct.
Require Import Ground.SimplList.
Require Import Ground.SimplABList.
Require Import Ground.SimplMore.
Require Import Ground.SimplDTT.
Require Import Ground.SimplLtN_Chap0_Construct.
Require Import Ground.SimplLtN_Chap1_Array_as_Function.
Require Import Ground.SimplLtN_Chap1_Sec1_Boolean_Array.
Require Import Ground.SimplLtNIndexedArray.

Require Import Ground.SimplDTT.
Require Import Ground.SimplLtN.
Require Import Ground.SimplEnum.
Require Import Ground.SimplEnumCmp.

Require Import Ground.MiniBool_VAX.
Require Import Ground.MiniBool_Logic.

Require Import FiniteCurryfy.
Require Import FiniteBooleanFunction.

Require Import MaxLddGen.
Require Import LddO_Primitives.
Require Import LddO_OPrim.

Require Import PrimUGen.
Require Import LddU_Primitives.

(* Section 1. Definition if Terminal and Primitives *)
(* SubSection 1. Defining Terminals *)
Inductive term : nat -> Type :=
| Cst (n:nat) (b:bool) : term n
| PiN (n:nat) (k:ltN n) (b:bool) : term n.

Lemma Cst_eq_Cst n b1 b2 : Cst n b1 = Cst n b2 <-> b1 = b2. Proof. split; solve_by_invert. Qed.
Lemma PiN_eq_PiN n k1 b1 k2 b2 : PiN n k1 b1 = PiN n k2 b2 <-> k1 = k2 /\ b1 = b2. Proof. split; solve_by_invert. Qed.
Lemma Cst_eq_PiN n b1 k2 b2 : Cst n b1 = PiN n k2 b2 <-> False. Proof. solve_by_invert. Qed.
Lemma PiN_eq_Cst n k1 b1 b2 : PiN n k1 b1 = Cst n b2 <-> False. Proof. solve_by_invert. Qed.

Hint Rewrite Cst_eq_Cst : curry1_rewrite.
Hint Rewrite PiN_eq_PiN : curry1_rewrite.
Hint Rewrite Cst_eq_PiN : curry1_rewrite.
Hint Rewrite PiN_eq_Cst : curry1_rewrite.

Definition sem_term {n} (t:term n) : bf n :=
match t with
| Cst n b => cst_bf n b
| PiN n k b => piN n k b
end.

Definition term_is_Cst {n} (t:term n) : bool :=
  match t with Cst _ _ => true | PiN _ _ _ => false end.
Definition term_is_PiN {n} (t:term n) : bool := negb(term_is_Cst t).

Lemma term_is_Cst_Cst n b : term_is_Cst(Cst n b) = true. Proof. reflexivity. Qed.
Lemma term_is_Cst_PiN n k b : term_is_Cst(PiN n k b) = false. Proof. reflexivity. Qed.
Lemma term_is_PiN_Cst n b : term_is_PiN(Cst n b) = false. Proof. reflexivity. Qed.
Lemma term_is_PiN_PiN n k b : term_is_PiN(PiN n k b) = true. Proof. reflexivity. Qed.

Hint Rewrite term_is_Cst_Cst : curry1_rewrite.
Hint Rewrite term_is_Cst_PiN : curry1_rewrite.
Hint Rewrite term_is_PiN_Cst : curry1_rewrite.
Hint Rewrite term_is_PiN_PiN : curry1_rewrite.

Definition extract_term {n} (f:bf n) : option(term n) :=
match extract_cst f with
| Some b => Some(Cst n b)
| None =>
  match extract_piN f with
  | Some kb => Some(PiN n (fst kb) (snd kb))
  | None => None
  end
end.

Lemma extract_term_correct {n} f t :
  @extract_term n f = Some t <-> f = sem_term t.
Proof with curry1.
unfold extract_term, sem_term...
dependent destruction t...
- dest_match_step...
  unfold opmap; dest_match_step...
- dest_match_step...
  unfold opmap; dest_match_step...
Qed.
Hint Rewrite @extract_term_correct : curry1_rewrite.

(* [NOTE] we enforce constantes to be part of terminals *)
Definition term_of_bool (f:bf 0) : term 0 :=
  Cst 0 (f(fun _ => false)).

Lemma extract_term_cst_bf n b : extract_term (cst_bf n b) = Some(Cst n b).
Proof. curry1. Qed.
Hint Rewrite extract_term_cst_bf : curry1_rewrite.

Lemma term_of_bool_cst_bf b : term_of_bool (cst_bf 0 b) = Cst 0 b.
Proof. reflexivity. Qed.
Hint Rewrite term_of_bool_cst_bf : curry1_rewrite.

Lemma extract_term_0 (f:bf 0) : extract_term f = Some(term_of_bool f).
Proof. rewrite (bf0 f); curry1. Qed.

Definition is_term {n} (f:bf n) : bool :=
  @PrimUGen.is_term term (@extract_term) n f.

Definition prim (n:nat) : Type := oprim * ltN n.

Definition sem_prim {n} (p:prim(S n)) (f:bf n) : bf (S n) := intro_uP_bf (oPrim_of_oprim (fst p)) (snd p) f.
Definition detect_prim {n} (p:prim(S n)) (f:bf(S n)) : bool := is_uP_bf (oPrim_of_oprim (fst p)) (snd p) f.
Definition elim_prim {n} (p:prim(S n)) (f:bf(S n)) : bf n := elim_uP_bf (oPrim_of_oprim (fst p)) (snd p) f.

Definition extract_prim {n} p (f:bf(S n)) : option(bf n) :=
  if detect_prim p f then Some(elim_prim p f) else None.

Definition is_prim {n} (p:prim _) (f:bf(S n)) :=
  @PrimUGen.is_prim prim (@extract_prim) n p f.

Definition is_prim' {n} (p:prim _) (f:bf n) :=
  @PrimUGen.is_prim' prim (@extract_prim) n p f.

Lemma sem_prim_as_detect_prim {n} p f g :
  @sem_prim n p f = g <-> detect_prim p g = true /\ f = elim_prim p g.
Proof with curry1.
unfold detect_prim, elim_prim, sem_prim.
rewrite (intro_uP_bf_as_is_uP_bf (oPrim_of_oprim (fst p)) (snd p) f g)...
Qed.

Lemma extract_prim_iff_sem_prim {n} p f xf :
  @extract_prim n p f = Some xf <-> sem_prim p xf = f.
Proof with curry1.
unfold extract_prim...
rewrite sem_prim_as_detect_prim...
Qed.
Hint Rewrite @extract_prim_iff_sem_prim.

Lemma is_term_cst_bf n c : is_term (cst_bf n c) = true.
Proof with curry1.
unfold is_term, PrimUGen.is_term...
Qed.
Hint Rewrite is_term_cst_bf : curry1_rewrite.

Lemma extract_term_piN n k b : extract_term (piN n k b) = Some(PiN n k b).
Proof. unfold extract_term; curry1. Qed.
Hint Rewrite extract_term_piN : curry1_rewrite.

Lemma is_term_piN n k b : is_term (piN n k b) = true.
Proof. unfold is_term, PrimUGen.is_term; curry1. Qed.
Hint Rewrite is_term_piN : curry1_rewrite.

Lemma is_term_intro_uC_bf if0 th0 n k cst0 : is_term (intro_uP_bf (oC if0 th0) k (cst_bf n cst0)) = true.
Proof with curry1.
simpl...
dest_match_step...
Qed.
Hint Rewrite is_term_intro_uC_bf : curry1_rewrite.

Lemma elim_prim_cst_bf n p b : elim_prim p (cst_bf (S n) b) = cst_bf _ b.
Proof. unfold elim_prim; curry1. Qed.
Hint Rewrite elim_prim_cst_bf : curry1_rewrite.

Lemma sem_prim_uP p {n} k f: @sem_prim n (p, k) f = intro_uP_bf (oPrim_of_oprim p) k f.
Proof. reflexivity. Qed.
Hint Rewrite @sem_prim_uP : curry1_rewrite.

Lemma is_term_sem_prim {n} (f:bf n): is_term f = false -> forall p, is_term (sem_prim p f) = false.
Proof with curry1.
unfold sem_prim.
unfold is_term, PrimUGen.is_term...
unfold extract_term in *...
destruct p as [o k]...
destruct(extract_cst (intro_uP_bf (oPrim_of_oprim o) k f))eqn:E0...
- rewrite intro_uP_bf_as_is_uP_bf in E0...
  unfold elim_uP_bf in *...
- destruct(extract_piN (intro_uP_bf (oPrim_of_oprim o) k f))eqn:E1...
  rewrite intro_uP_bf_as_is_uP_bf in E1...
  unfold elim_uP_bf in *...
  dest_match_step...
Qed.

Lemma is_term_intro_uP_bf_false_using_is_term_false {n} (f:bf n) :
  is_term f = false -> forall o k, is_term (intro_uP_bf o k f) = false.
Proof. intros H o k; specialize(is_term_sem_prim f H (oprim_of_oPrim o, k)); curry1. Qed.

Lemma elim_prim_sem_prim {n} p (f:bf n) : elim_prim p (sem_prim p f) = f.
Proof. unfold elim_prim, sem_prim; curry1. Qed.
Hint Rewrite @elim_prim_sem_prim : curry1_rewrite.

Definition cmp_prim {n} : prim n -> prim n -> ord := cmp_prod cmp_oprim (@cmp_ltN n).

Lemma cmp_P_cmp_prim n : cmp_P(@cmp_prim n).
Proof. apply (cmp_P_cmp_prod _ _ cmp_P_cmp_oprim (cmp_P_cmp_ltN _)). Qed.

Definition pcmp_prim {n} (x y:prim n) : option ord := Some(cmp_prim x y).

Lemma pcmp_P_pcmp_prim n : pcmp_P (@pcmp_prim n).
Proof with curry1.
destruct (cmp_P_cmp_prim n) as [RP TP AP].
unfold pcmp_prim.
constructor...
- unfold pcmp_reflexive...
- unfold pcmp_transitive...
  specialize(TP x y z)...
- unfold pcmp_asymmetric...
Qed.

(*
Definition minsat_uP_bf (p:oPrim) {n} (f:bf(S n)) : option(ltN(S n)) :=
  array_minsat (fun k => is_uP_bf p k f).
 *)

(* [EFFICIENT]
Definition minsat_prim {n} (f:bf(S n)) : option(prim(S n)) :=
match minsat_uP_bf oU f with
| Some k => Some(OU, k)
| None =>
match minsat_uP_bf oX f with
| Some k => Some(OX, k)
| None =>
let th0 := f(fun _ => false) in
match minsat_uP_bf (oC false th0) f with
| Some k => Some(OC false th0, k)
| None =>
let th1 := f(fun _ => true) in
match minsat_uP_bf (oC true  th1) f with
| Some k => Some(OC true  th1, k)
| None => None
end
end
end
end.
 *)

Definition minsat_prim {n} (p:prim n -> bool) : option(prim n) :=
  minsat_prod minsat_oprim array_minsat p.

Lemma minsat_positive_minsat_prim {n} : minsat_positive (@cmp_prim n) (@minsat_prim n).
Proof with curry1.
apply minsat_positive_minsat_prod.
- apply cmp_P_cmp_oprim.
- apply cmp_P_cmp_ltN.
- apply minsat_positive_minsat_oprim.
- apply minsat_positive_array_minsat.
Qed.

Definition minsat_prim_uP_bf {n} (f:bf(S n)) : option(prim(S n)) :=
  minsat_prim (fun p => detect_prim p f).

Definition is_prim_free{n} (f:bf n) : bool :=
  match n as n0 return bf n0 -> bool with
  | 0 => (fun _ => true)
  | S n' => (fun f => isNone(minsat_prim_uP_bf f))
  end f.

Lemma minsat_prim_eq_None_iff_forall_is_uP_bf_free n f :
  @minsat_prim_uP_bf n f = None <-> (forall p, detect_prim p f = false).
Proof with curry1.
rewrite <- functional_extensionality_iff...
unfold minsat_prim_uP_bf...
specialize(@minsat_positive_minsat_prim (S n)) as HH.
rewrite minsat_positive_iff_minsat_alternative in HH.
rewrite (HH _ _)...
Qed.

Lemma minsat_prim_eq_None_iff_is_prim_free n f :
  @minsat_prim_uP_bf n f = None <-> is_prim_free f = true.
Proof. unfold is_prim_free; curry1. Qed.

Lemma is_prim_free_correct {n} (f:bf n) :
  is_prim_free f = true <-> forall p, is_prim' p f = false.
Proof with curry1.
destruct n...
unfold PrimUGen.is_prim...
rewrite minsat_prim_eq_None_iff_forall_is_uP_bf_free...
unfold extract_prim...
split; intros H x; specialize(H x)...
Qed.

Definition xprim {F} : nat -> Type :=
  @PrimUGen.xprim term prim F.

Definition extract_xprim {n} (f:bf n) : @xprim bf n :=
match n as n0 return bf n0 -> @xprim bf n0 with
| O => (fun f => XTerm _ _ (term_of_bool f))
| S n' => (fun f =>
  match extract_term f with
  | Some t => XTerm _ _ t
  | None =>
  match minsat_prim_uP_bf f with
  | Some p => XPrim _ _ p (elim_prim p f)
  | None => (XSha _ _ (fnary_evalo1 f false) (fnary_evalo1 f true))
  end
  end)
end f.

Definition sem_xprim {n} (xp:@xprim bf n) : bf n :=
  @PrimUGen.sem_xprim term (@sem_term) prim (@sem_prim) n xp.

(*
Lemma minsat_uU_bf {n} k b : minsat_uP_bf oU (piN (S n) k b) = None <-> n = 0.
Proof with curry1.
unfold minsat_uP_bf.
rewrite array_minsat_eq_None...
split...
- destruct n...
  destruct k as [[|k] Hk]...
  + specialize(f_equal (fun f => f (ltN_of_nat 1 (S(S n)) eq_refl)) H)...
  + specialize(f_equal (fun f => f (ltN_of_nat 0 (S(S n)) eq_refl)) H)...
- apply functional_extensionality...
  rewrite(ltN_1_unique x)...
Qed.
Hint Rewrite @minsat_uU_bf : curry1_rewrite.

Lemma minsat_uX_bf {n} k b : minsat_uP_bf oX (piN (S n) k b) = Some k.
Proof with curry1.
unfold minsat_uP_bf.
rewrite array_minsat_eq_Some_iff_is_min_positive...
unfold is_min_positive...
Qed.
Hint Rewrite @minsat_uX_bf : curry1_rewrite.
 *)

Lemma isSome_extract_prim {n} (f:bf(S n)) p : isSome (extract_prim p f) = detect_prim p f.
Proof. unfold extract_prim; dest_match_step; curry1. Qed.
Hint Rewrite @isSome_extract_prim : curry1_rewrite.

Lemma extract_prim_eq_None {n} p f : @extract_prim n p f = None <-> detect_prim p f = false.
Proof. unfold extract_prim; curry1. Qed.

Lemma minsat_prim_uP_bf_eq_Some {n} f p:
  @minsat_prim_uP_bf n f = Some p <-> is_min_positive cmp_prim (fun p => is_prim' p f) p.
Proof with curry1.
unfold minsat_prim_uP_bf.
specialize(@minsat_positive_minsat_prim(S n)) as H.
rewrite minsat_positive_iff_minsat_alternative in H.
rewrite (H _)...
unfold PrimUGen.is_prim...
specialize(isSome_extract_prim f) as HH.
rewrite <- functional_extensionality_iff in HH.
rewrite HH...
Qed.

Definition is_lowest_prim' {n} (p:prim n) f := (is_lowest_prim' prim (@extract_prim) (@pcmp_prim)) p f.

Lemma detect_prim_sem_prim_same {n} p (f:bf n) : detect_prim p (sem_prim p f) = true.
Proof. unfold detect_prim, sem_prim; curry1. Qed.
Hint Rewrite @detect_prim_sem_prim_same : curry1_rewrite.

Lemma sem_prim_elim_prim_same {n} p (f:bf(S n)) :
  sem_prim p (elim_prim p f) = f <-> detect_prim p f = true.
Proof. unfold sem_prim, elim_prim, detect_prim; curry1. Qed.
Hint Rewrite @sem_prim_elim_prim_same : curry1_rewrite.

Lemma extract_xprim_minimal : forall {n} (f:bf n),
    match extract_xprim f with
    | XTerm _ _ _ => True
    | XPrim _ _  p fp => is_term f = false /\ is_lowest_prim' p (sem_prim p fp)
    | XSha _ _ _ _ => is_term f = false /\ is_prim_free f = true
    end.
Proof with curry1.
unfold extract_xprim...
dest_match_step...
dest_match_step...
assert(is_term f = false).
{ unfold is_term, PrimUGen.is_term... }
dest_match...
rewrite minsat_prim_uP_bf_eq_Some in D0...
unfold is_min_positive, PrimUGen.is_prim in *...
unfold is_lowest_prim', PrimUGen.is_lowest_prim'...
unfold PrimUGen.is_prim...
specialize(H1 p')...
unfold pcmp_prim...
rewrite <- sem_prim_elim_prim_same in H0.
rewrite H0 in H3...
Qed.

Lemma sem_xprim_extract_xprim {n} (f:bf n) : sem_xprim (extract_xprim f) = f.
Proof with curry1.
destruct n...
- rewrite(bf0 f)...
- dest_match_step...
  dest_match_step...
  rewrite minsat_prim_uP_bf_eq_Some in D0...
  unfold is_min_positive in D0...
  unfold PrimUGen.is_prim in H...
Qed.

Definition same_prim {n} (f1 f2:bf n) : Prop :=
  @PrimUGen.same_prim prim (@extract_prim) n f1 f2.

Lemma is_term_sem_prim_cst_bf n p b : is_term (sem_prim p (cst_bf n b)) = true.
Proof. destruct p as [[] k]; curry1. Qed.
Hint Rewrite @is_term_sem_prim_cst_bf : curry1_rewrite.

Lemma is_uP_bf_intro_intro_uP_bf_same_using_is_term_false {n} p1 p2 k (f:bf n) :
  is_term(intro_uP_bf p2 k f) = false -> is_uP_bf p1 k (intro_uP_bf p2 k f) = true <-> p1 = p2.
Proof with curry1.
intro H1.
split; intro H2...
destruct(oprim_of_oPrim p1)eqn:E1;
  rewrite oprim_of_oPrim_eq_iff in E1;
  destruct(oprim_of_oPrim p2)eqn:E2;
  rewrite oprim_of_oPrim_eq_iff in E2; curry1;
  try rewrite is_uP_bf_iff_fnary_evalu1 in *;
  unfold is_uP_bf in H2...
- dest_match_step...
- dest_match_step...
- dest_match_step...
Qed.

Lemma is_uP_bf_ltN_intro_intro_uP_bf_using_is_term_sem_prim_false {n} p1 p2 k1 k2' (f:bf(S n)) :
is_term (intro_uP_bf (oPrim_of_oprim p1) k1 f) = false ->
  is_uP_bf (oPrim_of_oprim p2) (ltN_intro k1 k2') (intro_uP_bf (oPrim_of_oprim p1) k1 f) =
    (oprim_compatible p1 p2) && (is_uP_bf (oPrim_of_oprim p2) k2' f).
Proof with curry1.
intro H.
destruct(oprim_compatible p1 p2)eqn:F0...
- rewrite eq_iff_eq_true...
  destruct p2...
  + repeat rewrite is_uU_bf_as_fnary_evalu1.
    repeat rewrite fnary_evalu1_intro_uP_bf...
  + repeat rewrite is_uX_bf_as_fnary_evalu1.
    repeat rewrite fnary_evalu1_intro_uP_bf...
    destruct p1...
    -- repeat rewrite neg_bf_intro_uU_bf...
    -- repeat rewrite neg_bf_intro_uX_bf...
  + repeat rewrite is_uP_bf_iff_fnary_evalu1...
    rewrite fnary_evalu1_intro_uP_bf...
    destruct p1...
- apply reverse_bool_eq...
  destruct p2...
  + destruct p1...
  + rewrite is_uX_bf_as_fnary_evalu1 in H0...
    repeat rewrite fnary_evalu1_intro_uP_bf in H0...
    destruct p1...
    rewrite (shannon_expansion_u_gen (ltN_comm_intro k2' k1) if0) in H0...
  + rewrite is_uP_bf_iff_fnary_evalu1 in H0...
    rewrite fnary_evalu1_intro_uP_bf in H0...
    destruct p1...
Qed.

Lemma is_uP_bf_intro_intro_uP_bf_using_is_term_sem_prim_false_and_ltN_pop_eq_B {n} p1 p2 k1 k2 k2' (f:bf(S n)) :
  ltN_pop k1 k2 = BB k2' ->
  is_term (intro_uP_bf (oPrim_of_oprim p1) k1 f) = false ->
    is_uP_bf (oPrim_of_oprim p2) k2 (intro_uP_bf (oPrim_of_oprim p1) k1 f) =
      (oprim_compatible p1 p2) && (is_uP_bf (oPrim_of_oprim p2) k2' f).
Proof with curry1.
intros E0 H...
apply is_uP_bf_ltN_intro_intro_uP_bf_using_is_term_sem_prim_false...
Qed.

Lemma same_prim_preserved {n} (f1 f2:bf n) :
  same_prim f1 f2 -> forall p,
    let pf1 := sem_prim p f1 in
    let pf2 := sem_prim p f2 in
    is_term pf1 = false -> is_term pf2 = false -> same_prim pf1 pf2.
Proof with curry1.
unfold same_prim...
unfold PrimUGen.same_prim, PrimUGen.is_prim', PrimUGen.is_prim in *...
destruct n...
- rewrite (bf0 f1) in *...
- unfold detect_prim, sem_prim...
  destruct p as [o1 k1].
  destruct p0 as [o2 k2]...
  destruct(ltN_pop k1 k2) as [|k2'] eqn:E0...
  + rewrite eq_iff_eq_true.
    rewrite (is_uP_bf_intro_intro_uP_bf_same_using_is_term_false _ _ _ _ H1).
    rewrite (is_uP_bf_intro_intro_uP_bf_same_using_is_term_false _ _ _ _ H0)...
  + specialize(H(o2, k2'))...
    rewrite(is_uP_bf_ltN_intro_intro_uP_bf_using_is_term_sem_prim_false _ _ _ _ _ H0).
    rewrite(is_uP_bf_ltN_intro_intro_uP_bf_using_is_term_sem_prim_false _ _ _ _ _ H1)...
Qed.

Lemma is_prim_sem_prim_with_is_prim_free {n} f :
    is_prim_free f = true -> is_term f = false -> forall p p', is_prim p' (@sem_prim n p f) = true -> p = p'.
Proof with curry1.
intros.
unfold is_prim, PrimUGen.is_prim, sem_prim in *...
unfold detect_prim in H1...
destruct p' as [o1 k1]...
destruct p as [o2 k2]...
destruct(ltN_pop k1 k2) as [|k2'] eqn:E0...
- rewrite is_uP_bf_intro_intro_uP_bf_same_using_is_term_false in H1...
  + rewrite <- oprim_of_oPrim_eq_iff in H1...
  + apply is_term_intro_uP_bf_false_using_is_term_false...
- destruct(ltN_pop (ltN_intro k1 k2') k1) as [|k1'] eqn:E1; [curry1|].
  destruct n; [curry1|].
  rewrite (is_uP_bf_intro_intro_uP_bf_using_is_term_sem_prim_false_and_ltN_pop_eq_B _ _ _ _ _ _ E1) in H1...
  + rewrite minsat_prim_eq_None_iff_forall_is_uP_bf_free in H...
    specialize(H (o1, (ltN_comm_intro k2' k1)))...
    unfold detect_prim in H...
  + apply is_term_intro_uP_bf_false_using_is_term_false...
Qed.

(* describe transformations (that is non-normalized trans) *)
Definition trans : option nat -> nat -> Type :=
  PrimUGen.trans term prim.

Definition LDD : nat -> Type :=
 (PrimUGen.LDD
    term (@sem_term) (@extract_term) (@extract_term_correct)
  term_of_bool extract_term_0 prim (@sem_prim) (@extract_prim)
  (@extract_prim_iff_sem_prim)
  (@pcmp_prim) (@pcmp_P_pcmp_prim)
  (@is_prim_free)
  (@is_prim_free_correct)
  (@extract_xprim)
  (@sem_xprim_extract_xprim)
  (@extract_xprim_minimal)
  (@is_term_sem_prim)
  (@same_prim_preserved)).

Definition extract : forall {n}, bf n -> LDD n.
Proof. apply @PrimUGen.extract. Defined.

Definition sem : forall {n}, LDD n -> bf n.
Proof. eapply @PrimUGen.sem. Defined.

Theorem sem_extract {n} (f:bf n) : sem(extract f) = f.
Proof. apply PrimUGen.sem_extract. Qed.

Theorem extract_sem {n} (l:LDD n) : extract(sem l) = l.
Proof. apply PrimUGen.extract_sem. Qed.

Theorem LDD_uniqueness {n} (l1 l2:LDD n) : sem l1 = sem l2 <-> l1 = l2.
Proof. apply PrimUGen.LDD_uniqueness. Qed.

Theorem LDD_existence {n} (f:bf n) : exists (l:LDD n), sem l = f.
Proof. apply PrimUGen.LDD_existence. Qed.

(* TESTS *)

Definition f1 : bf 4 := (fun (x:ba 4) =>
  let x0 := x(ltN_of_nat 0 4 (eq_refl _)) in
  let x1 := x(ltN_of_nat 1 4 (eq_refl _)) in
  let x2 := x(ltN_of_nat 2 4 (eq_refl _)) in
  let x3 := x(ltN_of_nat 3 4 (eq_refl _)) in
  xorb x0 (xorb x1 (xorb x2 x3))).

Definition ldd1 : LDD 4 := extract f1.

Definition f2 : bf 10 := (fun (x:ba 10) =>
  let x0 := x(ltN_of_nat 0 10 (eq_refl _)) in
  let x1 := x(ltN_of_nat 1 10 (eq_refl _)) in
  let x2 := x(ltN_of_nat 2 10 (eq_refl _)) in
  let x3 := x(ltN_of_nat 3 10 (eq_refl _)) in
  let x4 := x(ltN_of_nat 4 10 (eq_refl _)) in
  let x5 := x(ltN_of_nat 5 10 (eq_refl _)) in
  let x6 := x(ltN_of_nat 6 10 (eq_refl _)) in
  let x7 := x(ltN_of_nat 7 10 (eq_refl _)) in
  let x8 := x(ltN_of_nat 8 10 (eq_refl _)) in
  let x9 := x(ltN_of_nat 9 10 (eq_refl _)) in
  xorb x0 (xorb x1 (xorb x2 (xorb x3 (xorb x4
 (xorb x5 (xorb x6 (xorb x7 (xorb x8 x9))))))))).

Definition ldd2 : LDD 10 := extract f2.

Definition f3 : bf 10 := (fun (x:ba 10) =>
  let x1 := x(ltN_of_nat 1 10 (eq_refl _)) in
  let x2 := x(ltN_of_nat 2 10 (eq_refl _)) in
  let x3 := x(ltN_of_nat 3 10 (eq_refl _)) in
  let x4 := x(ltN_of_nat 4 10 (eq_refl _)) in
  (negb x1) && (xorb x3 (x2 && x4))).

Definition ldd3 : LDD 10 := extract f3.

Extraction "ldd_u_ucx_tests" extract ldd1 ldd2 ldd3.

(* When Evaluated in OCaml using OCaml's toplevel.
val ldd1 : (term, (oprim, nat) prod) trans ldd =
  Leaf (S (S (S (S O))),
   EPrim (None, S (S (S O)), Pair (OX, O),
    EPrim (None, S (S O), Pair (OX, O),
     EPrim (None, S O, Pair (OX, O), ETerm (S O, PiN (S O, O, False))))))
val f2 : (nat -> bool) -> bool = <fun>
val ldd2 : (term, (oprim, nat) prod) trans ldd =
  Leaf (S (S (S (S (S (S (S (S (S (S O))))))))),
   EPrim (None, S (S (S (S (S (S (S (S (S O)))))))), Pair (OX, O),
    EPrim (None, S (S (S (S (S (S (S (S O))))))), Pair (OX, O),
     EPrim (None, S (S (S (S (S (S (S O)))))), Pair (OX, O),
      EPrim (None, S (S (S (S (S (S O))))), Pair (OX, O),
       EPrim (None, S (S (S (S (S O)))), Pair (OX, O),
        EPrim (None, S (S (S (S O))), Pair (OX, O),
         EPrim (None, S (S (S O)), Pair (OX, O),
          EPrim (None, S (S O), Pair (OX, O),
           EPrim (None, S O, Pair (OX, O), ETerm (S O, PiN (S O, O, False))))))))))))
val f3 : (nat -> bool) -> bool = <fun>
val ldd3 : (term, (oprim, nat) prod) trans ldd =
  Leaf (S (S (S (S (S (S (S (S (S (S O))))))))),
   EPrim (None, S (S (S (S (S (S (S (S (S O)))))))), Pair (OU, O),
    EPrim (None, S (S (S (S (S (S (S (S O))))))), Pair (OU, S (S (S (S O)))),
     EPrim (None, S (S (S (S (S (S (S O)))))), Pair (OU, S (S (S (S O)))),
      EPrim (None, S (S (S (S (S (S O))))), Pair (OU, S (S (S (S O)))),
       EPrim (None, S (S (S (S (S O)))), Pair (OU, S (S (S (S O)))),
        EPrim (None, S (S (S (S O))), Pair (OU, S (S (S (S O)))),
         EPrim (None, S (S (S O)), Pair (OC (True, False), O),
          EPrim (None, S (S O), Pair (OX, S O),
           EPrim (None, S O, Pair (OC (False, False), O),
            ETerm (S O, PiN (S O, O, False))))))))))))
(* U_{0, 5, 6, 7, 8, 9} C10_{0} X_{1} C00_{0} (piN 1 0 0) *)

 *)
