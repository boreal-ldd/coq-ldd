# LGPL-3.0 Linking Exception
#
# Copyright (c) 2018-2020 Joan Thibault (joan.thibault@irisa.fr)
#
# GuaCaml : A Simple Extension of OCaml's Standard Library
#
# format.sh : simpl format script for OCaml code
# - remove consecutive empty lines
# - remove trailing spaces
# - replace '\t' by ' '

targets1="$(grep -Rl $'\t' | grep -v git) \
          $(grep -Rl  " $" | grep -v git)"
targets2="$(echo $targets1 | tr ' ' '\n' | sort -u)"
targets3="$(echo $targets2 | tr ' ' '\n' | grep "\.v$")"
for file in $targets3
do
  echo "reformat: " $file
	python format.py $file
done
