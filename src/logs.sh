for file in $(ls *.v.out); do python autoparse.py $file > $file.dic; done
python rewrap.py FKEY *.v.out.dic > files.dic
wc $(cat files.fof) > wc.out
python parse_wc.py wc.out > wc.dic
python merge.py files.dic wc.dic > make.dic
python tablify.py make.dic > make.csv
cat make.csv
