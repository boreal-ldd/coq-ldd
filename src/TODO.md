## Section 1. Local Optimization / Rewritting of formula (TREE)

- AIG (or NAX, or CNAX) delay balancing
- tiling + local optimization + combination

## Section 2. DAG-ification

- show that AIG (tree version) \equiv AIG (idem, NAX, or/and CNAX)
- show correspondance between tree and DAG operators

## Section 3. LDD Manager for O-NUCX (LDT-O-NUCX -> LDD-O-NUCX)

- show that LDT-O-NUCX \equiv LDD-O-NUCX (mod graph-isomorphism)
	+ using a [collapse : LDT -> LDD] (reduction function)
	+ using a [syntint : LDD -> LDT] (syntactic interpretation)
	+ showing that (up to graph-isomorphism) there exists a unique LDD representing a given LDT
		* using [dag-collapse : LDD (normalized) -> LDD (strongly-normalized)]
	+ showing that given a vector of function, it has a unique (up to graph isomorphism) LDD representing it.
		* using [dag-collapse : list LDD -> list LDD]
	+ showing that
		\[
			\forall (man:manager) (HR:reduced man)
			(id1 id2:ident) (H1:valid\_ident man id1) (H2:valid\_ident man id2)
			syntint man id1 = syntint man id2 <-> id1 = id2
		\]
		Shows that syntactical equality on LDT is locally decidable on LDD.

## Section 4. Generalization

Designing a general purpose (Tree/DAG)-structure for coq with associated correspondance Lemmas and Theorems.
