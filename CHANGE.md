2020 / 12 / 23 : added tactic `dtt_dest_match_step` in SimplProp extending `dest_match_step` to cases where `remember` can be used.
